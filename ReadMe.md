# Four Minutes to Midnight
© 1987 by Michael White and Martin Rennie

Genre: Science Fiction

YUID: xb85fh425xyst2at

IFDB: https://ifdb.org/viewgame?id=xb85fh425xyst2at

## Info

Tools: UnQuill, The Inker, ngPAWS, UNPAWS
- N Game fully tested?
- Y Missing images?
- Y Errors rendering images?
- Y Modern code for ngPAWS?
- 1 Edit txp (1) or sce (2)

## Notes

- This versions has no graphics (The Inker failed to export them). To be added later
- The graphics here are messed up

## Test ngPAWS port

https://interactivefiction.gitlab.io/Four_Minutes_to_Midnight-1987-Michael_White_and_Martin_Rennie/
