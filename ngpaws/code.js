// This file is (C) Carlos Sanchez 2014, released under the MIT license


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES AND CONSTANTS ///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// CONSTANTS 
var VOCABULARY_ID = 0;
var VOCABULARY_WORD = 1;
var VOCABULARY_TYPE = 2;

var WORDTYPE_VERB = 0;
var WORDTYPE_NOUN = 1
var WORDTYPE_ADJECT = 2;
var WORDTYPE_ADVERB = 3;
var WORDTYPE_PRONOUN = 4;
var WORDTYPE_CONJUNCTION = 5;
var WORDTYPE_PREPOSITION = 6;

var TIMER_MILLISECONDS  = 40;

var RESOURCE_TYPE_IMG = 1;
var RESOURCE_TYPE_SND = 2;

var PROCESS_RESPONSE = 0;
var PROCESS_DESCRIPTION = 1;
var PROCESS_TURN = 2;

var DIV_TEXT_SCROLL_STEP = 40;


// Aux
var SET_VALUE = 255; // Value assigned by SET condact
var EMPTY_WORD = 255; // Value for word types when no match is found (as for  sentences without adjective or name)
var MAX_WORD_LENGHT = 10;  // Number of characters considered per word
var FLAG_COUNT = 256;  // Number of flags
var NUM_CONNECTION_VERBS = 14; // Number of verbs used as connection, from 0 to N - 1
var NUM_CONVERTIBLE_NOUNS = 20;
var NUM_PROPER_NOUNS = 50; // Number of proper nouns, can't be used as pronoun reference
var EMPTY_OBJECT = 255; // To remark there is no object when the action requires a objno parameter
var NO_EXIT = 255;  // If an exit does not exist, its value is this value
var MAX_CHANNELS = 17; // Number of SFX channels
var RESOURCES_DIR='dat/';


//Attributes
var ATTR_LIGHT=0;			// Object produces light
var ATTR_WEARABLE=1;		// Object is wearable
var ATTR_CONTAINER=2;       // Object is a container
var ATTR_NPC=3;             // Object is actually an NPC
var ATTR_CONCEALED = 4; /// Present but not visible
var ATTR_EDIBLE = 5;   /// Can be eaten
var ATTR_DRINKABLE=6;
var ATTR_ENTERABLE = 7;
var ATTR_FEMALE = 8;
var ATTR_LOCKABLE = 9;
var ATTR_LOCKED = 10;
var ATTR_MALE = 11;
var ATTR_NEUTER=12;
var ATTR_OPENABLE =13;
var ATTR_OPEN=14;
var ATTR_PLURALNAME = 15;
var ATTR_TRANSPARENT=16;
var ATTR_SCENERY=17;
var ATTR_SUPPORTER = 18;
var ATTR_SWITCHABLE=19;
var ATTR_ON  =20;
var ATTR_STATIC  =21;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// INTERNAL STRINGS ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General messages & strings
var STR_NEWLINE = '<br />';
var STR_PROMPT_START = '<span class="feedback">&gt; ';
var STR_PROMPT_END = '</span>';
var STR_RAMSAVE_FILENAME = 'RAMSAVE_SAVEGAME';



// Runtime error messages
var STR_WRONG_SYSMESS = 'WARNING: System message requested does not exist.'; 
var STR_WRONG_LOCATION = 'WARNING: Location requested does not exist.'; 
var STR_WRONG_MESSAGE = 'WARNING: Message requested does not exist.'; 
var STR_WRONG_PROCESS = 'WARNING: Process requested does not exist.' 
var STR_RAMLOAD_ERROR= 'WARNING: You can\'t restore game as it has not yet been saved.'; 
var STR_RUNTIME_VERSION  = 'ngPAWS runtime (C) 2014 Carlos Sanchez.  Released under {URL|http://www.opensource.org/licenses/MIT| MIT license}.\nBuzz sound libray (C) Jay Salvat. Released under the {URL|http://www.opensource.org/licenses/MIT| MIT license} \n jQuery (C) jQuery Foundation. Released under the {URL|https://jquery.org/license/| MIT license}.';
var STR_TRANSCRIPT = 'To copy the transcript to your clipboard, press Ctrl+C, then press Enter';

var STR_INVALID_TAG_SEQUENCE = 'Invalid tag sequence: ';
var STR_INVALID_TAG_SEQUENCE_EMPTY = 'Invalid tag sequence.';
var STR_INVALID_TAG_SEQUENCE_BADPARAMS = 'Invalid tag sequence: bad parameters.';
var STR_INVALID_TAG_SEQUENCE_BADTAG = 'Invalid tag sequence: unknown tag.';
var STR_BADIE = 'You are using a very old version of Internet Explorer. Some features of this product won\'t be avaliable, and other may not work properly. For a better experience please upgrade your browser or install some other one like Firefox, Chrome or Opera.\n\nIt\'s up to you to continue but be warned your experience may be affected.';
var STR_INVALID_OBJECT = 'WARNING: Trying to access object that does not exist'


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////     FLAGS     ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var FLAG_LIGHT = 0;
var FLAG_OBJECTS_CARRIED_COUNT = 1;
var FLAG_AUTODEC2 = 2; 
var FLAG_AUTODEC3 = 3;
var FLAG_AUTODEC4 = 4;
var FLAG_AUTODEC5 = 5;
var FLAG_AUTODEC6 = 6;
var FLAG_AUTODEC7 = 7;
var FLAG_AUTODEC8 = 8;
var FLAG_AUTODEC9 = 9;
var FLAG_AUTODEC10 = 10;
var FLAG_ESCAPE = 11;
var FLAG_PARSER_SETTINGS = 12;
var FLAG_PICTURE_SETTINGS = 29
var FLAG_SCORE = 30;
var FLAG_TURNS_LOW = 31;
var FLAG_TURNS_HIGH = 32;
var FLAG_VERB = 33;
var FLAG_NOUN1 =34;
var FLAG_ADJECT1 = 35;
var FLAG_ADVERB = 36;
var FLAG_MAXOBJECTS_CARRIED = 37;
var FLAG_LOCATION = 38;
var FLAG_TOPLINE = 39;   // deprecated
var FLAG_MODE = 40;  // deprecated
var FLAG_PROTECT = 41;   // deprecated
var FLAG_PROMPT = 42; 
var FLAG_PREP = 43;
var FLAG_NOUN2 = 44;
var FLAG_ADJECT2 = 45;
var FLAG_PRONOUN = 46;
var FLAG_PRONOUN_ADJECT = 47;
var FLAG_TIMEOUT_LENGTH = 48;
var FLAG_TIMEOUT_SETTINGS = 49; 
var FLAG_DOALL_LOC = 50;
var FLAG_REFERRED_OBJECT = 51;
var FLAG_MAXWEIGHT_CARRIED = 52;
var FLAG_OBJECT_LIST_FORMAT = 53;
var FLAG_REFERRED_OBJECT_LOCATION = 54;
var FLAG_REFERRED_OBJECT_WEIGHT = 55;
var FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES = 56;
var FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES = 57;
var FLAG_EXPANSION1 = 58;
var FLAG_EXPANSION2 = 59;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// SPECIAL LOCATIONS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var LOCATION_WORN = 253;
var LOCATION_CARRIED = 254;
var LOCATION_NONCREATED = 252;
var LOCATION_HERE = 255;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////  SYSTEM MESSAGES  ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



var SYSMESS_ISDARK = 0;
var SYSMESS_YOUCANSEE = 1;
var SYSMESS_PROMPT0 = 2;
var SYSMESS_PROMPT1 = 3;
var SYSMESS_PROMPT2 = 4
var SYSMESS_PROMPT3= 5;
var SYSMESS_IDONTUNDERSTAND = 6;
var SYSMESS_WRONGDIRECTION = 7
var SYSMESS_CANTDOTHAT = 8;
var SYSMESS_YOUARECARRYING = 9;
var SYSMESS_WORN = 10;
var SYSMESS_CARRYING_NOTHING = 11;
var SYSMESS_AREYOUSURE = 12;
var SYSMESS_PLAYAGAIN = 13;
var SYSMESS_FAREWELL = 14;
var SYSMESS_OK = 15;
var SYSMESS_PRESSANYKEY = 16;
var SYSMESS_TURNS_START = 17;
var SYSMESS_TURNS_CONTINUE = 18;
var SYSMESS_TURNS_PLURAL = 19;
var SYSMESS_TURNS_END = 20;
var SYSMESS_SCORE_START= 21;
var SYSMESS_SCORE_END =22;
var SYSMESS_YOURENOTWEARINGTHAT = 23;
var SYSMESS_YOUAREALREADYWEARINGTHAT = 24;
var SYSMESS_YOUALREADYHAVEOBJECT = 25;
var SYSMESS_CANTSEETHAT = 26;
var SYSMESS_CANTCARRYANYMORE = 27;
var SYSMESS_YOUDONTHAVETHAT = 28;
var SYSMESS_YOUAREALREADYWAERINGOBJECT = 29;
var SYSMESS_YES = 30;
var SYSMESS_NO = 31;
var SYSMESS_MORE = 32;
var SYSMESS_CARET = 33;
var SYSMESS_TIMEOUT=35;
var SYSMESS_YOUTAKEOBJECT = 36;
var SYSMESS_YOUWEAROBJECT = 37;
var SYSMESS_YOUREMOVEOBJECT = 38;
var SYSMESS_YOUDROPOBJECT = 39;
var SYSMESS_YOUCANTWEAROBJECT = 40;
var SYSMESS_YOUCANTREMOVEOBJECT = 41;
var SYSMESS_CANTREMOVE_TOOMANYOBJECTS = 42;
var SYSMESS_WEIGHSTOOMUCH = 43;
var SYSMESS_YOUPUTOBJECTIN = 44;
var SYSMESS_YOUCANTTAKEOBJECTOUTOF = 45;
var SYSMESS_LISTSEPARATOR = 46;
var SYSMESS_LISTLASTSEPARATOR = 47;
var SYSMESS_LISTEND = 48;
var SYSMESS_YOUDONTHAVEOBJECT = 49;
var SYSMESS_YOUARENOTWEARINGOBJECT = 50;
var SYSMESS_PUTINTAKEOUTTERMINATION = 51;
var SYSMESS_THATISNOTIN = 52;
var SYSMESS_EMPTYOBJECTLIST = 53;
var SYSMESS_FILENOTFOUND = 54;
var SYSMESS_CORRUPTFILE = 55;
var SYSMESS_IOFAILURE = 56;
var SYSMESS_DIRECTORYFULL = 57;
var SYSMESS_LOADFILE = 58;
var SYSMESS_FILENOTFOUND = 59;
var SYSMESS_SAVEFILE = 60;
var SYSMESS_SORRY = 61;
var SYSMESS_NONSENSE_SENTENCE = 62;
var SYSMESS_NPCLISTSTART = 63;
var SYSMESS_NPCLISTCONTINUE = 64;
var SYSMESS_NPCLISTCONTINUE_PLURAL = 65;
var SYSMESS_INSIDE_YOUCANSEE = 66;
var SYSMESS_OVER_YOUCANSEE = 67;
var SYSMESS_YOUPUTOBJECTON = 68;
var SYSMESS_YOUCANTTAKEOBJECTFROM = 69;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// GLOBAL VARS //////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Parser vars
var last_player_orders = [];   // Store last player orders, to be able to restore it when pressing arrow up
var last_player_orders_pointer = 0;
var parser_word_found;
var player_order_buffer = '';
var player_order = ''; // Current player order
var previous_verb = EMPTY_WORD;
var previous_noun = EMPTY_WORD;
var previous_adject = EMPTY_WORD;
var pronoun_suffixes = [];


//Settings
var graphicsON = true; 
var soundsON = true; 
var interruptDisabled = false;
var showWarnings = true;

// waitkey commands callback function
var waitkey_callback_function = [];

//PAUSE
var inPause=false;
var pauseRemainingTime = 0;



// Transcript
var inTranscript = false;
var transcript = '';


// Block
var inBlock = false;
var unblock_process = null;


// END
var inEND = false;

//QUIT
var inQUIT = false;

//ANYKEY
var inAnykey = false;

//GETKEY
var inGetkey = false;
var getkey_return_flag = null;

// Status flags
var done_flag;
var describe_location_flag;
var in_response;
var success;

// doall control
var doall_flag;
var process_in_doall;
var entry_for_doall	= '';
var current_process;


var timeout_progress = 0;
var ramsave_value = null;
var num_objects;


// The flags
var flags = new Array();


// The sound channels
var soundChannels = [];
var soundLoopCount = [];

//The last free object attribute
var nextFreeAttr = 22;

//Autocomplete array
var autocomplete = new Array();
var autocompleteStep = 0;
var autocompleteBaseWord = '';
// PROCESSES

interruptProcessExists = false;

function pro000()
{
process_restart=true;
pro000_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p000e0000:
	{
 		if (skipdoall('p000e0000')) break p000e0000;
 		ACChook(0);
		if (done_flag) break pro000_restart;
		{}

	}

	// _ _
	p000e0001:
	{
 		if (skipdoall('p000e0001')) break p000e0001;
 		ACChook(1);
		if (done_flag) break pro000_restart;
		{}

	}

	// N _
	p000e0002:
	{
 		if (skipdoall('p000e0002')) break p000e0002;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0002;
 		}
		if (!CNDat(3)) break p000e0002;
		if (!CNDnotzero(113)) break p000e0002;
		if (!CNDeq(27,140)) break p000e0002;
 		ACCminus(27,5);
 		ACCwriteln(2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0003:
	{
 		if (skipdoall('p000e0003')) break p000e0003;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0003;
 		}
		if (!CNDat(3)) break p000e0003;
		if (!CNDeq(27,160)) break p000e0003;
		if (!CNDnotzero(113)) break p000e0003;
 		ACCminus(27,10);
 		ACCclear(5);
 		ACCwriteln(3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0004:
	{
 		if (skipdoall('p000e0004')) break p000e0004;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0004;
 		}
		if (!CNDat(3)) break p000e0004;
		if (!CNDnotzero(113)) break p000e0004;
		if (!CNDeq(27,170)) break p000e0004;
 		ACCminus(27,20);
 		ACCwriteln(4);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0005:
	{
 		if (skipdoall('p000e0005')) break p000e0005;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0005;
 		}
		if (!CNDat(3)) break p000e0005;
		if (!CNDeq(113,0)) break p000e0005;
 		ACCwriteln(5);
		{}

	}

	// N _
	p000e0006:
	{
 		if (skipdoall('p000e0006')) break p000e0006;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0006;
 		}
		if (!CNDat(30)) break p000e0006;
 		ACCwriteln(6);
		{}

	}

	// N _
	p000e0007:
	{
 		if (skipdoall('p000e0007')) break p000e0007;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0007;
 		}
		if (!CNDat(33)) break p000e0007;
 		ACCwriteln(7);
		{}

	}

	// N _
	p000e0008:
	{
 		if (skipdoall('p000e0008')) break p000e0008;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0008;
 		}
		if (!CNDat(43)) break p000e0008;
 		ACCwriteln(8);
		{}

	}

	// N _
	p000e0009:
	{
 		if (skipdoall('p000e0009')) break p000e0009;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0009;
 		}
		if (!CNDat(46)) break p000e0009;
 		ACCwriteln(9);
		{}

	}

	// N _
	p000e0010:
	{
 		if (skipdoall('p000e0010')) break p000e0010;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0010;
 		}
		if (!CNDat(49)) break p000e0010;
 		ACCwriteln(10);
		{}

	}

	// N _
	p000e0011:
	{
 		if (skipdoall('p000e0011')) break p000e0011;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0011;
 		}
		if (!CNDat(28)) break p000e0011;
 		ACCwriteln(11);
		{}

	}

	// N _
	p000e0012:
	{
 		if (skipdoall('p000e0012')) break p000e0012;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0012;
 		}
		if (!CNDnotzero(0)) break p000e0012;
		if (!CNDnotzero(113)) break p000e0012;
		if (!CNDchance(50)) break p000e0012;
 		ACCclear(113);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
		{}

	}

	// N _
	p000e0013:
	{
 		if (skipdoall('p000e0013')) break p000e0013;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0013;
 		}
		if (!CNDnotzero(0)) break p000e0013;
		if (!CNDnotzero(114)) break p000e0013;
		if (!CNDchance(30)) break p000e0013;
 		ACCclear(114);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
		{}

	}

	// N _
	p000e0014:
	{
 		if (skipdoall('p000e0014')) break p000e0014;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0014;
 		}
		if (!CNDnotzero(0)) break p000e0014;
		if (!CNDnotzero(116)) break p000e0014;
		if (!CNDchance(75)) break p000e0014;
 		ACCclear(116);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
		{}

	}

	// N _
	p000e0015:
	{
 		if (skipdoall('p000e0015')) break p000e0015;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0015;
 		}
		if (!CNDnotzero(0)) break p000e0015;
 		ACCwriteln(12);
		{}

	}

	// N _
	p000e0016:
	{
 		if (skipdoall('p000e0016')) break p000e0016;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0016;
 		}
		if (!CNDat(32)) break p000e0016;
		if (!CNDpresent(33)) break p000e0016;
 		ACCwriteln(13);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0017:
	{
 		if (skipdoall('p000e0017')) break p000e0017;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0017;
 		}
		if (!CNDat(32)) break p000e0017;
		if (!CNDgt(8,0)) break p000e0017;
 		ACClet(8,1);
 		ACCgoto(31);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0018:
	{
 		if (skipdoall('p000e0018')) break p000e0018;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0018;
 		}
		if (!CNDat(3)) break p000e0018;
		if (!CNDeq(27,161)) break p000e0018;
 		ACCwriteln(14);
		{}

	}

	// N _
	p000e0019:
	{
 		if (skipdoall('p000e0019')) break p000e0019;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0019;
 		}
		if (!CNDeq(20,0)) break p000e0019;
		if (!CNDat(3)) break p000e0019;
		if (!CNDeq(113,0)) break p000e0019;
		if (!CNDeq(27,160)) break p000e0019;
 		ACCminus(27,10);
		{}

	}

	// N _
	p000e0020:
	{
 		if (skipdoall('p000e0020')) break p000e0020;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0020;
 		}
		if (!CNDat(3)) break p000e0020;
		if (!CNDnotzero(113)) break p000e0020;
		if (!CNDeq(27,150)) break p000e0020;
 		ACCminus(27,10);
 		ACCwriteln(15);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// N _
	p000e0021:
	{
 		if (skipdoall('p000e0021')) break p000e0021;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0021;
 		}
		if (!CNDat(52)) break p000e0021;
		if (!CNDcarried(0)) break p000e0021;
 		ACCwriteln(16);
 		ACCpause(100);
 		function anykey00000() 
		{
		}
 		waitKey(anykey00000);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// N _
	p000e0022:
	{
 		if (skipdoall('p000e0022')) break p000e0022;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0022;
 		}
		if (!CNDat(52)) break p000e0022;
		if (!CNDcarried(37)) break p000e0022;
 		ACCwriteln(17);
 		ACCpause(100);
 		function anykey00001() 
		{
		}
 		waitKey(anykey00001);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// N _
	p000e0023:
	{
 		if (skipdoall('p000e0023')) break p000e0023;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0023;
 		}
		if (!CNDat(52)) break p000e0023;
		if (!CNDnotcarr(37)) break p000e0023;
		if (!CNDnotcarr(0)) break p000e0023;
 		ACCgoto(50);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0024:
	{
 		if (skipdoall('p000e0024')) break p000e0024;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0024;
 		}
		if (!CNDat(54)) break p000e0024;
		if (!CNDnotzero(5)) break p000e0024;
 		ACClet(5,1);
 		ACCgoto(52);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0025:
	{
 		if (skipdoall('p000e0025')) break p000e0025;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0025;
 		}
		if (!CNDat(52)) break p000e0025;
		if (!CNDeq(5,0)) break p000e0025;
 		ACCgoto(52);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// N _
	p000e0026:
	{
 		if (skipdoall('p000e0026')) break p000e0026;
 		if (in_response)
		{
			if (!CNDverb(1)) break p000e0026;
 		}
		if (!CNDat(54)) break p000e0026;
 		ACCgoto(52);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0027:
	{
 		if (skipdoall('p000e0027')) break p000e0027;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0027;
 		}
		if (!CNDat(3)) break p000e0027;
		if (!CNDeq(113,0)) break p000e0027;
 		ACCwriteln(18);
		{}

	}

	// S _
	p000e0028:
	{
 		if (skipdoall('p000e0028')) break p000e0028;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0028;
 		}
		if (!CNDat(3)) break p000e0028;
		if (!CNDlt(27,130)) break p000e0028;
 		ACCwriteln(19);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0029:
	{
 		if (skipdoall('p000e0029')) break p000e0029;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0029;
 		}
		if (!CNDat(3)) break p000e0029;
		if (!CNDeq(27,135)) break p000e0029;
		if (!CNDnotzero(113)) break p000e0029;
 		ACCplus(27,5);
 		ACCwriteln(20);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0030:
	{
 		if (skipdoall('p000e0030')) break p000e0030;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0030;
 		}
		if (!CNDat(3)) break p000e0030;
		if (!CNDeq(27,140)) break p000e0030;
		if (!CNDnotzero(113)) break p000e0030;
 		ACCplus(27,10);
 		ACCwriteln(21);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0031:
	{
 		if (skipdoall('p000e0031')) break p000e0031;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0031;
 		}
		if (!CNDat(3)) break p000e0031;
		if (!CNDeq(20,1)) break p000e0031;
		if (!CNDeq(27,150)) break p000e0031;
		if (!CNDnotzero(113)) break p000e0031;
 		ACCplus(27,11);
 		ACCset(5);
 		ACCwriteln(22);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0032:
	{
 		if (skipdoall('p000e0032')) break p000e0032;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0032;
 		}
		if (!CNDat(3)) break p000e0032;
		if (!CNDnotzero(113)) break p000e0032;
		if (!CNDeq(27,160)) break p000e0032;
 		ACCplus(27,10);
 		ACCclear(5);
 		ACCwriteln(23);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0033:
	{
 		if (skipdoall('p000e0033')) break p000e0033;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0033;
 		}
		if (!CNDat(19)) break p000e0033;
		if (!CNDnotzero(112)) break p000e0033;
 		ACCwriteln(24);
 		ACCclear(112);
 		ACCcreate(1);
 		ACCminus(17,2);
 		ACCminus(18,6);
 		ACCminus(19,1);
 		ACCminus(21,8);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0034:
	{
 		if (skipdoall('p000e0034')) break p000e0034;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0034;
 		}
		if (!CNDat(19)) break p000e0034;
		if (!CNDnotzero(113)) break p000e0034;
 		ACCclear(113);
 		ACCcreate(2);
 		ACCwriteln(25);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0035:
	{
 		if (skipdoall('p000e0035')) break p000e0035;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0035;
 		}
		if (!CNDat(19)) break p000e0035;
		if (!CNDnotzero(114)) break p000e0035;
 		ACCclear(114);
 		ACCcreate(3);
 		ACCwriteln(26);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0036:
	{
 		if (skipdoall('p000e0036')) break p000e0036;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0036;
 		}
		if (!CNDat(19)) break p000e0036;
		if (!CNDnotzero(115)) break p000e0036;
 		ACCclear(115);
 		ACCcreate(4);
 		ACCwriteln(27);
 		ACCminus(17,6);
 		ACCminus(18,3);
 		ACCminus(19,4);
 		ACCminus(21,6);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0037:
	{
 		if (skipdoall('p000e0037')) break p000e0037;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0037;
 		}
		if (!CNDat(19)) break p000e0037;
		if (!CNDnotzero(116)) break p000e0037;
 		ACCclear(116);
 		ACCwriteln(28);
 		ACCcreate(5);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0038:
	{
 		if (skipdoall('p000e0038')) break p000e0038;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0038;
 		}
		if (!CNDat(19)) break p000e0038;
		if (!CNDeq(112,0)) break p000e0038;
		if (!CNDeq(113,0)) break p000e0038;
		if (!CNDeq(114,0)) break p000e0038;
		if (!CNDeq(115,0)) break p000e0038;
		if (!CNDeq(116,0)) break p000e0038;
 		ACCgoto(74);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0039:
	{
 		if (skipdoall('p000e0039')) break p000e0039;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0039;
 		}
		if (!CNDat(50)) break p000e0039;
 		ACCwriteln(29);
		{}

	}

	// S _
	p000e0040:
	{
 		if (skipdoall('p000e0040')) break p000e0040;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0040;
 		}
		if (!CNDat(35)) break p000e0040;
 		ACCgoto(34);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0041:
	{
 		if (skipdoall('p000e0041')) break p000e0041;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0041;
 		}
		if (!CNDat(2)) break p000e0041;
 		ACCwriteln(30);
		{}

	}

	// S _
	p000e0042:
	{
 		if (skipdoall('p000e0042')) break p000e0042;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0042;
 		}
		if (!CNDat(30)) break p000e0042;
 		ACCwriteln(31);
		{}

	}

	// S _
	p000e0043:
	{
 		if (skipdoall('p000e0043')) break p000e0043;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0043;
 		}
		if (!CNDat(33)) break p000e0043;
 		ACCwriteln(32);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0044:
	{
 		if (skipdoall('p000e0044')) break p000e0044;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0044;
 		}
		if (!CNDat(43)) break p000e0044;
 		ACCwriteln(33);
		{}

	}

	// S _
	p000e0045:
	{
 		if (skipdoall('p000e0045')) break p000e0045;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0045;
 		}
		if (!CNDat(46)) break p000e0045;
 		ACCwriteln(34);
		{}

	}

	// S _
	p000e0046:
	{
 		if (skipdoall('p000e0046')) break p000e0046;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0046;
 		}
		if (!CNDnotzero(0)) break p000e0046;
		if (!CNDnotzero(113)) break p000e0046;
		if (!CNDchance(50)) break p000e0046;
 		ACCclear(113);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
		{}

	}

	// S _
	p000e0047:
	{
 		if (skipdoall('p000e0047')) break p000e0047;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0047;
 		}
		if (!CNDnotzero(0)) break p000e0047;
		if (!CNDnotzero(114)) break p000e0047;
		if (!CNDchance(30)) break p000e0047;
 		ACCclear(114);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
		{}

	}

	// S _
	p000e0048:
	{
 		if (skipdoall('p000e0048')) break p000e0048;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0048;
 		}
		if (!CNDnotzero(0)) break p000e0048;
		if (!CNDnotzero(116)) break p000e0048;
		if (!CNDchance(75)) break p000e0048;
 		ACCclear(116);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
		{}

	}

	// S _
	p000e0049:
	{
 		if (skipdoall('p000e0049')) break p000e0049;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0049;
 		}
		if (!CNDnotzero(0)) break p000e0049;
 		ACCwriteln(35);
		{}

	}

	// S _
	p000e0050:
	{
 		if (skipdoall('p000e0050')) break p000e0050;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0050;
 		}
		if (!CNDat(31)) break p000e0050;
		if (!CNDabsent(33)) break p000e0050;
		if (!CNDeq(112,0)) break p000e0050;
 		ACClet(8,5);
 		ACCgoto(32);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0051:
	{
 		if (skipdoall('p000e0051')) break p000e0051;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0051;
 		}
		if (!CNDat(31)) break p000e0051;
		if (!CNDpresent(33)) break p000e0051;
 		ACCwriteln(36);
		{}

	}

	// S _
	p000e0052:
	{
 		if (skipdoall('p000e0052')) break p000e0052;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0052;
 		}
		if (!CNDat(31)) break p000e0052;
		if (!CNDabsent(33)) break p000e0052;
		if (!CNDnotzero(112)) break p000e0052;
 		ACCplus(8,6);
 		ACCgoto(32);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// S _
	p000e0053:
	{
 		if (skipdoall('p000e0053')) break p000e0053;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0053;
 		}
		if (!CNDat(3)) break p000e0053;
		if (!CNDeq(27,161)) break p000e0053;
 		ACCwriteln(37);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0054:
	{
 		if (skipdoall('p000e0054')) break p000e0054;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0054;
 		}
		if (!CNDat(3)) break p000e0054;
		if (!CNDnotzero(113)) break p000e0054;
		if (!CNDeq(20,0)) break p000e0054;
		if (!CNDeq(27,160)) break p000e0054;
 		ACCplus(27,10);
 		ACCwriteln(38);
 		ACCclear(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0055:
	{
 		if (skipdoall('p000e0055')) break p000e0055;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0055;
 		}
		if (!CNDat(3)) break p000e0055;
		if (!CNDnotzero(113)) break p000e0055;
		if (!CNDeq(20,0)) break p000e0055;
		if (!CNDeq(27,150)) break p000e0055;
 		ACCplus(27,20);
 		ACCwriteln(39);
 		ACCclear(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0056:
	{
 		if (skipdoall('p000e0056')) break p000e0056;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0056;
 		}
		if (!CNDat(3)) break p000e0056;
		if (!CNDnotzero(113)) break p000e0056;
 		ACCwriteln(40);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// S _
	p000e0057:
	{
 		if (skipdoall('p000e0057')) break p000e0057;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0057;
 		}
		if (!CNDat(52)) break p000e0057;
		if (!CNDpresent(20)) break p000e0057;
 		ACCwriteln(41);
		{}

	}

	// S _
	p000e0058:
	{
 		if (skipdoall('p000e0058')) break p000e0058;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0058;
 		}
		if (!CNDat(52)) break p000e0058;
		if (!CNDabsent(20)) break p000e0058;
 		ACCgoto(54);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0059:
	{
 		if (skipdoall('p000e0059')) break p000e0059;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0059;
 		}
		if (!CNDat(4)) break p000e0059;
		if (!CNDnotcarr(24)) break p000e0059;
 		ACCgoto(5);
 		ACClet(111,95);
 		ACCwriteln(42);
 		ACCanykey();
 		function anykey00002() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00002);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// E _
	p000e0060:
	{
 		if (skipdoall('p000e0060')) break p000e0060;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0060;
 		}
		if (!CNDat(4)) break p000e0060;
		if (!CNDcarried(24)) break p000e0060;
 		ACCwriteln(43);
 		ACCanykey();
 		function anykey00003() 
		{
 		ACCgoto(5);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00003);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// E _
	p000e0061:
	{
 		if (skipdoall('p000e0061')) break p000e0061;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0061;
 		}
		if (!CNDat(3)) break p000e0061;
		if (!CNDeq(27,170)) break p000e0061;
 		ACCplus(27,10);
 		ACCwriteln(44);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// E _
	p000e0062:
	{
 		if (skipdoall('p000e0062')) break p000e0062;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0062;
 		}
		if (!CNDat(3)) break p000e0062;
		if (!CNDnotzero(113)) break p000e0062;
 		ACCwriteln(45);
		{}

	}

	// E _
	p000e0063:
	{
 		if (skipdoall('p000e0063')) break p000e0063;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0063;
 		}
		if (!CNDat(3)) break p000e0063;
		if (!CNDeq(113,0)) break p000e0063;
 		ACCwriteln(46);
		{}

	}

	// E _
	p000e0064:
	{
 		if (skipdoall('p000e0064')) break p000e0064;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0064;
 		}
		if (!CNDat(7)) break p000e0064;
		if (!CNDnotzero(113)) break p000e0064;
 		ACCwriteln(47);
		{}

	}

	// E _
	p000e0065:
	{
 		if (skipdoall('p000e0065')) break p000e0065;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0065;
 		}
		if (!CNDat(7)) break p000e0065;
		if (!CNDeq(113,0)) break p000e0065;
 		ACCgoto(8);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0066:
	{
 		if (skipdoall('p000e0066')) break p000e0066;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0066;
 		}
		if (!CNDat(49)) break p000e0066;
 		ACCwriteln(48);
		{}

	}

	// E _
	p000e0067:
	{
 		if (skipdoall('p000e0067')) break p000e0067;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0067;
 		}
		if (!CNDat(55)) break p000e0067;
 		ACCwriteln(49);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// E _
	p000e0068:
	{
 		if (skipdoall('p000e0068')) break p000e0068;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0068;
 		}
		if (!CNDnotzero(0)) break p000e0068;
		if (!CNDnotzero(113)) break p000e0068;
		if (!CNDchance(50)) break p000e0068;
 		ACCclear(113);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
		{}

	}

	// E _
	p000e0069:
	{
 		if (skipdoall('p000e0069')) break p000e0069;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0069;
 		}
		if (!CNDnotzero(0)) break p000e0069;
		if (!CNDnotzero(114)) break p000e0069;
		if (!CNDchance(30)) break p000e0069;
 		ACCclear(114);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
		{}

	}

	// E _
	p000e0070:
	{
 		if (skipdoall('p000e0070')) break p000e0070;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0070;
 		}
		if (!CNDnotzero(0)) break p000e0070;
		if (!CNDnotzero(116)) break p000e0070;
		if (!CNDchance(75)) break p000e0070;
 		ACCclear(116);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
		{}

	}

	// E _
	p000e0071:
	{
 		if (skipdoall('p000e0071')) break p000e0071;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0071;
 		}
		if (!CNDnotzero(0)) break p000e0071;
 		ACCwriteln(50);
		{}

	}

	// E _
	p000e0072:
	{
 		if (skipdoall('p000e0072')) break p000e0072;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0072;
 		}
		if (!CNDat(68)) break p000e0072;
		if (!CNDpresent(39)) break p000e0072;
		if (!CNDnotcarr(17)) break p000e0072;
 		ACCwriteln(51);
 		ACCanykey();
 		function anykey00004() 
		{
 		ACCgoto(64);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00004);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// E _
	p000e0073:
	{
 		if (skipdoall('p000e0073')) break p000e0073;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0073;
 		}
		if (!CNDat(68)) break p000e0073;
		if (!CNDabsent(39)) break p000e0073;
 		ACCgoto(69);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// E _
	p000e0074:
	{
 		if (skipdoall('p000e0074')) break p000e0074;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0074;
 		}
		if (!CNDat(24)) break p000e0074;
 		ACCwriteln(52);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// W _
	p000e0075:
	{
 		if (skipdoall('p000e0075')) break p000e0075;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0075;
 		}
		if (!CNDat(5)) break p000e0075;
		if (!CNDnotcarr(24)) break p000e0075;
 		ACCplus(111,3);
 		ACCwriteln(53);
 		ACCpause(100);
 		function anykey00005() 
		{
 		ACCgoto(4);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00005);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// W _
	p000e0076:
	{
 		if (skipdoall('p000e0076')) break p000e0076;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0076;
 		}
		if (!CNDat(5)) break p000e0076;
		if (!CNDcarried(24)) break p000e0076;
 		ACCwriteln(54);
 		ACCpause(100);
 		function anykey00006() 
		{
 		ACCgoto(4);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00006);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// W _
	p000e0077:
	{
 		if (skipdoall('p000e0077')) break p000e0077;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0077;
 		}
		if (!CNDat(3)) break p000e0077;
		if (!CNDeq(27,180)) break p000e0077;
 		ACCwriteln(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// W _
	p000e0078:
	{
 		if (skipdoall('p000e0078')) break p000e0078;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0078;
 		}
		if (!CNDat(3)) break p000e0078;
		if (!CNDnotzero(113)) break p000e0078;
 		ACCwriteln(56);
		{}

	}

	// W _
	p000e0079:
	{
 		if (skipdoall('p000e0079')) break p000e0079;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0079;
 		}
		if (!CNDat(3)) break p000e0079;
		if (!CNDeq(113,0)) break p000e0079;
 		ACCwriteln(57);
		{}

	}

	// W _
	p000e0080:
	{
 		if (skipdoall('p000e0080')) break p000e0080;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0080;
 		}
		if (!CNDat(11)) break p000e0080;
		if (!CNDeq(113,0)) break p000e0080;
 		ACCgoto(10);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0081:
	{
 		if (skipdoall('p000e0081')) break p000e0081;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0081;
 		}
		if (!CNDat(11)) break p000e0081;
		if (!CNDnotzero(113)) break p000e0081;
 		ACCwriteln(58);
		{}

	}

	// W _
	p000e0082:
	{
 		if (skipdoall('p000e0082')) break p000e0082;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0082;
 		}
		if (!CNDat(20)) break p000e0082;
		if (!CNDpresent(30)) break p000e0082;
 		ACCwriteln(59);
		{}

	}

	// W _
	p000e0083:
	{
 		if (skipdoall('p000e0083')) break p000e0083;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0083;
 		}
		if (!CNDat(20)) break p000e0083;
		if (!CNDabsent(30)) break p000e0083;
 		ACCgoto(21);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// W _
	p000e0084:
	{
 		if (skipdoall('p000e0084')) break p000e0084;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0084;
 		}
		if (!CNDnotzero(0)) break p000e0084;
		if (!CNDnotzero(113)) break p000e0084;
		if (!CNDchance(50)) break p000e0084;
 		ACCclear(113);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
		{}

	}

	// W _
	p000e0085:
	{
 		if (skipdoall('p000e0085')) break p000e0085;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0085;
 		}
		if (!CNDnotzero(0)) break p000e0085;
		if (!CNDnotzero(114)) break p000e0085;
		if (!CNDchance(30)) break p000e0085;
 		ACCclear(114);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
		{}

	}

	// W _
	p000e0086:
	{
 		if (skipdoall('p000e0086')) break p000e0086;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0086;
 		}
		if (!CNDnotzero(0)) break p000e0086;
		if (!CNDnotzero(116)) break p000e0086;
		if (!CNDchance(75)) break p000e0086;
 		ACCclear(116);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
		{}

	}

	// W _
	p000e0087:
	{
 		if (skipdoall('p000e0087')) break p000e0087;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0087;
 		}
		if (!CNDnotzero(0)) break p000e0087;
 		ACCwriteln(60);
		{}

	}

	// NE _
	p000e0088:
	{
 		if (skipdoall('p000e0088')) break p000e0088;
 		if (in_response)
		{
			if (!CNDverb(5)) break p000e0088;
 		}
		if (!CNDnotzero(0)) break p000e0088;
 		ACCwriteln(61);
		{}

	}

	// NE _
	p000e0089:
	{
 		if (skipdoall('p000e0089')) break p000e0089;
 		if (in_response)
		{
			if (!CNDverb(5)) break p000e0089;
 		}
		if (!CNDat(3)) break p000e0089;
		if (!CNDnotzero(113)) break p000e0089;
 		ACCwriteln(62);
		{}

	}

	// NE _
	p000e0090:
	{
 		if (skipdoall('p000e0090')) break p000e0090;
 		if (in_response)
		{
			if (!CNDverb(5)) break p000e0090;
 		}
		if (!CNDat(3)) break p000e0090;
		if (!CNDeq(113,0)) break p000e0090;
 		ACCwriteln(63);
		{}

	}

	// NW _
	p000e0091:
	{
 		if (skipdoall('p000e0091')) break p000e0091;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0091;
 		}
		if (!CNDnotzero(0)) break p000e0091;
 		ACCwriteln(64);
		{}

	}

	// NW _
	p000e0092:
	{
 		if (skipdoall('p000e0092')) break p000e0092;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0092;
 		}
		if (!CNDat(3)) break p000e0092;
		if (!CNDnotzero(113)) break p000e0092;
 		ACCwriteln(65);
		{}

	}

	// NW _
	p000e0093:
	{
 		if (skipdoall('p000e0093')) break p000e0093;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0093;
 		}
		if (!CNDat(3)) break p000e0093;
		if (!CNDeq(113,0)) break p000e0093;
 		ACCwriteln(66);
		{}

	}

	// NW _
	p000e0094:
	{
 		if (skipdoall('p000e0094')) break p000e0094;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0094;
 		}
		if (!CNDat(64)) break p000e0094;
		if (!CNDnotzero(112)) break p000e0094;
		if (!CNDeq(113,0)) break p000e0094;
		if (!CNDeq(114,0)) break p000e0094;
		if (!CNDeq(115,0)) break p000e0094;
		if (!CNDeq(116,0)) break p000e0094;
 		ACCwriteln(67);
 		ACCpause(100);
 		function anykey00007() 
		{
 		ACCgoto(65);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00007);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// NW _
	p000e0095:
	{
 		if (skipdoall('p000e0095')) break p000e0095;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0095;
 		}
		if (!CNDat(64)) break p000e0095;
		if (!CNDeq(112,0)) break p000e0095;
		if (!CNDnotzero(113)) break p000e0095;
		if (!CNDeq(114,0)) break p000e0095;
		if (!CNDeq(115,0)) break p000e0095;
		if (!CNDeq(116,0)) break p000e0095;
 		ACCgoto(65);
 		ACCwriteln(68);
 		ACCpause(100);
 		function anykey00008() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00008);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// NW _
	p000e0096:
	{
 		if (skipdoall('p000e0096')) break p000e0096;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0096;
 		}
		if (!CNDat(64)) break p000e0096;
		if (!CNDeq(112,0)) break p000e0096;
		if (!CNDeq(113,0)) break p000e0096;
		if (!CNDnotzero(114)) break p000e0096;
		if (!CNDeq(115,0)) break p000e0096;
		if (!CNDeq(116,0)) break p000e0096;
 		ACCgoto(65);
 		ACCwriteln(69);
 		ACCpause(100);
 		function anykey00009() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00009);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// NW _
	p000e0097:
	{
 		if (skipdoall('p000e0097')) break p000e0097;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0097;
 		}
		if (!CNDat(64)) break p000e0097;
		if (!CNDeq(112,0)) break p000e0097;
		if (!CNDeq(113,0)) break p000e0097;
		if (!CNDeq(114,0)) break p000e0097;
		if (!CNDnotzero(115)) break p000e0097;
		if (!CNDeq(116,0)) break p000e0097;
 		ACCgoto(65);
 		ACCwriteln(70);
 		ACCpause(100);
 		function anykey00010() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00010);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// NW _
	p000e0098:
	{
 		if (skipdoall('p000e0098')) break p000e0098;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0098;
 		}
		if (!CNDat(64)) break p000e0098;
		if (!CNDeq(112,0)) break p000e0098;
		if (!CNDeq(113,0)) break p000e0098;
		if (!CNDeq(114,0)) break p000e0098;
		if (!CNDeq(115,0)) break p000e0098;
		if (!CNDnotzero(116)) break p000e0098;
 		ACCgoto(65);
 		ACCwriteln(71);
 		ACCpause(100);
 		function anykey00011() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00011);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// NW _
	p000e0099:
	{
 		if (skipdoall('p000e0099')) break p000e0099;
 		if (in_response)
		{
			if (!CNDverb(6)) break p000e0099;
 		}
		if (!CNDat(64)) break p000e0099;
 		ACCwriteln(72);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SE _
	p000e0100:
	{
 		if (skipdoall('p000e0100')) break p000e0100;
 		if (in_response)
		{
			if (!CNDverb(7)) break p000e0100;
 		}
		if (!CNDnotzero(0)) break p000e0100;
 		ACCwriteln(73);
		{}

	}

	// SE _
	p000e0101:
	{
 		if (skipdoall('p000e0101')) break p000e0101;
 		if (in_response)
		{
			if (!CNDverb(7)) break p000e0101;
 		}
		if (!CNDat(3)) break p000e0101;
		if (!CNDnotzero(113)) break p000e0101;
 		ACCwriteln(74);
		{}

	}

	// SE _
	p000e0102:
	{
 		if (skipdoall('p000e0102')) break p000e0102;
 		if (in_response)
		{
			if (!CNDverb(7)) break p000e0102;
 		}
		if (!CNDat(3)) break p000e0102;
		if (!CNDeq(113,0)) break p000e0102;
 		ACCwriteln(75);
		{}

	}

	// SW _
	p000e0103:
	{
 		if (skipdoall('p000e0103')) break p000e0103;
 		if (in_response)
		{
			if (!CNDverb(8)) break p000e0103;
 		}
		if (!CNDnotzero(0)) break p000e0103;
 		ACCwriteln(76);
		{}

	}

	// SW _
	p000e0104:
	{
 		if (skipdoall('p000e0104')) break p000e0104;
 		if (in_response)
		{
			if (!CNDverb(8)) break p000e0104;
 		}
		if (!CNDat(3)) break p000e0104;
		if (!CNDnotzero(113)) break p000e0104;
 		ACCwriteln(77);
		{}

	}

	// SW _
	p000e0105:
	{
 		if (skipdoall('p000e0105')) break p000e0105;
 		if (in_response)
		{
			if (!CNDverb(8)) break p000e0105;
 		}
		if (!CNDat(3)) break p000e0105;
		if (!CNDeq(113,0)) break p000e0105;
 		ACCwriteln(78);
		{}

	}

	// CLIM _
	p000e0106:
	{
 		if (skipdoall('p000e0106')) break p000e0106;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0106;
 		}
		if (!CNDat(22)) break p000e0106;
		if (!CNDnotworn(11)) break p000e0106;
 		ACCwriteln(79);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// CLIM _
	p000e0107:
	{
 		if (skipdoall('p000e0107')) break p000e0107;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0107;
 		}
		if (!CNDat(22)) break p000e0107;
		if (!CNDworn(11)) break p000e0107;
 		ACCgoto(23);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// D _
	p000e0108:
	{
 		if (skipdoall('p000e0108')) break p000e0108;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0108;
 		}
		if (!CNDat(27)) break p000e0108;
		if (!CNDpresent(28)) break p000e0108;
 		ACCwriteln(80);
 		ACCpause(100);
 		function anykey00012() 
		{
 		ACCgoto(16);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00012);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// D _
	p000e0109:
	{
 		if (skipdoall('p000e0109')) break p000e0109;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0109;
 		}
		if (!CNDat(27)) break p000e0109;
		if (!CNDabsent(28)) break p000e0109;
 		ACCgoto(26);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE _
	p000e0110:
	{
 		if (skipdoall('p000e0110')) break p000e0110;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0110;
 		}
		if (!CNDnotat(3)) break p000e0110;
		if (!CNDnotzero(0)) break p000e0110;
 		ACCwriteln(81);
		{}

	}

	// ENTE _
	p000e0111:
	{
 		if (skipdoall('p000e0111')) break p000e0111;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0111;
 		}
		if (!CNDpresent(25)) break p000e0111;
		if (!CNDnotcarr(24)) break p000e0111;
 		ACCgoto(3);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE _
	p000e0112:
	{
 		if (skipdoall('p000e0112')) break p000e0112;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0112;
 		}
		if (!CNDpresent(32)) break p000e0112;
 		ACCgoto(36);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE _
	p000e0113:
	{
 		if (skipdoall('p000e0113')) break p000e0113;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0113;
 		}
		if (!CNDcarried(24)) break p000e0113;
 		ACCwriteln(82);
		{}

	}

	// ENTE _
	p000e0114:
	{
 		if (skipdoall('p000e0114')) break p000e0114;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0114;
 		}
		if (!CNDat(10)) break p000e0114;
		if (!CNDnotcarr(24)) break p000e0114;
 		ACCgoto(12);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE _
	p000e0115:
	{
 		if (skipdoall('p000e0115')) break p000e0115;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0115;
 		}
		if (!CNDat(16)) break p000e0115;
		if (!CNDnotcarr(24)) break p000e0115;
 		ACCgoto(19);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE _
	p000e0116:
	{
 		if (skipdoall('p000e0116')) break p000e0116;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0116;
 		}
		if (!CNDpresent(40)) break p000e0116;
 		ACCgoto(79);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTE _
	p000e0117:
	{
 		if (skipdoall('p000e0117')) break p000e0117;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0117;
 		}
		if (!CNDat(17)) break p000e0117;
		if (!CNDnotcarr(24)) break p000e0117;
 		ACCgoto(18);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0118:
	{
 		if (skipdoall('p000e0118')) break p000e0118;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0118;
 		}
		if (!CNDnotat(3)) break p000e0118;
		if (!CNDnotzero(0)) break p000e0118;
 		ACCwriteln(83);
		{}

	}

	// EXIT _
	p000e0119:
	{
 		if (skipdoall('p000e0119')) break p000e0119;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0119;
 		}
		if (!CNDat(3)) break p000e0119;
		if (!CNDlt(27,140)) break p000e0119;
 		ACCgoto(2);
 		ACCcreate(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0120:
	{
 		if (skipdoall('p000e0120')) break p000e0120;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0120;
 		}
		if (!CNDat(3)) break p000e0120;
		if (!CNDeq(27,140)) break p000e0120;
 		ACCgoto(28);
 		ACCcreate(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0121:
	{
 		if (skipdoall('p000e0121')) break p000e0121;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0121;
 		}
		if (!CNDat(3)) break p000e0121;
		if (!CNDeq(27,150)) break p000e0121;
 		ACCgoto(33);
 		ACCcreate(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0122:
	{
 		if (skipdoall('p000e0122')) break p000e0122;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0122;
 		}
		if (!CNDat(3)) break p000e0122;
		if (!CNDeq(27,160)) break p000e0122;
 		ACCgoto(43);
 		ACCcreate(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0123:
	{
 		if (skipdoall('p000e0123')) break p000e0123;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0123;
 		}
		if (!CNDat(3)) break p000e0123;
		if (!CNDeq(27,170)) break p000e0123;
 		ACCgoto(49);
 		ACCcreate(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0124:
	{
 		if (skipdoall('p000e0124')) break p000e0124;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0124;
 		}
		if (!CNDeq(27,180)) break p000e0124;
 		ACCgoto(71);
 		ACCcreate(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0125:
	{
 		if (skipdoall('p000e0125')) break p000e0125;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0125;
 		}
		if (!CNDat(36)) break p000e0125;
		if (!CNDlt(31,4)) break p000e0125;
 		ACCgoto(34);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0126:
	{
 		if (skipdoall('p000e0126')) break p000e0126;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0126;
 		}
		if (!CNDat(36)) break p000e0126;
		if (!CNDeq(31,4)) break p000e0126;
 		ACCgoto(42);
 		ACCcreate(32);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EXIT _
	p000e0127:
	{
 		if (skipdoall('p000e0127')) break p000e0127;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0127;
 		}
		if (!CNDat(3)) break p000e0127;
		if (!CNDeq(27,161)) break p000e0127;
 		ACCgoto(43);
 		ACCcreate(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0128:
	{
 		if (skipdoall('p000e0128')) break p000e0128;
 		if (in_response)
		{
			if (!CNDverb(13)) break p000e0128;
 		}
		if (!CNDat(23)) break p000e0128;
 		ACCgoto(24);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0129:
	{
 		if (skipdoall('p000e0129')) break p000e0129;
 		if (in_response)
		{
			if (!CNDverb(13)) break p000e0129;
 		}
		if (!CNDat(24)) break p000e0129;
 		ACCwriteln(84);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// JUMP _
	p000e0130:
	{
 		if (skipdoall('p000e0130')) break p000e0130;
 		if (in_response)
		{
			if (!CNDverb(13)) break p000e0130;
 		}
 		ACCwriteln(85);
		{}

	}

	// GIVE _
	p000e0131:
	{
 		if (skipdoall('p000e0131')) break p000e0131;
 		if (in_response)
		{
			if (!CNDverb(25)) break p000e0131;
 		}
 		ACCwriteln(86);
		{}

	}

	// SCOR _
	p000e0132:
	{
 		if (skipdoall('p000e0132')) break p000e0132;
 		if (in_response)
		{
			if (!CNDverb(26)) break p000e0132;
 		}
 		ACCscore();
		{}

	}

	// BAST _
	p000e0133:
	{
 		if (skipdoall('p000e0133')) break p000e0133;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0133;
 		}
		if (!CNDnotzero(112)) break p000e0133;
 		ACCwriteln(87);
		{}

	}

	// BAST _
	p000e0134:
	{
 		if (skipdoall('p000e0134')) break p000e0134;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0134;
 		}
		if (!CNDnotzero(113)) break p000e0134;
 		ACCwriteln(88);
		{}

	}

	// BAST _
	p000e0135:
	{
 		if (skipdoall('p000e0135')) break p000e0135;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0135;
 		}
		if (!CNDnotzero(114)) break p000e0135;
 		ACCwriteln(89);
		{}

	}

	// BAST _
	p000e0136:
	{
 		if (skipdoall('p000e0136')) break p000e0136;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0136;
 		}
		if (!CNDnotzero(115)) break p000e0136;
 		ACCwriteln(90);
		{}

	}

	// BAST _
	p000e0137:
	{
 		if (skipdoall('p000e0137')) break p000e0137;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0137;
 		}
		if (!CNDnotzero(116)) break p000e0137;
 		ACCwriteln(91);
		{}

	}

	// BAST _
	p000e0138:
	{
 		if (skipdoall('p000e0138')) break p000e0138;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0138;
 		}
		if (!CNDeq(112,0)) break p000e0138;
		if (!CNDeq(113,0)) break p000e0138;
		if (!CNDeq(114,0)) break p000e0138;
		if (!CNDeq(115,0)) break p000e0138;
		if (!CNDeq(116,0)) break p000e0138;
 		ACCwriteln(92);
		{}

	}

	// TYPE 9ZA
	p000e0139:
	{
 		if (skipdoall('p000e0139')) break p000e0139;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0139;
			if (!CNDnoun1(28)) break p000e0139;
 		}
		if (!CNDgt(6,29)) break p000e0139;
		if (!CNDeq(7,0)) break p000e0139;
 		ACClet(7,50);
 		ACCwriteln(93);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 9ZA
	p000e0140:
	{
 		if (skipdoall('p000e0140')) break p000e0140;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0140;
			if (!CNDnoun1(28)) break p000e0140;
 		}
		if (!CNDgt(6,29)) break p000e0140;
		if (!CNDgt(7,50)) break p000e0140;
 		ACCwriteln(94);
		{}

	}

	// TYPE 9ZA
	p000e0141:
	{
 		if (skipdoall('p000e0141')) break p000e0141;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0141;
			if (!CNDnoun1(28)) break p000e0141;
 		}
		if (!CNDgt(6,29)) break p000e0141;
		if (!CNDeq(7,50)) break p000e0141;
 		ACCwriteln(95);
		{}

	}

	// TYPE 0ZA
	p000e0142:
	{
 		if (skipdoall('p000e0142')) break p000e0142;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0142;
			if (!CNDnoun1(29)) break p000e0142;
 		}
		if (!CNDgt(6,29)) break p000e0142;
		if (!CNDeq(7,0)) break p000e0142;
 		ACClet(7,100);
 		ACCwriteln(96);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0ZA
	p000e0143:
	{
 		if (skipdoall('p000e0143')) break p000e0143;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0143;
			if (!CNDnoun1(29)) break p000e0143;
 		}
		if (!CNDgt(6,29)) break p000e0143;
		if (!CNDeq(7,50)) break p000e0143;
 		ACCwriteln(97);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0ZA
	p000e0144:
	{
 		if (skipdoall('p000e0144')) break p000e0144;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0144;
			if (!CNDnoun1(29)) break p000e0144;
 		}
		if (!CNDgt(6,29)) break p000e0144;
		if (!CNDgt(7,50)) break p000e0144;
 		ACCwriteln(98);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 9YB
	p000e0145:
	{
 		if (skipdoall('p000e0145')) break p000e0145;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0145;
			if (!CNDnoun1(30)) break p000e0145;
 		}
		if (!CNDgt(6,29)) break p000e0145;
		if (!CNDeq(4,0)) break p000e0145;
 		ACClet(4,50);
 		ACCwriteln(99);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 9YB
	p000e0146:
	{
 		if (skipdoall('p000e0146')) break p000e0146;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0146;
			if (!CNDnoun1(30)) break p000e0146;
 		}
		if (!CNDgt(6,29)) break p000e0146;
		if (!CNDeq(4,100)) break p000e0146;
 		ACCwriteln(100);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 9YB
	p000e0147:
	{
 		if (skipdoall('p000e0147')) break p000e0147;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0147;
			if (!CNDnoun1(30)) break p000e0147;
 		}
		if (!CNDgt(6,29)) break p000e0147;
		if (!CNDeq(4,50)) break p000e0147;
 		ACCwriteln(101);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0YB
	p000e0148:
	{
 		if (skipdoall('p000e0148')) break p000e0148;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0148;
			if (!CNDnoun1(31)) break p000e0148;
 		}
		if (!CNDgt(6,29)) break p000e0148;
		if (!CNDeq(4,0)) break p000e0148;
 		ACClet(4,100);
 		ACCwriteln(102);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0YB
	p000e0149:
	{
 		if (skipdoall('p000e0149')) break p000e0149;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0149;
			if (!CNDnoun1(31)) break p000e0149;
 		}
		if (!CNDgt(6,29)) break p000e0149;
		if (!CNDeq(4,50)) break p000e0149;
 		ACCwriteln(103);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0YB
	p000e0150:
	{
 		if (skipdoall('p000e0150')) break p000e0150;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0150;
			if (!CNDnoun1(31)) break p000e0150;
 		}
		if (!CNDgt(6,29)) break p000e0150;
		if (!CNDeq(4,100)) break p000e0150;
 		ACCwriteln(104);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0XC
	p000e0151:
	{
 		if (skipdoall('p000e0151')) break p000e0151;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0151;
			if (!CNDnoun1(32)) break p000e0151;
 		}
		if (!CNDgt(6,29)) break p000e0151;
		if (!CNDeq(8,0)) break p000e0151;
 		ACClet(8,100);
 		ACCwriteln(105);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0XC
	p000e0152:
	{
 		if (skipdoall('p000e0152')) break p000e0152;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0152;
			if (!CNDnoun1(32)) break p000e0152;
 		}
		if (!CNDgt(6,29)) break p000e0152;
		if (!CNDeq(8,50)) break p000e0152;
 		ACCwriteln(106);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 0XC
	p000e0153:
	{
 		if (skipdoall('p000e0153')) break p000e0153;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0153;
			if (!CNDnoun1(32)) break p000e0153;
 		}
		if (!CNDgt(6,29)) break p000e0153;
		if (!CNDeq(8,100)) break p000e0153;
 		ACCwriteln(107);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 9XC
	p000e0154:
	{
 		if (skipdoall('p000e0154')) break p000e0154;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0154;
			if (!CNDnoun1(33)) break p000e0154;
 		}
		if (!CNDgt(6,29)) break p000e0154;
		if (!CNDeq(8,0)) break p000e0154;
 		ACClet(8,50);
 		ACCwriteln(108);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 9XC
	p000e0155:
	{
 		if (skipdoall('p000e0155')) break p000e0155;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0155;
			if (!CNDnoun1(33)) break p000e0155;
 		}
		if (!CNDgt(6,29)) break p000e0155;
		if (!CNDeq(8,100)) break p000e0155;
 		ACCwriteln(109);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 9XC
	p000e0156:
	{
 		if (skipdoall('p000e0156')) break p000e0156;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0156;
			if (!CNDnoun1(33)) break p000e0156;
 		}
		if (!CNDgt(6,29)) break p000e0156;
		if (!CNDeq(8,50)) break p000e0156;
 		ACCwriteln(110);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 4
	p000e0157:
	{
 		if (skipdoall('p000e0157')) break p000e0157;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0157;
			if (!CNDnoun1(34)) break p000e0157;
 		}
		if (!CNDgt(6,2)) break p000e0157;
 		ACClet(6,70);
 		ACCwriteln(111);
		{}

	}

	// TYPE 3
	p000e0158:
	{
 		if (skipdoall('p000e0158')) break p000e0158;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0158;
			if (!CNDnoun1(35)) break p000e0158;
 		}
		if (!CNDgt(6,2)) break p000e0158;
 		ACClet(6,59);
 		ACCwriteln(112);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 2
	p000e0159:
	{
 		if (skipdoall('p000e0159')) break p000e0159;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0159;
			if (!CNDnoun1(36)) break p000e0159;
 		}
		if (!CNDgt(6,2)) break p000e0159;
 		ACClet(6,50);
 		ACCwriteln(113);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE 1
	p000e0160:
	{
 		if (skipdoall('p000e0160')) break p000e0160;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0160;
			if (!CNDnoun1(37)) break p000e0160;
 		}
		if (!CNDgt(6,2)) break p000e0160;
 		ACClet(6,40);
 		ACCwriteln(114);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE YES
	p000e0161:
	{
 		if (skipdoall('p000e0161')) break p000e0161;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0161;
			if (!CNDnoun1(39)) break p000e0161;
 		}
		if (!CNDat(54)) break p000e0161;
		if (!CNDpresent(38)) break p000e0161;
		if (!CNDeq(3,10)) break p000e0161;
		if (!CNDeq(6,1)) break p000e0161;
 		ACClet(6,30);
 		ACCwriteln(115);
 		ACCpause(100);
 		function anykey00013() 
		{
 		ACClet(5,15);
 		ACCdone();
		return;
		}
 		waitKey(anykey00013);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TYPE _
	p000e0162:
	{
 		if (skipdoall('p000e0162')) break p000e0162;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0162;
 		}
		if (!CNDabsent(38)) break p000e0162;
 		ACCwriteln(116);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE _
	p000e0163:
	{
 		if (skipdoall('p000e0163')) break p000e0163;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0163;
 		}
		if (!CNDpresent(38)) break p000e0163;
		if (!CNDlt(6,60)) break p000e0163;
 		ACCwriteln(117);
		{}

	}

	// DIAL 1922
	p000e0164:
	{
 		if (skipdoall('p000e0164')) break p000e0164;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0164;
			if (!CNDnoun1(42)) break p000e0164;
 		}
		if (!CNDat(54)) break p000e0164;
		if (!CNDpresent(38)) break p000e0164;
		if (!CNDeq(3,10)) break p000e0164;
 		ACClet(6,1);
 		ACCwriteln(118);
 		ACCpause(50);
 		function anykey00014() 
		{
 		ACCwriteln(119);
 		ACCdone();
		return;
		}
 		waitKey(anykey00014);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DIAL 1922
	p000e0165:
	{
 		if (skipdoall('p000e0165')) break p000e0165;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0165;
			if (!CNDnoun1(42)) break p000e0165;
 		}
		if (!CNDat(54)) break p000e0165;
		if (!CNDabsent(38)) break p000e0165;
 		ACCwriteln(120);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIAL 1922
	p000e0166:
	{
 		if (skipdoall('p000e0166')) break p000e0166;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0166;
			if (!CNDnoun1(42)) break p000e0166;
 		}
		if (!CNDat(54)) break p000e0166;
		if (!CNDeq(3,0)) break p000e0166;
 		ACCwriteln(121);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIAL _
	p000e0167:
	{
 		if (skipdoall('p000e0167')) break p000e0167;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0167;
 		}
		if (!CNDat(54)) break p000e0167;
 		ACCwriteln(122);
		{}

	}

	// ATTA _
	p000e0168:
	{
 		if (skipdoall('p000e0168')) break p000e0168;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0168;
 		}
 		ACCwriteln(123);
		{}

	}

	// READ BIBL
	p000e0169:
	{
 		if (skipdoall('p000e0169')) break p000e0169;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0169;
			if (!CNDnoun1(48)) break p000e0169;
 		}
		if (!CNDat(48)) break p000e0169;
 		ACCwriteln(124);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// READ BOOK
	p000e0170:
	{
 		if (skipdoall('p000e0170')) break p000e0170;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0170;
			if (!CNDnoun1(82)) break p000e0170;
 		}
		if (!CNDcarried(22)) break p000e0170;
		if (!CNDeq(9,0)) break p000e0170;
 		ACCwriteln(125);
		{}

	}

	// READ BOOK
	p000e0171:
	{
 		if (skipdoall('p000e0171')) break p000e0171;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0171;
			if (!CNDnoun1(82)) break p000e0171;
 		}
		if (!CNDcarried(22)) break p000e0171;
		if (!CNDeq(9,10)) break p000e0171;
 		ACCwriteln(126);
		{}

	}

	// READ BOOK
	p000e0172:
	{
 		if (skipdoall('p000e0172')) break p000e0172;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0172;
			if (!CNDnoun1(82)) break p000e0172;
 		}
		if (!CNDcarried(22)) break p000e0172;
		if (!CNDeq(9,20)) break p000e0172;
 		ACCwriteln(127);
		{}

	}

	// SEAR _
	p000e0173:
	{
 		if (skipdoall('p000e0173')) break p000e0173;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0173;
 		}
 		ACCwriteln(128);
		{}

	}

	// EXAM CAN
	p000e0174:
	{
 		if (skipdoall('p000e0174')) break p000e0174;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0174;
			if (!CNDnoun1(46)) break p000e0174;
 		}
		if (!CNDpresent(35)) break p000e0174;
 		ACCwriteln(129);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM CAN
	p000e0175:
	{
 		if (skipdoall('p000e0175')) break p000e0175;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0175;
			if (!CNDnoun1(46)) break p000e0175;
 		}
		if (!CNDpresent(36)) break p000e0175;
 		ACCwriteln(130);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRE
	p000e0176:
	{
 		if (skipdoall('p000e0176')) break p000e0176;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0176;
			if (!CNDnoun1(49)) break p000e0176;
 		}
		if (!CNDeq(6,30)) break p000e0176;
 		ACCwriteln(131);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRE
	p000e0177:
	{
 		if (skipdoall('p000e0177')) break p000e0177;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0177;
			if (!CNDnoun1(49)) break p000e0177;
 		}
		if (!CNDeq(6,40)) break p000e0177;
 		ACCwriteln(132);
 		ACCminus(6,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRE
	p000e0178:
	{
 		if (skipdoall('p000e0178')) break p000e0178;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0178;
			if (!CNDnoun1(49)) break p000e0178;
 		}
		if (!CNDeq(6,50)) break p000e0178;
 		ACCwriteln(133);
 		ACCminus(6,20);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRE
	p000e0179:
	{
 		if (skipdoall('p000e0179')) break p000e0179;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0179;
			if (!CNDnoun1(49)) break p000e0179;
 		}
		if (!CNDeq(6,59)) break p000e0179;
 		ACCwriteln(134);
 		ACCminus(6,29);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM SCRE
	p000e0180:
	{
 		if (skipdoall('p000e0180')) break p000e0180;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0180;
			if (!CNDnoun1(49)) break p000e0180;
 		}
		if (!CNDgt(6,60)) break p000e0180;
 		ACCwriteln(135);
		{}

	}

	// EXAM SCRE
	p000e0181:
	{
 		if (skipdoall('p000e0181')) break p000e0181;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0181;
			if (!CNDnoun1(49)) break p000e0181;
 		}
		if (!CNDgt(6,60)) break p000e0181;
		if (!CNDeq(7,50)) break p000e0181;
 		ACCwriteln(136);
		{}

	}

	// EXAM SCRE
	p000e0182:
	{
 		if (skipdoall('p000e0182')) break p000e0182;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0182;
			if (!CNDnoun1(49)) break p000e0182;
 		}
		if (!CNDgt(6,60)) break p000e0182;
		if (!CNDgt(7,50)) break p000e0182;
 		ACCwriteln(137);
		{}

	}

	// EXAM SCRE
	p000e0183:
	{
 		if (skipdoall('p000e0183')) break p000e0183;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0183;
			if (!CNDnoun1(49)) break p000e0183;
 		}
		if (!CNDgt(6,60)) break p000e0183;
		if (!CNDeq(4,50)) break p000e0183;
 		ACCwriteln(138);
		{}

	}

	// EXAM SCRE
	p000e0184:
	{
 		if (skipdoall('p000e0184')) break p000e0184;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0184;
			if (!CNDnoun1(49)) break p000e0184;
 		}
		if (!CNDgt(6,60)) break p000e0184;
		if (!CNDeq(8,50)) break p000e0184;
 		ACCwriteln(139);
		{}

	}

	// EXAM SCRE
	p000e0185:
	{
 		if (skipdoall('p000e0185')) break p000e0185;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0185;
			if (!CNDnoun1(49)) break p000e0185;
 		}
		if (!CNDgt(6,60)) break p000e0185;
		if (!CNDeq(8,100)) break p000e0185;
 		ACCwriteln(140);
		{}

	}

	// EXAM SCRE
	p000e0186:
	{
 		if (skipdoall('p000e0186')) break p000e0186;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0186;
			if (!CNDnoun1(49)) break p000e0186;
 		}
		if (!CNDgt(6,60)) break p000e0186;
		if (!CNDeq(4,100)) break p000e0186;
 		ACCwriteln(141);
		{}

	}

	// EXAM SCRE
	p000e0187:
	{
 		if (skipdoall('p000e0187')) break p000e0187;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0187;
			if (!CNDnoun1(49)) break p000e0187;
 		}
		if (!CNDat(54)) break p000e0187;
		if (!CNDeq(6,1)) break p000e0187;
		if (!CNDeq(3,10)) break p000e0187;
 		ACCwriteln(142);
 		ACCwriteln(143);
		{}

	}

	// EXAM BOOT
	p000e0188:
	{
 		if (skipdoall('p000e0188')) break p000e0188;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0188;
			if (!CNDnoun1(55)) break p000e0188;
 		}
		if (!CNDpresent(31)) break p000e0188;
 		ACCwriteln(144);
		{}

	}

	// EXAM BUTT
	p000e0189:
	{
 		if (skipdoall('p000e0189')) break p000e0189;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0189;
			if (!CNDnoun1(74)) break p000e0189;
 		}
		if (!CNDat(60)) break p000e0189;
 		ACCwriteln(145);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BOOK
	p000e0190:
	{
 		if (skipdoall('p000e0190')) break p000e0190;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0190;
			if (!CNDnoun1(82)) break p000e0190;
 		}
		if (!CNDcarried(22)) break p000e0190;
 		ACCwriteln(146);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM ELEC
	p000e0191:
	{
 		if (skipdoall('p000e0191')) break p000e0191;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0191;
			if (!CNDnoun1(83)) break p000e0191;
 		}
		if (!CNDpresent(19)) break p000e0191;
 		ACCwriteln(147);
		{}

	}

	// EXAM COMP
	p000e0192:
	{
 		if (skipdoall('p000e0192')) break p000e0192;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0192;
			if (!CNDnoun1(84)) break p000e0192;
 		}
		if (!CNDpresent(18)) break p000e0192;
 		ACCwriteln(148);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMP
	p000e0193:
	{
 		if (skipdoall('p000e0193')) break p000e0193;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0193;
			if (!CNDnoun1(84)) break p000e0193;
 		}
		if (!CNDpresent(37)) break p000e0193;
 		ACCwriteln(149);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM COMP
	p000e0194:
	{
 		if (skipdoall('p000e0194')) break p000e0194;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0194;
			if (!CNDnoun1(84)) break p000e0194;
 		}
		if (!CNDpresent(38)) break p000e0194;
 		ACCwriteln(150);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM BACT
	p000e0195:
	{
 		if (skipdoall('p000e0195')) break p000e0195;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0195;
			if (!CNDnoun1(85)) break p000e0195;
 		}
		if (!CNDpresent(17)) break p000e0195;
 		ACCwriteln(151);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EXAM _
	p000e0196:
	{
 		if (skipdoall('p000e0196')) break p000e0196;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0196;
 		}
		if (!CNDnotat(54)) break p000e0196;
 		ACCwriteln(152);
		{}

	}

	// HIST SYLV
	p000e0197:
	{
 		if (skipdoall('p000e0197')) break p000e0197;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0197;
			if (!CNDnoun1(92)) break p000e0197;
 		}
		if (!CNDnotzero(116)) break p000e0197;
 		ACCwriteln(153);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HIST BILL
	p000e0198:
	{
 		if (skipdoall('p000e0198')) break p000e0198;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0198;
			if (!CNDnoun1(93)) break p000e0198;
 		}
		if (!CNDnotzero(115)) break p000e0198;
 		ACCwriteln(154);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HIST SAM
	p000e0199:
	{
 		if (skipdoall('p000e0199')) break p000e0199;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0199;
			if (!CNDnoun1(94)) break p000e0199;
 		}
		if (!CNDnotzero(114)) break p000e0199;
 		ACCwriteln(155);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HIST SUE
	p000e0200:
	{
 		if (skipdoall('p000e0200')) break p000e0200;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0200;
			if (!CNDnoun1(95)) break p000e0200;
 		}
		if (!CNDnotzero(113)) break p000e0200;
 		ACCwriteln(156);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HIST DAVE
	p000e0201:
	{
 		if (skipdoall('p000e0201')) break p000e0201;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0201;
			if (!CNDnoun1(96)) break p000e0201;
 		}
		if (!CNDnotzero(112)) break p000e0201;
 		ACCwriteln(157);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HIST _
	p000e0202:
	{
 		if (skipdoall('p000e0202')) break p000e0202;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0202;
 		}
 		ACCwriteln(158);
		{}

	}

	// SLEE _
	p000e0203:
	{
 		if (skipdoall('p000e0203')) break p000e0203;
 		if (in_response)
		{
			if (!CNDverb(53)) break p000e0203;
 		}
 		ACCwriteln(159);
 		ACCplus(111,5);
 		ACCpause(0);
 		function anykey00015() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00015);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0204:
	{
 		if (skipdoall('p000e0204')) break p000e0204;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0204;
 		}
		if (!CNDlt(111,25)) break p000e0204;
 		ACCwriteln(160);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0205:
	{
 		if (skipdoall('p000e0205')) break p000e0205;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0205;
 		}
		if (!CNDgt(111,24)) break p000e0205;
		if (!CNDlt(111,45)) break p000e0205;
 		ACCwriteln(161);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0206:
	{
 		if (skipdoall('p000e0206')) break p000e0206;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0206;
 		}
		if (!CNDgt(111,44)) break p000e0206;
		if (!CNDlt(111,70)) break p000e0206;
 		ACCwriteln(162);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0207:
	{
 		if (skipdoall('p000e0207')) break p000e0207;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0207;
 		}
		if (!CNDgt(111,69)) break p000e0207;
		if (!CNDlt(111,90)) break p000e0207;
 		ACCwriteln(163);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0208:
	{
 		if (skipdoall('p000e0208')) break p000e0208;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0208;
 		}
		if (!CNDgt(111,89)) break p000e0208;
		if (!CNDlt(111,100)) break p000e0208;
 		ACCwriteln(164);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0209:
	{
 		if (skipdoall('p000e0209')) break p000e0209;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0209;
 		}
		if (!CNDgt(111,99)) break p000e0209;
		if (!CNDlt(111,106)) break p000e0209;
 		ACCwriteln(165);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0210:
	{
 		if (skipdoall('p000e0210')) break p000e0210;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0210;
 		}
		if (!CNDgt(111,105)) break p000e0210;
		if (!CNDlt(111,112)) break p000e0210;
 		ACCwriteln(166);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TIME _
	p000e0211:
	{
 		if (skipdoall('p000e0211')) break p000e0211;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0211;
 		}
		if (!CNDgt(111,111)) break p000e0211;
		if (!CNDlt(111,118)) break p000e0211;
 		ACCwriteln(167);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX GENE
	p000e0212:
	{
 		if (skipdoall('p000e0212')) break p000e0212;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0212;
			if (!CNDnoun1(89)) break p000e0212;
 		}
		if (!CNDpresent(12)) break p000e0212;
		if (!CNDlt(18,12)) break p000e0212;
 		ACCwriteln(168);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX GENE
	p000e0213:
	{
 		if (skipdoall('p000e0213')) break p000e0213;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0213;
			if (!CNDnoun1(89)) break p000e0213;
 		}
		if (!CNDpresent(12)) break p000e0213;
		if (!CNDgt(18,11)) break p000e0213;
		if (!CNDcarried(34)) break p000e0213;
		if (!CNDcarried(26)) break p000e0213;
		if (!CNDnotworn(31)) break p000e0213;
 		ACCwriteln(169);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// FIX GENE
	p000e0214:
	{
 		if (skipdoall('p000e0214')) break p000e0214;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0214;
			if (!CNDnoun1(89)) break p000e0214;
 		}
		if (!CNDpresent(12)) break p000e0214;
		if (!CNDgt(18,11)) break p000e0214;
		if (!CNDworn(31)) break p000e0214;
		if (!CNDcarried(26)) break p000e0214;
		if (!CNDcarried(34)) break p000e0214;
 		ACCwriteln(170);
 		ACClet(31,2);
 		ACCswap(12,13);
 		ACCplus(111,25);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX GENE
	p000e0215:
	{
 		if (skipdoall('p000e0215')) break p000e0215;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0215;
			if (!CNDnoun1(89)) break p000e0215;
 		}
		if (!CNDpresent(12)) break p000e0215;
		if (!CNDnotcarr(26)) break p000e0215;
		if (!CNDnotcarr(34)) break p000e0215;
 		ACCwriteln(171);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX GENE
	p000e0216:
	{
 		if (skipdoall('p000e0216')) break p000e0216;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0216;
			if (!CNDnoun1(89)) break p000e0216;
 		}
		if (!CNDpresent(12)) break p000e0216;
		if (!CNDnotcarr(34)) break p000e0216;
 		ACCwriteln(172);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX GENE
	p000e0217:
	{
 		if (skipdoall('p000e0217')) break p000e0217;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0217;
			if (!CNDnoun1(89)) break p000e0217;
 		}
		if (!CNDpresent(12)) break p000e0217;
		if (!CNDnotcarr(26)) break p000e0217;
 		ACCwriteln(173);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FIX _
	p000e0218:
	{
 		if (skipdoall('p000e0218')) break p000e0218;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0218;
 		}
 		ACCwriteln(174);
		{}

	}

	// ASK SUE
	p000e0219:
	{
 		if (skipdoall('p000e0219')) break p000e0219;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0219;
			if (!CNDnoun1(95)) break p000e0219;
 		}
		if (!CNDat(27)) break p000e0219;
		if (!CNDnotzero(113)) break p000e0219;
 		ACCwriteln(175);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASK SUE
	p000e0220:
	{
 		if (skipdoall('p000e0220')) break p000e0220;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0220;
			if (!CNDnoun1(95)) break p000e0220;
 		}
		if (!CNDat(24)) break p000e0220;
		if (!CNDnotzero(113)) break p000e0220;
 		ACCwriteln(176);
		{}

	}

	// ASK DAVE
	p000e0221:
	{
 		if (skipdoall('p000e0221')) break p000e0221;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0221;
			if (!CNDnoun1(96)) break p000e0221;
 		}
		if (!CNDat(31)) break p000e0221;
		if (!CNDnotzero(112)) break p000e0221;
		if (!CNDabsent(33)) break p000e0221;
 		ACCwriteln(177);
		{}

	}

	// ASK _
	p000e0222:
	{
 		if (skipdoall('p000e0222')) break p000e0222;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0222;
 		}
		if (!CNDeq(21,18)) break p000e0222;
		if (!CNDpresent(12)) break p000e0222;
 		ACCwriteln(178);
		{}

	}

	// ASK _
	p000e0223:
	{
 		if (skipdoall('p000e0223')) break p000e0223;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0223;
 		}
		if (!CNDeq(112,0)) break p000e0223;
		if (!CNDeq(113,0)) break p000e0223;
		if (!CNDeq(114,0)) break p000e0223;
		if (!CNDeq(115,0)) break p000e0223;
		if (!CNDeq(116,0)) break p000e0223;
		if (!CNDabsent(1)) break p000e0223;
		if (!CNDabsent(2)) break p000e0223;
		if (!CNDabsent(3)) break p000e0223;
		if (!CNDabsent(4)) break p000e0223;
		if (!CNDabsent(5)) break p000e0223;
 		ACCwriteln(179);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ASK _
	p000e0224:
	{
 		if (skipdoall('p000e0224')) break p000e0224;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0224;
 		}
 		ACCwriteln(180);
		{}

	}

	// CONN ELEC
	p000e0225:
	{
 		if (skipdoall('p000e0225')) break p000e0225;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0225;
			if (!CNDnoun1(83)) break p000e0225;
 		}
		if (!CNDat(54)) break p000e0225;
		if (!CNDpresent(37)) break p000e0225;
		if (!CNDcarried(19)) break p000e0225;
		if (!CNDgt(17,9)) break p000e0225;
 		ACCdestroy(37);
 		ACCcreate(38);
 		ACCdestroy(19);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// CONN ELEC
	p000e0226:
	{
 		if (skipdoall('p000e0226')) break p000e0226;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0226;
			if (!CNDnoun1(83)) break p000e0226;
 		}
		if (!CNDat(54)) break p000e0226;
		if (!CNDpresent(37)) break p000e0226;
		if (!CNDcarried(19)) break p000e0226;
		if (!CNDlt(17,10)) break p000e0226;
 		ACCwriteln(181);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN ELEC
	p000e0227:
	{
 		if (skipdoall('p000e0227')) break p000e0227;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0227;
			if (!CNDnoun1(83)) break p000e0227;
 		}
		if (!CNDnotcarr(19)) break p000e0227;
 		ACCwriteln(182);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN ELEC
	p000e0228:
	{
 		if (skipdoall('p000e0228')) break p000e0228;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0228;
			if (!CNDnoun1(83)) break p000e0228;
 		}
		if (!CNDabsent(37)) break p000e0228;
		if (!CNDabsent(38)) break p000e0228;
		if (!CNDabsent(18)) break p000e0228;
 		ACCwriteln(183);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN ELEC
	p000e0229:
	{
 		if (skipdoall('p000e0229')) break p000e0229;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0229;
			if (!CNDnoun1(83)) break p000e0229;
 		}
		if (!CNDnotat(54)) break p000e0229;
 		ACCwriteln(184);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN META
	p000e0230:
	{
 		if (skipdoall('p000e0230')) break p000e0230;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0230;
			if (!CNDnoun1(86)) break p000e0230;
 		}
		if (!CNDat(20)) break p000e0230;
		if (!CNDlt(17,10)) break p000e0230;
 		ACCwriteln(185);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN META
	p000e0231:
	{
 		if (skipdoall('p000e0231')) break p000e0231;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0231;
			if (!CNDnoun1(86)) break p000e0231;
 		}
		if (!CNDat(20)) break p000e0231;
		if (!CNDgt(17,10)) break p000e0231;
		if (!CNDcarried(16)) break p000e0231;
 		ACCswap(16,15);
 		ACCwriteln(186);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONN _
	p000e0232:
	{
 		if (skipdoall('p000e0232')) break p000e0232;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0232;
 		}
 		ACCwriteln(187);
		{}

	}

	// OPEN WIND
	p000e0233:
	{
 		if (skipdoall('p000e0233')) break p000e0233;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0233;
			if (!CNDnoun1(59)) break p000e0233;
 		}
		if (!CNDat(25)) break p000e0233;
 		ACCgoto(78);
 		ACCwriteln(188);
 		ACCpause(75);
 		function anykey00016() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00016);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// OPEN WIND
	p000e0234:
	{
 		if (skipdoall('p000e0234')) break p000e0234;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0234;
			if (!CNDnoun1(59)) break p000e0234;
 		}
		if (!CNDat(78)) break p000e0234;
 		ACCwriteln(189);
 		ACCgoto(25);
 		ACCpause(75);
 		function anykey00017() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00017);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0235:
	{
 		if (skipdoall('p000e0235')) break p000e0235;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0235;
			if (!CNDnoun1(63)) break p000e0235;
 		}
		if (!CNDpresent(30)) break p000e0235;
 		ACCwriteln(190);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0236:
	{
 		if (skipdoall('p000e0236')) break p000e0236;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0236;
			if (!CNDnoun1(63)) break p000e0236;
 		}
		if (!CNDat(50)) break p000e0236;
		if (!CNDnotcarr(15)) break p000e0236;
 		ACCwriteln(191);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0237:
	{
 		if (skipdoall('p000e0237')) break p000e0237;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0237;
			if (!CNDnoun1(63)) break p000e0237;
 		}
		if (!CNDat(50)) break p000e0237;
		if (!CNDcarried(15)) break p000e0237;
 		ACCgoto(52);
 		ACCwriteln(192);
 		ACCpause(75);
 		function anykey00018() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00018);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0238:
	{
 		if (skipdoall('p000e0238')) break p000e0238;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0238;
			if (!CNDnoun1(63)) break p000e0238;
 		}
		if (!CNDat(50)) break p000e0238;
		if (!CNDnotcarr(15)) break p000e0238;
 		ACCwriteln(193);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0239:
	{
 		if (skipdoall('p000e0239')) break p000e0239;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0239;
			if (!CNDnoun1(63)) break p000e0239;
 		}
		if (!CNDat(55)) break p000e0239;
		if (!CNDcarried(15)) break p000e0239;
 		ACCwriteln(194);
 		ACCgoto(56);
 		ACCpause(100);
 		function anykey00019() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00019);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0240:
	{
 		if (skipdoall('p000e0240')) break p000e0240;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0240;
			if (!CNDnoun1(63)) break p000e0240;
 		}
		if (!CNDat(60)) break p000e0240;
		if (!CNDeq(10,10)) break p000e0240;
 		ACCcreate(17);
 		ACCplus(10,1);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0241:
	{
 		if (skipdoall('p000e0241')) break p000e0241;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0241;
			if (!CNDnoun1(63)) break p000e0241;
 		}
		if (!CNDat(60)) break p000e0241;
		if (!CNDeq(10,0)) break p000e0241;
 		ACCwriteln(195);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0242:
	{
 		if (skipdoall('p000e0242')) break p000e0242;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0242;
			if (!CNDnoun1(63)) break p000e0242;
 		}
		if (!CNDat(60)) break p000e0242;
		if (!CNDeq(10,11)) break p000e0242;
 		ACCwriteln(196);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN BACT
	p000e0243:
	{
 		if (skipdoall('p000e0243')) break p000e0243;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0243;
			if (!CNDnoun1(85)) break p000e0243;
 		}
		if (!CNDcarried(17)) break p000e0243;
 		ACCwriteln(197);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// OPEN _
	p000e0244:
	{
 		if (skipdoall('p000e0244')) break p000e0244;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0244;
 		}
		if (!CNDat(55)) break p000e0244;
		if (!CNDnotcarr(15)) break p000e0244;
 		ACCwriteln(198);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN _
	p000e0245:
	{
 		if (skipdoall('p000e0245')) break p000e0245;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0245;
 		}
		if (!CNDat(31)) break p000e0245;
 		ACCwriteln(199);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN _
	p000e0246:
	{
 		if (skipdoall('p000e0246')) break p000e0246;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0246;
 		}
 		ACCwriteln(200);
		{}

	}

	// HELP _
	p000e0247:
	{
 		if (skipdoall('p000e0247')) break p000e0247;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0247;
 		}
 		ACCcls();
 		ACCwriteln(201);
 		ACCanykey();
 		function anykey00020() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00020);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// BELT SUE
	p000e0248:
	{
 		if (skipdoall('p000e0248')) break p000e0248;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0248;
			if (!CNDnoun1(95)) break p000e0248;
 		}
		if (!CNDnotzero(113)) break p000e0248;
 		ACCclear(113);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
 		ACCcreate(29);
 		ACCwriteln(202);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BELT SUE
	p000e0249:
	{
 		if (skipdoall('p000e0249')) break p000e0249;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0249;
			if (!CNDnoun1(95)) break p000e0249;
 		}
		if (!CNDatgt(27)) break p000e0249;
		if (!CNDnotzero(113)) break p000e0249;
 		ACCwriteln(203);
 		ACCclear(113);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BELT _
	p000e0250:
	{
 		if (skipdoall('p000e0250')) break p000e0250;
 		if (in_response)
		{
			if (!CNDverb(66)) break p000e0250;
 		}
 		ACCwriteln(204);
		{}

	}

	// TIE COIL
	p000e0251:
	{
 		if (skipdoall('p000e0251')) break p000e0251;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0251;
			if (!CNDnoun1(68)) break p000e0251;
 		}
		if (!CNDat(27)) break p000e0251;
		if (!CNDcarried(27)) break p000e0251;
 		ACCswap(27,28);
 		ACCdrop(28);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// TIE COIL
	p000e0252:
	{
 		if (skipdoall('p000e0252')) break p000e0252;
 		if (in_response)
		{
			if (!CNDverb(67)) break p000e0252;
			if (!CNDnoun1(68)) break p000e0252;
 		}
		if (!CNDcarried(27)) break p000e0252;
		if (!CNDnotat(27)) break p000e0252;
 		ACCwriteln(205);
		{}

	}

	// DRIV _
	p000e0253:
	{
 		if (skipdoall('p000e0253')) break p000e0253;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0253;
 		}
		if (!CNDat(3)) break p000e0253;
		if (!CNDeq(113,0)) break p000e0253;
 		ACCwriteln(206);
		{}

	}

	// DRIV _
	p000e0254:
	{
 		if (skipdoall('p000e0254')) break p000e0254;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0254;
 		}
		if (!CNDat(3)) break p000e0254;
		if (!CNDnotzero(113)) break p000e0254;
 		ACCwriteln(207);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL CAN
	p000e0255:
	{
 		if (skipdoall('p000e0255')) break p000e0255;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0255;
			if (!CNDnoun1(46)) break p000e0255;
 		}
		if (!CNDat(45)) break p000e0255;
		if (!CNDcarried(35)) break p000e0255;
 		ACCswap(35,36);
 		ACCok();
		break pro000_restart;
		{}

	}

	// FILL CAN
	p000e0256:
	{
 		if (skipdoall('p000e0256')) break p000e0256;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0256;
			if (!CNDnoun1(46)) break p000e0256;
 		}
		if (!CNDnotat(45)) break p000e0256;
		if (!CNDcarried(35)) break p000e0256;
 		ACCwriteln(208);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL CAN
	p000e0257:
	{
 		if (skipdoall('p000e0257')) break p000e0257;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0257;
			if (!CNDnoun1(46)) break p000e0257;
 		}
		if (!CNDcarried(36)) break p000e0257;
 		ACCwriteln(209);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL BUS
	p000e0258:
	{
 		if (skipdoall('p000e0258')) break p000e0258;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0258;
			if (!CNDnoun1(71)) break p000e0258;
 		}
		if (!CNDat(2)) break p000e0258;
		if (!CNDeq(27,125)) break p000e0258;
 		ACCplus(27,10);
 		ACCwriteln(210);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL BUS
	p000e0259:
	{
 		if (skipdoall('p000e0259')) break p000e0259;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0259;
			if (!CNDnoun1(71)) break p000e0259;
 		}
		if (!CNDat(2)) break p000e0259;
		if (!CNDeq(27,120)) break p000e0259;
 		ACCwriteln(211);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL BUS
	p000e0260:
	{
 		if (skipdoall('p000e0260')) break p000e0260;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0260;
			if (!CNDnoun1(71)) break p000e0260;
 		}
		if (!CNDcarried(36)) break p000e0260;
		if (!CNDat(43)) break p000e0260;
		if (!CNDeq(20,1)) break p000e0260;
 		ACCclear(20);
 		ACCminus(27,1);
 		ACCwriteln(212);
 		ACCdestroy(36);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL BACT
	p000e0261:
	{
 		if (skipdoall('p000e0261')) break p000e0261;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0261;
			if (!CNDnoun1(85)) break p000e0261;
 		}
		if (!CNDpresent(17)) break p000e0261;
 		ACCwriteln(213);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL _
	p000e0262:
	{
 		if (skipdoall('p000e0262')) break p000e0262;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0262;
 		}
		if (!CNDat(2)) break p000e0262;
		if (!CNDeq(27,135)) break p000e0262;
 		ACCwriteln(214);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FILL _
	p000e0263:
	{
 		if (skipdoall('p000e0263')) break p000e0263;
 		if (in_response)
		{
			if (!CNDverb(72)) break p000e0263;
 		}
 		ACCwriteln(215);
		{}

	}

	// PRES OVER
	p000e0264:
	{
 		if (skipdoall('p000e0264')) break p000e0264;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0264;
			if (!CNDnoun1(43)) break p000e0264;
 		}
		if (!CNDcarried(22)) break p000e0264;
		if (!CNDeq(9,0)) break p000e0264;
 		ACCplus(9,10);
 		ACCok();
		break pro000_restart;
		{}

	}

	// PRES OVER
	p000e0265:
	{
 		if (skipdoall('p000e0265')) break p000e0265;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0265;
			if (!CNDnoun1(43)) break p000e0265;
 		}
		if (!CNDcarried(22)) break p000e0265;
		if (!CNDeq(9,10)) break p000e0265;
 		ACCplus(9,10);
 		ACCok();
		break pro000_restart;
		{}

	}

	// PRES OVER
	p000e0266:
	{
 		if (skipdoall('p000e0266')) break p000e0266;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0266;
			if (!CNDnoun1(43)) break p000e0266;
 		}
		if (!CNDeq(9,20)) break p000e0266;
 		ACCclear(9);
 		ACCok();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0267:
	{
 		if (skipdoall('p000e0267')) break p000e0267;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0267;
			if (!CNDnoun1(75)) break p000e0267;
 		}
		if (!CNDlt(27,120)) break p000e0267;
		if (!CNDat(1)) break p000e0267;
 		ACClet(27,120);
 		ACCwriteln(216);
 		ACCcreate(40);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0268:
	{
 		if (skipdoall('p000e0268')) break p000e0268;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0268;
			if (!CNDnoun1(75)) break p000e0268;
 		}
		if (!CNDat(1)) break p000e0268;
		if (!CNDeq(27,125)) break p000e0268;
 		ACCwriteln(217);
 		ACCminus(27,5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0269:
	{
 		if (skipdoall('p000e0269')) break p000e0269;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0269;
			if (!CNDnoun1(75)) break p000e0269;
 		}
		if (!CNDat(1)) break p000e0269;
		if (!CNDeq(27,120)) break p000e0269;
 		ACCwriteln(218);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0270:
	{
 		if (skipdoall('p000e0270')) break p000e0270;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0270;
			if (!CNDnoun1(75)) break p000e0270;
 		}
		if (!CNDeq(10,0)) break p000e0270;
		if (!CNDat(60)) break p000e0270;
 		ACClet(10,10);
 		ACCwriteln(219);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0271:
	{
 		if (skipdoall('p000e0271')) break p000e0271;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0271;
			if (!CNDnoun1(75)) break p000e0271;
 		}
		if (!CNDat(54)) break p000e0271;
		if (!CNDeq(3,0)) break p000e0271;
 		ACCwriteln(220);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0272:
	{
 		if (skipdoall('p000e0272')) break p000e0272;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0272;
			if (!CNDnoun1(75)) break p000e0272;
 		}
		if (!CNDpresent(38)) break p000e0272;
		if (!CNDat(54)) break p000e0272;
 		ACCclear(3);
 		ACCwriteln(221);
 		ACCclear(5);
 		ACCclear(6);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GREE
	p000e0273:
	{
 		if (skipdoall('p000e0273')) break p000e0273;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0273;
			if (!CNDnoun1(75)) break p000e0273;
 		}
		if (!CNDat(54)) break p000e0273;
 		ACCwriteln(222);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GO
	p000e0274:
	{
 		if (skipdoall('p000e0274')) break p000e0274;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0274;
			if (!CNDnoun1(76)) break p000e0274;
 		}
		if (!CNDat(1)) break p000e0274;
		if (!CNDeq(27,120)) break p000e0274;
 		ACCwriteln(223);
 		ACCplus(27,5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GO
	p000e0275:
	{
 		if (skipdoall('p000e0275')) break p000e0275;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0275;
			if (!CNDnoun1(76)) break p000e0275;
 		}
		if (!CNDat(1)) break p000e0275;
		if (!CNDeq(27,125)) break p000e0275;
 		ACCwriteln(224);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GO
	p000e0276:
	{
 		if (skipdoall('p000e0276')) break p000e0276;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0276;
			if (!CNDnoun1(76)) break p000e0276;
 		}
		if (!CNDat(36)) break p000e0276;
		if (!CNDeq(31,3)) break p000e0276;
 		ACCplus(31,1);
 		ACCwriteln(225);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GO
	p000e0277:
	{
 		if (skipdoall('p000e0277')) break p000e0277;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0277;
			if (!CNDnoun1(76)) break p000e0277;
 		}
		if (!CNDat(36)) break p000e0277;
		if (!CNDgt(31,3)) break p000e0277;
 		ACCwriteln(226);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GO
	p000e0278:
	{
 		if (skipdoall('p000e0278')) break p000e0278;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0278;
			if (!CNDnoun1(76)) break p000e0278;
 		}
		if (!CNDat(60)) break p000e0278;
		if (!CNDeq(10,10)) break p000e0278;
 		ACCclear(10);
 		ACCwriteln(227);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GO
	p000e0279:
	{
 		if (skipdoall('p000e0279')) break p000e0279;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0279;
			if (!CNDnoun1(76)) break p000e0279;
 		}
		if (!CNDat(54)) break p000e0279;
		if (!CNDeq(3,0)) break p000e0279;
 		ACClet(3,10);
 		ACCwriteln(228);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES GO
	p000e0280:
	{
 		if (skipdoall('p000e0280')) break p000e0280;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0280;
			if (!CNDnoun1(76)) break p000e0280;
 		}
		if (!CNDat(54)) break p000e0280;
		if (!CNDeq(3,10)) break p000e0280;
 		ACCwriteln(229);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES _
	p000e0281:
	{
 		if (skipdoall('p000e0281')) break p000e0281;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0281;
 		}
		if (!CNDat(1)) break p000e0281;
		if (!CNDeq(27,135)) break p000e0281;
 		ACCwriteln(230);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES _
	p000e0282:
	{
 		if (skipdoall('p000e0282')) break p000e0282;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0282;
 		}
 		ACCwriteln(231);
		{}

	}

	// PRES _
	p000e0283:
	{
 		if (skipdoall('p000e0283')) break p000e0283;
 		if (in_response)
		{
			if (!CNDverb(73)) break p000e0283;
 		}
		if (!CNDat(60)) break p000e0283;
		if (!CNDeq(10,11)) break p000e0283;
 		ACCwriteln(232);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EAT FOOD
	p000e0284:
	{
 		if (skipdoall('p000e0284')) break p000e0284;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0284;
			if (!CNDnoun1(77)) break p000e0284;
 		}
		if (!CNDat(40)) break p000e0284;
 		ACCok();
		break pro000_restart;
		{}

	}

	// EAT FOOD
	p000e0285:
	{
 		if (skipdoall('p000e0285')) break p000e0285;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0285;
			if (!CNDnoun1(77)) break p000e0285;
 		}
		if (!CNDat(53)) break p000e0285;
 		ACCok();
		break pro000_restart;
		{}

	}

	// EAT FOOD
	p000e0286:
	{
 		if (skipdoall('p000e0286')) break p000e0286;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0286;
			if (!CNDnoun1(77)) break p000e0286;
 		}
		if (!CNDat(9)) break p000e0286;
 		ACCok();
		break pro000_restart;
		{}

	}

	// EAT FOOD
	p000e0287:
	{
 		if (skipdoall('p000e0287')) break p000e0287;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0287;
			if (!CNDnoun1(77)) break p000e0287;
 		}
		if (!CNDat(19)) break p000e0287;
 		ACCok();
		break pro000_restart;
		{}

	}

	// EAT FOOD
	p000e0288:
	{
 		if (skipdoall('p000e0288')) break p000e0288;
 		if (in_response)
		{
			if (!CNDverb(78)) break p000e0288;
			if (!CNDnoun1(77)) break p000e0288;
 		}
		if (!CNDat(62)) break p000e0288;
 		ACCok();
		break pro000_restart;
		{}

	}

	// PART _
	p000e0289:
	{
 		if (skipdoall('p000e0289')) break p000e0289;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0289;
 		}
 		ACCwriteln(233);
		{}

	}

	// PART _
	p000e0290:
	{
 		if (skipdoall('p000e0290')) break p000e0290;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0290;
 		}
		if (!CNDnotzero(113)) break p000e0290;
 		ACCwriteln(234);
		{}

	}

	// PART _
	p000e0291:
	{
 		if (skipdoall('p000e0291')) break p000e0291;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0291;
 		}
		if (!CNDnotzero(114)) break p000e0291;
 		ACCwriteln(235);
		{}

	}

	// PART _
	p000e0292:
	{
 		if (skipdoall('p000e0292')) break p000e0292;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0292;
 		}
		if (!CNDnotzero(115)) break p000e0292;
 		ACCwriteln(236);
		{}

	}

	// PART _
	p000e0293:
	{
 		if (skipdoall('p000e0293')) break p000e0293;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0293;
 		}
		if (!CNDnotzero(116)) break p000e0293;
 		ACCwriteln(237);
		{}

	}

	// PART _
	p000e0294:
	{
 		if (skipdoall('p000e0294')) break p000e0294;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0294;
 		}
		if (!CNDnotzero(112)) break p000e0294;
 		ACCwriteln(238);
		{}

	}

	// PART _
	p000e0295:
	{
 		if (skipdoall('p000e0295')) break p000e0295;
 		if (in_response)
		{
			if (!CNDverb(80)) break p000e0295;
 		}
		if (!CNDzero(112)) break p000e0295;
		if (!CNDzero(113)) break p000e0295;
		if (!CNDzero(114)) break p000e0295;
		if (!CNDzero(115)) break p000e0295;
		if (!CNDzero(116)) break p000e0295;
 		ACCwriteln(239);
		{}

	}

	// FEED SYLV
	p000e0296:
	{
 		if (skipdoall('p000e0296')) break p000e0296;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0296;
			if (!CNDnoun1(92)) break p000e0296;
 		}
		if (!CNDat(40)) break p000e0296;
		if (!CNDnotzero(116)) break p000e0296;
 		ACCminus(26,100);
 		ACCwriteln(240);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SYLV
	p000e0297:
	{
 		if (skipdoall('p000e0297')) break p000e0297;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0297;
			if (!CNDnoun1(92)) break p000e0297;
 		}
		if (!CNDat(53)) break p000e0297;
		if (!CNDnotzero(116)) break p000e0297;
 		ACCminus(26,100);
 		ACCwriteln(241);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SYLV
	p000e0298:
	{
 		if (skipdoall('p000e0298')) break p000e0298;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0298;
			if (!CNDnoun1(92)) break p000e0298;
 		}
		if (!CNDat(9)) break p000e0298;
		if (!CNDnotzero(116)) break p000e0298;
 		ACCminus(26,100);
 		ACCwriteln(242);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SYLV
	p000e0299:
	{
 		if (skipdoall('p000e0299')) break p000e0299;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0299;
			if (!CNDnoun1(92)) break p000e0299;
 		}
		if (!CNDat(19)) break p000e0299;
		if (!CNDnotzero(116)) break p000e0299;
 		ACCminus(26,100);
 		ACCwriteln(243);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SYLV
	p000e0300:
	{
 		if (skipdoall('p000e0300')) break p000e0300;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0300;
			if (!CNDnoun1(92)) break p000e0300;
 		}
		if (!CNDat(62)) break p000e0300;
		if (!CNDnotzero(116)) break p000e0300;
 		ACCminus(26,100);
 		ACCwriteln(244);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED BILL
	p000e0301:
	{
 		if (skipdoall('p000e0301')) break p000e0301;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0301;
			if (!CNDnoun1(93)) break p000e0301;
 		}
		if (!CNDnotzero(115)) break p000e0301;
		if (!CNDat(40)) break p000e0301;
 		ACCminus(25,100);
 		ACCwriteln(245);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED BILL
	p000e0302:
	{
 		if (skipdoall('p000e0302')) break p000e0302;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0302;
			if (!CNDnoun1(93)) break p000e0302;
 		}
		if (!CNDat(53)) break p000e0302;
		if (!CNDnotzero(115)) break p000e0302;
 		ACCminus(25,100);
 		ACCwriteln(246);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED BILL
	p000e0303:
	{
 		if (skipdoall('p000e0303')) break p000e0303;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0303;
			if (!CNDnoun1(93)) break p000e0303;
 		}
		if (!CNDat(9)) break p000e0303;
		if (!CNDnotzero(115)) break p000e0303;
 		ACCminus(25,100);
 		ACCwriteln(247);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED BILL
	p000e0304:
	{
 		if (skipdoall('p000e0304')) break p000e0304;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0304;
			if (!CNDnoun1(93)) break p000e0304;
 		}
		if (!CNDat(19)) break p000e0304;
		if (!CNDnotzero(115)) break p000e0304;
 		ACCminus(25,100);
 		ACCwriteln(248);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED BILL
	p000e0305:
	{
 		if (skipdoall('p000e0305')) break p000e0305;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0305;
			if (!CNDnoun1(93)) break p000e0305;
 		}
		if (!CNDat(62)) break p000e0305;
		if (!CNDnotzero(115)) break p000e0305;
 		ACCminus(25,100);
 		ACCwriteln(249);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SAM
	p000e0306:
	{
 		if (skipdoall('p000e0306')) break p000e0306;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0306;
			if (!CNDnoun1(94)) break p000e0306;
 		}
		if (!CNDat(40)) break p000e0306;
		if (!CNDnotzero(114)) break p000e0306;
 		ACCminus(24,100);
 		ACCwriteln(250);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SAM
	p000e0307:
	{
 		if (skipdoall('p000e0307')) break p000e0307;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0307;
			if (!CNDnoun1(94)) break p000e0307;
 		}
		if (!CNDat(53)) break p000e0307;
		if (!CNDnotzero(114)) break p000e0307;
 		ACCminus(24,100);
 		ACCwriteln(251);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SAM
	p000e0308:
	{
 		if (skipdoall('p000e0308')) break p000e0308;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0308;
			if (!CNDnoun1(94)) break p000e0308;
 		}
		if (!CNDat(9)) break p000e0308;
		if (!CNDnotzero(114)) break p000e0308;
 		ACCminus(24,100);
 		ACCwriteln(252);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SAM
	p000e0309:
	{
 		if (skipdoall('p000e0309')) break p000e0309;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0309;
			if (!CNDnoun1(94)) break p000e0309;
 		}
		if (!CNDat(19)) break p000e0309;
		if (!CNDnotzero(114)) break p000e0309;
 		ACCminus(24,100);
 		ACCwriteln(253);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SAM
	p000e0310:
	{
 		if (skipdoall('p000e0310')) break p000e0310;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0310;
			if (!CNDnoun1(94)) break p000e0310;
 		}
		if (!CNDat(62)) break p000e0310;
		if (!CNDnotzero(114)) break p000e0310;
 		ACCminus(24,100);
 		ACCwriteln(254);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SUE
	p000e0311:
	{
 		if (skipdoall('p000e0311')) break p000e0311;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0311;
			if (!CNDnoun1(95)) break p000e0311;
 		}
		if (!CNDat(40)) break p000e0311;
		if (!CNDnotzero(113)) break p000e0311;
 		ACCminus(23,100);
 		ACCwriteln(255);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SUE
	p000e0312:
	{
 		if (skipdoall('p000e0312')) break p000e0312;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0312;
			if (!CNDnoun1(95)) break p000e0312;
 		}
		if (!CNDat(53)) break p000e0312;
		if (!CNDnotzero(113)) break p000e0312;
 		ACCminus(23,100);
 		ACCwriteln(256);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SUE
	p000e0313:
	{
 		if (skipdoall('p000e0313')) break p000e0313;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0313;
			if (!CNDnoun1(95)) break p000e0313;
 		}
		if (!CNDat(9)) break p000e0313;
		if (!CNDnotzero(113)) break p000e0313;
 		ACCminus(23,100);
 		ACCwriteln(257);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SUE
	p000e0314:
	{
 		if (skipdoall('p000e0314')) break p000e0314;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0314;
			if (!CNDnoun1(95)) break p000e0314;
 		}
		if (!CNDat(19)) break p000e0314;
		if (!CNDnotzero(113)) break p000e0314;
 		ACCminus(23,100);
 		ACCwriteln(258);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED SUE
	p000e0315:
	{
 		if (skipdoall('p000e0315')) break p000e0315;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0315;
			if (!CNDnoun1(95)) break p000e0315;
 		}
		if (!CNDat(62)) break p000e0315;
		if (!CNDnotzero(113)) break p000e0315;
 		ACCminus(23,100);
 		ACCwriteln(259);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED DAVE
	p000e0316:
	{
 		if (skipdoall('p000e0316')) break p000e0316;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0316;
			if (!CNDnoun1(96)) break p000e0316;
 		}
		if (!CNDat(40)) break p000e0316;
		if (!CNDnotzero(112)) break p000e0316;
 		ACCminus(22,100);
 		ACCwriteln(260);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED DAVE
	p000e0317:
	{
 		if (skipdoall('p000e0317')) break p000e0317;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0317;
			if (!CNDnoun1(96)) break p000e0317;
 		}
		if (!CNDat(53)) break p000e0317;
		if (!CNDnotzero(112)) break p000e0317;
 		ACCminus(22,100);
 		ACCwriteln(261);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED DAVE
	p000e0318:
	{
 		if (skipdoall('p000e0318')) break p000e0318;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0318;
			if (!CNDnoun1(96)) break p000e0318;
 		}
		if (!CNDat(9)) break p000e0318;
		if (!CNDnotzero(112)) break p000e0318;
 		ACCminus(22,100);
 		ACCwriteln(262);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED DAVE
	p000e0319:
	{
 		if (skipdoall('p000e0319')) break p000e0319;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0319;
			if (!CNDnoun1(96)) break p000e0319;
 		}
		if (!CNDat(19)) break p000e0319;
		if (!CNDnotzero(112)) break p000e0319;
 		ACCminus(22,100);
 		ACCwriteln(263);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED DAVE
	p000e0320:
	{
 		if (skipdoall('p000e0320')) break p000e0320;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0320;
			if (!CNDnoun1(96)) break p000e0320;
 		}
		if (!CNDat(62)) break p000e0320;
		if (!CNDnotzero(112)) break p000e0320;
 		ACCminus(22,100);
 		ACCwriteln(264);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// FEED _
	p000e0321:
	{
 		if (skipdoall('p000e0321')) break p000e0321;
 		if (in_response)
		{
			if (!CNDverb(81)) break p000e0321;
 		}
 		ACCwriteln(265);
		{}

	}

	// STAT SYLV
	p000e0322:
	{
 		if (skipdoall('p000e0322')) break p000e0322;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0322;
			if (!CNDnoun1(92)) break p000e0322;
 		}
		if (!CNDnotzero(0)) break p000e0322;
		if (!CNDnotzero(113)) break p000e0322;
		if (!CNDnotzero(116)) break p000e0322;
 		ACCwriteln(266);
 		ACCwriteln(267);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// STAT BILL
	p000e0323:
	{
 		if (skipdoall('p000e0323')) break p000e0323;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0323;
			if (!CNDnoun1(93)) break p000e0323;
 		}
		if (!CNDnotzero(0)) break p000e0323;
		if (!CNDnotzero(113)) break p000e0323;
		if (!CNDnotzero(115)) break p000e0323;
 		ACCwriteln(268);
 		ACCwriteln(269);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// STAT SAM
	p000e0324:
	{
 		if (skipdoall('p000e0324')) break p000e0324;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0324;
			if (!CNDnoun1(94)) break p000e0324;
 		}
		if (!CNDnotzero(0)) break p000e0324;
		if (!CNDnotzero(113)) break p000e0324;
		if (!CNDnotzero(114)) break p000e0324;
 		ACCwriteln(270);
 		ACCwriteln(271);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// STAT SUE
	p000e0325:
	{
 		if (skipdoall('p000e0325')) break p000e0325;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0325;
			if (!CNDnoun1(95)) break p000e0325;
 		}
		if (!CNDnotzero(0)) break p000e0325;
		if (!CNDnotzero(113)) break p000e0325;
 		ACCwriteln(272);
 		ACCwriteln(273);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// STAT DAVE
	p000e0326:
	{
 		if (skipdoall('p000e0326')) break p000e0326;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0326;
			if (!CNDnoun1(96)) break p000e0326;
 		}
		if (!CNDnotzero(0)) break p000e0326;
		if (!CNDnotzero(112)) break p000e0326;
		if (!CNDnotzero(113)) break p000e0326;
 		ACCwriteln(274);
 		ACCwriteln(275);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// STAT _
	p000e0327:
	{
 		if (skipdoall('p000e0327')) break p000e0327;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0327;
 		}
		if (!CNDeq(113,0)) break p000e0327;
 		ACCwriteln(276);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// STAT _
	p000e0328:
	{
 		if (skipdoall('p000e0328')) break p000e0328;
 		if (in_response)
		{
			if (!CNDverb(97)) break p000e0328;
 		}
		if (!CNDeq(0,0)) break p000e0328;
 		ACCwriteln(277);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV SYLV
	p000e0329:
	{
 		if (skipdoall('p000e0329')) break p000e0329;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0329;
			if (!CNDnoun1(92)) break p000e0329;
 		}
		if (!CNDnotat(70)) break p000e0329;
		if (!CNDnotzero(116)) break p000e0329;
 		ACCclear(116);
 		ACCcreate(5);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
 		ACCok();
		break pro000_restart;
		{}

	}

	// LEAV SYLV
	p000e0330:
	{
 		if (skipdoall('p000e0330')) break p000e0330;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0330;
			if (!CNDnoun1(92)) break p000e0330;
 		}
		if (!CNDat(70)) break p000e0330;
		if (!CNDnotzero(116)) break p000e0330;
 		ACCclear(116);
 		ACCwriteln(278);
 		ACCminus(17,1);
 		ACCplus(30,5);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV SYLV
	p000e0331:
	{
 		if (skipdoall('p000e0331')) break p000e0331;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0331;
			if (!CNDnoun1(92)) break p000e0331;
 		}
		if (!CNDeq(116,0)) break p000e0331;
 		ACCwriteln(279);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV BILL
	p000e0332:
	{
 		if (skipdoall('p000e0332')) break p000e0332;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0332;
			if (!CNDnoun1(93)) break p000e0332;
 		}
		if (!CNDnotat(70)) break p000e0332;
		if (!CNDnotzero(115)) break p000e0332;
 		ACCclear(115);
 		ACCcreate(4);
 		ACCminus(17,6);
 		ACCminus(18,3);
 		ACCminus(19,4);
 		ACCminus(21,6);
 		ACCok();
		break pro000_restart;
		{}

	}

	// LEAV BILL
	p000e0333:
	{
 		if (skipdoall('p000e0333')) break p000e0333;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0333;
			if (!CNDnoun1(93)) break p000e0333;
 		}
		if (!CNDat(70)) break p000e0333;
		if (!CNDnotzero(115)) break p000e0333;
 		ACCclear(115);
 		ACCwriteln(280);
 		ACCplus(30,20);
 		ACCminus(17,6);
 		ACCminus(18,3);
 		ACCminus(19,4);
 		ACCminus(21,6);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV BILL
	p000e0334:
	{
 		if (skipdoall('p000e0334')) break p000e0334;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0334;
			if (!CNDnoun1(93)) break p000e0334;
 		}
		if (!CNDeq(115,0)) break p000e0334;
 		ACCwriteln(281);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV SAM
	p000e0335:
	{
 		if (skipdoall('p000e0335')) break p000e0335;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0335;
			if (!CNDnoun1(94)) break p000e0335;
 		}
		if (!CNDnotat(70)) break p000e0335;
		if (!CNDnotzero(114)) break p000e0335;
 		ACCclear(114);
 		ACCcreate(3);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
 		ACCok();
		break pro000_restart;
		{}

	}

	// LEAV SAM
	p000e0336:
	{
 		if (skipdoall('p000e0336')) break p000e0336;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0336;
			if (!CNDnoun1(94)) break p000e0336;
 		}
		if (!CNDat(70)) break p000e0336;
		if (!CNDnotzero(114)) break p000e0336;
 		ACCclear(114);
 		ACCwriteln(282);
 		ACCplus(30,20);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV SAM
	p000e0337:
	{
 		if (skipdoall('p000e0337')) break p000e0337;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0337;
			if (!CNDnoun1(94)) break p000e0337;
 		}
		if (!CNDeq(114,0)) break p000e0337;
 		ACCwriteln(283);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV SUE
	p000e0338:
	{
 		if (skipdoall('p000e0338')) break p000e0338;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0338;
			if (!CNDnoun1(95)) break p000e0338;
 		}
		if (!CNDnotat(70)) break p000e0338;
		if (!CNDnotzero(113)) break p000e0338;
 		ACCclear(113);
 		ACCcreate(2);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
 		ACCok();
		break pro000_restart;
		{}

	}

	// LEAV SUE
	p000e0339:
	{
 		if (skipdoall('p000e0339')) break p000e0339;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0339;
			if (!CNDnoun1(95)) break p000e0339;
 		}
		if (!CNDat(70)) break p000e0339;
		if (!CNDnotzero(113)) break p000e0339;
 		ACCclear(113);
 		ACCwriteln(284);
 		ACCplus(30,20);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV SUE
	p000e0340:
	{
 		if (skipdoall('p000e0340')) break p000e0340;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0340;
			if (!CNDnoun1(95)) break p000e0340;
 		}
		if (!CNDeq(113,0)) break p000e0340;
 		ACCwriteln(285);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV DAVE
	p000e0341:
	{
 		if (skipdoall('p000e0341')) break p000e0341;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0341;
			if (!CNDnoun1(96)) break p000e0341;
 		}
		if (!CNDnotat(70)) break p000e0341;
		if (!CNDnotzero(112)) break p000e0341;
 		ACCclear(112);
 		ACCcreate(1);
 		ACCminus(17,2);
 		ACCminus(18,6);
 		ACCminus(19,1);
 		ACCminus(21,8);
 		ACCok();
		break pro000_restart;
		{}

	}

	// LEAV DAVE
	p000e0342:
	{
 		if (skipdoall('p000e0342')) break p000e0342;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0342;
			if (!CNDnoun1(96)) break p000e0342;
 		}
		if (!CNDat(70)) break p000e0342;
		if (!CNDnotzero(112)) break p000e0342;
 		ACCwriteln(286);
 		ACCclear(112);
 		ACCplus(30,20);
 		ACCminus(17,2);
 		ACCminus(18,6);
 		ACCminus(19,1);
 		ACCminus(21,8);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV DAVE
	p000e0343:
	{
 		if (skipdoall('p000e0343')) break p000e0343;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0343;
			if (!CNDnoun1(96)) break p000e0343;
 		}
		if (!CNDeq(112,0)) break p000e0343;
 		ACCwriteln(287);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// LEAV _
	p000e0344:
	{
 		if (skipdoall('p000e0344')) break p000e0344;
 		if (in_response)
		{
			if (!CNDverb(98)) break p000e0344;
 		}
 		ACCwriteln(288);
		{}

	}

	// ENLI SYLV
	p000e0345:
	{
 		if (skipdoall('p000e0345')) break p000e0345;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0345;
			if (!CNDnoun1(92)) break p000e0345;
 		}
		if (!CNDpresent(5)) break p000e0345;
 		ACCdestroy(5);
 		ACCset(116);
 		ACCplus(17,1);
 		ACCplus(18,1);
 		ACCplus(19,6);
 		ACCplus(21,1);
 		ACCok();
		break pro000_restart;
		{}

	}

	// ENLI SYLV
	p000e0346:
	{
 		if (skipdoall('p000e0346')) break p000e0346;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0346;
			if (!CNDnoun1(92)) break p000e0346;
 		}
		if (!CNDnotzero(116)) break p000e0346;
 		ACCwriteln(289);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI SYLV
	p000e0347:
	{
 		if (skipdoall('p000e0347')) break p000e0347;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0347;
			if (!CNDnoun1(92)) break p000e0347;
 		}
		if (!CNDpresent(10)) break p000e0347;
 		ACCwriteln(290);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI BILL
	p000e0348:
	{
 		if (skipdoall('p000e0348')) break p000e0348;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0348;
			if (!CNDnoun1(93)) break p000e0348;
 		}
		if (!CNDpresent(4)) break p000e0348;
 		ACCdestroy(4);
 		ACCset(115);
 		ACCplus(17,6);
 		ACCplus(18,3);
 		ACCplus(19,4);
 		ACCplus(21,6);
 		ACCok();
		break pro000_restart;
		{}

	}

	// ENLI BILL
	p000e0349:
	{
 		if (skipdoall('p000e0349')) break p000e0349;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0349;
			if (!CNDnoun1(93)) break p000e0349;
 		}
		if (!CNDnotzero(115)) break p000e0349;
 		ACCwriteln(291);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI BILL
	p000e0350:
	{
 		if (skipdoall('p000e0350')) break p000e0350;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0350;
			if (!CNDnoun1(93)) break p000e0350;
 		}
		if (!CNDpresent(9)) break p000e0350;
 		ACCwriteln(292);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI SAM
	p000e0351:
	{
 		if (skipdoall('p000e0351')) break p000e0351;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0351;
			if (!CNDnoun1(94)) break p000e0351;
 		}
		if (!CNDpresent(3)) break p000e0351;
 		ACCdestroy(3);
 		ACCset(114);
 		ACCplus(17,4);
 		ACCplus(18,4);
 		ACCplus(19,4);
 		ACCplus(21,4);
 		ACCok();
		break pro000_restart;
		{}

	}

	// ENLI SAM
	p000e0352:
	{
 		if (skipdoall('p000e0352')) break p000e0352;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0352;
			if (!CNDnoun1(94)) break p000e0352;
 		}
		if (!CNDnotzero(114)) break p000e0352;
 		ACCwriteln(293);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI SAM
	p000e0353:
	{
 		if (skipdoall('p000e0353')) break p000e0353;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0353;
			if (!CNDnoun1(94)) break p000e0353;
 		}
		if (!CNDpresent(8)) break p000e0353;
 		ACCwriteln(294);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI SUE
	p000e0354:
	{
 		if (skipdoall('p000e0354')) break p000e0354;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0354;
			if (!CNDnoun1(95)) break p000e0354;
 		}
		if (!CNDpresent(2)) break p000e0354;
 		ACCdestroy(2);
 		ACCset(113);
 		ACCplus(17,1);
 		ACCplus(18,2);
 		ACCplus(19,8);
 		ACCplus(21,3);
 		ACCok();
		break pro000_restart;
		{}

	}

	// ENLI SUE
	p000e0355:
	{
 		if (skipdoall('p000e0355')) break p000e0355;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0355;
			if (!CNDnoun1(95)) break p000e0355;
 		}
		if (!CNDnotzero(113)) break p000e0355;
 		ACCwriteln(295);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI SUE
	p000e0356:
	{
 		if (skipdoall('p000e0356')) break p000e0356;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0356;
			if (!CNDnoun1(95)) break p000e0356;
 		}
		if (!CNDpresent(7)) break p000e0356;
 		ACCwriteln(296);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI DAVE
	p000e0357:
	{
 		if (skipdoall('p000e0357')) break p000e0357;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0357;
			if (!CNDnoun1(96)) break p000e0357;
 		}
		if (!CNDpresent(1)) break p000e0357;
 		ACCdestroy(1);
 		ACCset(112);
 		ACCplus(17,2);
 		ACCplus(18,6);
 		ACCplus(19,1);
 		ACCplus(21,8);
 		ACCok();
		break pro000_restart;
		{}

	}

	// ENLI DAVE
	p000e0358:
	{
 		if (skipdoall('p000e0358')) break p000e0358;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0358;
			if (!CNDnoun1(96)) break p000e0358;
 		}
		if (!CNDnotzero(112)) break p000e0358;
 		ACCwriteln(297);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI DAVE
	p000e0359:
	{
 		if (skipdoall('p000e0359')) break p000e0359;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0359;
			if (!CNDnoun1(96)) break p000e0359;
 		}
		if (!CNDpresent(6)) break p000e0359;
 		ACCwriteln(298);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENLI _
	p000e0360:
	{
 		if (skipdoall('p000e0360')) break p000e0360;
 		if (in_response)
		{
			if (!CNDverb(99)) break p000e0360;
 		}
 		ACCwriteln(299);
		{}

	}

	// CARR TROL
	p000e0361:
	{
 		if (skipdoall('p000e0361')) break p000e0361;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0361;
			if (!CNDnoun1(45)) break p000e0361;
 		}
		if (!CNDpresent(0)) break p000e0361;
 		ACCget(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR TROL
	p000e0362:
	{
 		if (skipdoall('p000e0362')) break p000e0362;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0362;
			if (!CNDnoun1(45)) break p000e0362;
 		}
		if (!CNDpresent(37)) break p000e0362;
 		ACCget(37);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR TROL
	p000e0363:
	{
 		if (skipdoall('p000e0363')) break p000e0363;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0363;
			if (!CNDnoun1(45)) break p000e0363;
 		}
		if (!CNDpresent(38)) break p000e0363;
 		ACCwriteln(300);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR CAN
	p000e0364:
	{
 		if (skipdoall('p000e0364')) break p000e0364;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0364;
			if (!CNDnoun1(46)) break p000e0364;
 		}
		if (!CNDpresent(35)) break p000e0364;
 		ACCget(35);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR CAN
	p000e0365:
	{
 		if (skipdoall('p000e0365')) break p000e0365;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0365;
			if (!CNDnoun1(46)) break p000e0365;
 		}
		if (!CNDpresent(36)) break p000e0365;
 		ACCget(36);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR BIBL
	p000e0366:
	{
 		if (skipdoall('p000e0366')) break p000e0366;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0366;
			if (!CNDnoun1(48)) break p000e0366;
 		}
		if (!CNDat(48)) break p000e0366;
 		ACCwriteln(301);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR COIL
	p000e0367:
	{
 		if (skipdoall('p000e0367')) break p000e0367;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0367;
			if (!CNDnoun1(68)) break p000e0367;
 		}
		if (!CNDpresent(27)) break p000e0367;
 		ACCget(27);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR COIL
	p000e0368:
	{
 		if (skipdoall('p000e0368')) break p000e0368;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0368;
			if (!CNDnoun1(68)) break p000e0368;
 		}
		if (!CNDpresent(28)) break p000e0368;
 		ACCswap(28,27);
 		ACCget(27);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR BICY
	p000e0369:
	{
 		if (skipdoall('p000e0369')) break p000e0369;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0369;
			if (!CNDnoun1(79)) break p000e0369;
 		}
		if (!CNDpresent(23)) break p000e0369;
 		ACCget(23);
		if (!success) break pro000_restart;
 		ACCswap(23,24);
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR FEE
	p000e0370:
	{
 		if (skipdoall('p000e0370')) break p000e0370;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0370;
			if (!CNDnoun1(81)) break p000e0370;
 		}
 		ACCget(23);
		if (!success) break pro000_restart;
 		ACCswap(23,24);
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR COMP
	p000e0371:
	{
 		if (skipdoall('p000e0371')) break p000e0371;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0371;
			if (!CNDnoun1(84)) break p000e0371;
 		}
		if (!CNDpresent(18)) break p000e0371;
		if (!CNDlt(21,19)) break p000e0371;
 		ACCwriteln(302);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR COMP
	p000e0372:
	{
 		if (skipdoall('p000e0372')) break p000e0372;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0372;
			if (!CNDnoun1(84)) break p000e0372;
 		}
		if (!CNDpresent(37)) break p000e0372;
 		ACCwriteln(303);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR COMP
	p000e0373:
	{
 		if (skipdoall('p000e0373')) break p000e0373;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0373;
			if (!CNDnoun1(84)) break p000e0373;
 		}
		if (!CNDpresent(18)) break p000e0373;
		if (!CNDgt(21,18)) break p000e0373;
		if (!CNDabsent(0)) break p000e0373;
 		ACCwriteln(304);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR COMP
	p000e0374:
	{
 		if (skipdoall('p000e0374')) break p000e0374;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0374;
			if (!CNDnoun1(84)) break p000e0374;
 		}
		if (!CNDpresent(18)) break p000e0374;
		if (!CNDpresent(0)) break p000e0374;
		if (!CNDgt(21,18)) break p000e0374;
 		ACCdestroy(18);
 		ACCswap(0,37);
 		ACCwriteln(305);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR COMP
	p000e0375:
	{
 		if (skipdoall('p000e0375')) break p000e0375;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0375;
			if (!CNDnoun1(84)) break p000e0375;
 		}
		if (!CNDpresent(38)) break p000e0375;
 		ACCwriteln(306);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR CASE
	p000e0376:
	{
 		if (skipdoall('p000e0376')) break p000e0376;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0376;
			if (!CNDnoun1(88)) break p000e0376;
 		}
		if (!CNDat(32)) break p000e0376;
 		ACCget(14);
		if (!success) break pro000_restart;
 		ACCminus(8,2);
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR CASE
	p000e0377:
	{
 		if (skipdoall('p000e0377')) break p000e0377;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0377;
			if (!CNDnoun1(88)) break p000e0377;
 		}
		if (!CNDnotat(32)) break p000e0377;
 		ACCget(14);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR SUE
	p000e0378:
	{
 		if (skipdoall('p000e0378')) break p000e0378;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0378;
			if (!CNDnoun1(95)) break p000e0378;
 		}
		if (!CNDpresent(29)) break p000e0378;
 		ACCget(29);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR SUE
	p000e0379:
	{
 		if (skipdoall('p000e0379')) break p000e0379;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0379;
			if (!CNDnoun1(95)) break p000e0379;
 		}
		if (!CNDabsent(29)) break p000e0379;
 		ACCwriteln(307);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CARR _
	p000e0380:
	{
 		if (skipdoall('p000e0380')) break p000e0380;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0380;
 		}
 		ACCautog();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// CARR _
	p000e0381:
	{
 		if (skipdoall('p000e0381')) break p000e0381;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0381;
 		}
 		ACCwriteln(308);
		{}

	}

	// DR TROL
	p000e0382:
	{
 		if (skipdoall('p000e0382')) break p000e0382;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0382;
			if (!CNDnoun1(45)) break p000e0382;
 		}
		if (!CNDpresent(0)) break p000e0382;
 		ACCdrop(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR TROL
	p000e0383:
	{
 		if (skipdoall('p000e0383')) break p000e0383;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0383;
			if (!CNDnoun1(45)) break p000e0383;
 		}
		if (!CNDpresent(37)) break p000e0383;
 		ACCdrop(37);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR CAN
	p000e0384:
	{
 		if (skipdoall('p000e0384')) break p000e0384;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0384;
			if (!CNDnoun1(46)) break p000e0384;
 		}
		if (!CNDcarried(35)) break p000e0384;
 		ACCdrop(35);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR CAN
	p000e0385:
	{
 		if (skipdoall('p000e0385')) break p000e0385;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0385;
			if (!CNDnoun1(46)) break p000e0385;
 		}
		if (!CNDcarried(36)) break p000e0385;
 		ACCdrop(36);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR COIL
	p000e0386:
	{
 		if (skipdoall('p000e0386')) break p000e0386;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0386;
			if (!CNDnoun1(68)) break p000e0386;
 		}
 		ACCdrop(27);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR BICY
	p000e0387:
	{
 		if (skipdoall('p000e0387')) break p000e0387;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0387;
			if (!CNDnoun1(79)) break p000e0387;
 		}
		if (!CNDcarried(24)) break p000e0387;
 		ACCswap(24,23);
 		ACCdrop(23);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR FEE
	p000e0388:
	{
 		if (skipdoall('p000e0388')) break p000e0388;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0388;
			if (!CNDnoun1(81)) break p000e0388;
 		}
		if (!CNDcarried(24)) break p000e0388;
 		ACCswap(24,23);
 		ACCdrop(23);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR COMP
	p000e0389:
	{
 		if (skipdoall('p000e0389')) break p000e0389;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0389;
			if (!CNDnoun1(84)) break p000e0389;
 		}
 		ACCdrop(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR BACT
	p000e0390:
	{
 		if (skipdoall('p000e0390')) break p000e0390;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0390;
			if (!CNDnoun1(85)) break p000e0390;
 		}
 		ACCwriteln(309);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DR CASE
	p000e0391:
	{
 		if (skipdoall('p000e0391')) break p000e0391;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0391;
			if (!CNDnoun1(88)) break p000e0391;
 		}
 		ACCdrop(14);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR SUE
	p000e0392:
	{
 		if (skipdoall('p000e0392')) break p000e0392;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0392;
			if (!CNDnoun1(95)) break p000e0392;
 		}
 		ACCdrop(29);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR _
	p000e0393:
	{
 		if (skipdoall('p000e0393')) break p000e0393;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0393;
 		}
 		ACCautod();
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DR _
	p000e0394:
	{
 		if (skipdoall('p000e0394')) break p000e0394;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0394;
 		}
 		ACCwriteln(310);
		{}

	}

	// REMO BOOT
	p000e0395:
	{
 		if (skipdoall('p000e0395')) break p000e0395;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0395;
			if (!CNDnoun1(55)) break p000e0395;
 		}
 		ACCremove(31);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO BOLT
	p000e0396:
	{
 		if (skipdoall('p000e0396')) break p000e0396;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0396;
			if (!CNDnoun1(64)) break p000e0396;
 		}
		if (!CNDat(20)) break p000e0396;
		if (!CNDcarried(26)) break p000e0396;
		if (!CNDpresent(30)) break p000e0396;
		if (!CNDgt(21,7)) break p000e0396;
 		ACCdestroy(30);
 		ACCwriteln(311);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO BOLT
	p000e0397:
	{
 		if (skipdoall('p000e0397')) break p000e0397;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0397;
			if (!CNDnoun1(64)) break p000e0397;
 		}
		if (!CNDat(20)) break p000e0397;
		if (!CNDpresent(30)) break p000e0397;
		if (!CNDlt(21,8)) break p000e0397;
 		ACCwriteln(312);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// REMO ELEC
	p000e0398:
	{
 		if (skipdoall('p000e0398')) break p000e0398;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0398;
			if (!CNDnoun1(83)) break p000e0398;
 		}
		if (!CNDpresent(38)) break p000e0398;
 		ACCdestroy(38);
 		ACCcreate(37);
 		ACCcreate(19);
 		ACCget(19);
		if (!success) break pro000_restart;
 		ACCclear(6);
 		ACCclear(5);
 		ACCclear(3);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// REMO GLOV
	p000e0399:
	{
 		if (skipdoall('p000e0399')) break p000e0399;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0399;
			if (!CNDnoun1(90)) break p000e0399;
 		}
 		ACCremove(11);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// REMO _
	p000e0400:
	{
 		if (skipdoall('p000e0400')) break p000e0400;
 		if (in_response)
		{
			if (!CNDverb(102)) break p000e0400;
 		}
 		ACCwriteln(313);
		{}

	}

	// WEAR BOOT
	p000e0401:
	{
 		if (skipdoall('p000e0401')) break p000e0401;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0401;
			if (!CNDnoun1(55)) break p000e0401;
 		}
 		ACCwear(31);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// WEAR GLOV
	p000e0402:
	{
 		if (skipdoall('p000e0402')) break p000e0402;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0402;
			if (!CNDnoun1(90)) break p000e0402;
 		}
 		ACCwear(11);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0403:
	{
 		if (skipdoall('p000e0403')) break p000e0403;
 		ACChook(314);
		if (done_flag) break pro000_restart;
		{}

	}

	// WEAR _
	p000e0404:
	{
 		if (skipdoall('p000e0404')) break p000e0404;
 		if (in_response)
		{
			if (!CNDverb(103)) break p000e0404;
 		}
 		ACCwriteln(315);
		{}

	}

	// I _
	p000e0405:
	{
 		if (skipdoall('p000e0405')) break p000e0405;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0405;
 		}
 		ACCwriteln(316);
 		ACCinven();
		break pro000_restart;
		{}

	}

	// L _
	p000e0406:
	{
 		if (skipdoall('p000e0406')) break p000e0406;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0406;
 		}
 		ACClet(2,101);
 		ACCset(29);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// Q _
	p000e0407:
	{
 		if (skipdoall('p000e0407')) break p000e0407;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0407;
 		}
		if (!CNDlt(30,80)) break p000e0407;
 		ACCquit();
 		function anykey00021() 
		{
 		ACCscore();
 		ACCend();
		return;
		}
 		waitKey(anykey00021);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// Q _
	p000e0408:
	{
 		if (skipdoall('p000e0408')) break p000e0408;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0408;
 		}
		if (!CNDeq(30,80)) break p000e0408;
 		ACCwriteln(317);
 		ACCscore();
 		ACCend();
		break pro000_restart;
		{}

	}

	// SAVE _
	p000e0409:
	{
 		if (skipdoall('p000e0409')) break p000e0409;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0409;
 		}
 		ACClet(2,101);
 		ACCsave();
		break pro000_restart;
		{}

	}

	// LOAD _
	p000e0410:
	{
 		if (skipdoall('p000e0410')) break p000e0410;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0410;
 		}
 		ACClet(2,101);
 		ACCload();
		break pro000_restart;
		{}

	}

	// WORD _
	p000e0411:
	{
 		if (skipdoall('p000e0411')) break p000e0411;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0411;
 		}
 		ACClet(28,19);
 		ACCpause(255);
 		function anykey00022() 
		{
 		ACCwriteln(318);
 		ACCdone();
		return;
		}
 		waitKey(anykey00022);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// PICT _
	p000e0412:
	{
 		if (skipdoall('p000e0412')) break p000e0412;
 		if (in_response)
		{
			if (!CNDverb(110)) break p000e0412;
 		}
 		ACClet(28,19);
 		ACCpause(19);
 		function anykey00023() 
		{
 		ACCwriteln(319);
 		ACCset(29);
 		ACCdone();
		return;
		}
 		waitKey(anykey00023);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// STOR _
	p000e0413:
	{
 		if (skipdoall('p000e0413')) break p000e0413;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0413;
 		}
 		ACClet(28,21);
 		ACCpause(21);
 		function anykey00024() 
		{
 		ACCok();
		return;
		}
 		waitKey(anykey00024);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CALL _
	p000e0414:
	{
 		if (skipdoall('p000e0414')) break p000e0414;
 		if (in_response)
		{
			if (!CNDverb(112)) break p000e0414;
 		}
 		ACClet(28,21);
 		ACCpause(50);
 		function anykey00025() 
		{
 		ACCok();
		return;
		}
 		waitKey(anykey00025);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// VERS _
	p000e0415:
	{
 		if (skipdoall('p000e0415')) break p000e0415;
 		if (in_response)
		{
			if (!CNDverb(203)) break p000e0415;
 		}
 		ACCwriteln(320);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0416:
	{
 		if (skipdoall('p000e0416')) break p000e0416;
 		ACChook(321);
		if (done_flag) break pro000_restart;
		{}

	}


}
}

function pro001()
{
process_restart=true;
pro001_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p001e0000:
	{
 		if (skipdoall('p001e0000')) break p001e0000;
 		ACChook(322);
		if (done_flag) break pro001_restart;
		{}

	}

	// _ _
	p001e0001:
	{
 		if (skipdoall('p001e0001')) break p001e0001;
		if (!CNDat(0)) break p001e0001;
 		ACCbclear(12,5);
		{}

	}

	// _ _
	p001e0002:
	{
 		if (skipdoall('p001e0002')) break p001e0002;
		if (!CNDislight()) break p001e0002;
 		ACClistobj();
 		ACClistnpc(getFlag(38));
		{}

	}


}
}

function pro002()
{
process_restart=true;
pro002_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p002e0000:
	{
 		if (skipdoall('p002e0000')) break p002e0000;
		if (!CNDeq(31,0)) break p002e0000;
		if (!CNDeq(32,0)) break p002e0000;
 		ACCability(254,254);
		{}

	}

	// _ _
	p002e0001:
	{
 		if (skipdoall('p002e0001')) break p002e0001;
 		ACChook(323);
		if (done_flag) break pro002_restart;
		{}

	}

	// _ _
	p002e0002:
	{
 		if (skipdoall('p002e0002')) break p002e0002;
		if (!CNDlt(8,6)) break p002e0002;
		if (!CNDgt(8,0)) break p002e0002;
 		ACCwriteln(324);
		{}

	}

	// _ _
	p002e0003:
	{
 		if (skipdoall('p002e0003')) break p002e0003;
		if (!CNDat(32)) break p002e0003;
		if (!CNDlt(8,1)) break p002e0003;
		if (!CNDabsent(33)) break p002e0003;
 		ACCcreate(33);
 		ACCwriteln(325);
		{}

	}

	// _ _
	p002e0004:
	{
 		if (skipdoall('p002e0004')) break p002e0004;
		if (!CNDat(31)) break p002e0004;
		if (!CNDeq(8,1)) break p002e0004;
 		ACCwriteln(326);
 		ACCcreate(33);
		{}

	}

	// _ _
	p002e0005:
	{
 		if (skipdoall('p002e0005')) break p002e0005;
		if (!CNDat(32)) break p002e0005;
		if (!CNDnotzero(112)) break p002e0005;
		if (!CNDlt(8,5)) break p002e0005;
		if (!CNDgt(8,0)) break p002e0005;
 		ACCwriteln(327);
		{}

	}

	// _ _
	p002e0006:
	{
 		if (skipdoall('p002e0006')) break p002e0006;
		if (!CNDnotzero(31)) break p002e0006;
 		ACCminus(31,1);
		{}

	}

	// _ _
	p002e0007:
	{
 		if (skipdoall('p002e0007')) break p002e0007;
		if (!CNDat(0)) break p002e0007;
 		ACClet(27,12);
		{}

	}

	// _ _
	p002e0008:
	{
 		if (skipdoall('p002e0008')) break p002e0008;
		if (!CNDatgt(0)) break p002e0008;
 		ACCplus(111,1);
		{}

	}

	// _ _
	p002e0009:
	{
 		if (skipdoall('p002e0009')) break p002e0009;
		if (!CNDgt(111,95)) break p002e0009;
		if (!CNDlt(111,100)) break p002e0009;
 		ACCwriteln(328);
		{}

	}

	// _ _
	p002e0010:
	{
 		if (skipdoall('p002e0010')) break p002e0010;
		if (!CNDeq(111,100)) break p002e0010;
 		ACCwriteln(329);
 		ACCset(0);
		{}

	}

	// _ _
	p002e0011:
	{
 		if (skipdoall('p002e0011')) break p002e0011;
		if (!CNDeq(0,0)) break p002e0011;
		if (!CNDgt(111,100)) break p002e0011;
		if (!CNDlt(111,111)) break p002e0011;
 		ACCset(0);
 		ACCwriteln(330);
		{}

	}

	// _ _
	p002e0012:
	{
 		if (skipdoall('p002e0012')) break p002e0012;
		if (!CNDgt(111,111)) break p002e0012;
		if (!CNDlt(111,116)) break p002e0012;
 		ACCwriteln(331);
		{}

	}

	// _ _
	p002e0013:
	{
 		if (skipdoall('p002e0013')) break p002e0013;
		if (!CNDnotzero(0)) break p002e0013;
		if (!CNDgt(111,115)) break p002e0013;
 		ACCwriteln(332);
 		ACCclear(0);
 		ACCpause(100);
 		function anykey00026() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00026);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0014:
	{
 		if (skipdoall('p002e0014')) break p002e0014;
		if (!CNDnotzero(0)) break p002e0014;
		if (!CNDgt(111,117)) break p002e0014;
 		ACClet(111,0);
 		ACCclear(0);
		{}

	}

	// _ _
	p002e0015:
	{
 		if (skipdoall('p002e0015')) break p002e0015;
		if (!CNDlt(111,112)) break p002e0015;
		if (!CNDnotzero(0)) break p002e0015;
 		ACCwriteln(333);
		{}

	}

	// _ _
	p002e0016:
	{
 		if (skipdoall('p002e0016')) break p002e0016;
		if (!CNDat(11)) break p002e0016;
		if (!CNDlt(32,36)) break p002e0016;
 		ACClet(32,200);
		{}

	}

	// _ _
	p002e0017:
	{
 		if (skipdoall('p002e0017')) break p002e0017;
		if (!CNDat(10)) break p002e0017;
		if (!CNDeq(32,200)) break p002e0017;
 		ACCminus(32,165);
		{}

	}

	// _ _
	p002e0018:
	{
 		if (skipdoall('p002e0018')) break p002e0018;
		if (!CNDeq(32,200)) break p002e0018;
		if (!CNDchance(10)) break p002e0018;
 		ACCwriteln(334);
		{}

	}

	// _ _
	p002e0019:
	{
 		if (skipdoall('p002e0019')) break p002e0019;
		if (!CNDat(76)) break p002e0019;
 		ACCplus(32,1);
		{}

	}

	// _ _
	p002e0020:
	{
 		if (skipdoall('p002e0020')) break p002e0020;
		if (!CNDat(77)) break p002e0020;
 		ACCplus(32,1);
		{}

	}

	// _ _
	p002e0021:
	{
 		if (skipdoall('p002e0021')) break p002e0021;
		if (!CNDeq(32,205)) break p002e0021;
 		ACCwriteln(335);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0022:
	{
 		if (skipdoall('p002e0022')) break p002e0022;
		if (!CNDat(19)) break p002e0022;
 		ACClet(32,200);
		{}

	}

	// _ _
	p002e0023:
	{
 		if (skipdoall('p002e0023')) break p002e0023;
		if (!CNDgt(32,200)) break p002e0023;
 		ACCwriteln(336);
		{}

	}

	// _ _
	p002e0024:
	{
 		if (skipdoall('p002e0024')) break p002e0024;
		if (!CNDat(6)) break p002e0024;
		if (!CNDcarried(29)) break p002e0024;
 		ACCdestroy(29);
 		ACCcreate(2);
 		ACCwriteln(337);
		{}

	}

	// _ _
	p002e0025:
	{
 		if (skipdoall('p002e0025')) break p002e0025;
		if (!CNDgt(111,117)) break p002e0025;
 		ACCclear(0);
 		ACClet(111,1);
		{}

	}

	// _ _
	p002e0026:
	{
 		if (skipdoall('p002e0026')) break p002e0026;
		if (!CNDnotat(0)) break p002e0026;
		if (!CNDeq(111,0)) break p002e0026;
 		ACCwriteln(338);
		{}

	}

	// _ _
	p002e0027:
	{
 		if (skipdoall('p002e0027')) break p002e0027;
		if (!CNDat(0)) break p002e0027;
 		ACClet(27,110);
		{}

	}

	// _ _
	p002e0028:
	{
 		if (skipdoall('p002e0028')) break p002e0028;
		if (!CNDlt(27,115)) break p002e0028;
 		ACCminus(27,1);
		{}

	}

	// _ _
	p002e0029:
	{
 		if (skipdoall('p002e0029')) break p002e0029;
		if (!CNDnotat(0)) break p002e0029;
		if (!CNDlt(27,110)) break p002e0029;
 		ACCwriteln(339);
		{}

	}

	// _ _
	p002e0030:
	{
 		if (skipdoall('p002e0030')) break p002e0030;
		if (!CNDeq(27,106)) break p002e0030;
 		ACCwriteln(340);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0031:
	{
 		if (skipdoall('p002e0031')) break p002e0031;
		if (!CNDat(23)) break p002e0031;
		if (!CNDnotworn(11)) break p002e0031;
 		ACCwriteln(341);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0032:
	{
 		if (skipdoall('p002e0032')) break p002e0032;
		if (!CNDat(23)) break p002e0032;
		if (!CNDnotzero(113)) break p002e0032;
 		ACCwriteln(342);
 		ACCclear(113);
		{}

	}

	// _ _
	p002e0033:
	{
 		if (skipdoall('p002e0033')) break p002e0033;
		if (!CNDat(23)) break p002e0033;
		if (!CNDnotzero(112)) break p002e0033;
 		ACCwriteln(343);
 		ACCclear(112);
 		ACCminus(17,2);
 		ACCminus(18,6);
 		ACCminus(19,1);
 		ACCminus(21,8);
		{}

	}

	// _ _
	p002e0034:
	{
 		if (skipdoall('p002e0034')) break p002e0034;
		if (!CNDat(23)) break p002e0034;
		if (!CNDnotzero(113)) break p002e0034;
 		ACCwriteln(344);
 		ACCclear(113);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
		{}

	}

	// _ _
	p002e0035:
	{
 		if (skipdoall('p002e0035')) break p002e0035;
		if (!CNDat(23)) break p002e0035;
		if (!CNDnotzero(114)) break p002e0035;
 		ACCwriteln(345);
 		ACCclear(114);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
		{}

	}

	// _ _
	p002e0036:
	{
 		if (skipdoall('p002e0036')) break p002e0036;
		if (!CNDat(23)) break p002e0036;
		if (!CNDnotzero(115)) break p002e0036;
 		ACCclear(115);
 		ACCwriteln(346);
 		ACCminus(17,6);
 		ACCminus(18,3);
 		ACCminus(19,4);
 		ACCminus(21,6);
		{}

	}

	// _ _
	p002e0037:
	{
 		if (skipdoall('p002e0037')) break p002e0037;
		if (!CNDat(23)) break p002e0037;
		if (!CNDnotzero(116)) break p002e0037;
 		ACCwriteln(347);
 		ACCclear(116);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
		{}

	}

	// _ _
	p002e0038:
	{
 		if (skipdoall('p002e0038')) break p002e0038;
		if (!CNDat(75)) break p002e0038;
 		ACCplus(32,1);
		{}

	}

	// _ _
	p002e0039:
	{
 		if (skipdoall('p002e0039')) break p002e0039;
		if (!CNDeq(30,100)) break p002e0039;
 		ACCwriteln(348);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0040:
	{
 		if (skipdoall('p002e0040')) break p002e0040;
		if (!CNDeq(7,100)) break p002e0040;
		if (!CNDeq(4,100)) break p002e0040;
		if (!CNDeq(8,100)) break p002e0040;
 		ACCplus(30,15);
 		ACCminus(7,1);
 		ACCwriteln(349);
 		ACClet(5,3);
		{}

	}

	// _ _
	p002e0041:
	{
 		if (skipdoall('p002e0041')) break p002e0041;
		if (!CNDnotzero(0)) break p002e0041;
		if (!CNDnotzero(4)) break p002e0041;
		if (!CNDabsent(0)) break p002e0041;
 		ACCplus(4,1);
		{}

	}

	// _ _
	p002e0042:
	{
 		if (skipdoall('p002e0042')) break p002e0042;
		if (!CNDlt(5,195)) break p002e0042;
		if (!CNDgt(5,20)) break p002e0042;
 		ACCwriteln(350);
 		ACCdestroy(40);
 		ACCclear(5);
		{}

	}

	// _ _
	p002e0043:
	{
 		if (skipdoall('p002e0043')) break p002e0043;
		if (!CNDeq(5,9)) break p002e0043;
 		ACCwriteln(351);
		{}

	}

	// _ _
	p002e0044:
	{
 		if (skipdoall('p002e0044')) break p002e0044;
		if (!CNDeq(5,4)) break p002e0044;
 		ACCwriteln(352);
		{}

	}

	// _ _
	p002e0045:
	{
 		if (skipdoall('p002e0045')) break p002e0045;
		if (!CNDeq(5,2)) break p002e0045;
 		ACCwriteln(353);
		{}

	}

	// _ _
	p002e0046:
	{
 		if (skipdoall('p002e0046')) break p002e0046;
		if (!CNDeq(5,1)) break p002e0046;
 		ACCwriteln(354);
 		ACCdestroy(38);
 		ACClet(6,3);
 		ACCclear(3);
 		ACCclear(5);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0047:
	{
 		if (skipdoall('p002e0047')) break p002e0047;
		if (!CNDgt(6,60)) break p002e0047;
 		ACCminus(6,5);
		{}

	}

	// _ _
	p002e0048:
	{
 		if (skipdoall('p002e0048')) break p002e0048;
		if (!CNDeq(6,60)) break p002e0048;
 		ACClet(6,30);
		{}

	}

	// _ _
	p002e0049:
	{
 		if (skipdoall('p002e0049')) break p002e0049;
		if (!CNDpresent(40)) break p002e0049;
		if (!CNDlt(5,201)) break p002e0049;
		if (!CNDgt(5,194)) break p002e0049;
 		ACCwriteln(355);
		{}

	}

	// _ _
	p002e0050:
	{
 		if (skipdoall('p002e0050')) break p002e0050;
		if (!CNDat(79)) break p002e0050;
		if (!CNDeq(5,195)) break p002e0050;
 		ACCwriteln(356);
 		ACCwriteln(357);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0051:
	{
 		if (skipdoall('p002e0051')) break p002e0051;
		if (!CNDeq(5,195)) break p002e0051;
 		ACCwriteln(358);
 		ACCdestroy(40);
 		ACCclear(5);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0052:
	{
 		if (skipdoall('p002e0052')) break p002e0052;
		if (!CNDeq(112,0)) break p002e0052;
		if (!CNDpresent(40)) break p002e0052;
 		ACCwriteln(359);
		{}

	}

	// _ _
	p002e0053:
	{
 		if (skipdoall('p002e0053')) break p002e0053;
		if (!CNDat(1)) break p002e0053;
		if (!CNDeq(2,100)) break p002e0053;
		if (!CNDabsent(40)) break p002e0053;
 		ACCwriteln(360);
 		ACCminus(2,1);
		{}

	}

	// _ _
	p002e0054:
	{
 		if (skipdoall('p002e0054')) break p002e0054;
		if (!CNDgt(21,21)) break p002e0054;
		if (!CNDnotzero(112)) break p002e0054;
		if (!CNDchance(1)) break p002e0054;
 		ACCclear(112);
 		ACCcreate(1);
 		ACCminus(21,8);
 		ACCminus(17,2);
 		ACCminus(18,6);
 		ACCminus(19,1);
 		ACCwriteln(361);
		{}

	}

	// _ _
	p002e0055:
	{
 		if (skipdoall('p002e0055')) break p002e0055;
		if (!CNDgt(21,21)) break p002e0055;
		if (!CNDnotzero(113)) break p002e0055;
		if (!CNDchance(10)) break p002e0055;
 		ACCclear(113);
 		ACCcreate(2);
 		ACCminus(21,3);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCwriteln(362);
		{}

	}

	// _ _
	p002e0056:
	{
 		if (skipdoall('p002e0056')) break p002e0056;
		if (!CNDgt(21,21)) break p002e0056;
		if (!CNDnotzero(114)) break p002e0056;
		if (!CNDchance(5)) break p002e0056;
 		ACCclear(114);
 		ACCcreate(3);
 		ACCminus(21,4);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCwriteln(363);
		{}

	}

	// _ _
	p002e0057:
	{
 		if (skipdoall('p002e0057')) break p002e0057;
		if (!CNDgt(21,21)) break p002e0057;
		if (!CNDnotzero(115)) break p002e0057;
		if (!CNDchance(1)) break p002e0057;
 		ACCclear(115);
 		ACCcreate(4);
 		ACCminus(21,6);
 		ACCminus(17,6);
 		ACCminus(18,3);
 		ACCminus(19,4);
 		ACCwriteln(364);
		{}

	}

	// _ _
	p002e0058:
	{
 		if (skipdoall('p002e0058')) break p002e0058;
		if (!CNDgt(21,21)) break p002e0058;
		if (!CNDnotzero(116)) break p002e0058;
		if (!CNDchance(20)) break p002e0058;
 		ACCclear(116);
 		ACCcreate(5);
 		ACCminus(21,1);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCwriteln(365);
		{}

	}

	// _ _
	p002e0059:
	{
 		if (skipdoall('p002e0059')) break p002e0059;
		if (!CNDat(34)) break p002e0059;
		if (!CNDeq(31,1)) break p002e0059;
 		ACCcreate(32);
 		ACCwriteln(366);
 		ACCplus(31,1);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0060:
	{
 		if (skipdoall('p002e0060')) break p002e0060;
		if (!CNDnotzero(0)) break p002e0060;
		if (!CNDeq(9,10)) break p002e0060;
 		ACCplus(9,1);
		{}

	}

	// _ _
	p002e0061:
	{
 		if (skipdoall('p002e0061')) break p002e0061;
		if (!CNDnotzero(0)) break p002e0061;
		if (!CNDeq(9,20)) break p002e0061;
 		ACCplus(9,1);
		{}

	}

	// _ _
	p002e0062:
	{
 		if (skipdoall('p002e0062')) break p002e0062;
		if (!CNDat(52)) break p002e0062;
		if (!CNDpresent(20)) break p002e0062;
		if (!CNDeq(2,100)) break p002e0062;
 		ACCwriteln(367);
 		ACCminus(2,1);
		{}

	}

	// _ _
	p002e0063:
	{
 		if (skipdoall('p002e0063')) break p002e0063;
		if (!CNDnotzero(3)) break p002e0063;
		if (!CNDnotzero(0)) break p002e0063;
 		ACCplus(3,1);
		{}

	}

	// _ _
	p002e0064:
	{
 		if (skipdoall('p002e0064')) break p002e0064;
		if (!CNDat(68)) break p002e0064;
		if (!CNDpresent(39)) break p002e0064;
		if (!CNDpresent(17)) break p002e0064;
 		ACCdestroy(39);
 		ACCwriteln(368);
		{}

	}

	// _ _
	p002e0065:
	{
 		if (skipdoall('p002e0065')) break p002e0065;
		if (!CNDatgt(0)) break p002e0065;
		if (!CNDnotzero(112)) break p002e0065;
 		ACCplus(22,1);
		{}

	}

	// _ _
	p002e0066:
	{
 		if (skipdoall('p002e0066')) break p002e0066;
		if (!CNDatgt(0)) break p002e0066;
		if (!CNDnotzero(113)) break p002e0066;
 		ACCplus(23,1);
		{}

	}

	// _ _
	p002e0067:
	{
 		if (skipdoall('p002e0067')) break p002e0067;
		if (!CNDatgt(0)) break p002e0067;
		if (!CNDnotzero(114)) break p002e0067;
 		ACCplus(24,1);
		{}

	}

	// _ _
	p002e0068:
	{
 		if (skipdoall('p002e0068')) break p002e0068;
		if (!CNDatgt(0)) break p002e0068;
		if (!CNDnotzero(115)) break p002e0068;
 		ACCplus(25,1);
		{}

	}

	// _ _
	p002e0069:
	{
 		if (skipdoall('p002e0069')) break p002e0069;
		if (!CNDatgt(0)) break p002e0069;
		if (!CNDnotzero(116)) break p002e0069;
 		ACCplus(26,1);
		{}

	}

	// _ _
	p002e0070:
	{
 		if (skipdoall('p002e0070')) break p002e0070;
		if (!CNDnotzero(112)) break p002e0070;
		if (!CNDgt(22,80)) break p002e0070;
 		ACCwriteln(369);
		{}

	}

	// _ _
	p002e0071:
	{
 		if (skipdoall('p002e0071')) break p002e0071;
		if (!CNDnotzero(112)) break p002e0071;
		if (!CNDgt(22,96)) break p002e0071;
 		ACCwriteln(370);
 		ACCclear(112);
 		ACCcreate(6);
 		ACCclear(22);
 		ACCminus(17,2);
 		ACCminus(18,6);
 		ACCminus(19,1);
 		ACCminus(21,8);
		{}

	}

	// _ _
	p002e0072:
	{
 		if (skipdoall('p002e0072')) break p002e0072;
		if (!CNDnotzero(113)) break p002e0072;
		if (!CNDgt(23,40)) break p002e0072;
 		ACCwriteln(371);
		{}

	}

	// _ _
	p002e0073:
	{
 		if (skipdoall('p002e0073')) break p002e0073;
		if (!CNDnotzero(113)) break p002e0073;
		if (!CNDgt(23,51)) break p002e0073;
 		ACCwriteln(372);
 		ACCclear(113);
 		ACCcreate(7);
 		ACCclear(23);
 		ACCminus(17,1);
 		ACCminus(18,2);
 		ACCminus(19,8);
 		ACCminus(21,3);
		{}

	}

	// _ _
	p002e0074:
	{
 		if (skipdoall('p002e0074')) break p002e0074;
		if (!CNDnotzero(114)) break p002e0074;
		if (!CNDgt(24,60)) break p002e0074;
 		ACCwriteln(373);
		{}

	}

	// _ _
	p002e0075:
	{
 		if (skipdoall('p002e0075')) break p002e0075;
		if (!CNDnotzero(114)) break p002e0075;
		if (!CNDgt(24,71)) break p002e0075;
 		ACCwriteln(374);
 		ACCclear(114);
 		ACCcreate(8);
 		ACCclear(24);
 		ACCminus(17,4);
 		ACCminus(18,4);
 		ACCminus(19,4);
 		ACCminus(21,4);
		{}

	}

	// _ _
	p002e0076:
	{
 		if (skipdoall('p002e0076')) break p002e0076;
		if (!CNDnotzero(115)) break p002e0076;
		if (!CNDgt(25,50)) break p002e0076;
 		ACCwriteln(375);
		{}

	}

	// _ _
	p002e0077:
	{
 		if (skipdoall('p002e0077')) break p002e0077;
		if (!CNDnotzero(115)) break p002e0077;
		if (!CNDgt(25,61)) break p002e0077;
 		ACCwriteln(376);
 		ACCclear(115);
 		ACCcreate(9);
 		ACCclear(25);
 		ACCminus(17,6);
 		ACCminus(18,3);
 		ACCminus(19,4);
 		ACCminus(21,6);
		{}

	}

	// _ _
	p002e0078:
	{
 		if (skipdoall('p002e0078')) break p002e0078;
		if (!CNDnotzero(116)) break p002e0078;
		if (!CNDgt(26,30)) break p002e0078;
 		ACCwriteln(377);
		{}

	}

	// _ _
	p002e0079:
	{
 		if (skipdoall('p002e0079')) break p002e0079;
		if (!CNDnotzero(116)) break p002e0079;
		if (!CNDgt(26,36)) break p002e0079;
 		ACCwriteln(378);
 		ACCcreate(10);
 		ACCclear(116);
 		ACCclear(26);
 		ACCminus(17,1);
 		ACCminus(18,1);
 		ACCminus(19,6);
 		ACCminus(21,1);
		{}

	}

	// _ _
	p002e0080:
	{
 		if (skipdoall('p002e0080')) break p002e0080;
		if (!CNDat(0)) break p002e0080;
 		ACCplus(20,1);
 		ACCgoto(2);
 		ACClet(28,8);
 		ACCpause(8);
 		function anykey00029() 
		{
 		ACCdesc();
		return;
		}
 		function anykey00028() 
		{
 		ACCanykey();
 		waitKey(anykey00029);
		}
 		function anykey00027() 
		{
 		ACCminus(31,1);
 		ACClet(5,200);
 		ACClet(28,22);
 		ACCpause(1);
 		waitKey(anykey00028);
		}
 		waitKey(anykey00027);
		done_flag=true;
		break pro002_restart;
		{}

	}

	// _ _
	p002e0081:
	{
 		if (skipdoall('p002e0081')) break p002e0081;
		if (!CNDat(78)) break p002e0081;
		if (!CNDgt(0,19)) break p002e0081;
 		ACCwriteln(379);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0082:
	{
 		if (skipdoall('p002e0082')) break p002e0082;
		if (!CNDgt(8,49)) break p002e0082;
 		ACCplus(8,1);
		{}

	}

	// _ _
	p002e0083:
	{
 		if (skipdoall('p002e0083')) break p002e0083;
		if (!CNDeq(5,252)) break p002e0083;
 		ACCwriteln(380);
		{}

	}

	// _ _
	p002e0084:
	{
 		if (skipdoall('p002e0084')) break p002e0084;
		if (!CNDeq(5,235)) break p002e0084;
 		ACCwriteln(381);
		{}

	}

	// _ _
	p002e0085:
	{
 		if (skipdoall('p002e0085')) break p002e0085;
		if (!CNDeq(5,240)) break p002e0085;
 		ACCwriteln(382);
		{}

	}

	// _ _
	p002e0086:
	{
 		if (skipdoall('p002e0086')) break p002e0086;
		if (!CNDeq(5,234)) break p002e0086;
 		ACCwriteln(383);
 		ACCscore();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0087:
	{
 		if (skipdoall('p002e0087')) break p002e0087;
		if (!CNDat(52)) break p002e0087;
		if (!CNDpresent(20)) break p002e0087;
		if (!CNDnotzero(113)) break p002e0087;
		if (!CNDnotzero(116)) break p002e0087;
		if (!CNDeq(112,0)) break p002e0087;
		if (!CNDeq(114,0)) break p002e0087;
		if (!CNDeq(115,0)) break p002e0087;
 		ACCwriteln(384);
 		ACCdestroy(20);
		{}

	}

	// _ _
	p002e0088:
	{
 		if (skipdoall('p002e0088')) break p002e0088;
		if (!CNDeq(10,10)) break p002e0088;
		if (!CNDnotzero(0)) break p002e0088;
		if (!CNDabsent(0)) break p002e0088;
 		ACCplus(10,1);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0089:
	{
 		if (skipdoall('p002e0089')) break p002e0089;
		if (!CNDnotzero(0)) break p002e0089;
		if (!CNDeq(10,11)) break p002e0089;
		if (!CNDabsent(0)) break p002e0089;
 		ACCplus(10,1);
 		ACCdone();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0090:
	{
 		if (skipdoall('p002e0090')) break p002e0090;
		if (!CNDnotzero(6)) break p002e0090;
 		ACCplus(6,1);
		{}

	}

	// _ _
	p002e0091:
	{
 		if (skipdoall('p002e0091')) break p002e0091;
		if (!CNDgt(7,10)) break p002e0091;
 		ACCplus(7,1);
		{}

	}


}
}

last_process = 2;
// This file is (C) Carlos Sanchez 2014, released under the MIT license

// This function is called first by the start() function that runs when the game starts for the first time
var h_init = function()
{
}


// This function is called last by the start() function that runs when the game starts for the first time
var h_post =  function()
{
}

// This function is called when the engine tries to write any text
var h_writeText =  function (text)
{
	return text;
}

//This function is called every time the user types any order
var h_playerOrder = function(player_order)
{
	return player_order;
}

// This function is called every time a location is described, just after the location text is written
var h_description_init =  function ()
{
}

// This function is called every time a location is described, just after the process 1 is executed
var h_description_post = function()
{
}


// this function is called when the savegame object has been created, in order to be able to add more custom properties
var h_saveGame = function(savegame_object)
{
	return savegame_object;
}


// this function is called after the restore game function has restored the standard information in savegame, in order to restore any additional data included in a patched (by h_saveGame) savegame.
var h_restoreGame = function(savegame_object)
{
}

// this funcion is called before writing a message about player order beeing impossible to understand
var h_invalidOrder = function(player_order)
{
}

// this function is called when a sequence tag is found giving a chance for any hook library to provide a response
// tagparams receives the params inside the tag as an array  {XXXX|nn|mm|yy} => ['XXXX', 'nn', 'mm', 'yy']
var h_sequencetag = function (tagparams)
{
	return '';
}

// this function is called from certain points in the response or process tables via the HOOK condact. Depending on the string received it can do something or not.
// it's designed to allow direct javascript code to take control in the start database just installing a plugin library (avoiding the wirter need to enter code to activate the library)
var h_code = function(str)
{
	return false;
}


// this function is called from the keydown evente handler used by block and other functions to emulate a pause or waiting for a keypress. It is designed to allow plugin condacts or
// libraries to attend those key presses and react accordingly. In case a hook function decides that the standard keydown functions should not be processed, the hook function should return false.
// Also, any h_keydown replacement should probably do the same.
var h_keydown = function (event)
{
	return true;
}


// this function is called every time a process is called,  either by the internall loop of by the PROCESS condact, just before running it.
var h_preProcess = function(procno)
{

}

// this function is called every time a process is called just after the process exits (no matter which DONE status it has), either by the internall loop of by the PROCESS condact
var h_postProcess= function (procno)
{

}// This file is (C) Carlos Sanchez 2014, and is released under the MIT license

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ACCdesc()
{
    describe_location_flag = true;
    ACCbreak(); // Cancel doall loop
}


function ACCdone()
{
    done_flag = true;
}

function CNDat(locno)
{
  return (loc_here()==locno);
}

function CNDnotat(locno)
{
     return (loc_here()!=locno);
}


function CNDatgt(locno)
{
     return (loc_here()>locno);
}


function CNDatlt(locno)
{
     return (loc_here()<locno);
}

function CNDpresent(objno)
{
    var loc = getObjectLocation(objno);
    if (loc == loc_here()) return true;
    if (loc == LOCATION_WORN) return true;
    if (loc == LOCATION_CARRIED) return true;
    if ( (!bittest(getFlag(FLAG_PARSER_SETTINGS),7)) && (objectIsContainer(loc) || objectIsSupporter(loc))  &&  (loc<=last_object_number)  && (CNDpresent(loc)) )  // Extended context and object in another object that is present
    {
        if (objectIsSupporter(loc)) return true;  // On supporter
        if ( objectIsContainer(loc) && objectIsAttr(loc, ATTR_OPENABLE) && objectIsAttr(loc, ATTR_OPEN)) return true; // In a openable & open container
        if ( objectIsContainer(loc) && (!objectIsAttr(loc, ATTR_OPENABLE)) ) return true; // In a not openable container
    }
    return false;
}

function CNDabsent(objno)
{
    return !CNDpresent(objno);
}

function CNDworn(objno)
{
    return (getObjectLocation(objno) == LOCATION_WORN);
}

function CNDnotworn(objno)
{
    return !CNDworn(objno);
}

function CNDcarried(objno)
{
    return (getObjectLocation(objno) == LOCATION_CARRIED);
}

function CNDnotcarr(objno)
{
    return !CNDcarried(objno);
}


function CNDchance(percent)
{
     var val = Math.floor((Math.random()*101));
     return (val<=percent);
}

function CNDzero(flagno)
{
    return (getFlag(flagno) == 0);
}

function CNDnotzero(flagno)
{
     return !CNDzero(flagno)
}


function CNDeq(flagno, value)
{
    return (getFlag(flagno) == value);
}

function CNDnoteq(flagno,value)
{
    return !CNDeq(flagno, value);
}

function CNDgt(flagno, value)
{
    return (getFlag(flagno) > value);
}

function CNDlt(flagno, value)
{
    return (getFlag(flagno) < value);
}


function CNDadject1(wordno)
{
    return (getFlag(FLAG_ADJECT1) == wordno);
}

function CNDadverb(wordno)
{
    return (getFlag(FLAG_ADVERB) == wordno);
}


function CNDtimeout()
{
     return bittest(getFlag(FLAG_TIMEOUT_SETTINGS),7);
}


function CNDisat(objno, locno)
{
    return (getObjectLocation(objno) == locno);

}


function CNDisnotat(objno, locno)
{
    return !CNDisat(objno, locno);
}



function CNDprep(wordno)
{
    return (getFlag(FLAG_PREP) == wordno);
}




function CNDnoun2(wordno)
{
    return (getFlag(FLAG_NOUN2) == wordno);
}

function CNDadject2(wordno)
{
    return (getFlag(FLAG_ADJECT2) == wordno);
}

function CNDsame(flagno1,flagno2)
{
    return (getFlag(flagno1) == getFlag(flagno2));
}


function CNDnotsame(flagno1,flagno2)
{
    return (getFlag(flagno1) != getFlag(flagno2));
}

function ACCinven()
{
    var count = 0;
    writeSysMessage(SYSMESS_YOUARECARRYING);
    ACCnewline();
    var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS),3);
    var i;
    for (i=0;i<num_objects;i++)
    {
        if ((getObjectLocation(i)) == LOCATION_CARRIED)
        {

            if ((listnpcs_with_objects) || (!objectIsNPC(i)))
            {
                writeObject(i);
                if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
                ACCnewline();
                count++;
            }
        }
        if (getObjectLocation(i) == LOCATION_WORN)
        {
            if (listnpcs_with_objects || (!objectIsNPC(i)))
            {
                writeObject(i);
                writeSysMessage(SYSMESS_WORN);
                count++;
                ACCnewline();
            }
        }
    }
    if (!count)
    {
         writeSysMessage(SYSMESS_CARRYING_NOTHING);
         ACCnewline();
    }

    if (!listnpcs_with_objects)
    {
        var numNPC = getNPCCountAt(LOCATION_CARRIED);
        if (numNPC) ACClistnpc(LOCATION_CARRIED);
    }
    done_flag = true;
}

function desc()
{
    describe_location_flag = true;
}


function ACCquit()
{
    inQUIT = true;
    writeSysMessage(SYSMESS_AREYOUSURE);
}


function ACCend()
{
    $('.input').hide();
    inEND = true;
    writeSysMessage(SYSMESS_PLAYAGAIN);
    done_flag = true;
}


function done()
{
    done_flag = true;
}

function ACCok()
{
    writeSysMessage(SYSMESS_OK);
    done_flag = true;
}



function ACCramsave()
{
    ramsave_value = getSaveGameObject();
    var savegame_object = getSaveGameObject();
    savegame =   JSON.stringify(savegame_object);
    localStorage.setItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME, savegame);
}

function ACCramload()
{
    if (ramsave_value==null)
    {
        var json_str = localStorage.getItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME);
        if (json_str)
        {
            savegame_object = JSON.parse(json_str.trim());
            restoreSaveGameObject(savegame_object);
            ACCdesc();
            focusInput();
            return;
        }
        else
        {
            writeText (STR_RAMLOAD_ERROR);
            ACCnewline();
            done_flag = true;
            return;
        }
    }
    restoreSaveGameObject(ramsave_value);
    ACCdesc();
}

function ACCsave()
{
    var savegame_object = getSaveGameObject();
    savegame =   JSON.stringify(savegame_object);
    filename = prompt(getSysMessageText(SYSMESS_SAVEFILE),'');
    if ( filename !== null ) localStorage.setItem('ngpaws_savegame_' + filename.toUpperCase(), savegame);
    ACCok();
}


function ACCload()
{
    var json_str;
    filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
    if ( filename !== null ) json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
    if (json_str)
    {
        savegame_object = JSON.parse(json_str.trim());
        restoreSaveGameObject(savegame_object);
    }
    else
    {
        writeSysMessage(SYSMESS_FILENOTFOUND);
        ACCnewline();
        done_flag = true; return;
    }
    ACCdesc();
    focusInput();
}



function ACCturns()
{
    var turns = getFlag(FLAG_TURNS_HIGH) * 256 +  getFlag(FLAG_TURNS_LOW);
    writeSysMessage(SYSMESS_TURNS_START);
    writeText(turns + '');
    writeSysMessage(SYSMESS_TURNS_CONTINUE);
    if (turns > 1) writeSysMessage(SYSMESS_TURNS_PLURAL);
    writeSysMessage(SYSMESS_TURNS_END);
}

function ACCscore()
{
    var score = getFlag(FLAG_SCORE);
    writeSysMessage(SYSMESS_SCORE_START);
    writeText(score + '');
    writeSysMessage(SYSMESS_SCORE_END);
}


function ACCcls()
{
    clearScreen();
}

function ACCdropall()
{
    // Done in two different loops cause PAW did it like that, just a question of retro compatibility
    var i;
    for (i=0;i<num_objects;i++) if (getObjectLocation(i) == LOCATION_CARRIED)setObjectLocation(i, getFlag(FLAG_LOCATION));
    for (i=0;i<num_objects;i++) if (getObjectLocation(i) == LOCATION_WORN)setObjectLocation(i, getFlag(FLAG_LOCATION));
}


function ACCautog()
{
    objno = findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
    objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
    if (!bittest(getFlag(FLAG_PARSER_SETTINGS),7))  // Extended context for objects
    for (var i=0; i<num_objects;i++) // Try to find it in present containers/supporters
    {
        if (CNDpresent(i) && (isAccesibleContainer(i) || objectIsAttr(i, ATTR_SUPPORTER)) )  // If there is another object present that is an accesible container or a supporter
        {
            objno =findMatchingObject(i);
            if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
        }
    }
    success = false;
    writeSysMessage(SYSMESS_CANTSEETHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautod()
{
    var objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
    objno =findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
    success = false;
    writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautow()
{
    var objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
    objno =findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
    success = false;
    writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautor()
{
    var objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
    objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
    objno =findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
    success = false;
    writeSysMessage(SYSMESS_YOURENOTWEARINGTHAT);
    ACCnewtext();
    ACCdone();
}



function ACCpause(value)
{
 if (value == 0) value = 256;
 pauseRemainingTime = Math.floor(value /50 * 1000);
 inPause = true;
 showAnykeyLayer();
}

function ACCgoto(locno)
{
    setFlag(FLAG_LOCATION,locno);
}

function ACCmessage(mesno)
{
    writeMessage(mesno);
    ACCnewline();
}


function ACCremove(objno)
{
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_CARRIED:
        case loc_here():
            writeSysMessage(SYSMESS_YOUARENOTWEARINGOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case LOCATION_WORN:
            if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
            {
                writeSysMessage(SYSMESS_CANTREMOVE_TOOMANYOBJECTS);
                ACCnewtext();
                ACCdone();
                return;
            }
            setObjectLocation(objno, LOCATION_CARRIED);
            writeSysMessage(SYSMESS_YOUREMOVEOBJECT);
            success = true;
            break;

        default:
            writeSysMessage(SYSMESS_YOUARENOTWEARINGTHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;
    }
}


function trytoGet(objno)  // auxiliaty function for ACCget
{
    if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
    {
        writeSysMessage(SYSMESS_CANTCARRYANYMORE);
        ACCnewtext();
        ACCdone();
        doall_flag = false;
        return;
    }
    var weight = 0;
    weight += getObjectWeight(objno);
    weight +=  getLocationObjectsWeight(LOCATION_CARRIED);
    weight +=  getLocationObjectsWeight(LOCATION_WORN);
    if (weight > getFlag(FLAG_MAXWEIGHT_CARRIED))
    {
        writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
        ACCnewtext();
        ACCdone();
        return;
    }
    setObjectLocation(objno, LOCATION_CARRIED);
    writeSysMessage(SYSMESS_YOUTAKEOBJECT);
    success = true;
}


 function ACCget(objno)
 {
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_CARRIED:
        case LOCATION_WORN:
            writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case loc_here():
            trytoGet(objno);
            break;

        default:
            if  ((locno<=last_object_number) && (CNDpresent(locno)))    // If it's not here, carried or worn but it present, that means that bit 7 of flag 12 is cleared, thus you can get objects from present containers/supporters
            {
                trytoGet(objno);
            }
            else
            {
                writeSysMessage(SYSMESS_CANTSEETHAT);
                ACCnewtext();
                ACCdone();
                return;
                break;
            }
    }
 }

function ACCdrop(objno)
{
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_WORN:
            writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case loc_here():
            writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;


        case LOCATION_CARRIED:
            setObjectLocation(objno, loc_here());
            writeSysMessage(SYSMESS_YOUDROPOBJECT);
            success = true;
            break;

        default:
            writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;
    }
}

function ACCwear(objno)
{
    success = false;
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setReferredObject(objno);
    var locno = getObjectLocation(objno);
    switch (locno)
    {
        case LOCATION_WORN:
            writeSysMessage(SYSMESS_YOUAREALREADYWAERINGOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;

        case loc_here():
            writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
            ACCnewtext();
            ACCdone();
            return;
            break;


        case LOCATION_CARRIED:
            if (!objectIsWearable(objno))
            {
                writeSysMessage(SYSMESS_YOUCANTWEAROBJECT);
                ACCnewtext();
                ACCdone();
                return;
            }
            setObjectLocation(objno, LOCATION_WORN);
            writeSysMessage(SYSMESS_YOUWEAROBJECT);
            success = true;
            break;

        default:
            writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
            ACCnewtext();
            ACCdone();
            return;
            break;
    }
}



function ACCdestroy(objno)
{
    setObjectLocation(objno, LOCATION_NONCREATED);
}


function ACCcreate(objno)
{
    setObjectLocation(objno, loc_here());
}


function ACCswap(objno1,objno2)
{
    var locno1 = getObjectLocation (objno1);
    var locno2 = getObjectLocation (objno2);
    ACCplace (objno1,locno2);
    ACCplace (objno2,locno1);
    setReferredObject(objno2);
}


function ACCplace(objno, locno)
{
    setObjectLocation(objno, locno);
}

function ACCset(flagno)
{
    setFlag(flagno, SET_VALUE);
}

function ACCclear(flagno)
{
    setFlag(flagno,0);
}

function ACCplus(flagno,value)
{
    var newval = getFlag(flagno) + value;
    setFlag(flagno, newval);
}

function ACCminus(flagno,value)
{
    var newval = getFlag(flagno) - value;
    if (newval < 0) newval = 0;
    setFlag(flagno, newval);
}

function ACClet(flagno,value)
{
    setFlag(flagno,value);
}

function ACCnewline()
{
    writeText(STR_NEWLINE);
}

function ACCprint(flagno)
{
    writeText(getFlag(flagno) +'');
}

function ACCsysmess(sysno)
{
    writeSysMessage(sysno);
}

function ACCcopyof(objno,flagno)
{
    setFlag(flagno, getObjectLocation(objno))
}

function ACCcopyoo(objno1, objno2)
{
    setObjectLocation(objno2,getObjectLocation(objno1));
    setReferredObject(objno2);
}

function ACCcopyfo(flagno,objno)
{
    setObjectLocation(objno, getFlag(flagno));
}

function ACCcopyff(flagno1, flagno2)
{
    setFlag(flagno2, getFlag(flagno1));
}

function ACCadd(flagno1, flagno2)
{
    var newval = getFlag(flagno1) + getFlag(flagno2);
    setFlag(flagno2, newval);
}

function ACCsub(flagno1,flagno2)
{
    var newval = getFlag(flagno2) - getFlag(flagno1);
    if (newval < 0) newval = 0;
    setFlag(flagno2, newval);
}


function CNDparse()
{
    return (!getLogicSentence());
}


function ACClistat(locno, container_objno)   // objno is a container/suppoter number, used to list contents of objects
{
  var listingContainer = false;
  if (arguments.length > 1) listingContainer = true;
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);
  objscount = objscount - concealed_or_scenery_objcount;
  if (!listingContainer) setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),7));
  if (!objscount) return;
  var continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  if (listingContainer)
    {
        writeText(' (');
        if (objectIsAttr(container_objno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_OVER_YOUCANSEE); else if (objectIsAttr(container_objno, ATTR_CONTAINER)) writeSysMessage(SYSMESS_INSIDE_YOUCANSEE);
        continouslisting = true;  // listing contents of container always continuous
    }

  if (!listingContainer)
  {
    setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),7));
    if (!continouslisting) ACCnewline();
  }
  var progresscount = 0;
  for (var i=0;i<num_objects;i++)
  {
    if (getObjectLocation(i) == locno)
        if  ( ((!objectIsNPC(i)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))  && (!objectIsAttr(i,ATTR_CONCEALED)) && (!objectIsAttr(i,ATTR_SCENERY))   ) // if not an NPC or parser setting say NPCs are considered objects, and object is not concealed nor scenery
          {
             writeText(getObjectText(i));
             if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
             progresscount++
             if (continouslisting)
             {
                    if (progresscount <= objscount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
                    if (progresscount == objscount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
                    if (!listingContainer) if (progresscount == objscount ) writeSysMessage(SYSMESS_LISTEND);
             } else ACCnewline();
          };
  }
  if (arguments.length > 1) writeText(')');
}


function ACClistnpc(locno)
{
  var npccount =  getNPCCountAt(locno);
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),5));
  if (!npccount) return;
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),5));
  continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  writeSysMessage(SYSMESS_NPCLISTSTART);
  if (!continouslisting) ACCnewline();
  if (npccount==1)  writeSysMessage(SYSMESS_NPCLISTCONTINUE); else writeSysMessage(SYSMESS_NPCLISTCONTINUE_PLURAL);
  var progresscount = 0;
  var i;
  for (i=0;i<num_objects;i++)
  {
    if (getObjectLocation(i) == locno)
        if ( (objectIsNPC(i)) && (!objectIsAttr(i,ATTR_CONCEALED)) ) // only NPCs not concealed
          {
             writeText(getObjectText(i));
             progresscount++
             if (continouslisting)
             {
                if (progresscount <= npccount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
                if (progresscount == npccount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
                if (progresscount == npccount ) writeSysMessage(SYSMESS_LISTEND);
             } else ACCnewline();
          };
  }
}


function ACClistobj()
{
  var locno = loc_here();
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);

  objscount = objscount - concealed_or_scenery_objcount;
  if (objscount)
  {
      writeSysMessage(SYSMESS_YOUCANSEE);
      ACClistat(loc_here());
  }
}

function ACCprocess(procno)
{
    if (procno > last_process)
    {
        writeText(STR_WRONG_PROCESS);
        ACCnewtext();
        ACCdone();
    }
    callProcess(procno);
    if (describe_location_flag) done_flag = true;
}

function ACCmes(mesno)
{
    writeMessage(mesno);
}

function ACCmode(mode)
{
    setFlag(FLAG_MODE, mode);
}

function ACCtime(length, settings)
{
    setFlag(FLAG_TIMEOUT_LENGTH, length);
    setFlag(FLAG_TIMEOUT_SETTINGS, settings);
}

function ACCdoall(locno)
{
    doall_flag = true;
    if (locno == LOCATION_HERE) locno = loc_here();
    // Each object will be considered for doall loop if is at locno and it's not the object specified by the NOUN2/ADJECT2 pair and it's not a NPC (or setting to consider NPCs as objects is set)
    setFlag(FLAG_DOALL_LOC, locno);
    var doall_obj;
    doall_loop:
    for (doall_obj=0;(doall_obj<num_objects) && (doall_flag);doall_obj++)
    {
        if (getObjectLocation(doall_obj) == locno)
            if ((!objectIsNPC(doall_obj)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))
             if (!objectIsAttr(doall_obj, ATTR_CONCEALED))
              if (!objectIsAttr(doall_obj, ATTR_SCENERY))
                if (!( (objectsNoun[doall_obj]==getFlag(FLAG_NOUN2))  &&    ((objectsAdjective[doall_obj]==getFlag(FLAG_ADJECT2)) || (objectsAdjective[doall_obj]==EMPTY_WORD)) ) ) // implements "TAKE ALL EXCEPT BIG SWORD"
                {
                    setFlag(FLAG_NOUN1, objectsNoun[doall_obj]);
                    setFlag(FLAG_ADJECT1, objectsAdjective[doall_obj]);
                    setReferredObject(doall_obj);
                    callProcess(process_in_doall);
                    if (describe_location_flag)
                        {
                            doall_flag = false;
                            entry_for_doall = '';
                            break doall_loop;
                        }
                }
    }
    doall_flag = false;
    entry_for_doall = '';
    if (describe_location_flag) descriptionLoop();
}

function ACCprompt(value)  // deprecated
{
    setFlag(FLAG_PROMPT, value);
    setInputPlaceHolder();
}


function ACCweigh(objno, flagno)
{
    var weight = getObjectWeight(objno);
    setFlag(flagno, weight);
}

function ACCputin(objno, locno)
{
    success = false;
    setReferredObject(objno);
    if (getObjectLocation(objno) == LOCATION_WORN)
    {
        writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectLocation(objno) == loc_here())
    {
        writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectLocation(objno) == LOCATION_CARRIED)
    {
        setObjectLocation(objno, locno);
        if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUPUTOBJECTON); else writeSysMessage(SYSMESS_YOUPUTOBJECTIN);
        writeText(getObjectFixArticles(locno));
        writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
        success = true;
        return;
    }

    writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
    ACCnewtext();
    ACCdone();
}


function ACCtakeout(objno, locno)
{
    success = false;
    setReferredObject(objno);
    if ((getObjectLocation(objno) == LOCATION_WORN) || (getObjectLocation(objno) == LOCATION_CARRIED))
    {
        writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectLocation(objno) == loc_here())
    {
        if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
        writeText(getObjectFixArticles(locno));
        writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getObjectWeight(objno) + getLocationObjectsWeight(LOCATION_WORN) + getLocationObjectsWeight(LOCATION_CARRIED) >  getFlag(FLAG_MAXWEIGHT_CARRIED))
    {
        writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
        ACCnewtext();
        ACCdone();
        return;
    }

    if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
    {
        writeSysMessage(SYSMESS_CANTCARRYANYMORE);
        ACCnewtext();
        ACCdone();
        return;
    }

    setObjectLocation(objno, LOCATION_CARRIED);
    writeSysMessage(SYSMESS_YOUTAKEOBJECT);
    success = true;


}
function ACCnewtext()
{
    player_order_buffer = '';
}

function ACCability(maxObjectsCarried, maxWeightCarried)
{
    setFlag(FLAG_MAXOBJECTS_CARRIED, maxObjectsCarried);
    setFlag(FLAG_MAXWEIGHT_CARRIED, maxWeightCarried);
}

function ACCweight(flagno)
{
    var weight_carried = getLocationObjectsWeight(LOCATION_CARRIED);
    var weight_worn = getLocationObjectsWeight(LOCATION_WORN);
    var total_weight = weight_worn + weight_carried;
    setFlag(flagno, total_weight);
}


function ACCrandom(flagno)
{
     setFlag(flagno, 1 + Math.floor((Math.random()*100)));
}

function ACCwhato()
{
    var whatofound = getReferredObject();
    if (whatofound != EMPTY_OBJECT) setReferredObject(whatofound);
}

function ACCputo(locno)
{
    setObjectLocation(getFlag(FLAG_REFERRED_OBJECT), locno);
}

function ACCnotdone()
{
    done_flag = false;
}

function ACCautop(locno)
{
    var objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
    objno = findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
    objno = findMatchingObject(null); // anywhere
    if (objno != EMPTY_OBJECT)
        {
            writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
            ACCnewtext();
            ACCdone();
            return;
        };

    success = false;
    writeSysMessage(SYSMESS_CANTDOTHAT);
    ACCnewtext();
    ACCdone();
}


function ACCautot(locno)
{

    var objno =findMatchingObject(locno);
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
    objno =findMatchingObject(LOCATION_CARRIED);
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
    objno =findMatchingObject(LOCATION_WORN);
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
    objno = findMatchingObject(loc_here());
    if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };

    objno = findMatchingObject(null); // anywhere
    if (objno != EMPTY_OBJECT)
        {
            if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
            writeText(getObjectFixArticles(locno));
            writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION)
            ACCnewtext();
            ACCdone();
            return;
        };

    success = false;
    writeSysMessage(SYSMESS_CANTDOTHAT);
    ACCnewtext();
    ACCdone();

}


function CNDmove(flagno)
{
    var locno = getFlag(flagno);
    var dirno = getFlag(FLAG_VERB);
    var destination = getConnection( locno,  dirno);
    if (destination != -1)
        {
             setFlag(flagno, destination);
             return true;
        }
    return false;
}


function ACCextern(writeno)
{
    eval(writemessages[writeno]);
}


function ACCpicture(picno)
{
    drawPicture(picno);
}



function ACCgraphic(option)
{
    graphicsON = (option==1);
    if (!graphicsON) hideGraphicsWindow();
}

function ACCbeep(sfxno, channelno, times)
{
    if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
    sfxplay(sfxno, channelno, times, 'play');
}

function ACCsound(value)
{
    soundsON = (value==1);
    if (!soundsON) sfxstopall();
}

function CNDozero(objno, attrno)
{
    if (attrno > 63) return false;
    return !objectIsAttr(objno, attrno);

}

function CNDonotzero(objno, attrno)
{
    return objectIsAttr(objno, attrno);
}

function ACCoset(objno, attrno)
{
    if (attrno > 63) return;
    if (attrno <= 31)
    {
        attrs = getObjectLowAttributes(objno);
        var attrs = bitset(attrs, attrno);
        setObjectLowAttributes(objno, attrs);
        return;
    }
    var attrs = getObjectHighAttributes(objno);
    attrno = attrno - 32;
    attrs = bitset(attrs, attrno);
    setObjectHighAttributes(objno, attrs);

}

function ACCoclear(objno, attrno)
{
    if (attrno > 63) return;
    if (attrno <= 31)
    {
        var attrs = getObjectLowAttributes(objno);
        attrs = bitclear(attrs, attrno);
        setObjectLowAttributes(objno, attrs);
        return;
    }
    var attrs = getObjectHighAttributes(objno);
    attrno = attrno - 32;
    attrs = bitclear(attrs, attrno);
    setObjectHighAttributes(objno, attrs);

}


function CNDislight()
{
    if (!isDarkHere()) return true;
    return lightObjectsPresent();
}



function CNDisnotlight()
{
    return ! CNDislight();
}

function ACCversion()
{
    writeText(filterText(STR_RUNTIME_VERSION));
}


function ACCwrite(writeno)
{
    writeWriteMessage(writeno);
}

function ACCwriteln(writeno)
{
    writeWriteMessage(writeno);
    ACCnewline();
}

function ACCrestart()
{
  process_restart = true;
}


function ACCtranscript()
{
    $('#transcript_area').html(transcript);
    $('.transcript_layer').show();
    inTranscript = true;
}

function ACCanykey()
{
    writeSysMessage(SYSMESS_PRESSANYKEY);
    inAnykey = true;
}

function ACCgetkey(flagno)
{
    getkey_return_flag = flagno;
    inGetkey = true;
}


//////////////////
//   LEGACY     //
//////////////////

// From PAW PC
function ACCbell()
{
    // Empty, PAW PC legacy, just does nothing
}


// From PAW Spectrum
function ACCreset()
{
    // Legacy condact, does nothing now
}


function ACCpaper(color)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCink(color)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCborder(color)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCcharset(value)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCline(lineno)
{
    // Legacy condact, does nothing now, use CSS styles
}

function ACCinput()
{
    // Legacy condact, does nothing now
}

function ACCsaveat()
{
    // Legacy condact, does nothing now
}

function ACCbackat()
{
    // Legacy condact, does nothing now
}

function ACCprintat()
{
    // Legacy condact, does nothing now
}

function ACCprotect()
{
    // Legacy condact, does nothing now
}

// From Superglus


function ACCdebug()
{
    // Legacy condact, does nothing now
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS FOR COMPILER //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CNDverb(wordno)
{
    return (getFlag(FLAG_VERB) == wordno);
}


function CNDnoun1(wordno)
{
    return (getFlag(FLAG_NOUN1) == wordno);
}

//   PLUGINS    ;

//CND RNDWRITELN A 14 14 14 0

function ACCrndwriteln(writeno1,writeno2,writeno3)
{
	ACCrndwrite(writeno1,writeno2,writeno3);
	ACCnewline();
}
//CND SYNONYM A 15 13 0 0

function ACCsynonym(wordno1, wordno2)
{
   if (wordno1!=EMPTY_WORD) setFlag(FLAG_VERB, wordno1);
   if (wordno2!=EMPTY_WORD)	setFlag(FLAG_NOUN1, wordno2);
}
//CND RNDWRITE A 14 14 14 0

function ACCrndwrite(writeno1,writeno2,writeno3)
{
	var val = Math.floor((Math.random()*3));
	switch (val)
	{
		case 0 : writeWriteMessage(writeno1);break;
		case 1 : writeWriteMessage(writeno2);break;
		case 2 : writeWriteMessage(writeno3);break;
	}
}
//CND SETWEIGHT A 4 2 0 0

function ACCsetweight(objno, value)
{
   objectsWeight[objno] = value;
}

//CND BSET A 1 2 0 0

function ACCbset(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitset(getFlag(flagno),bitno));
}
//CND ISNOTMOV C 0 0 0 0

function CNDisnotmov()
{
	return !CNDismov();	
}

//CND VOLUME A 2 2 0 0

function ACCvolume(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxvolume(channelno, value);
}

//CND ISVIDEO C 0 0 0 0

function CNDisvideo()
{
	if (typeof videoElement == 'undefined') return false;
	if (!videoLoopCount) return false;
	if (videoElement.paused) return false;
	return true;
}

//CND GE C 1 2 0 0

function CNDge(flagno, valor)
{
	return (getFlag(flagno)>=valor);
}
//CND ISNOTDOALL C 0 0 0 0

function CNDisnotdoall()
{
	return !CNDisdoall();
}

//CND RESUMEVIDEO A 0 0 0 0


function ACCresumevideo()
{
	if (typeof videoElement != 'undefined') 
		if (videoElement.paused)
		  videoElement.play();
}

//CND PAUSEVIDEO A 0 0 0 0


function ACCpausevideo()
{
	if (typeof videoElement != 'undefined') 
		if (!videoElement.ended) 
		if (!videoElement.paused)
		   videoElement.pause();
}

//CND BZERO C 1 2 0 0

function CNDbzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (!bittest(getFlag(flagno), bitno));
}
//CND BNEG A 1 2 0 0

function ACCbneg(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitneg(getFlag(flagno),bitno));
}
//CND ISDOALL C 0 0 0 0

function CNDisdoall()
{
	return doall_flag;	
}

//CND ONEG A 4 2 0 0

function ACConeg(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitneg(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitneg(attrs, attrno);
	setObjectHighAttributes(objno, attrs);
}

//CND ISDONE C 0 0 0 0

function CNDisdone()
{
	return done_flag;	
}

//CND BLOCK A 14 2 2 0

function ACCblock(writeno, picno, procno)
{
   inBlock = true;
   disableInterrupt();
   $('.block_layer').hide();
   var text = getWriteMessageText(writeno);
   $('.block_text').html(text);
   
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var imgsrc = '<img class="block_picture" src="' + filename + '" />';
		$('.block_graphics').html(imgsrc);
	}
    if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
    $('.block_layer').show();

}
//CND HELP A 0 0 0 0

function ACChelp()
{
	if (getLang()=='EN') EnglishHelp(); else SpanishHelp();
}	

function EnglishHelp()
{
	writeText('HOW DO I SEND COMMANDS TO THE PC?');
	writeText(STR_NEWLINE);
	writeText('Use simple orders: OPEN DOOR, TAKE KEY, GO UP, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I MOVE IN THE MAP?');
	writeText(STR_NEWLINE);
	writeText('Usually you will have to use compass directions as north (shortcut: "N"), south (S), east (E), west (W) or other directions (up, down, enter, leave, etc.). Some games allow complex order like "go to well". Usually you would be able to know avaliable exits by location description, some games also provide the "EXITS" command.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK MY INVENTORY?');
	writeText(STR_NEWLINE);
	writeText('type INVENTORY (shortcut "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I USE THE OBJECTS?');
	writeText(STR_NEWLINE);
	writeText('Use the proper verb, that is, instead of USE KEY type OPEN.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK SOMETHING CLOSELY?');
	writeText(STR_NEWLINE);
	writeText('Use "examine" verb: EXAMINE DISH. (shortcut: EX)');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SEE AGAIN THE CURRENT LOCATION DSCRIPTION?');
	writeText(STR_NEWLINE);
	writeText('Type LOOK (shortcut "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I TALK TO OTHER CHARACTERS?');
	writeText(STR_NEWLINE);
	writeText('Most common methods are [CHARACTER, SENTENCE] or [SAY CHARACTER "SENTENCE"]. For instance: [JOHN, HELLO] o [SAY JOHN "HELLO"]. Some games also allow just [TALK TO JOHN]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING IN A CONTAINER, HOW CAN I TAKE SOMETHING OUT?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY IN BOX. TAKE KEY OUT OF BOX. INSERT KEY IN BOX. EXTRACT KEY FROM BOX.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING ON SOMETHING ELSE?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY ON TABLE. TAKE KEY FROM TABLE');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SAVE/RESTORE MY GAME?');
	writeText(STR_NEWLINE);
	writeText('Use SAVE/LOAD commands.');
	writeText(STR_NEWLINE + STR_NEWLINE);

}

function SpanishHelp()
{
	writeText('¿CÓMO DOY ORDENES AL PERSONAJE?');
	writeText(STR_NEWLINE);
	writeText('Utiliza órdenes en imperativo o infinitivo: ABRE PUERTA, COGER LLAVE, SUBIR, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO ME MUEVO POR EL JUEGO?');
	writeText(STR_NEWLINE);
	writeText('Por regla general, mediante los puntos cardinales como norte (abreviado "N"), sur (S), este (E), oeste (O) o direcciones espaciales (arriba, abajo, bajar, subir, entrar, salir, etc.). Algunas aventuras permiten también cosas como "ir a pozo". Normalmente podrás saber en qué dirección puedes ir por la descripción del sitio, aunque algunos juegos facilitan el comando "SALIDAS" que te dirá exactamente cuáles hay.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO SABER QUE OBJETOS LLEVO?');
	writeText(STR_NEWLINE);
	writeText('Teclea INVENTARIO (abreviado "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO USO LOS OBJETOS?');
	writeText(STR_NEWLINE);
	writeText('Utiliza el verbo correcto, en lugar de USAR ESCOBA escribe BARRER.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO MIRAR DE CERCA UN OBJETO U OBSERVARLO MÁS DETALLADAMENTE?');
	writeText(STR_NEWLINE);
	writeText('Con el verbo examinar: EXAMINAR PLATO. Generalmente se puede usar la abreviatura "EX": EX PLATO.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO VER DE NUEVO LA DESCRIPCIÓN DEL SITIO DONDE ESTOY?');
	writeText(STR_NEWLINE);
	writeText('Escribe MIRAR (abreviado "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO HABLO CON LOS PERSONAJES?');
	writeText(STR_NEWLINE);
	writeText('Los modos más comunes son [PERSONAJE, FRASE] o [DECIR A PERSONAJE "FRASE"]. Por ejemplo: [LUIS, HOLA] o [DECIR A LUIS "HOLA"]. En algunas aventuras también se puede utilizar el formato [HABLAR A LUIS]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO METO ALGO EN UN CONTENEDOR? ¿CÓMO LO SACO?');
	writeText(STR_NEWLINE);
	writeText('METER LLAVE EN CAJA. SACAR LLAVE DE CAJA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PONGO ALGO SOBRE ALGO? ¿CÓMO LO QUITO?');
	writeText(STR_NEWLINE);
	writeText('PONER LLAVE EN MESA. COGER LLAVE DE MESA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO GRABO Y CARGO LA PARTIDA?');
	writeText(STR_NEWLINE);
	writeText('Usa las órdenes SAVE y LOAD, o GRABAR y CARGAR.');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

//CND BNOTZERO C 1 2 0 0

function CNDbnotzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (bittest(getFlag(flagno), bitno));
}

//CND PLAYVIDEO A 14 2 2 0

var videoLoopCount;
var videoEscapable;
var videoElement;

function ACCplayvideo(strno, loopCount, settings)
{
	videoEscapable = settings & 1; // if bit 0 of settings is 1, video can be interrupted with ESC key
	if (loopCount == 0) loopCount = -1;
	videoLoopCount = loopCount;

	str = '<video id="videoframe" height="100%">';
	str = str + '<source src="dat/' + writemessages[strno] + '.mp4" type="video/mp4" codecs="avc1.4D401E, mp4a.40.2">';
	str = str + '<source src="dat/' + writemessages[strno] + '.webm" type="video/webm" codecs="vp8.0, vorbis">';
	str = str + '<source src="dat/' + writemessages[strno] + '.ogg" type="video/ogg" codecs="theora, vorbis">';
	str = str + '</video>';
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#videoframe').css('height','100%');
	$('#videoframe').css('display','block');
	$('#videoframe').css('margin-left','auto');
	$('#videoframe').css('margin-right','auto');
	$('#graphics').show();
	videoElement = document.getElementById('videoframe');
	videoElement.onended = function() 
	{
    	if (videoLoopCount == -1) videoElement.play();
    	else
    	{
    		videoLoopCount--;
    		if (videoLoopCount) videoElement.play();
    	}
	};
	videoElement.play();

}

// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_video_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#videoframe").length > 0) $("#videoframe").remove();	
	old_video_h_description_init();
}

// Hook into keypress to cancel video playing if ESC is pressed and video is skippable

var old_video_h_keydown =  h_keydown;
h_keydown = function (event)
{
 	if ((event.keyCode == 27) && (typeof videoElement != 'undefined') && (!videoElement.ended) && (videoEscapable)) 
 	{
 		videoElement.pause(); 
 		return false;  // we've finished attending ESC press
 	}
 	else return old_video_h_keydown(event);
}




//CND TEXTPIC A 2 2 0 0

function ACCtextpic(picno, align)
{
	var style = '';
	var post = '';
	var pre = '';
	switch(align)
	{
		case 0: post='<br style="clear:left">';break;
		case 1: style = 'float:left'; break;
		case 2: style = 'float:right'; break;
		case 3: pre='<center>';post='</center><br style="clear:left">';break;
	}
	filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var texto = pre + "<img alt='' class='textpic' style='"+style+"' src='"+filename+"' />" + post;
		writeText(texto);
		$(".text").scrollTop($(".text")[0].scrollHeight);
	}
}
//CND OBJFOUND C 2 9 0 0

function CNDobjfound(attrno, locno)
{

	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return true; }
	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return false;
}

//CND PICTUREAT A 2 2 2 0

/*
In order to determine the actual size of both background image and pictureat image they should be loaded, thus two chained "onload" are needed. That is, 
background image is loaded to determine its size, then pictureat image is loaded to determine its size. Size of currently displayed background image cannot
be used as it may have been already stretched.
*/

function ACCpictureat(x,y,picno)
{
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (!filename) return;

	// Check location has a picture, otherwise exit
	var currentBackgroundScreenImage = $('.location_picture');
	if (!currentBackgroundScreenImage) return;

	// Create a new image with the contents of current background image, to be able to calculate original height of image
	var virtualBackgroundImage = new Image();
	// Pass required data as image properties in order to be avaliable at "onload" event
	virtualBackgroundImage.bg_data=[];
	virtualBackgroundImage.bg_data.filename = filename; 
	virtualBackgroundImage.bg_data.x = x;
	virtualBackgroundImage.bg_data.y = y;
	virtualBackgroundImage.bg_data.picno = picno;
	virtualBackgroundImage.bg_data.currentBackgroundScreenImage = currentBackgroundScreenImage;


	// Event triggered when virtual background image is loaded
	virtualBackgroundImage.onload = function()
		{
			var originalBackgroundImageHeight = this.height;
			var scale = this.bg_data.currentBackgroundScreenImage.height() / originalBackgroundImageHeight;

			// Create a new image with the contents of picture to show with PICTUREAT, to be able to calculate height of image
			var virtualPictureAtImage = new Image();
			// Also pass data from background image as property so they are avaliable in the onload event
			virtualPictureAtImage.pa_data = [];
			virtualPictureAtImage.pa_data.x = this.bg_data.x;
			virtualPictureAtImage.pa_data.y = this.bg_data.y;
			virtualPictureAtImage.pa_data.picno = this.bg_data.picno;
			virtualPictureAtImage.pa_data.filename = this.bg_data.filename;
			virtualPictureAtImage.pa_data.scale = scale;
			virtualPictureAtImage.pa_data.currentBackgroundImageWidth = this.bg_data.currentBackgroundScreenImage.width();
			
			// Event triggered when virtual PCITUREAT image is loaded
			virtualPictureAtImage.onload = function ()
			{
		    		var imageHeight = this.height; 
					var x = Math.floor(this.pa_data.x * this.pa_data.scale);
					var y = Math.floor(this.pa_data.y * this.pa_data.scale);
					var newimageHeight = Math.floor(imageHeight * this.pa_data.scale);
					var actualBackgroundImageX = Math.floor((parseInt($('.graphics').width()) - this.pa_data.currentBackgroundImageWidth)/2);;
					var id = 'pictureat_' + this.pa_data.picno;

					// Add new image, notice we are not using the virtual image, but creating a new one
					$('.graphics').append('<img  alt="" id="'+id+'" style="display:none" />');				
					$('#' + id).css('position','absolute');
					$('#' + id).css('left', actualBackgroundImageX + x  + 'px');
					$('#' + id).css('top',y + 'px');
					$('#' + id).css('z-index','100');
					$('#' + id).attr('src', this.pa_data.filename);
					$('#' + id).css('height',newimageHeight + 'px');
					$('#' + id).show();
			}

			// Assign the virtual pictureat image the destinationsrc to trigger the "onload" event
			virtualPictureAtImage.src = this.bg_data.filename;
			};

	// Assign the virtual background image same src as current background to trigger the "onload" event
	virtualBackgroundImage.src = currentBackgroundScreenImage.attr("src");

}

//CND ISNOTRESP C 0 0 0 0

function CNDisnotresp()
{
	return !in_response;	
}

//CND ISSOUND C 1 0 0 0

function CNDissound(channelno)
{
	if ((channelno <1 ) || (channelno > MAX_CHANNELS)) return false;
    return channelActive(channelno);
}
//CND ZONE C 8 8 0 0

function CNDzone(locno1, locno2)
{

	if (loc_here()<locno1) return false;
	if (loc_here()>locno2) return false;
	return true;
}
//CND FADEOUT A 2 2 0 0

function ACCfadeout(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxfadeout(channelno, value);
}
//CND CLEAREXIT A 2 0 0 0

function ACCclearexit(wordno)
{
	if ((wordno >= NUM_CONNECTION_VERBS) || (wordno< 0 )) return;
	setConnection(loc_here(),wordno, -1);
}
//CND WHATOX2 A 1 0 0 0

function ACCwhatox2(flagno)
{	
	var auxNoun = getFlag(FLAG_NOUN1);
	var auxAdj = getFlag(FLAG_ADJECT1);
	setFlag(FLAG_NOUN1, getFlag(FLAG_NOUN2));
	setFlag(FLAG_ADJECT1, getFlag(FLAG_ADJECT2));
	var whatox2found = getReferredObject();
	setFlag(flagno,whatox2found);
	setFlag(FLAG_NOUN1, auxNoun);
	setFlag(FLAG_ADJECT1, auxAdj);
}
//CND COMMAND A 2 0 0 0

function ACCcommand(value)
{
	if (value) {$('.input').show();$('.input').focus();} else $('.input').hide();
}
//CND TITLE A 14 0 0 0

function ACCtitle(writeno)
{
	document.title = writemessages[writeno];
}
//CND LE C 1 2 0 0

function CNDle(flagno, valor)
{
	return (getFlag(flagno) <= valor);
}
//CND WARNINGS A 2 0 0 0

function ACCwarnings(value)
{
	if (value) showWarnings = true; else showWarnings = false;
}
//CND BCLEAR A 1 2 0 0

function ACCbclear(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitclear(getFlag(flagno), bitno));
}
//CND DIV A 1 2 0 0

function ACCdiv(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) / valor));
}
//CND OBJAT A 9 1 0 0

function ACCobjat(locno, flagno)
{
	setFlag(flagno, getObjectCountAt(locno));
}
//CND SILENCE A 2 0 0 0

function ACCsilence(channelno)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxstop(channelno);
}
//CND SETEXIT A 2 2 0 0

function ACCsetexit(value, locno)
{
	if (value < NUM_CONNECTION_VERBS) setConnection(loc_here(), value, locno);
}
//CND EXITS A 8 5 0 0

function ACCexits(locno,mesno)
{
  writeText(getExitsText(locno,mesno));
}

//CND HOOK A 14 0 0 5

function ACChook(writeno)
{
	h_code(writemessages[writeno]);
}
//CND RANDOMX A 1 2 0 0

function ACCrandomx(flagno, value)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*value)));
}
//CND ISNOTDONE C 0 0 0 0

function CNDisnotdone()
{
	return !CNDisdone();
}

//CND ATGE C 8 0 0 0

function CNDatge(locno)
{
	return (getFlag(FLAG_LOCATION) >= locno);
}

//CND ATLE C 8 0 0 0

function CNDatle(locno)
{
	return (getFlag(FLAG_LOCATION) <= locno);
}

//CND RESP A 0 0 0 0

function ACCresp()
{
	in_response = true;
}	

//CND ISNOTSOUND C 1 0 0 0

function CNDisnotsound(channelno)
{
  if ((channelno <1) || (channelno >MAX_CHANNELS)) return false;
  return !(CNDissound(channelno));
}
//CND ASK W 14 14 1 0

// Global vars for ASK


var inAsk = false;
var ask_responses = null;
var ask_flagno = null;



function ACCask(writeno, writenoOptions, flagno)
{
	inAsk = true;
	writeWriteMessage(writeno);
	ask_responses = getWriteMessageText(writenoOptions);
	ask_flagno = flagno;
}



// hook replacement
var old_ask_h_keydown  = h_keydown;
h_keydown  = function (event)
{
	if (inAsk)
	{
		var keyCodeAsChar = String.fromCharCode(event.keyCode).toLowerCase();
		if (ask_responses.indexOf(keyCodeAsChar)!= -1)
		{
			setFlag(ask_flagno, ask_responses.indexOf(keyCodeAsChar));
			inAsk = false;
			event.preventDefault();
            $('.input').show();
		    $('.input').focus();
		    hideBlock();
			waitKeyCallback();
		};
		return false; // if we are in ASK condact, no keypress should be considered other than ASK response
	} else return old_ask_h_keydown(event);
}

//CND MUL A 1 2 0 0

function ACCmul(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) * valor));
}
//CND NPCAT A 9 1 0 0

function ACCnpcat(locno, flagno)
{
	setFlag(flagno,getNPCCountAt(locno));
}

//CND SOFTBLOCK A 2 0 0 0

function ACCsoftblock(procno)
{
   inBlock = true;
   disableInterrupt();

   $('.block_layer').css('display','none');
   $('.block_text').html('');
   $('.block_graphics').html('');
   $('.block_layer').css('background','transparent');
   if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
   $('.block_layer').css('display','block');
}
//CND LISTCONTENTS A 9 0 0 0

function ACClistcontents(locno)
{
   ACClistat(locno, locno)
}
//CND SPACE A 0 0 0 0

function ACCspace()
{
	writeText(' ');
}
//CND ISNOTMUSIC C 0 0 0 0

function CNDisnotmusic()
{
  return !CNDismusic();
}

//CND BREAK A 0 0 0 0

function ACCbreak()
{
	doall_flag = false; 
	entry_for_doall = '';
}
//CND NORESP A 0 0 0 0

function ACCnoresp()
{
	in_response = false;
}	

//CND ISMUSIC C 0 0 0 0

function CNDismusic()
{
	return (CNDissound(0));	
}

//CND ISRESP C 0 0 0 0

function CNDisresp()
{
	return in_response;	
}

//CND MOD A 1 2 0 0

function ACCmod(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) % valor));
}
//CND FADEIN A 2 2 2 0

function ACCfadein(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'fadein');
}
//CND WHATOX A 1 0 0 0

function ACCwhatox(flagno)
{
	var whatoxfound = getReferredObject();
	setFlag(flagno,whatoxfound);
}

//CND GETEXIT A 2 2 0 0

function ACCgetexit(value,flagno)
{
	if (value >= NUM_CONNECTION_VERBS) 
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	var locno = getConnection(loc_here(),value);
	if (locno == -1)
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	setFlag(flagno,locno);
}
//CND LOG A 14 0 0 0

function ACClog(writeno)
{
  console_log(writemessages[writeno]);
}
//CND VOLUMEVIDEO A 2 0 0 0


function ACCvolumevideo(value)
{
	if (typeof videoElement != 'undefined') 
		videoElement.volume = value  / 65535;
}

//CND OBJNOTFOUND C 2 9 0 0

function CNDobjnotfound(attrno, locno)
{
	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return false; }

	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return true;
}
//CND ISMOV C 0 0 0 0

function CNDismov()
{
	if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)==EMPTY_WORD)) return true;

	if ((getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_VERB)==EMPTY_WORD)) return true;

    if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS)) return true;
    
    return false;
}

//CND YOUTUBE A 14 0 0 0

function ACCyoutube(strno)
{

	var str = '<iframe id="youtube" width="560" height="315" src="http://www.youtube.com/embed/' + writemessages[strno] + '?autoplay=1&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>'
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#youtube').css('height','100%');
	$('#youtube').css('display','block');
	$('#youtube').css('margin-left','auto');
	$('#youtube').css('margin-right','auto');
	$('.graphics').show();
}


// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_youtube_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#youtube").length > 0) $("#youtube").remove();	
	old_youtube_h_description_init();
}
//CND LISTSAVEDGAMES A 0 0 0 0

function ACClistsavedgames()
{
  var numberofgames = 0;
  for(var savedgames in localStorage)
  {
    gamePrefix = savedgames.substring(0,16); // takes out ngpaws_savegame_
    if (gamePrefix == "ngpaws_savegame_")
    {
      gameName = savedgames.substring(16);
      writelnText(gameName);
      numberofgames++;
    }
  }
  if (numberofgames == 0) 
  {
     if (getLang()=='EN') writelnText("No saved games found."); else writelnText("No hay ninguna partida guardada.");
  }
}


// This file is (C) Carlos Sanchez 2014, released under the MIT license


// IMPORTANT: Please notice this file must be encoded with the same encoding the index.html file is, so the "normalize" function works properly.
//            As currently the ngpwas compiler generates utf-8, and the index.html is using utf-8 also, this file must be using that encoding.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                         Auxiliary functions                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General functions
String.prototype.rights= function(n){
    if (n <= 0)
       return "";
    else if (n > String(this).length)
       return this;
    else {
       var iLen = String(this).length;
       return String(this).substring(iLen, iLen - n);
    }
}


String.prototype.firstToLower= function()
{
    return  this.charAt(0).toLowerCase() + this.slice(1);
}


// Returns true if using Internet Explorer 9 or below, where some features are not supported
function isBadIE () {
  var myNav = navigator.userAgent.toLowerCase();
  if (myNav.indexOf('msie') == -1) return false;
  ieversion =  parseInt(myNav.split('msie')[1]);
  return (ieversion<10);
}


function runningLocal()
{
    return (window.location.protocol == 'file:');
}


// Levenshtein function

function getLevenshteinDistance (a, b)
{
  if(a.length == 0) return b.length;
  if(b.length == 0) return a.length;

  var matrix = [];

  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }

  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }

  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }

  return matrix[b.length][a.length];
};

// waitKey helper for all key-wait condacts

function waitKey(callbackFunction)
{
    waitkey_callback_function.push(callbackFunction);
    showAnykeyLayer();
}

function waitKeyCallback()
{
    var callback = waitkey_callback_function.pop();
    if ( callback ) callback();
    if (describe_location_flag) descriptionLoop();
}


// Check DOALL entry

function skipdoall(entry)
{
    return  ((doall_flag==true) && (entry_for_doall!='') && (current_process==process_in_doall) && (entry_for_doall > entry));
}

// Dynamic attribute use functions
function getNextFreeAttribute()
{
    var value = nextFreeAttr;
    nextFreeAttr++;
    return value;
}


// Gender functions

function getSimpleGender(objno)  // Simple, for english
{
    isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
    if (isPlural) return "P";
    isFemale = objectIsAttr(objno, ATTR_FEMALE);
    if (isFemale) return "F";
    isMale = objectIsAttr(objno, ATTR_MALE);
    if (isMale) return "M";
    return "N"; // Neuter
}

function getAdvancedGender(objno)  // Complex, for spanish
{
    var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
    var isFemale = objectIsAttr(objno, ATTR_FEMALE);
    var isMale = objectIsAttr(objno, ATTR_MALE);

    if (!isPlural)
    {
        if (isFemale) return "F";
        if (isMale) return "M";
        return "N"; // Neuter
    }
    else
    {
        if (isFemale) return "PF";
        if (isMale) return "PM";
        return "PN"; // Neuter plural
    }

}

function getLang()
{
    var value = bittest(getFlag(FLAG_PARSER_SETTINGS),5);
    if (value) return "ES"; else return "EN";
}

function getObjectFixArticles(objno)
{
    var object_text = getObjectText(objno);
    var object_words = object_text.split(' ');
    if (object_words.length == 1) return object_text;
    var candidate = object_words[0];
    object_words.splice(0, 1);
    if (getLang()=='EN')
    {
        if ((candidate!='an') && (candidate!='a') && (candidate!='some')) return object_text;
        return 'the ' + object_words.join(' ');
    }
    else
    {
        if ( (candidate!='un') && (candidate!='una') && (candidate!='unos') && (candidate!='unas') && (candidate!='alguna') && (candidate!='algunos') && (candidate!='algunas') && (candidate!='algun')) return object_text;
        var gender = getAdvancedGender(objno);
        if (gender == 'F') return 'la ' + object_words.join(' ');
        if (gender == 'M') return 'el ' + object_words.join(' ');
        if (gender == 'N') return 'el ' + object_words.join(' ');
        if (gender == 'PF') return 'las ' + object_words.join(' ');
        if (gender == 'PM') return 'los ' + object_words.join(' ');
        if (gender == 'PN') return 'los ' + object_words.join(' ');
    }


}



// JS level log functions
function console_log(string)
{
    if (typeof console != "undefined") console.log(string);
}


// Resources functions
function getResourceById(resource_type, id)
{
    for (var i=0;i<resources.length;i++)
     if ((resources[i][0] == resource_type) && (resources[i][1]==id)) return resources[i][2];
    return false;
}

// Flag read/write functions
function getFlag(flagno)
{
     return flags[flagno];
}

function setFlag(flagno, value)
{
     flags[flagno] = value;
}

// Locations functions
function loc_here()  // Returns current location, avoid direct use of flags
{
     return getFlag(FLAG_LOCATION);
}


// Connections functions

function setConnection(locno1, dirno, locno2)
{
    connections[locno1][dirno] = locno2;
}

function getConnection(locno, dirno)
{
    return connections[locno][dirno];
}

// Objects text functions

function getObjectText(objno)
{
    return filterText(objects[objno]);
}


// Message text functions
function getMessageText(mesno)
{
    return filterText(messages[mesno]);
}

function getSysMessageText(sysno)
{
    return filterText(sysmessages[sysno]);
}

function getWriteMessageText(writeno)
{
    return filterText(writemessages[writeno]);
}

function getExitsText(locno,mesno)
{
  if ( locno === undefined ) return ''; // game hasn't fully initialised yet
  if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent()))
  {
        var exitcount = 0;
        for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1) exitcount++;
      if (exitcount)
      {
            var message = getMessageText(mesno);
            var exitcountprogress = 0;
            for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1)
            {
                exitcountprogress++;
                message += getMessageText(mesno + 2 + i);
                if (exitcountprogress == exitcount) message += getSysMessageText(SYSMESS_LISTEND);
                if (exitcountprogress == exitcount-1) message += getSysMessageText(SYSMESS_LISTLASTSEPARATOR);
                if (exitcountprogress <= exitcount-2) message += getSysMessageText(SYSMESS_LISTSEPARATOR);
          }
          return message;
      } else return getMessageText(mesno + 1);
  } else return getMessageText(mesno + 1);
}


// Location text functions
function getLocationText(locno)
{
    return  filterText(locations[locno]);
}



// Output processing functions
function implementTag(tag)
{
    tagparams = tag.split('|');
    for (var tagindex=0;tagindex<tagparams.length-1;tagindex++) tagparams[tagindex] = tagparams[tagindex].trim();
    if (tagparams.length == 0) {writeWarning(STR_INVALID_TAG_SEQUENCE_EMPTY); return ''}

    var resolved_hook_value = h_sequencetag(tagparams);
    if (resolved_hook_value!='') return resolved_hook_value;

    switch(tagparams[0].toUpperCase())
    {
        case 'URL': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                    return '<a target="newWindow" href="' + tagparams[1]+ '">' + tagparams[2] + '</a>'; // Note: _blank would get the underscore character replaced by current selected object so I prefer to use a different target name as most browsers will open a new window
                    break;
        case 'CLASS': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span class="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'STYLE': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span style="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'INK': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span style="color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'PAPER': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                      return '<span style="background-color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
                      break;
        case 'OBJECT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(objects[getFlag(tagparams[1])]) return getObjectFixArticles(getFlag(tagparams[1])); else return '';
                       break;
        case 'WEIGHT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(objectsWeight[getFlag(tagparams[1])]) return objectsWeight[getFlag(tagparams[1])]; else return '';
                       break;
        case 'OLOCATION': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                          if(objectsLocation[getFlag(tagparams[1])]) return objectsLocation[getFlag(tagparams[1])]; else return '';
                          break;
        case 'MESSAGE':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(messages[getFlag(tagparams[1])]) return getMessageText(getFlag(tagparams[1])); else return '';
                       break;
        case 'SYSMESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(sysmessages[getFlag(tagparams[1])]) return getSysMessageText(getFlag(tagparams[1])); else return '';
                       break;
        case 'LOCATION':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       if(locations[getFlag(tagparams[1])]) return getLocationText(getFlag(tagparams[1])); else return '';
        case 'EXITS':if (tagparams.length != 3 ) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       return getExitsText(/^@\d+/.test(tagparams[1]) ? getFlag(tagparams[1].substr(1)) : tagparams[1],parseInt(tagparams[2],10));
                       break;
        case 'PROCESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       callProcess(tagparams[1]);
                       return "";
                       break;
        case 'ACTION': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                       return '<a href="type: ' + tagparams[1] + '" onmouseup="orderEnteredLoop(\'' + tagparams[1]+ '\');return false;">' + tagparams[2] + '</a>';
                       break;
        case 'RESTART': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return '<a href="javascript: void(0)" onmouseup="restart()">' + tagparams[1] + '</a>';
                        break;
        case 'EXTERN': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return '<a href="javascript: void(0)" onmouseup="' + tagparams[1] + ' ">' + tagparams[2] + '</a>';
                        break;
        case 'TEXTPIC': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        var style = '';
                        var post = '';
                        var pre = '';
                        align = tagparams[2];
                        switch(align)
                        {
                            case 1: style = 'float:left'; break;
                            case 2: style = 'float:right'; break;
                            case 3: post = '<br />';
                            case 4: pre='<center>';post='</center>';break;
                        }
                        return pre + "<img class='textpic' style='"+style+"' src='"+ RESOURCES_DIR + tagparams[1]+"' />" + post;
                        break;
        case 'HTML': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return tagparams[1];
                        break;
        case 'FLAG': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                        return getFlag(tagparams[1]);
                        break;
        case 'OREF': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                    if(objects[getFlag(FLAG_REFERRED_OBJECT)]) return getObjectFixArticles(getFlag(FLAG_REFERRED_OBJECT)); else return '';
                    break;
        case 'TT':
        case 'TOOLTIP':
                    if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
                    var title = $('<span>'+tagparams[1]+'</span>').text().replace(/'/g,"&apos;").replace(/\n/g, "&#10;");
                    var text = tagparams[2];
                    return "<span title='"+title+"'>"+text+"</span>";
                    break;
        case 'OPRO': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};  // returns the pronoun for a given object, used for english start database
                     switch (getSimpleGender(getFlag(FLAG_REFERRED_OBJECT)))
                     {
                        case 'M' : return "him";
                        case "F" : return "her";
                        case "N" : return "it";
                        case "P" : return "them";  // plural returns them
                     }
                    break;

        default : return '[[[' + STR_INVALID_TAG_SEQUENCE_BADTAG + ' : ' + tagparams[0] + ']]]';
    }
}

function processTags(text)
{
    //Apply the {} tags filtering
    var pre, post, innerTag;
    tagfilter:
    while (text.indexOf('{') != -1)
    {
        if (( text.indexOf('}') == -1 ) || ((text.indexOf('}') < text.indexOf('{'))))
        {
            writeWarning(STR_INVALID_TAG_SEQUENCE + text);
            break tagfilter;
        }
        pre = text.substring(0,text.indexOf('{'));
        var openbracketcont = 1;
        pointer = text.indexOf('{') + 1;
        innerTag = ''
        while (openbracketcont>0)
        {
            if (text.charAt(pointer) == '{') openbracketcont++;
            if (text.charAt(pointer) == '}') openbracketcont--;
            if ( text.length <= pointer )
            {
                writeWarning(STR_INVALID_TAG_SEQUENCE + text);
                break tagfilter;
            }
            innerTag = innerTag + text.charAt(pointer);
            pointer++;
        }
        innerTag = innerTag.substring(0,innerTag.length - 1);
        post = text.substring(pointer);
        if (innerTag.indexOf('{') != -1 ) innerTag = processTags(innerTag);
        innerTag = implementTag(innerTag);
        text = pre + innerTag + post;
    }
    return text;
}

function filterText(text)
{
    // ngPAWS sequences
    text = processTags(text);


    // Superglus sequences (only \n remains)
    text = text.replace(/\n/g, STR_NEWLINE);

    // PAWS sequences (only underscore)
    objno = getFlag(FLAG_REFERRED_OBJECT);
    if ((objno != EMPTY_OBJECT) && (objects[objno]))    text = text.replace(/_/g,objects[objno].firstToLower()); else text = text.replace(/_/g,'');
    text = text.replace(/¬/g,' ');

    return text;
}


// Text Output functions
function writeText(text, skipAutoComplete)
{
    if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
    text = h_writeText(text); // hook
    $('.text').append(text);
    $('.text').scrollTop($('.text')[0].scrollHeight);
    addToTranscript(text);
    if (!skipAutoComplete) addToAutoComplete(text);
    focusInput();
}

function writeWarning(text)
{
    if (showWarnings) writeText(text)
}

function addToTranscript(text)
{
    transcript = transcript + text;
}

function writelnText(text, skipAutoComplete)
{
    if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
    writeText(text + STR_NEWLINE, skipAutoComplete);
}

function writeMessage(mesno)
{
    if (messages[mesno]!=null) writeText(getMessageText(mesno)); else writeWarning(STR_NEWLINE + STR_WRONG_MESSAGE + ' [' + mesno + ']');
}

function writeSysMessage(sysno)
{
        if (sysmessages[sysno]!=null) writeText(getSysMessageText(sysno)); else writeWarning(STR_NEWLINE + STR_WRONG_SYSMESS + ' [' + sysno + ']');
        $(".text").scrollTop($(".text")[0].scrollHeight);
}

function writeWriteMessage(writeno)
{
        writeText(getWriteMessageText(writeno));
}

function writeObject(objno)
{
    writeText(getObjectText(objno));
}

function clearTextWindow()
{
    $('.text').empty();
}


function clearInputWindow()
{
    $('.prompt').val('');
}


function writeLocation(locno)
{
    if (locations[locno]!=null) writeText(getLocationText(locno) + STR_NEWLINE); else writeWarning(STR_NEWLINE + STR_WRONG_LOCATION + ' [' + locno + ']');
}

// Screen control functions

function clearGraphicsWindow()
{
    $('.graphics').empty();
}


function clearScreen()
{
    clearInputWindow();
    clearTextWindow();
    clearGraphicsWindow();
}

function copyOrderToTextWindow(player_order)
{

    last_player_orders.push(player_order);
    last_player_orders_pointer = 0;
    clearInputWindow();
    writelnText(STR_PROMPT_START + player_order + STR_PROMPT_END, false);
}

function get_prev_player_order()
{
    if (!last_player_orders.length) return '';
    var last = last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];
    if (last_player_orders_pointer < last_player_orders.length - 1) last_player_orders_pointer++;
    return last;
}

function get_next_player_order()
{
    if (!last_player_orders.length || last_player_orders_pointer == 0) return '';
    last_player_orders_pointer--;
    return last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];

}



// Graphics functions


function hideGraphicsWindow()
{
        $('.text').removeClass('half_text');
        $('.text').addClass('all_text');
        $('.graphics').removeClass('half_graphics');
        $('.graphics').addClass('hidden');
        if ($('.location_picture')) $('.location_picture').remove();
}



function drawPicture(picno)
{
    var pictureDraw = false;
    if (graphicsON)
    {
        if ((isDarkHere()) && (!lightObjectsPresent())) picno = 0;
        var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
        if (filename)
        {
            $('.graphics').removeClass('hidden');
            $('.graphics').addClass('half_graphics');
            $('.text').removeClass('all_text');
            $('.text').addClass('half_text');
            $('.graphics').html('<img alt="" class="location_picture" src="' +  filename + '" />');
            $('.location_picture').css('height','100%');
            pictureDraw = true;
        }
    }

    if (!pictureDraw) hideGraphicsWindow();
}




function clearPictureAt() // deletes all pictures drawn by "pictureAT" condact
{
    $.each($('.graphics img'), function () {
        if ($(this)[0].className!= 'location_picture') $(this).remove();
    });

}

// Turns functions

function incTurns()
{
    turns = getFlag(FLAG_TURNS_LOW) + 256 * getFlag(FLAG_TURNS_HIGH)  + 1;
    setFlag(FLAG_TURNS_LOW, turns % 256);
    setFlag(FLAG_TURNS_HIGH, Math.floor(turns / 256));
}

// input box functions

function disableInput()
{
    $(".input").prop('disabled', true);
}

function enableInput()
{
    $(".input").prop('disabled', false);
}

function focusInput()
{
    $(".prompt").focus();
    timeout_progress = 0;
}

// Object default attributes functions

function objectIsNPC(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_NPC);
}

function objectIsLight(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_LIGHT);
}

function objectIsWearable(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_WEARABLE);
}

function objectIsContainer(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_CONTAINER);
}

function objectIsSupporter(objno)
{
    if (objno > last_object_number) return false;
    return bittest(getObjectLowAttributes(objno), ATTR_SUPPORTER);
}


function objectIsAttr(objno, attrno)
{
    if (attrno > 63) return false;
    var attrs = getObjectLowAttributes(objno);
    if (attrno > 31)
    {
        attrs = getObjectHighAttributes(objno);
        attrno = attrno - 32;
    }
    return bittest(attrs, attrno);
}

function isAccesibleContainer(objno)
{
    if (objectIsSupporter(objno)) return true;   // supporter
    if ( objectIsContainer(objno) && !objectIsAttr(objno, ATTR_OPENABLE) ) return true;  // No openable container
    if ( objectIsContainer(objno) && objectIsAttr(objno, ATTR_OPENABLE) && objectIsAttr(objno, ATTR_OPEN)  )  return true;  // No openable & open container
    return false;
}

//Objects and NPC functions

function findMatchingObject(locno)
{
    for (var i=0;i<num_objects;i++)
        if ((locno==-1) || (getObjectLocation(i) == locno))
         if (((objectsNoun[i]) == getFlag(FLAG_NOUN1)) && (((objectsAdjective[i]) == EMPTY_WORD) || ((objectsAdjective[i]) == getFlag(FLAG_ADJECT1))))  return i;
    return EMPTY_OBJECT;
}

function getReferredObject()
{
    var objectfound = EMPTY_OBJECT;
    refobject_search:
    {
        object_id = findMatchingObject(LOCATION_CARRIED);
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}

        object_id = findMatchingObject(LOCATION_WORN);
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}

        object_id = findMatchingObject(loc_here());
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}

        object_id = findMatchingObject(-1);
        if (object_id != EMPTY_OBJECT)  {objectfound = object_id; break refobject_search;}
    }
    return objectfound;
}


function getObjectLowAttributes(objno)
{
    return objectsAttrLO[objno];
}

function getObjectHighAttributes(objno)
{
    return objectsAttrHI[objno]
}


function setObjectLowAttributes(objno, attrs)
{
    objectsAttrLO[objno] = attrs;
}

function setObjectHighAttributes(objno, attrs)
{
    objectsAttrHI[objno] = attrs;
}


function getObjectLocation(objno)
{
    if (objno > last_object_number)
        writeWarning(STR_INVALID_OBJECT + ' [' + objno + ']');
    return objectsLocation[objno];
}

function setObjectLocation(objno, locno)
{
    if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) - 1);
    objectsLocation[objno] = locno;
    if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) + 1);
}



// Sets all flags associated to  referred object by current LS
function setReferredObject(objno)
{
    if (objno == EMPTY_OBJECT)
    {
        setFlag(FLAG_REFERRED_OBJECT, EMPTY_OBJECT);
        setFlag(FLAG_REFERRED_OBJECT_LOCATION, LOCATION_NONCREATED);
        setFlag(FLAG_REFERRED_OBJECT_WEIGHT, 0);
        setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, 0);
        setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, 0);
        return;
    }
    setFlag(FLAG_REFERRED_OBJECT, objno);
    setFlag(FLAG_REFERRED_OBJECT_LOCATION, getObjectLocation(objno));
    setFlag(FLAG_REFERRED_OBJECT_WEIGHT, getObjectWeight(objno));
    setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, getObjectLowAttributes(objno));
    setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, getObjectHighAttributes(objno));

}


function getObjectWeight(objno)
{
    var weight = objectsWeight[objno];
    if ( ((objectIsContainer(objno)) || (objectIsSupporter(objno))) && (weight!=0)) // Container with zero weigth are magic boxes, anything you put inside weigths zero
        weight = weight + getLocationObjectsWeight(objno);
    return weight;
}


function getLocationObjectsWeight(locno)
{
    var weight = 0;
    for (var i=0;i<num_objects;i++)
    {
        if (getObjectLocation(i) == locno)
        {
            objweight = objectsWeight[i];
            weight += objweight;
            if (objweight > 0)
            {
                if (  (objectIsContainer(i)) || (objectIsSupporter(i)) )
                {
                    weight += getLocationObjectsWeight(i);
                }
            }
        }
    }
    return weight;
}

function getObjectCountAt(locno)
{
    var count = 0;
    for (i=0;i<num_objects;i++)
    {
        if (getObjectLocation(i) == locno)
        {
            attr = getObjectLowAttributes(i);
            if (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)) count ++;  // Parser settings say we should include NPCs as objects
             else if (!objectIsNPC(i)) count++;     // or object is not an NPC
        }
    }
    return count;
}


function getObjectCountAtWithAttr(locno, attrnoArray)
{
    var count = 0;
    for (var i=0;i<num_objects;i++)
        if (getObjectLocation(i) == locno)
            for (var j=0;j<attrnoArray.length;j++)
                if (objectIsAttr(i, attrnoArray[j])) count++;
    return count;
}


function getNPCCountAt(locno)
{
    var count = 0;
    for (i=0;i<num_objects;i++)
        if ((getObjectLocation(i) == locno) &&  (objectIsNPC(i))) count++;
    return count;
}


// Location light function

function lightObjectsAt(locno)
{
    return getObjectCountAtWithAttr(locno, [ATTR_LIGHT]) > 0;
}


function lightObjectsPresent()
{
  if (lightObjectsAt(LOCATION_CARRIED)) return true;
  if (lightObjectsAt(LOCATION_WORN)) return true;
  if (lightObjectsAt(loc_here())) return true;
  return false;
}


function isDarkHere()
{
    return (getFlag(FLAG_LIGHT) != 0);
}

// Sound functions


function preloadsfx()
{
    for (var i=0;i<resources.length;i++)
        if (resources[i][0] == 'RESOURCE_TYPE_SND')
        {
            var fileparts = resources[i][2].split('.');
            var basename = fileparts[0];
            var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] , preload: true} );
        }
}

function sfxplay(sfxno, channelno, times, method)
{

    if (!soundsON) return;
    if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
    if (times == 0) times = -1; // more than 4000 million times
    var filename = getResourceById(RESOURCE_TYPE_SND, sfxno);
    if (filename)
    {
        var fileparts = filename.split('.');
        var basename = fileparts[0];
        var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] });
        if (soundChannels[channelno]) soundChannels[channelno].stop();
        soundLoopCount[channelno] = times;
        mySound.bind("ended", function(e) {
            for (sndloop=0;sndloop<MAX_CHANNELS;sndloop++)
                if (soundChannels[sndloop] == this)
                {
                    if (soundLoopCount[sndloop]==-1) {this.play(); return }
                    soundLoopCount[sndloop]--;
                    if (soundLoopCount[sndloop] > 0) {this.play(); return }
                    sfxstop(sndloop);
                    return;
                }
        });
        soundChannels[channelno] = mySound;
        if (method=='play') mySound.play(); else mySound.fadeIn(2000);
    }
}

function playLocationMusic(locno)
{
    if (soundsON)
        {
            sfxstop(0);
            sfxplay(locno, 0, 0, 'play');
        }
}

function musicplay(musicno, times)
{
    sfxplay(musicno, 0, times);
}

function channelActive(channelno)
{
    if (soundChannels[channelno]) return true; else return false;
}


function sfxstopall()
{
    for (channelno=0;channelno<MAX_CHANNELS;channelno++) sfxstop(channelno);

}


function sfxstop(channelno)
{
    if (soundChannels[channelno])
        {
            soundChannels[channelno].unbind('ended');
            soundChannels[channelno].stop();
            soundChannels[channelno] = null;
        }
}

function sfxvolume(channelno, value)
{
    if (soundChannels[channelno]) soundChannels[channelno].setVolume(Math.floor( value * 100 / 65535)); // Inherited volume condact uses a number among 0 and 65535, buzz library uses 0-100.
}

function isSFXPlaying(channelno)
{
    if (!soundChannels[channelno]) return false;
    return true;
}


function sfxfadeout(channelno, value)
{
    if (!soundChannels[channelno]) return;
    soundChannels[channelno].fadeOut(value, function() { sfxstop(channelno) });
}

// *** Process functions ***

function callProcess(procno)
{
    if (inEND) return;
    current_process = procno;
    var prostr = procno.toString();
    while (prostr.length < 3) prostr = "0" + prostr;
    if (procno==0) in_response = true;
    if (doall_flag && in_response) done_flag = false;
    if (!in_response) done_flag = false;
    h_preProcess(procno);
    eval("pro" + prostr + "()");
    h_postProcess(procno);
    if (procno==0) in_response = false;
}

// Bitwise functions

function bittest(value, bitno)
{
    mask = 1 << bitno;
    return ((value & mask) != 0);
}

function bitset(value, bitno)
{

    mask = 1 << bitno;
    return value | mask;
}

function bitclear(value, bitno)
{
    mask = 1 << bitno;
    return value & (~mask);
}


function bitneg(value, bitno)
{
    mask = 1 << bitno;
    return value ^ mask;

}

// Savegame functions
function getSaveGameObject()
{
    var savegame_object = new Object();
    // Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
    savegame_object.flags = flags.slice();
    savegame_object.objectsLocation = objectsLocation.slice();
    savegame_object.objectsWeight = objectsWeight.slice();
    savegame_object.objectsAttrLO = objectsAttrLO.slice();
    savegame_object.objectsAttrHI = objectsAttrHI.slice();
    savegame_object.connections = connections.slice();
    savegame_object.last_player_orders = last_player_orders.slice();
    savegame_object.last_player_orders_pointer = last_player_orders_pointer;
    savegame_object.transcript = transcript;
    savegame_object = h_saveGame(savegame_object);
    return savegame_object;
}

function restoreSaveGameObject(savegame_object)
{
    flags = savegame_object.flags;
    // Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
    objectsLocation = savegame_object.objectsLocation.slice();
    objectsWeight = savegame_object.objectsWeight.slice();
    objectsAttrLO = savegame_object.objectsAttrLO.slice();
    objectsAttrHI = savegame_object.objectsAttrHI.slice();
    connections = savegame_object.connections.slice();
    last_player_orders = savegame_object.last_player_orders.slice();
    last_player_orders_pointer = savegame_object.last_player_orders_pointer;
    transcript = savegame_object.transcript;
    h_restoreGame(savegame_object);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        The parser                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadPronounSufixes()
{

    var swapped;

    for (var j=0;j<vocabulary.length;j++) if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_PRONOUN)
             pronoun_suffixes.push(vocabulary[j][VOCABULARY_WORD]);
    // Now sort them so the longest are first, so you rather replace SELOS in (COGESELOS=>COGE SELOS == >TAKE THEM) than LOS (COGESELOS==> COGESE LOS ==> TAKExx THEM) that woul not be understood (COGESE is not a verb, COGE is)
    do {
        swapped = false;
        for (var i=0; i < pronoun_suffixes.length-1; i++)
        {
            if (pronoun_suffixes[i].length < pronoun_suffixes[i+1].length)
            {
                var temp = pronoun_suffixes[i];
                pronoun_suffixes[i] = pronoun_suffixes[i+1];
                pronoun_suffixes[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}


function findVocabulary(word, forceDisableLevenshtein)
{
    // Pending: in general this function is not very efficient. A solution where the vocabulary array is sorted by word so the first search can be binary search
    //          and possible typos are precalculated, so the distance is a lookup table instead of a function, would be much more efficient. On the other hand,
    //          the current solution is fast enough with a 1000+ words game that I don't consider improving this function to have high priority now.

    // Search word in vocabulary
    for (var j=0;j<vocabulary.length;j++)
        if (vocabulary[j][VOCABULARY_WORD] == word)
             return vocabulary[j];

    if (forceDisableLevenshtein) return null;

    if (word.length <=4) return null; // Don't try to fix typo for words with less than 5 length

    if (bittest(getFlag(FLAG_PARSER_SETTINGS), 8)) return null; // If matching is disabled, we won't try to use levhenstein distance

    // Search words in vocabulary with a Levenshtein distance of 1
    var distance2_match = null;
    for (var k=0;k<vocabulary.length;k++)
    {
        if ([WORDTYPE_VERB,WORDTYPE_NOUN,WORDTYPE_ADJECT,WORDTYPE_ADVERB].indexOf(vocabulary[k][VOCABULARY_TYPE])  != -1 )
        {
            var distance = getLevenshteinDistance(vocabulary[k][VOCABULARY_WORD], word);
            if ((!distance2_match) && (distance==2)) distance2_match = vocabulary[k]; // Save first word with distance=2, in case we don't find any word with distance 1
            if (distance <= 1) return vocabulary[k];
        }
    }

    // If we found any word with distance 2, return it, only if word was at least 7 characters long
    if ((distance2_match) &&  (word.length >6)) return distance2_match;

    // Word not found
    return null;
}

function normalize(player_order)
// Removes accented characters and makes sure every sentence separator (colon, semicolon, quotes, etc.) has one space before and after. Also, all separators are converted to comma
{
    var originalchars = 'áéíóúäëïöüâêîôûàèìòùÁÉÍÓÚÄËÏÖÜÂÊÎÔÛÀÈÌÒÙ';
    var i;
    var output = '';
    var pos;

    for (i=0;i<player_order.length;i++)
    {
        pos = originalchars.indexOf(player_order.charAt(i));
        if (pos!=-1) output = output + "aeiou".charAt(pos % 5); else
        {
            ch = player_order.charAt(i);
                if ((ch=='.') || (ch==',') || (ch==';') || (ch=='"') || (ch=='\'') || (ch=='«') || (ch=='»')) output = output + ' , '; else output = output + player_order.charAt(i);
        }

    }
    return output;
}

function toParserBuffer(player_order)  // Converts a player order in a list of sentences separated by dot.
{
     player_order = normalize(player_order);
     player_order = player_order.toUpperCase();

     var words = player_order.split(' ');
     for (var q=0;q<words.length;q++)
     {
        words[q] = words[q].trim();
        if  (words[q]!=',')
        {
            words[q] = words[q].trim();
            foundWord = findVocabulary(words[q], false);
            if (foundWord)
            {
                if (foundWord[VOCABULARY_TYPE]==WORDTYPE_CONJUNCTION)
                {
                words[q] = ','; // Replace conjunctions with commas
                }
            }
        }
     }

     var output = '';
     for (q=0;q<words.length;q++)
     {
        if (words[q] == ',') output = output + ','; else output = output + words[q] + ' ';
     }
     output = output.replace(/ ,/g,',');
     output = output.trim();
     player_order_buffer = output;
}

function getSentencefromBuffer()
{
    var sentences = player_order_buffer.split(',');
    var result = sentences[0];
    sentences.splice(0,1);
    player_order_buffer = sentences.join();
    return result;
}

function processPronounSufixes(words)
{
    // This procedure will split pronominal sufixes into separated words, so COGELA will become COGE LA at the end, and work exactly as TAKE IT does.
    // it's only for spanish so if lang is english then it makes no changes
    if (getLang() == 'EN') return words;
    var verbFound = false;
    if (!bittest(getFlag(FLAG_PARSER_SETTINGS),0)) return words;  // If pronoun sufixes inactive, just do nothing
    // First, we clear the word list from any match with pronouns, cause if we already have something that matches pronouns, probably is just concidence, like in COGE LA LLAVE
    var filtered_words = [];
    for (var q=0;q < words.length;q++)
    {
        foundWord = findVocabulary(words[q], false);
        if (foundWord)
            {
                if (foundWord[VOCABULARY_TYPE] != WORDTYPE_PRONOUN) filtered_words[filtered_words.length] = words[q];
            }
            else filtered_words[filtered_words.length] = words[q];
    }
    words = filtered_words;

    // Now let's start trying to get sufixes
    new_words = [];
    for (var k=0;k < words.length;k++)
    {
        words[k] = words[k].trim();
        foundWord = findVocabulary(words[k], true); // true to disable Levenshtein distance applied
        if (foundWord) if (foundWord[VOCABULARY_TYPE] == WORDTYPE_VERB) verbFound = true;  // If we found a verb, we don't look for pronoun sufixes, as they have to come together with verb
        suffixFound = false;
        pronunsufix_search:
        for (var l=0;(l<pronoun_suffixes.length) && (!suffixFound) && (!verbFound);l++)
        {

            if (pronoun_suffixes[l] == words[k].rights(pronoun_suffixes[l].length))
            {
                var verb_part = words[k].substring(0,words[k].length - pronoun_suffixes[l].length);
                var checkWord = findVocabulary(verb_part, false);
                if ((!checkWord)  || (checkWord[VOCABULARY_TYPE] != WORDTYPE_VERB))  // If the part before the supposed-to-be pronoun sufix is not a verb, then is not a pronoun sufix
                {
                    new_words.push(words[k]);
                    continue pronunsufix_search;
                }
                new_words.push(verb_part);  // split the word in two parts: verb + pronoun. Since that very moment it works like in english (COGERLO ==> COGER LO as of TAKE IT)
                new_words.push(pronoun_suffixes[l]);
                suffixFound = true;
                verbFound = true;
            }
        }
        if (!suffixFound) new_words.push(words[k]);
    }
    return new_words;
}

function getLogicSentence()
{
    parser_word_found = false; ;
    aux_verb = -1;
    aux_noun1 = -1;
    aux_adject1 = -1;
    aux_adverb = -1;
    aux_pronoun = -1
    aux_pronoun_adject = -1
    aux_preposition = -1;
    aux_noun2 = -1;
    aux_adject2 = -1;
    initializeLSWords();
    SL_found = false;

    var order = getSentencefromBuffer();
    setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS),1)); // Initialize flag that says an unknown word was found in the sentence


    words = order.split(" ");
    words = processPronounSufixes(words);
    wordsearch_loop:
    for (var i=0;i<words.length;i++)
    {
        original_word = currentword = words[i];
        if (currentword.length>10) currentword = currentword.substring(0,MAX_WORD_LENGHT);
        foundWord = findVocabulary(currentword, false);
        if (foundWord)
        {
            wordtype = foundWord[VOCABULARY_TYPE];
            word_id = foundWord[VOCABULARY_ID];

            switch (wordtype)
            {
                case WORDTYPE_VERB: if (aux_verb == -1)  aux_verb = word_id;
                                    break;

                case WORDTYPE_NOUN: if (aux_noun1 == -1) aux_noun1 = word_id; else if (aux_noun2 == -1) aux_noun2 = word_id;
                                    break;

                case WORDTYPE_ADJECT: if (aux_adject1 == -1) aux_adject1 = word_id; else if (aux_adject2 == -1) aux_adject2 = word_id;
                                      break;

                case WORDTYPE_ADVERB: if (aux_adverb == -1) aux_adverb = word_id;
                                      break;

                case WORDTYPE_PRONOUN: if (aux_pronoun == -1)
                                            {
                                                aux_pronoun = word_id;
                                                if ((previous_noun != EMPTY_WORD) && (aux_noun1 == -1))
                                                {
                                                    aux_noun1 = previous_noun;
                                                    if (previous_adject != EMPTY_WORD) aux_adject1 = previous_adject;
                                                }
                                            }

                                       break;

                case WORDTYPE_CONJUNCTION: break wordsearch_loop; // conjunction or nexus. Should not appear in this function, just added for security

                case WORDTYPE_PREPOSITION: if (aux_preposition == -1) aux_preposition = word_id;
                                           if (aux_noun1!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),2));  // Set bit that determines that a preposition word was found after first noun
                                           break;
            }

            // Nouns that can be converted to verbs
            if ((aux_noun1!=-1) && (aux_verb==-1) && (aux_noun1 < NUM_CONVERTIBLE_NOUNS))
            {
                aux_verb = aux_noun1;
                aux_noun1 = -1;
            }

            if ((aux_verb==-1) && (aux_noun1!=-1) && (previous_verb!=EMPTY_WORD)) aux_verb = previous_verb;  // Support "TAKE SWORD AND SHIELD" --> "TAKE WORD AND TAKE SHIELD"

            if ((aux_verb!=-1) || (aux_noun1!=-1) || (aux_adject1!=-1 || (aux_preposition!=-1) || (aux_adverb!=-1))) SL_found = true;



        } else if (aux_verb!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),1));  // Set bit that determines that an unknown word was found after the verb
    }

    if (SL_found)
    {
        if (aux_verb != -1) setFlag(FLAG_VERB, aux_verb);
        if (aux_noun1 != -1) setFlag(FLAG_NOUN1, aux_noun1);
        if (aux_adject1 != -1) setFlag(FLAG_ADJECT1, aux_adject1);
        if (aux_adverb != -1) setFlag(FLAG_ADVERB, aux_adverb);
        if (aux_pronoun != -1)
            {
                setFlag(FLAG_PRONOUN, aux_noun1);
                setFlag(FLAG_PRONOUN_ADJECT, aux_adject1);
            }
            else
            {
                setFlag(FLAG_PRONOUN, EMPTY_WORD);
                setFlag(FLAG_PRONOUN_ADJECT, EMPTY_WORD);
            }
        if (aux_preposition != -1) setFlag(FLAG_PREP, aux_preposition);
        if (aux_noun2 != -1) setFlag(FLAG_NOUN2, aux_noun2);
        if (aux_adject2 != -1) setFlag(FLAG_ADJECT2, aux_adject2);
        setReferredObject(getReferredObject());
        previous_verb = aux_verb;
        if ((aux_noun1!=-1) && (aux_noun1>=NUM_PROPER_NOUNS))
        {
            previous_noun = aux_noun1;
            if (aux_adject1!=-1) previous_adject = aux_adject1;
        }

    }
    if ((aux_verb + aux_noun1+ aux_adject1 + aux_adverb + aux_pronoun + aux_preposition + aux_noun2 + aux_adject2) != -8) parser_word_found = true;

    return SL_found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        Main functions and main loop                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Interrupt functions

function enableInterrupt()
{
    interruptDisabled = false;
}

function disableInterrupt()
{
    interruptDisabled = true;
}

function timer()
{
    // Timeout control
    timeout_progress=  timeout_progress + 1/32;  //timer happens every 40 milliseconds, but timeout counter should only increase every 1.28 seconds (according to PAWS documentation)
    timeout_length = getFlag(FLAG_TIMEOUT_LENGTH);
    if ((timeout_length) && (timeout_progress> timeout_length))  // time for timeout
    {
        timeout_progress = 0;
        if (($('.prompt').val() == '')  || (($('.prompt').val()!='') && (!bittest(getFlag(FLAG_TIMEOUT_SETTINGS),0))) )  // but first check there is no text type, or is allowed to timeout when text typed already
        {
            setFlag(FLAG_TIMEOUT_SETTINGS, bitset(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
            writeSysMessage(SYSMESS_TIMEOUT);
            callProcess(PROCESS_TURN);
        }
    }

    // PAUSE condact control
    if (inPause)
    {
        pauseRemainingTime = pauseRemainingTime - 40; // every tick = 40 milliseconds
        if (pauseRemainingTime<=0)
        {
            inPause = false;
            hideAnykeyLayer();
            waitKeyCallback()
        }
    }

    // Interrupt process control
    if (!interruptDisabled)
    if (interruptProcessExists)
    {
        callProcess(interrupt_proc);
        setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened
    }

}

// Initialize and finalize functions

function farewell()
{
    writeSysMessage(SYSMESS_FAREWELL);
    ACCnewline();
}


function initializeConnections()
{
  connections = [].concat(connections_start);
}

function initializeObjects()
{
  for (i=0;i<objects.length;i++)
  {
    objectsAttrLO = [].concat(objectsAttrLO_start);
    objectsAttrHI = [].concat(objectsAttrHI_start);
    objectsLocation = [].concat(objectsLocation_start);
    objectsWeight = [].concat(objectsWeight_start);
  }
}

function  initializeLSWords()
{
  setFlag(FLAG_PREP,EMPTY_WORD);
  setFlag(FLAG_NOUN2,EMPTY_WORD);
  setFlag(FLAG_ADJECT2,EMPTY_WORD);
  setFlag(FLAG_PRONOUN,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_VERB,EMPTY_WORD);
  setFlag(FLAG_NOUN1,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_ADVERB,EMPTY_WORD);
}


function initializeFlags()
{
  flags = [];
  for (var  i=0;i<FLAG_COUNT;i++) flags.push(0);
  setFlag(FLAG_MAXOBJECTS_CARRIED,4);
  setFlag(FLAG_PARSER_SETTINGS,9); // Pronoun sufixes active, DOALL and others ignore NPCs, etc. 00001001
  setFlag(FLAG_MAXWEIGHT_CARRIED,10);
  initializeLSWords();
  setFlag(FLAG_OBJECT_LIST_FORMAT,64); // List objects in a single sentence (comma separated)
  setFlag(FLAG_OBJECTS_CARRIED_COUNT,carried_objects);  // FALTA: el compilador genera esta variable, hay que cambiarlo en el compilador, ERA numero_inicial_de_objetos_llevados
}

function initializeInternalVars()
{
    num_objects = last_object_number + 1;
    transcript = '';
    timeout_progress = 0;
    previous_noun = EMPTY_WORD;
    previous_verb = EMPTY_WORD;
    previous_adject = EMPTY_WORD;
    player_order_buffer = '';
    last_player_orders = [];
    last_player_orders_pointer = 0;
    graphicsON = true;
    soundsON = true;
    interruptDisabled = false;
    unblock_process = null;
    done_flag = false;
    describe_location_flag =false;
    in_response = false;
    success = false;
    doall_flag = false;
    entry_for_doall = '';
}

function initializeSound()
{
    sfxstopall();
}




function initialize()
{
    preloadsfx();
    initializeInternalVars();
    initializeSound();
    initializeFlags();
    initializeObjects();
    initializeConnections();
}



// Main loops

function descriptionLoop()
{
    do
    {
        describe_location_flag = false;
        if (!getFlag(FLAG_MODE)) clearTextWindow();
        if ((isDarkHere()) && (!lightObjectsPresent())) writeSysMessage(SYSMESS_ISDARK); else writeLocation(loc_here());
        h_description_init();
        playLocationMusic(loc_here());
        if (loc_here()) drawPicture(loc_here()); else hideGraphicsWindow(); // Don't show picture at location 0
        ACCminus(FLAG_AUTODEC2,1);
        if (isDarkHere()) ACCminus(FLAG_AUTODEC3,1);
        if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC4,1);
        callProcess(PROCESS_DESCRIPTION);
        h_description_post();
        if (describe_location_flag) continue; // descriptionLoop() again without nesting
        describe_location_flag = false;
        callProcess(PROCESS_TURN);
        if (describe_location_flag) continue;
        describe_location_flag = false;
        focusInput();
        break; // Dirty trick to make this happen just one, but many times if descriptioLoop() should be repeated
    } while (true);

}

function orderEnteredLoop(player_order)
{
    previous_verb = EMPTY_WORD;
    setFlag(FLAG_TIMEOUT_SETTINGS, bitclear(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
    if (player_order == '') {writeSysMessage(SYSMESS_SORRY); ACCnewline(); return; };
    player_order = h_playerOrder(player_order); //hook
    copyOrderToTextWindow(player_order);
    toParserBuffer(player_order);
    do
    {
        describe_location_flag = false;
        ACCminus(FLAG_AUTODEC5,1);
        ACCminus(FLAG_AUTODEC6,1);
        ACCminus(FLAG_AUTODEC7,1);
        ACCminus(FLAG_AUTODEC8,1);
        if (isDarkHere()) ACCminus(FLAG_AUTODEC9,1);
        if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC10,1);

        if (describe_location_flag)
        {
            descriptionLoop();
            return;
        };

        if (getLogicSentence())
        {
            incTurns();
            done_flag = false;
            callProcess(PROCESS_RESPONSE); // Response table
            if (describe_location_flag)
            {
                descriptionLoop();
                return;
            };
            if (!done_flag)
            {
                if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (CNDmove(FLAG_LOCATION)))
                {
                    descriptionLoop();
                    return;
                } else if (getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) {writeSysMessage(SYSMESS_WRONGDIRECTION);ACCnewline();} else {writeSysMessage(SYSMESS_CANTDOTHAT);ACCnewline();};

            }
        } else
        {
            h_invalidOrder(player_order);
            if (parser_word_found) {writeSysMessage(SYSMESS_IDONTUNDERSTAND);   ACCnewline() }
                              else {writeSysMessage(SYSMESS_NONSENSE_SENTENCE); ACCnewline() };
        }
        callProcess(PROCESS_TURN);
    } while (player_order_buffer !='');
    previous_verb = ''; // Can't use previous verb if a new order is typed (we keep previous noun though, it can be used)
    focusInput();
}


function restart()
{
    location.reload();
}


function hideBlock()
{
    clearInputWindow();
    $('.block_layer').hide('slow');
    enableInterrupt();
    $('.input').show();
    focusInput();
}

function hideAnykeyLayer()
{
    $('.anykey_layer').hide();
    $('.input').show();
    focusInput();
}

function showAnykeyLayer()
{
    $('.anykey_layer').show();
    $('.input').hide();
}

//called when the block layer is closed
function closeBlock()
{
    if (!inBlock) return;
    inBlock = false;
    hideBlock();
    var proToCall = unblock_process;
    unblock_process = null;
    callProcess(proToCall);
    if (describe_location_flag) descriptionLoop();
}

function setInputPlaceHolder()
{
    var prompt_msg = getFlag(FLAG_PROMPT);
    if (!prompt_msg)
    {
        var random = Math.floor((Math.random()*100));
        if (random<30) prompt_msg = SYSMESS_PROMPT0; else
        if ((random>=30) && (random<60)) prompt_msg = SYSMESS_PROMPT1; else
        if ((random>=60) && (random<90)) prompt_msg = SYSMESS_PROMPT2; else
        if (random>=90) prompt_msg = SYSMESS_PROMPT3;
    }
    $('.prompt').attr('placeholder', $('<div>'+getSysMessageText(prompt_msg).replace(/(?:<br>)*$/,'').replace( /<br>/g, ', ' )+'</div>').text());
}


function divTextScrollUp()
{
    var currentPos = $('.text').scrollTop();
    if (currentPos>=DIV_TEXT_SCROLL_STEP) $('.text').scrollTop(currentPos - DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop(0);
}

function divTextScrollDown()
{
    var currentPos = $('.text').scrollTop();
    if (currentPos <= ($('.text')[0].scrollHeight - DIV_TEXT_SCROLL_STEP)) $('.text').scrollTop(currentPos + DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop($('.text')[0].scrollHeight);
}

// Autocomplete functions

function predictiveText(currentText)
{
    if (currentText == '') return currentText;
    var wordToComplete;
    var words = currentText.split(' ');
    if (autocompleteStep!=0) wordToComplete = autocompleteBaseWord; else wordToComplete = words[words.length-1];
    words[words.length-1] = completedWord(wordToComplete);
    return words.join(' ');
}


function initAutoComplete()
{
    for (var j=0;j<vocabulary.length;j++)
        if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_VERB)
            if (vocabulary[j][VOCABULARY_WORD].length >= 3)
                autocomplete.push(vocabulary[j][VOCABULARY_WORD].toLowerCase());
}

function addToAutoComplete(sentence)
{
    var words = sentence.split(' ');
    for (var i=0;i<words.length;i++)
    {
        var finalWord = '';
        for (var j=0;j<words[i].length;j++)
        {
            var c = words[i][j].toLowerCase();
            if ("abcdefghijklmnopqrstuvwxyzáéíóúàèìòùçäëïÖüâêîôû".indexOf(c) != -1) finalWord = finalWord + c;
            else break;
        }

        if (finalWord.length>=3)
        {
            var index = autocomplete.indexOf(finalWord);
            if (index!=-1) autocomplete.splice(index,1);
            autocomplete.push(finalWord);
        }
    }
}

function completedWord(word)
{
    if (word=='') return '';
   autocompleteBaseWord  =word;
   var foundCount = 0;
   for (var i = autocomplete.length-1;i>=0; i--)
   {
      if (autocomplete[i].length > word.length)
         if (autocomplete[i].indexOf(word)==0)
            {
                foundCount++;
                if (foundCount>autocompleteStep)
                {
                    autocompleteStep++;
                    return autocomplete[i];
                }
            }
   }
   return word;
}


// Exacution starts here, called by the html file on document.ready()
function start()
{
    h_init(); //hook
    $('.graphics').addClass('half_graphics');
    $('.text').addClass('half_text');
    if (isBadIE()) alert(STR_BADIE)
    loadPronounSufixes();
    setInputPlaceHolder();
    initAutoComplete();

    // Assign keypress action for input box (detect enter key press)
    $('.prompt').keypress(function(e) {
        if (e.which == 13)
        {
            setInputPlaceHolder();
            player_order = $('.prompt').val();
            if (player_order.charAt(0) == '#')
            {
                addToTranscript(player_order + STR_NEWLINE);
                clearInputWindow();
            }
            else
            if (player_order!='')
                    orderEnteredLoop(player_order);
        }
    });

    // Assign arrow up key press to recover last order
    $('.prompt').keyup( function(e) {
        if (e.which  == 38) $('.prompt').val(get_prev_player_order());
        if (e.which  == 40) $('.prompt').val(get_next_player_order());
    });


    // Assign tab keydown to complete word
    $('.prompt').keydown( function(e) {
        if (e.which == 9)
            {
                $('.prompt').val(predictiveText($('.prompt').val()));
                e.preventDefault();
            } else
            {
                autocompleteStep = 0;
                autocompleteBaseWord = ''; // Any keypress other than tab resets the autocomplete feature
            }
    });

    //Detect resize to change flag 12
     $(window).resize(function () {
        setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened
        clearPictureAt();
        return;
     });


     // assign any click on block layer --> close it
     $(document).click( function(e) {

    // if waiting for END response
    if (inEND)
    {
        restart();
        return;
    }

        if (inBlock)
        {
            closeBlock();
            e.preventDefault();
            return;
        }

        if (inAnykey)  // return for ANYKEY, accepts mouse click
        {
            inAnykey = false;
            hideAnykeyLayer();
            waitKeyCallback();
            e.preventDefault();
            return;
        }

     });

     //Make tap act as click
    document.addEventListener('touchstart', function(e) {$(document).click(); }, false);


    $(document).keydown(function(e) {

        if (!h_keydown(e)) return; // hook

        // if waiting for END response
        if (inEND)
        {
            var endYESresponse = getSysMessageText(SYSMESS_YES);
            var endNOresponse = getSysMessageText(SYSMESS_NO);
            if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
            if (!endNOresponse.length) endNOresponse = 'N';
            var endYESresponseCode = endYESresponse.charCodeAt(0);
            var endNOresponseCode = endNOresponse.charCodeAt(0);

            switch ( e.keyCode )
            {
                case endYESresponseCode:
                case 13: // return
                case 32: // space
                    location.reload();
                    break;
                case endNOresponseCode:
                    inEND = false;
                    sfxstopall();
                    $('body').hide('slow');
                    break;
            }
            return;
        }


        // if waiting for QUIT response
        if (inQUIT)
        {
            var endYESresponse = getSysMessageText(SYSMESS_YES);
            var endNOresponse = getSysMessageText(SYSMESS_NO);
            if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
            if (!endNOresponse.length) endNOresponse = 'N';
            var endYESresponseCode = endYESresponse.charCodeAt(0);
            var endNOresponseCode = endNOresponse.charCodeAt(0);

            switch ( e.keyCode )
            {
                case endYESresponseCode:
                case 13: // return
                case 32: // space
                    inQUIT=false;
                    e.preventDefault();
                    waitKeyCallback();
                    return;
                case endNOresponseCode:
                    inQUIT=false;
                    waitkey_callback_function.pop();
                    hideAnykeyLayer();
                    e.preventDefault();
                    break;
            }
        }

        // ignore uninteresting keys
        switch ( e.keyCode )
        {
            case 9:  // tab   \ keys used during
            case 13: // enter / keyboard navigation
            case 16: // shift
            case 17: // ctrl
            case 18: // alt
            case 20: // caps lock
            case 91: // left Windows key
            case 92: // left Windows key
            case 93: // left Windows key
            case 225: // right alt
                // do not focus the input - the user was probably doing something else
                // (e.g. alt-tab'ing to another window)
                return;
        }


        if (inGetkey)  // return for getkey
        {
            setFlag(getkey_return_flag, e.keyCode);
            getkey_return_flag = null;
            inGetkey = false;
            hideAnykeyLayer();
            e.preventDefault();
            waitKeyCallback();
            return;
        }

        // Scroll text window using PgUp/PgDown
        if (e.keyCode==33)  // PgUp
        {
            divTextScrollUp();
            e.preventDefault();
            return;
        }
        if (e.keyCode==34)  // PgDown
        {
            divTextScrollDown();
            return;
        }


        if (inAnykey)  // return for anykey
        {
            inAnykey = false;
            hideAnykeyLayer();
            e.preventDefault();
            waitKeyCallback();
            return;
        }

        // if keypress and block displayed, close it
        if (inBlock)
            {
                closeBlock();
                e.preventDefault();
                return;
            }


        // if ESC pressed and transcript layer visible, close it
        if ((inTranscript) &&  (e.keyCode == 27))
            {
                $('.transcript_layer').hide();
                inTranscript = false;
                e.preventDefault();
                return;
            }

    // focus the input if the user is likely to expect it
    // (but not if they're e.g. ctrl+c'ing some text)
    switch ( e.keyCode )
    {
        case 8: // backspace
        case 9: // tab
        case 13: // enter
            break;
        default:
            if ( !e.ctrlKey && !e.altKey ) focusInput();
    }

    });


    $(document).bind('wheel mousewheel',function(e)
    {
        if((e.originalEvent.wheelDelta||-e.originalEvent.deltaY) > 0) divTextScrollUp(); else divTextScrollDown();
    });


    initialize();
    descriptionLoop();
    focusInput();

    h_post();  //hook

    // Start interrupt process
    setInterval( timer, TIMER_MILLISECONDS );

}

$('document').ready(
    function ()
    {
        start();
    }
    );

// VOCABULARY

vocabulary = [];
vocabulary.push([32, "0XC", 1]);
vocabulary.push([31, "0YB", 1]);
vocabulary.push([29, "0ZA", 1]);
vocabulary.push([37, "1", 1]);
vocabulary.push([42, "1922", 1]);
vocabulary.push([36, "2", 1]);
vocabulary.push([35, "3", 1]);
vocabulary.push([34, "4", 1]);
vocabulary.push([33, "9XC", 1]);
vocabulary.push([30, "9YB", 1]);
vocabulary.push([28, "9ZA", 1]);
vocabulary.push([2, "AND", 5]);
vocabulary.push([58, "ASK", 0]);
vocabulary.push([44, "ATTA", 0]);
vocabulary.push([85, "BACT", 1]);
vocabulary.push([27, "BAST", 0]);
vocabulary.push([66, "BELT", 0]);
vocabulary.push([48, "BIBL", 1]);
vocabulary.push([79, "BICY", 1]);
vocabulary.push([79, "BIKE", 1]);
vocabulary.push([93, "BILL", 1]);
vocabulary.push([64, "BOLT", 1]);
vocabulary.push([82, "BOOK", 1]);
vocabulary.push([55, "BOOT", 1]);
vocabulary.push([85, "BOTT", 1]);
vocabulary.push([71, "BUS", 1]);
vocabulary.push([74, "BUTT", 1]);
vocabulary.push([112, "CALL", 0]);
vocabulary.push([46, "CAN", 1]);
vocabulary.push([100, "CARR", 0]);
vocabulary.push([88, "CASE", 1]);
vocabulary.push([9, "CLIM", 0]);
vocabulary.push([68, "COIL", 1]);
vocabulary.push([84, "COMP", 1]);
vocabulary.push([60, "CONN", 0]);
vocabulary.push([27, "CUNT", 0]);
vocabulary.push([79, "CYCL", 1]);
vocabulary.push([10, "D", 0]);
vocabulary.push([96, "DAVE", 1]);
vocabulary.push([41, "DIAL", 0]);
vocabulary.push([63, "DOOR", 1]);
vocabulary.push([10, "DOWN", 0]);
vocabulary.push([101, "DR", 0]);
vocabulary.push([70, "DRIV", 0]);
vocabulary.push([101, "DROP", 0]);
vocabulary.push([3, "E", 0]);
vocabulary.push([3, "EAST", 0]);
vocabulary.push([78, "EAT", 0]);
vocabulary.push([83, "ELEC", 1]);
vocabulary.push([99, "ENLI", 0]);
vocabulary.push([11, "ENTE", 0]);
vocabulary.push([51, "EXAM", 0]);
vocabulary.push([12, "EXIT", 0]);
vocabulary.push([81, "FEE", 1]);
vocabulary.push([81, "FEED", 0]);
vocabulary.push([44, "FIGH", 0]);
vocabulary.push([72, "FILL", 0]);
vocabulary.push([56, "FIX", 0]);
vocabulary.push([77, "FOOD", 1]);
vocabulary.push([34, "FOUR", 1]);
vocabulary.push([27, "FUCK", 0]);
vocabulary.push([89, "GENE", 1]);
vocabulary.push([85, "GERM", 1]);
vocabulary.push([100, "GET", 0]);
vocabulary.push([25, "GIVE", 0]);
vocabulary.push([90, "GLOV", 1]);
vocabulary.push([76, "GO", 1]);
vocabulary.push([75, "GREE", 1]);
vocabulary.push([65, "HELP", 0]);
vocabulary.push([52, "HIST", 0]);
vocabulary.push([66, "HIT", 0]);
vocabulary.push([104, "I", 0]);
vocabulary.push([70, "IGNI", 0]);
vocabulary.push([11, "IN", 0]);
vocabulary.push([65, "INFO", 0]);
vocabulary.push([60, "INSE", 0]);
vocabulary.push([55, "INSU", 1]);
vocabulary.push([104, "INVE", 0]);
vocabulary.push([83, "JACK", 1]);
vocabulary.push([13, "JUMP", 0]);
vocabulary.push([87, "KEY", 1]);
vocabulary.push([44, "KILL", 0]);
vocabulary.push([66, "KNOC", 0]);
vocabulary.push([105, "L", 0]);
vocabulary.push([90, "LADI", 1]);
vocabulary.push([13, "LEAP", 0]);
vocabulary.push([98, "LEAV", 0]);
vocabulary.push([108, "LOAD", 0]);
vocabulary.push([105, "LOOK", 0]);
vocabulary.push([56, "MEND", 0]);
vocabulary.push([86, "META", 1]);
vocabulary.push([88, "MONE", 1]);
vocabulary.push([1, "N", 0]);
vocabulary.push([5, "NE", 0]);
vocabulary.push([1, "NORT", 0]);
vocabulary.push([6, "NW", 0]);
vocabulary.push([75, "OFF", 1]);
vocabulary.push([76, "ON", 1]);
vocabulary.push([37, "ONE", 1]);
vocabulary.push([62, "OPEN", 0]);
vocabulary.push([12, "OUT", 0]);
vocabulary.push([43, "OVER", 1]);
vocabulary.push([43, "PAGE", 1]);
vocabulary.push([74, "PANE", 1]);
vocabulary.push([80, "PART", 0]);
vocabulary.push([46, "PETR", 1]);
vocabulary.push([110, "PICT", 0]);
vocabulary.push([27, "PISS", 0]);
vocabulary.push([83, "PLUG", 1]);
vocabulary.push([82, "POCK", 1]);
vocabulary.push([73, "PRES", 0]);
vocabulary.push([71, "PULL", 1]);
vocabulary.push([73, "PUSH", 0]);
vocabulary.push([106, "Q", 0]);
vocabulary.push([106, "QUIT", 0]);
vocabulary.push([105, "R", 0]);
vocabulary.push([47, "READ", 0]);
vocabulary.push([99, "RECR", 0]);
vocabulary.push([76, "RED", 1]);
vocabulary.push([105, "REDE", 0]);
vocabulary.push([102, "REMO", 0]);
vocabulary.push([56, "REPA", 0]);
vocabulary.push([108, "REST", 0]);
vocabulary.push([68, "ROPE", 1]);
vocabulary.push([2, "S", 0]);
vocabulary.push([94, "SAM", 1]);
vocabulary.push([107, "SAVE", 0]);
vocabulary.push([58, "SAY", 0]);
vocabulary.push([26, "SCOR", 0]);
vocabulary.push([49, "SCRE", 1]);
vocabulary.push([7, "SE", 0]);
vocabulary.push([50, "SEAR", 0]);
vocabulary.push([27, "SHIT", 0]);
vocabulary.push([44, "SLAY", 0]);
vocabulary.push([53, "SLEE", 0]);
vocabulary.push([2, "SOUT", 0]);
vocabulary.push([69, "SPAN", 1]);
vocabulary.push([70, "STAR", 0]);
vocabulary.push([97, "STAT", 0]);
vocabulary.push([111, "STOR", 0]);
vocabulary.push([86, "STRI", 1]);
vocabulary.push([95, "SUE", 1]);
vocabulary.push([88, "SUIT", 1]);
vocabulary.push([8, "SW", 0]);
vocabulary.push([13, "SWIN", 0]);
vocabulary.push([92, "SYLV", 1]);
vocabulary.push([100, "T", 0]);
vocabulary.push([100, "TAKE", 0]);
vocabulary.push([2, "THEN", 5]);
vocabulary.push([35, "THRE", 1]);
vocabulary.push([101, "THRO", 0]);
vocabulary.push([66, "THUM", 0]);
vocabulary.push([67, "TIE", 0]);
vocabulary.push([54, "TIME", 0]);
vocabulary.push([45, "TROL", 1]);
vocabulary.push([73, "TURN", 0]);
vocabulary.push([36, "TWO", 1]);
vocabulary.push([40, "TYPE", 0]);
vocabulary.push([9, "U", 0]);
vocabulary.push([102, "UNDO", 0]);
vocabulary.push([102, "UNFA", 0]);
vocabulary.push([62, "UNLO", 0]);
vocabulary.push([9, "UP", 0]);
vocabulary.push([203, "VERS", 0]);
vocabulary.push([4, "W", 0]);
vocabulary.push([53, "WAIT", 0]);
vocabulary.push([103, "WEAR", 0]);
vocabulary.push([4, "WEST", 0]);
vocabulary.push([80, "WHO", 0]);
vocabulary.push([59, "WIND", 1]);
vocabulary.push([83, "WIRE", 1]);
vocabulary.push([83, "WIRI", 1]);
vocabulary.push([109, "WORD", 0]);
vocabulary.push([32, "XC0", 1]);
vocabulary.push([33, "XC9", 1]);
vocabulary.push([31, "YB0", 1]);
vocabulary.push([30, "YB9", 1]);
vocabulary.push([39, "YES", 1]);
vocabulary.push([29, "ZA0", 1]);
vocabulary.push([28, "ZA9", 1]);



// SYS MESSAGES

total_sysmessages=69;

sysmessages = [];

sysmessages[0] = "Everything is dark!<br>";
sysmessages[1] = "Close to hand you can also see<br>";
sysmessages[2] = "\nWhat Next?<br>";
sysmessages[3] = "\nWhat Next?<br>";
sysmessages[4] = "\nWhat Now?<br>";
sysmessages[5] = "Please type in your orders";
sysmessages[6] = "I nearly understand..<br>";
sysmessages[7] = "you can't go that way.<br>";
sysmessages[8] = "Please be more specific.<br>";
sysmessages[9] = "You have with you:-";
sysmessages[10] = "(worn)";
sysmessages[11] = "Nothing.<br>";
sysmessages[12] = "{CLASS|center|REALLY QUIT?}<br>";
sysmessages[13] = "{CLASS|center|END OF GAME \nANOTHER GO? (Y/N)}<br>";
sysmessages[14] = "Good bye...<br>";
sysmessages[15] = "OK<br>";
sysmessages[16] = "MORE...<br>";
sysmessages[17] = "You have typed ";
sysmessages[18] = " turn";
sysmessages[19] = "s";
sysmessages[20] = ".<br>";
sysmessages[21] = "You have scored ";
sysmessages[22] = " out of 1OO.<br>";
sysmessages[23] = "You're not wearing it.<br>";
sysmessages[24] = "Your hands are full.<br>";
sysmessages[25] = "You already have it!<br>";
sysmessages[26] = "Go and find it then!<br>";
sysmessages[27] = "You can't carry any more!<br>";
sysmessages[28] = "You don't have it!<br>";
sysmessages[29] = "You ARE wearing it!<br>";
sysmessages[30] = "Y<br>";
sysmessages[31] = "N<br>";
sysmessages[32] = "More...<br>";
sysmessages[33] = "><br>";
sysmessages[34] = "<br>";
sysmessages[35] = "Time passes...<br>";
sysmessages[36] = "";
sysmessages[37] = "";
sysmessages[38] = "";
sysmessages[39] = "";
sysmessages[40] = "Please be more specific.<br>";
sysmessages[41] = "Please be more specific.<br>";
sysmessages[42] = "Your hands are full.<br>";
sysmessages[43] = "You can't carry any more!<br>";
sysmessages[44] = "You put {OREF} into<br>";
sysmessages[45] = "{OREF} is not into<br>";
sysmessages[46] = "<br>";
sysmessages[47] = "<br>";
sysmessages[48] = "<br>";
sysmessages[49] = "You don't have it!<br>";
sysmessages[50] = "Please be more specific.<br>";
sysmessages[51] = ".<br>";
sysmessages[52] = "That is not into<br>";
sysmessages[53] = "nothing at all<br>";
sysmessages[54] = "File not found.<br>";
sysmessages[55] = "File corrupt.<br>";
sysmessages[56] = "I/O error. File not saved.<br>";
sysmessages[57] = "Directory full.<br>";
sysmessages[58] = "Please enter savegame name you used when saving the game status.";
sysmessages[59] = "Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>";
sysmessages[60] = "Please enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.";
sysmessages[61] = "<br>";
sysmessages[62] = "Sorry? Please try other words.<br>";
sysmessages[63] = "Here<br>";
sysmessages[64] = "you can see<br>";
sysmessages[65] = "you can see<br>";
sysmessages[66] = "inside you see<br>";
sysmessages[67] = "on top you see<br>";
sysmessages[68] = "(extra message added so the newlines below aren't included in the message above)\n\n\n\n\n\n";

// USER MESSAGES

total_messages=14;

messages = [];

messages[1000] = "Exits: ";
messages[1001] = "You can't see any exits\n";
messages[1003] = "{ACTION|nort|nort}";
messages[1004] = "{ACTION|sout|sout}";
messages[1005] = "{ACTION|east|east}";
messages[1006] = "{ACTION|west|west}";
messages[1007] = "{ACTION|ne|ne}";
messages[1008] = "{ACTION|nw|nw}";
messages[1009] = "{ACTION|se|se}";
messages[1010] = "{ACTION|sw|sw}";
messages[1011] = "{ACTION|up|up}";
messages[1012] = "{ACTION|down|down}";
messages[1014] = "{ACTION|out|out}";
messages[1015] = "(extra message added so the newlines below aren't included in the message above)\n\n\n\n\n\n";

// WRITE MESSAGES

total_writemessages=385;

writemessages = [];

writemessages[0] = "RESPONSE_START";
writemessages[1] = "RESPONSE_USER";
writemessages[2] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[3] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[4] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[5] = "No one here can drive the bus!";
writemessages[6] = "It looks a long way-I'd get some transport if I were you!";
writemessages[7] = "It looks a long way-I'd get some transport if I were you!";
writemessages[8] = "It looks a long way-I'd get some transport if I were you!";
writemessages[9] = "It looks a long way-I'd get some transport if I were you!";
writemessages[10] = "It looks a long way-I'd get some transport if I were you!";
writemessages[11] = "It looks a long way-I'd get some transport if I were you!";
writemessages[12] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[13] = "You're not strong enough!";
writemessages[14] = "The mini-bus is out of gas!";
writemessages[15] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[16] = "The trolley won't fit through the door!";
writemessages[17] = "The trolley won't fit through the door!";
writemessages[18] = "No one here can drive the bus!";
writemessages[19] = "The mini-bus is out of gas!";
writemessages[20] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[21] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[22] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[23] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[24] = "&#34;I'll stay here!&#34;";
writemessages[25] = "&#34;UGH!I'm not going in there!&#34;";
writemessages[26] = "&#34;I'll hang around here.&#34;";
writemessages[27] = "&#34;Too cold in there for me!&#34;";
writemessages[28] = "&#34;OOH NO!I'll wait here!&#34;";
writemessages[29] = "The door is locked";
writemessages[30] = "It looks a long way-I'd get some transport if I were you!";
writemessages[31] = "It looks a long way-I'd get some transport if I were you!";
writemessages[32] = "It looks a long way-I'd get some transport if I were you!";
writemessages[33] = "It looks a long way-I'd get some transport if I were you!";
writemessages[34] = "It looks a long way-I'd get some transport if I were you!";
writemessages[35] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[36] = "You're not strong enough!";
writemessages[37] = "The mini-bus is out of gas!";
writemessages[38] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[39] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[40] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[41] = "The mad-man blocks you!";
writemessages[42] = "You move along the road..";
writemessages[43] = "You move along the road on the bicycle..";
writemessages[44] = "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt.";
writemessages[45] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[46] = "No one here can drive the bus!";
writemessages[47] = "&#34;I'm not going in there-it's dark in there!&#34;";
writemessages[48] = "It looks a long way-I'd get some transport if I were you!";
writemessages[49] = "The door is locked";
writemessages[50] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[51] = "&#34;GET BACK TO THE CAMP!&#34;A fight breaks out,but you are overcome!";
writemessages[52] = "You fall to your death!";
writemessages[53] = "You move along the road..";
writemessages[54] = "You move along the road on the bicycle..";
writemessages[55] = "The mini-bus is out of gas!";
writemessages[56] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[57] = "No one here can drive the bus!";
writemessages[58] = "&#34;I'm not going in there-it's dark in there!&#34;";
writemessages[59] = "It's bolted shut!";
writemessages[60] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[61] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[62] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[63] = "No one here can drive the bus!";
writemessages[64] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[65] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[66] = "No one here can drive the bus!";
writemessages[67] = "Go on then-but don't go making a nuisance of yourself!";
writemessages[68] = "Go on then-but don't go making a nuisance of yourself!";
writemessages[69] = "Go on then-but don't go making a nuisance of yourself!";
writemessages[70] = "Go on then-but don't go making a nuisance of yourself!";
writemessages[71] = "Go on then-but don't go making a nuisance of yourself!";
writemessages[72] = "Hang on!Hang on!Don't be in such a hurry-it's more than me job's worth to just let anyone in!";
writemessages[73] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[74] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[75] = "No one here can drive the bus!";
writemessages[76] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[77] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[78] = "No one here can drive the bus!";
writemessages[79] = "The cables are greased!You slip down the shaft!";
writemessages[80] = "You descend the rope!";
writemessages[81] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[82] = "I'd drop the cycle 1st!";
writemessages[83] = "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}";
writemessages[84] = "You fall to your death!";
writemessages[85] = "Please be more specific.";
writemessages[86] = "Nobody here wants it!";
writemessages[87] = "What are you cussing for?";
writemessages[88] = "That's exactly how I feel!";
writemessages[89] = "Sheesh!Bad language as well!";
writemessages[90] = "You're not the only one round here,you know!";
writemessages[91] = "REALLY!";
writemessages[92] = "Talking to ourselves now,huh?";
writemessages[93] = "MISSILES LAUNCHED..";
writemessages[94] = "--ACTION UNAVAILABLE--";
writemessages[95] = "--ACTION UNAVAILABLE--";
writemessages[96] = "MISSILES DE-ARMED";
writemessages[97] = "--ACTION UNAVAILABLE--";
writemessages[98] = "--ACTION UNAVAILABLE--";
writemessages[99] = "MISSILES LAUNCHED..";
writemessages[100] = "--ACTION UNAVAILABLE--";
writemessages[101] = "--ACTION UNAVAILABLE--";
writemessages[102] = "MISSILES DE-ARMED";
writemessages[103] = "--ACTION UNAVAILABLE--";
writemessages[104] = "--ACTION UNAVAILABLE--";
writemessages[105] = "MISSILES DE-ARMED";
writemessages[106] = "--ACTION UNAVAILABLE--";
writemessages[107] = "--ACTION UNAVAILABLE--";
writemessages[108] = "MISSILES LAUNCHED..";
writemessages[109] = "--ACTION UNAVAILABLE--";
writemessages[110] = "--ACTION UNAVAILABLE--";
writemessages[111] = "-PLEASE PROCEED SECURITY OPTION-";
writemessages[112] = "-PLEASE PROCEED SECURITY OPTION-";
writemessages[113] = "-PLEASE PROCEED SECURITY OPTION-";
writemessages[114] = "-PLEASE PROCEED SECURITY OPTION-";
writemessages[115] = "{CLASS|center|LETS PLAY GLOBAL    }   THERMONUCLEAR WARFARE...";
writemessages[116] = "Please be more specific.";
writemessages[117] = "PLEASE BE MORE SPECIFIC.";
writemessages[118] = "GREETINGS,COMMANDER WALKER.";
writemessages[119] = "   SHALL WE PLAY A GAME?";
writemessages[120] = "Sounds like interference!";
writemessages[121] = "Sounds like interference!";
writemessages[122] = "LINE DEAD";
writemessages[123] = "Don't you think there's been enough of that already?";
writemessages[124] = "..shout unto God with the voice of triumph for..he is a great king over all the Earth..       {CLASS|center|          (Psalm 47)}";
writemessages[125] = "???????????????????\n? ?? ACB Co. 2356 A-H\n? ?? D.Amri. 3578\n? ?? B.hart. 4789\n\n???????????????????";
writemessages[126] = "???????????????????\n? ?? Dr.Iril 4912 I-P\n? ?? P.Larl  1101\n? ?? S.King  00\n\n???????????????????";
writemessages[127] = "???????????????????\n? ?? T.Satim 8901 Q-Z\n? ?? M.White 9010\n? ?? F.Cross 1922\n\n???????????????????";
writemessages[128] = "You find nothing.";
writemessages[129] = "It's empty.";
writemessages[130] = "It's full.";
writemessages[131] = "     ????????????????????????\n     ??**COMPUTER COMMAND**??\n     ????????????????????????   {CLASS|center|1=HELP MENU   \nMAIN MENU             \n [2]STRIKES/MAIN SITINGS\n[3]STATUS 1ST STRIKE  \n [4]DE-ARM/*FIRE OPTIONS}";
writemessages[132] = "{CLASS|center|         HELP MENU\n         ?????????}                [1]FOLLOW MENU'S                 [2]TYPE CMNDS.";
writemessages[133] = "{CLASS|center|[MENU 2]\n}WEST COAST DEFENCE\n     LOCNO.Z VANCOUVER CODE A\n      &#34; &#34;  Z NEVADA    CODE A\n      &#34; &#34;  Y ARIZONA   CODE B EAST COAST DEFENCE\n     LOCNO.Y MISSOURI  CODE B\n      &#34; &#34;  X MINNESOTA CODE C\n      &#34; &#34;  X MEMPHIS   CODE C";
writemessages[134] = "{CLASS|center|[ MENU 3 ]\n}TARGET DE-ARM:(STRIKE ABORT)- ASSIGN LOCATION/CODE.\nTARGET LAUNCH:(DUAL-KEY OVER- RIDE):ASSIGN LOCATION/CODE.";
writemessages[135] = "{CLASS|center|          [ MENU 4 ]\n TYPE 9 TO FIRE/CODE LOCNO (?)\n TYPE 0 TO DE-ARM /LOCNO   (?)}";
writemessages[136] = "A:?";
writemessages[137] = "A:?";
writemessages[138] = "B:?";
writemessages[139] = "C:?";
writemessages[140] = "C:?";
writemessages[141] = "B:?";
writemessages[142] = "GREETINGS,COMMANDER WALKER.";
writemessages[143] = "   SHALL WE PLAY A GAME?";
writemessages[144] = "Insulated against shocks.";
writemessages[145] = "2 Buttons:- ON + OFF";
writemessages[146] = "CMDR.C.WALKER.";
writemessages[147] = "3 dual leads.";
writemessages[148] = "A heavy-duty military Dual drive system,it has 3 edge connectors, labelled:SCREEN:POWER:MODEM.";
writemessages[149] = "A heavy-duty military Dual drive system,it has 3 edge connectors, labelled:SCREEN:POWER:MODEM.";
writemessages[150] = "A heavy-duty military Dual drive system,it has 3 edge connectors, labelled:SCREEN:POWER:MODEM.";
writemessages[151] = "A highly dangerous bacteria, developed at the cost of millions of dollars to kill mass amounts of people:it seems to have been very effective.";
writemessages[152] = "You notice nothing unusual.";
writemessages[153] = "I've been waiting for somebody for an age!";
writemessages[154] = "There's a nuclear command centre a couple of hundred miles south where we'll have to de-arm all the bombs by command computer if we are to be free from nuclear attack!";
writemessages[155] = "I used to be with the fairground until the power went off..";
writemessages[156] = "When the plague broke me and my husband were in Mexico-we rushed back to our families-it was too late.They died some 3 weeks ago, -I headed N,passing a huge refugee camp.";
writemessages[157] = "The food will suffer from bacter infection if moved-and attack is also more likely.My brother was killed by looters in New York, stealing Hi-Fi's&#59;crazy isn't it- no electricity,but the stores are looted!";
writemessages[158] = "Please be more specific.";
writemessages[159] = "Time passes...";
writemessages[160] = "Approx 6 am.";
writemessages[161] = "Approx 10 am.";
writemessages[162] = "Approx 12 Noon.";
writemessages[163] = "Approx 5 pm.";
writemessages[164] = "Approx 9 pm.";
writemessages[165] = "Approx 12 Midnight";
writemessages[166] = "Approx 3 am.";
writemessages[167] = "Approx 5 a.m.";
writemessages[168] = "Your engineering capabilities are not high enough!";
writemessages[169] = "The generator crackles into life but you are not insulated-bolts of electricity shower you!";
writemessages[170] = "The generator takes a long time to fix,but a few of you get it in working order after a while. As you flick the switch the generator buzzes into life,the boots protecting you!";
writemessages[171] = "You don't have all the correct tools";
writemessages[172] = "You don't have all the correct tools";
writemessages[173] = "You don't have all the correct tools";
writemessages[174] = "Please be more specific.";
writemessages[175] = "We could climb down with a rope!";
writemessages[176] = "We'd fall!";
writemessages[177] = "I might be able to hold it back a little while..";
writemessages[178] = "&#34;looks repairable&#34;";
writemessages[179] = "Talking to ourselves now,huh?";
writemessages[180] = "You are told nothing of interest";
writemessages[181] = "Your technical abilities are not high enough!";
writemessages[182] = "You have no wiring!";
writemessages[183] = "To what?";
writemessages[184] = "To what?";
writemessages[185] = "Your partys' engineering ability is not high enough to ensure the use of the machine correctly!";
writemessages[186] = "The lathe turns a new key!";
writemessages[187] = "Please be more specific.";
writemessages[188] = "You open it and enter as it closes behind you..";
writemessages[189] = "You open it and enter as it closes behind you..";
writemessages[190] = "You're not strong enough!";
writemessages[191] = "The door is locked";
writemessages[192] = "You open it and enter as it closes behind you..";
writemessages[193] = "You have no key";
writemessages[194] = "You open it and enter as it closes behind you..";
writemessages[195] = "The pressurised chamber explodes as it is mechanically opened!Gas fills the room!";
writemessages[196] = "NO POWER";
writemessages[197] = "You really should watch what you are doing!";
writemessages[198] = "You have no key";
writemessages[199] = "You'll need a couple of people to move the bolts-and some kind of wrench!";
writemessages[200] = "Please be more specific.";
writemessages[201] = "HISTORY x gives recent history of a character. TIME gives time, WAIT  SLEEP passing it. FEED x will feed a character, but only if food is available at that    location. STATISTICS x will telly ou a characters skills in      engineering, technical, social strength. The larger the party, the higher the skill. This      command only works at night with a pyschic in your party. RECRUIT x will get a character into your party, LEAVE x releasing them.  PARTY gives your party. STORE   will ram save your positon,     CALL loading it back. WORDS is  for text only, PICTURES turning pictures back on. Too big a party results in stress, and          characters may leave. This is   shown by a }} symbol.";
writemessages[202] = "Sue slumps to the ground!";
writemessages[203] = "&#34;I don't know if you're just plain sexist or some kind of a moron-But I'm off!&#34;";
writemessages[204] = "..shout unto God with the voice of triumph for..he is a great king over all the Earth..       {CLASS|center|          (Psalm 47)}";
writemessages[205] = "To what?";
writemessages[206] = "No one here can drive the bus!";
writemessages[207] = "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;";
writemessages[208] = "With what?";
writemessages[209] = "It's full.";
writemessages[210] = "You fill the bus,emptying the damaged tanks.";
writemessages[211] = "The pumps are off.";
writemessages[212] = "You fill the bus and discard the can.";
writemessages[213] = "With what?";
writemessages[214] = "PUMP EMPTY.";
writemessages[215] = "Please be more specific.";
writemessages[216] = "The car hits one of the pumps and overturns!";
writemessages[217] = "OFF";
writemessages[218] = "Nothing happens.";
writemessages[219] = "OFF";
writemessages[220] = "OFF";
writemessages[221] = "OFF";
writemessages[222] = "OFF";
writemessages[223] = "ON";
writemessages[224] = "Nothing happens.";
writemessages[225] = "The car climbs slowly to the top of the dipper and stops!";
writemessages[226] = "Nothing happens.";
writemessages[227] = "ON";
writemessages[228] = "ON";
writemessages[229] = "ON";
writemessages[230] = "PUMP EMPTY.";
writemessages[231] = "Please be more specific.";
writemessages[232] = "NO POWER";
writemessages[233] = "{CLASS|center| ?????????????????????????\n ?Your party consists of:?\n ?????????????????????????}";
writemessages[234] = "{CLASS|center|Sue Youart.}";
writemessages[235] = "{CLASS|center|Sam Wieyenski.}";
writemessages[236] = "{CLASS|center|Bill price.}";
writemessages[237] = "{CLASS|center|Sylvia Wade.}";
writemessages[238] = "{CLASS|center|Dave Kellern.}";
writemessages[239] = "{CLASS|center|No-one.}";
writemessages[240] = "The food is hastily scoffed!";
writemessages[241] = "The food is hastily scoffed!";
writemessages[242] = "The food is hastily scoffed!";
writemessages[243] = "The food is hastily scoffed!";
writemessages[244] = "The food is hastily scoffed!";
writemessages[245] = "The food is hastily scoffed!";
writemessages[246] = "The food is hastily scoffed!";
writemessages[247] = "The food is hastily scoffed!";
writemessages[248] = "The food is hastily scoffed!";
writemessages[249] = "The food is hastily scoffed!";
writemessages[250] = "The food is hastily scoffed!";
writemessages[251] = "The food is hastily scoffed!";
writemessages[252] = "The food is hastily scoffed!";
writemessages[253] = "The food is hastily scoffed!";
writemessages[254] = "The food is hastily scoffed!";
writemessages[255] = "The food is hastily scoffed!";
writemessages[256] = "The food is hastily scoffed!";
writemessages[257] = "The food is hastily scoffed!";
writemessages[258] = "The food is hastily scoffed!";
writemessages[259] = "The food is hastily scoffed!";
writemessages[260] = "The food is hastily scoffed!";
writemessages[261] = "The food is hastily scoffed!";
writemessages[262] = "The food is hastily scoffed!";
writemessages[263] = "The food is hastily scoffed!";
writemessages[264] = "The food is hastily scoffed!";
writemessages[265] = "Please be more specific.";
writemessages[266] = "{CLASS|center|-The statistics are-}";
writemessages[267] = "Sylvia Wade:T 1:E 1:S 6:ST 1.";
writemessages[268] = "{CLASS|center|-The statistics are-}";
writemessages[269] = "Bill Price:T 6:E 3:S 4:ST 6.";
writemessages[270] = "{CLASS|center|-The statistics are-}";
writemessages[271] = "Sam Wieyenski:T 4:E 4:S 4:ST 4.";
writemessages[272] = "{CLASS|center|-The statistics are-}";
writemessages[273] = "Sue Youart:T 1:E 2:S 8:ST 3.";
writemessages[274] = "{CLASS|center|-The statistics are-}";
writemessages[275] = "Dave Kellern:T 2:E 6:S 1:ST 8.";
writemessages[276] = "No-one here is a capable psychic";
writemessages[277] = "Sue says that she only seems to be able to do that when night falls!";
writemessages[278] = "Goodbye-whenever you're around, don't forget to come and see me!";
writemessages[279] = "-Not in party-";
writemessages[280] = "You ever need me,just give me a call-d'you hear,now?";
writemessages[281] = "-Not in party-";
writemessages[282] = "This place looks just fine to me -thanks for everything!";
writemessages[283] = "-Not in party-";
writemessages[284] = "&#34;I've got kind of fond of you being round-come and see me OK?";
writemessages[285] = "-Not in party-";
writemessages[286] = "&#34;So long,friend-I'll be seeing you!&#34;";
writemessages[287] = "-Not in party-";
writemessages[288] = "Please be more specific.";
writemessages[289] = "-Already in party-";
writemessages[290] = "Gosh!I never knew you were into necrophilia!";
writemessages[291] = "-Already in party-";
writemessages[292] = "Gosh!I never knew you were into necrophilia!";
writemessages[293] = "-Already in party-";
writemessages[294] = "Gosh!I never knew you were into necrophilia!";
writemessages[295] = "-Already in party-";
writemessages[296] = "Gosh!I never knew you were into necrophilia!";
writemessages[297] = "-Already in party-";
writemessages[298] = "Gosh!I never knew you were into necrophilia!";
writemessages[299] = "Please be more specific.";
writemessages[300] = "You're not strong enough!";
writemessages[301] = "It's nailed down!";
writemessages[302] = "You're not strong enough!";
writemessages[303] = "Goddamnit!It took long enough to get it where it is!";
writemessages[304] = "You'll need something to put it on-it's too heavy to carry about";
writemessages[305] = "The computer slips onto the trolley.";
writemessages[306] = "Goddamnit!It took long enough to get it where it is!";
writemessages[307] = "Please be more specific.";
writemessages[308] = "Please be more specific.";
writemessages[309] = "You really should watch what you are doing!";
writemessages[310] = "Please be more specific.";
writemessages[311] = "The door falls into the shaft!";
writemessages[312] = "You'll need a couple of people to move the bolts-and some kind of wrench!";
writemessages[313] = "Please be more specific.";
writemessages[314] = "RESPONSE_DEFAULT_START";
writemessages[315] = "Please be more specific.";
writemessages[316] = "{CLASS|center|-??INVENTORY??-}";
writemessages[317] = "You have formed the nucleus of a new colony to re-build your past but still nuclear missiles lie undiscovered,a permanent threat to your new life!";
writemessages[318] = "Text - only selected.";
writemessages[319] = "Pictures and text selected.";
writemessages[320] = "VERSION A03.\nWRITTEN BY M. WHITE.\nGRAPHICS BY MARTIN RENNIE.\n(  EIGHTH DAY 1987 ).";
writemessages[321] = "RESPONSE_DEFAULT_END";
writemessages[322] = "PRO1";
writemessages[323] = "PRO2";
writemessages[324] = "The vault door is closing..";
writemessages[325] = "The door slams shut!";
writemessages[326] = "The door slams shut!";
writemessages[327] = "I can't keep the door open much longer-HURRY!";
writemessages[328] = "{CLASS|center| ?\n ???\nThe Sun is setting!}";
writemessages[329] = "{CLASS|center| ?}                ???The sun sets!";
writemessages[330] = "{CLASS|center| ?}                ???The sun sets!";
writemessages[331] = "{CLASS|center| ?\n??? \nThe sun is rising!}";
writemessages[332] = "                ? The sun has\n                ???      risen!";
writemessages[333] = "{CLASS|center| ?\n??? }Darkness looms all around-on the edges of the night sounds of movement can be heard..";
writemessages[334] = "??You can hear a phone ringing\n  in the distance! ??";
writemessages[335] = "You freeze to death!";
writemessages[336] = "You are very cold!";
writemessages[337] = "Sue comes to.&#34;Do that again and you'll regret it!&#34;";
writemessages[338] = "                ? The sun has\n                ???      risen!";
writemessages[339] = "A car is careering down the road towards the station!";
writemessages[340] = "The car hits the pumps!With a massive explosion the station erupts in a ball of flames!";
writemessages[341] = "The cables are greased!You slip down the shaft!";
writemessages[342] = "Sue screams as she falls!";
writemessages[343] = "Dave falls!";
writemessages[344] = "Sue screams as she falls!";
writemessages[345] = "Sam yells as he falls!";
writemessages[346] = "Bill falls to his death!";
writemessages[347] = "Sylvia falls!";
writemessages[348] = "CONGRATULATIONS! With the threat of nuclear destruction no longer threatening your colony,you have formed the nucleus of a bright new future,full of hope.";
writemessages[349] = "-PLEASE PROCEED SECURITY OPTION-";
writemessages[350] = "The car explodes!!";
writemessages[351] = "-PLEASE PROCEED SECURITY OPTION-";
writemessages[352] = "-PLEASE PROCEED SECURITY OPTION-";
writemessages[353] = "..SECURITY COMPUTER SHUTDOWN..";
writemessages[354] = "The computer self-destructs!";
writemessages[355] = "Flames lick about the car!";
writemessages[356] = "The car explodes!!";
writemessages[357] = "-engulfing you in flames!";
writemessages[358] = "The car explodes!!";
writemessages[359] = "Help!I'm trapped!";
writemessages[360] = "Wreckage is strewn across the ground.";
writemessages[361] = "{CLASS|center|  ??}";
writemessages[362] = "{CLASS|center|  ??}";
writemessages[363] = "{CLASS|center|  ??}";
writemessages[364] = "{CLASS|center|  ??}";
writemessages[365] = "{CLASS|center|  ??}";
writemessages[366] = "A roller-coaster car rolls down into the stalls!";
writemessages[367] = "The madman is agitated by your presence,and twitches nervously! &#34;GO AWAY!&#34;";
writemessages[368] = "The road-block is hastily given up when the bottle is seen!";
writemessages[369] = "Dave says he is very hungry!";
writemessages[370] = "Dave dies of hunger!";
writemessages[371] = "Sue says she is very hungry!";
writemessages[372] = "Sue dies of hunger!";
writemessages[373] = "Sam says he is starving!";
writemessages[374] = "Sam collapses with starvation!";
writemessages[375] = "Bill grumbles that he is hungry!";
writemessages[376] = "Bill dies from lack of food!";
writemessages[377] = "Sylvia complains that she is a little bit peckish!";
writemessages[378] = "Sylvia slumps to the ground!";
writemessages[379] = "The ledge collapses beneath your parties' weight!";
writemessages[380] = "You notice a man running across the top of the tank!";
writemessages[381] = "You hear mad laughter nearby!";
writemessages[382] = "You hear mad laughter nearby!";
writemessages[383] = "With a Tremendous roar that scorches your lungs the tanks ignite into a ball of flames, incinerating you!!";
writemessages[384] = "Sue and Sylvia calm the mad-man down.As you approach,however,he leaps past you and runs away!";

// LOCATION MESSAGES

total_location_messages=81;

locations = [];

locations[0] = "Welcome to ~ Four Minutes to Midnight ~ from Eighth Day.\n\nAfter a bacteriological disaster has wiped out 90 of the world'sp opulation, you must find five  c ompanions to establish a colonya cross the states of America to w in. Also de - arm your dead    c ountry's nuclear stockpile to  c reate a secure future for you  a nd your companions.\n                               N ote that there are many extra  c ommands involved in playing theg ame - type help for a summary  o f these extra commands.\n                               N ow press a key to play...";
locations[1] = "You are beside a large box that is marked, ~ Gasoline Pump Controls ~. A red button is marked, ~ ON ~, whilst a green button is marked, ~ OFF ~.";
locations[2] = "\n\nYou are on the forecourt of a small gasoline station beside a row of several neglected pumps. There is a small road running to the {ACTION|east|east} that seems to disappear to the horizon. The garage fronting is to the {ACTION|west|west}, the station itself being in a great state of general disrepair.";
locations[3] = "You are seated in the cab of a grubby mini - bus looking through dust covered windows.";
locations[4] = "You are on a long road leading east and {ACTION|west|west}. It stretches over a thin strip of neglected fields for what must be many miles.";
locations[5] = "You are on a small rise over a large city. The road runs west whilst a freeway runs {ACTION|south|south} to the city from where comes a disgusting, sickly sweet smell.";
locations[6] = "You are on a freeway that leads {ACTION|south|south}. A large sign reading,\n~ CITY LIMITS ONE MILE ~ hangs from a gantry over the six lane highway. Cars are slewed across the six lane highway in their hundreds. The scene of death is terrible, the smell of decay strong in the defiled air.";
locations[7] = "\n\nYou are on the corner of east 4th and 5th - the city is oddly silent and dark. A small drugstore is {ACTION|south|south} whilst the open tunnel of the main freeway cross - river tunnel is east. No lights illuminate its darkened, silent interior. Cars are jack - knifed across the roads in great numbers, the traffic discarded.";
locations[8] = "You are in the tunnel. The strong sickening smell of death rises from all around whilst cars seem to block the tunnel, the round concrete walls\nseeming to be clogged with all kinds of mechanical debris. Further {ACTION|east|east} the tunnel darkens.";
locations[9] = "You are in a looted drugstore. The plate glass window has long since been shattered,but oddly enough,large amounts of tinned food cover every wall.";
locations[10] = "You are crawling on the roof of a shattered car in the pitch black darkness-scurrying rats eyes blink in the darkness!";
locations[11] = "You are just inside the exit of the tunnel by a huge pile of rotting bodies.The tunnel seems blocked,though a futile struggle has created a hole in the debris leading {ACTION|south|south}.";
locations[12] = "You are inside one of the cars by the drivers seat.In the front seat the corpse of a middle aged man straddles the smashed wind- screen.";
locations[13] = "You are on the west side at the junction of 6th and 7th";
locations[14] = "You are {ACTION|northside|northside} of 3rd and 2nd below the blocks of {ACTION|westside|westside}.";
locations[15] = "You are {ACTION|east|east} of blocks 8th and 9th {ACTION|west|west} of {ACTION|westside|westside} and {ACTION|north|north} of the road leading to the {ACTION|south|south}";
locations[16] = "You are outside a towering department store south of 4th and 5th.A large hotel is {ACTION|east|east}. Large,tattered  drapes hang over head marked,&#34;PLACEYS DEPARTMENT STORE&#34;.";
locations[17] = "You are outside a once plush hotel.Revolving doors lead into the hotel lobby whilst the street continues {ACTION|west|west}.";
locations[18] = "You are in the lobby of the hotel.Rich red velvet trimmings cover every furnishing whilst the bodies of several well- dressed people lie scattered across the furniture.";
locations[19] = "You are north of the warehouse on the inside of the store in the womens hosiery department. Shop dummies (newly stripped), lie discarded on the floor whilst now still escalators run {ACTION|up|up} to the 1st floor marked, &#34;HARDWARE AND LOST PROPERTY&#34;. Food parcels have been torn open all over the dept.";
locations[20] = "You are on the 1st floor at the top of the escalator.Long aisles run in all directions,whilst a small lathe is below a hand written card marked,&#34;Key-cutting service $1.00 only!&#34;A lift is W.";
locations[21] = "\n\nYou are in a graffiti covered lift.Large aerosoled letters cover the rusty metal walls, whilst an open inspection hatch which looks as if it has been recently removed,is in the ceiling.";
locations[22] = "You are on the roof of the lift below a set of greased steel cables in the lift shaft.A dim light shines from above,as light shines into the shaft from the 2nd floor.The lift rocks slowly!";
locations[23] = "You are clinging to thick steel cables high above the lift and just by the open doors of the 2nd floor.The doors are open, but just out of reach.";
locations[24] = "You are on the second floor by an open lift shaft beside a long row of smashed cash registers. The tills have been levered open and emptied of all money.The run of registers continues {ACTION|west|west}.";
locations[25] = "You are at the western end of the line of registers beside lines of dirty windows.";
locations[26] = "You are south of the department by an open fire exit that leads {ACTION|up|up} onto the roof.";
locations[27] = "\n\nYou are on the roof of the store overlooking the city.Strangely silent,the towering blocks of skyscrapers and office blocks rise like a vast monument whilst the sweet smell of decay is so strong as to be sickening in its intensity.The breeze is still this high,seeming to hang over the city like an enormous shroud clinging to the now still blocks of rising concrete and steel.";
locations[28] = "You are at the northern end of a street along a small town by the entrance to a rustic school- house to the {ACTION|east|east}.The school bell rings softly as it swings back and forth in the breeze.";
locations[29] = "You are in an empty classroom below a large blackboard on which is drawn the picture of a grinning giraffe.Crudely drawn pictures cover every wall and the desks are small as if to be minute,defenceless.";
locations[30] = "You are at the southern end of the street outside a small provincial bank.Several cars lie over the road,slowly rusting.";
locations[31] = "You are before a counter in the bank.The road is back {ACTION|east|east} while the vault is south.A strong wind blows in from outside.";
locations[32] = "You are in the vault beside the thick security doors.The vault (and accompanying safe deposit boxes)have all been levered open";
locations[33] = "\n\nYou are outside a large fair- ground that is on a permanent site outside the small town that lies south.Through the fields of high scaffolding the rides and big dipper rise high above.The fair is silent,the strung coloured bulbs and glittering signs unlit.";
locations[34] = "You are at the base of the towering big dipper that rises high into the air above you.";
locations[35] = "You are north of the fairground.";
locations[36] = "{CLASS|center| ?????????????????????????????\n ? You are in a bumper car on?\n ? the big dipper.A large    ?\n ? button at your feet is    ?\n ? marked,&#34;GO&#34;               ?\n ?????????????????????????????}";
locations[37] = "You are in the silent fairground between rides {ACTION|east|east} of the roller coaster and {ACTION|west|west} of the hall of mirrors whilst long lines of creaking swings are {ACTION|north|north}.";
locations[38] = "You are beside a row of rusted metal swings that clank in the breeze,swaying slowly.";
locations[39] = "You are in the hall of mirrors.";
locations[40] = "You are in the hall of mirrors amongst scattered food parcels!";
locations[41] = "You are in the hall of mirrors.";
locations[42] = "You are on the top of the big dipper high above the fair.The narrow track swings back {ACTION|down|down} to the ground,the scaffold rising around you on all sides.The narrow track is the only way {ACTION|down|down}.";
locations[43] = "You are below a huge gas tank, upon which hangs a steel ladder. To the {ACTION|south|south} a small church is beside the long N/{ACTION|S|S} highway.";
locations[44] = "You are on a steel platform half way {ACTION|up|up} the tank.";
locations[45] = "You are atop the tank on tight metal,indicating full tanks.Taps for gasoline run all around,but to your horror you see hundreds of tied bombs wired to the tank!";
locations[46] = "\n\nYou are west of a small wooden slat church in the shadow of a tall gasoline tank.";
locations[47] = "You are in the alcove of the church by the open entrance door leading in to the {ACTION|east|east}.";
locations[48] = "You are amongst pews beside rows of kneeling corpses.Nearby,an open Bible lies on the pulpit.";
locations[49] = "You are at the end of a long drive by an abandoned checkpoint that almost blocks the road.To the {ACTION|south|south} small concrete domes rise in great numbers,but are oddly spaced.Far east,tall sand dunes rise on the horizon.";
locations[50] = "You are beneath the embedded concrete silo's that run in spaced distances both west and south.The pits are open,but the sides of the concrete walls are in darkness,making it impossible to see into them.A huge door is south.";
locations[51] = "You are in a small cupboard.";
locations[52] = "You are in the foyer of a cold stone bunker that throws shadows everywhere.Walk-in cupboards are {ACTION|E|E}+{ACTION|W|W},whilst war command centre is south.";
locations[53] = "You are in the cluttered spares department.Army canteen food racks cover the walls!";
locations[54] = "You are in War Command Centre.A video screen fills the north wall,whilst a mounted phone is on the wall beside a power point and an large,inset modem.";
locations[55] = "You are outside a small research station that falls to the east.A large sign reads&#34;Bacteriological Warfare Research Station- Strictly no admittance!&#34;";
locations[56] = "You are just inside the doorway of the instillation south of a long corridor.Bodies litter the floor,all of which are dressed in white gowns.";
locations[57] = "This corridor runs {ACTION|north|north}/{ACTION|south|south}, whilst a small laboratory lies {ACTION|east|east}.";
locations[58] = "The corridor here runs {ACTION|south|south}, but a large laboratory is {ACTION|east|east}.";
locations[59] = "You are in a laboratory amongst work tables and pipette stands.";
locations[60] = "You are beside a panel in a small room,the main feature of which is a large vacuum chamber.";
locations[61] = "You are on the edge of a desert {ACTION|west|west} of a high mountain pass.A log cabin is {ACTION|south|south}.";
locations[62] = "You are in the cabin.Food lies scattered about the floor!";
locations[63] = "You are at the top of the pass overlooking a sprawling refugee camp.Even from here the smell  n oise is terrible.A high fence  s urrounds the camp,a small gate b eing {ACTION|east|east}.";
locations[64] = "You are at a gap in the fence that is fashioned crudely into\na border-point.Several &#34;guards&#34; loiter at the entrance NW.";
locations[65] = "\n\nYou are on a {ACTION|N|N}/{ACTION|S|S} path through a refugee camp.The squalor,disease and suffering is terrible to see Corpses lie unburied all around, wavering,sickly camp-fires light pitiful scenes of death as you pass.";
locations[66] = "You are north of the camp.{ACTION|SW|SW} the exit leads through a fence onto open flat plains.Smoke hangs over the camp {ACTION|south|south},like a pyre.";
locations[67] = "The road flattens out here to a deep valley across long plains that reach {ACTION|east|east}.";
locations[68] = "You are on the borders of a small town.The main street is east.";
locations[69] = "You are in the main street outside a small hall,the porch of which is covered in long, thick strands of ivy.The main entrance is {ACTION|east|east}.";
locations[70] = "You are in a small,clean,hall.";
locations[71] = "You are approaching the borders of a low desert that rises on all sides {ACTION|east|east}.";
locations[72] = "You are in the desert.";
locations[73] = "You in the desert below a low shelf of limestone rock.";
locations[74] = "You are {ACTION|north|north} of the store ware- house {ACTION|south|south} of the 1st floor.A terribly cold chill seems to come from the {ACTION|south|south},and clouds of dank fog cover the ground.";
locations[75] = "\n\nYou are in the cold store ware- house amongst blocks of packed ice that are slowly melting.";
locations[76] = "You are in the cold store ware- house amongst blocks of packed ice that are slowly melting.";
locations[77] = "You are amongst blocks of packed stinking ice in the cold-store";
locations[78] = "You are on a slim window ledge- masonry crumbles underfoot!";
locations[79] = "You are in a burning car.Smoke fills the wreck!";
locations[80] = "(extra message added so the newlines below aren't included in the message above)\n\n\n\n\n\n";

// CONNECTIONS

connections = [];
connections_start = [];

connections[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[1] = [ -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[2] = [ -1, -1, -1, 4, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[3] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[4] = [ -1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[5] = [ -1, -1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[6] = [ -1, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[7] = [ -1, 6, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[8] = [ -1, -1, -1, 10, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[9] = [ -1, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 7, -1, -1, -1 ];
connections[10] = [ -1, -1, -1, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[11] = [ -1, -1, 13, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[12] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, -1, -1, -1 ];
connections[13] = [ -1, 14, 13, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[14] = [ -1, 15, 14, 15, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[15] = [ -1, 13, 14, 14, 15, -1, 15, -1, 16, -1, -1, -1, -1, -1, -1, -1 ];
connections[16] = [ -1, -1, -1, 17, -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[17] = [ -1, -1, -1, -1, 16, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[18] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 17, -1, -1, -1 ];
connections[19] = [ -1, -1, -1, 16, -1, -1, -1, -1, -1, 20, -1, -1, 16, -1, -1, -1 ];
connections[20] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 19, -1, -1, -1, -1, -1 ];
connections[21] = [ -1, -1, -1, 20, -1, -1, -1, -1, -1, 22, -1, -1, 20, -1, -1, -1 ];
connections[22] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 21, -1, -1, -1, -1, -1 ];
connections[23] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, -1, -1, -1, -1 ];
connections[24] = [ -1, -1, -1, -1, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[25] = [ -1, -1, 26, 24, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[26] = [ -1, 25, -1, -1, -1, -1, -1, -1, -1, 27, -1, -1, -1, -1, -1, -1 ];
connections[27] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[28] = [ -1, -1, 30, 29, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[29] = [ -1, -1, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, 28, -1, -1, -1 ];
connections[30] = [ -1, 28, -1, -1, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[31] = [ -1, -1, -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[33] = [ -1, -1, -1, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[34] = [ -1, 35, 37, -1, 33, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[35] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[36] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[37] = [ -1, 38, -1, 39, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[38] = [ -1, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[39] = [ -1, 40, 40, 40, 40, -1, 41, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[40] = [ -1, 40, 40, 41, 41, -1, -1, -1, 40, -1, -1, -1, -1, -1, -1, -1 ];
connections[41] = [ -1, 40, 40, 40, 40, 40, 40, 37, 41, -1, -1, -1, -1, -1, -1, -1 ];
connections[42] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, -1, -1, -1, -1, -1 ];
connections[43] = [ -1, -1, 46, -1, -1, -1, -1, -1, -1, 44, -1, -1, -1, -1, -1, -1 ];
connections[44] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 45, 43, -1, -1, -1, -1, -1 ];
connections[45] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 44, -1, -1, -1, -1, -1 ];
connections[46] = [ -1, 43, -1, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[47] = [ -1, -1, -1, 48, 46, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[48] = [ -1, -1, -1, -1, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[49] = [ -1, -1, 50, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[50] = [ -1, 49, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[51] = [ -1, -1, -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[52] = [ -1, -1, -1, 53, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[53] = [ -1, -1, -1, -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[54] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[55] = [ -1, -1, -1, -1, -1, -1, -1, -1, 72, -1, -1, -1, -1, -1, -1, -1 ];
connections[56] = [ -1, 57, -1, -1, 55, -1, -1, -1, -1, -1, -1, -1, 55, -1, -1, -1 ];
connections[57] = [ -1, 58, 56, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[58] = [ -1, -1, 57, 59, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[59] = [ -1, -1, -1, -1, 58, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[60] = [ -1, -1, -1, -1, 57, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[61] = [ -1, -1, 62, 63, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[62] = [ -1, 61, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[63] = [ -1, -1, -1, 64, 61, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[64] = [ -1, -1, -1, -1, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[65] = [ -1, 66, 64, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[66] = [ -1, -1, 65, -1, -1, -1, -1, -1, 67, -1, -1, -1, -1, -1, -1, -1 ];
connections[67] = [ -1, -1, -1, 68, -1, 66, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[68] = [ -1, -1, -1, -1, 67, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[69] = [ -1, -1, -1, 70, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[70] = [ -1, -1, -1, -1, 69, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[71] = [ -1, 71, 71, 71, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[72] = [ -1, 72, 72, 71, 71, 72, 55, 72, 73, -1, -1, -1, -1, -1, -1, -1 ];
connections[73] = [ -1, 71, 71, 72, 72, 73, 73, 61, 71, -1, -1, -1, -1, -1, -1, -1 ];
connections[74] = [ -1, 76, 75, 74, 74, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[75] = [ -1, 75, 75, 75, 75, -1, -1, -1, 76, -1, -1, -1, -1, -1, -1, -1 ];
connections[76] = [ -1, 76, 75, 75, 76, 77, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[77] = [ -1, 75, 75, 75, 75, 76, 76, 76, 76, -1, -1, -1, 74, -1, -1, -1 ];
connections[78] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[79] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1 ];
connections[80] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];

connections_start[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[1] = [ -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[2] = [ -1, -1, -1, 4, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[3] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[4] = [ -1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[5] = [ -1, -1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[6] = [ -1, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[7] = [ -1, 6, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[8] = [ -1, -1, -1, 10, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[9] = [ -1, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 7, -1, -1, -1 ];
connections_start[10] = [ -1, -1, -1, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[11] = [ -1, -1, 13, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[12] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, -1, -1, -1 ];
connections_start[13] = [ -1, 14, 13, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[14] = [ -1, 15, 14, 15, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[15] = [ -1, 13, 14, 14, 15, -1, 15, -1, 16, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[16] = [ -1, -1, -1, 17, -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[17] = [ -1, -1, -1, -1, 16, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[18] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 17, -1, -1, -1 ];
connections_start[19] = [ -1, -1, -1, 16, -1, -1, -1, -1, -1, 20, -1, -1, 16, -1, -1, -1 ];
connections_start[20] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 19, -1, -1, -1, -1, -1 ];
connections_start[21] = [ -1, -1, -1, 20, -1, -1, -1, -1, -1, 22, -1, -1, 20, -1, -1, -1 ];
connections_start[22] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 21, -1, -1, -1, -1, -1 ];
connections_start[23] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, -1, -1, -1, -1 ];
connections_start[24] = [ -1, -1, -1, -1, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[25] = [ -1, -1, 26, 24, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[26] = [ -1, 25, -1, -1, -1, -1, -1, -1, -1, 27, -1, -1, -1, -1, -1, -1 ];
connections_start[27] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[28] = [ -1, -1, 30, 29, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[29] = [ -1, -1, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, 28, -1, -1, -1 ];
connections_start[30] = [ -1, 28, -1, -1, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[31] = [ -1, -1, -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[33] = [ -1, -1, -1, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[34] = [ -1, 35, 37, -1, 33, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[35] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[36] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[37] = [ -1, 38, -1, 39, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[38] = [ -1, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[39] = [ -1, 40, 40, 40, 40, -1, 41, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[40] = [ -1, 40, 40, 41, 41, -1, -1, -1, 40, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[41] = [ -1, 40, 40, 40, 40, 40, 40, 37, 41, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[42] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 37, -1, -1, -1, -1, -1 ];
connections_start[43] = [ -1, -1, 46, -1, -1, -1, -1, -1, -1, 44, -1, -1, -1, -1, -1, -1 ];
connections_start[44] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 45, 43, -1, -1, -1, -1, -1 ];
connections_start[45] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 44, -1, -1, -1, -1, -1 ];
connections_start[46] = [ -1, 43, -1, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[47] = [ -1, -1, -1, 48, 46, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[48] = [ -1, -1, -1, -1, 47, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[49] = [ -1, -1, 50, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[50] = [ -1, 49, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[51] = [ -1, -1, -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[52] = [ -1, -1, -1, 53, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[53] = [ -1, -1, -1, -1, 52, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[54] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[55] = [ -1, -1, -1, -1, -1, -1, -1, -1, 72, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[56] = [ -1, 57, -1, -1, 55, -1, -1, -1, -1, -1, -1, -1, 55, -1, -1, -1 ];
connections_start[57] = [ -1, 58, 56, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[58] = [ -1, -1, 57, 59, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[59] = [ -1, -1, -1, -1, 58, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[60] = [ -1, -1, -1, -1, 57, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[61] = [ -1, -1, 62, 63, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[62] = [ -1, 61, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[63] = [ -1, -1, -1, 64, 61, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[64] = [ -1, -1, -1, -1, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[65] = [ -1, 66, 64, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[66] = [ -1, -1, 65, -1, -1, -1, -1, -1, 67, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[67] = [ -1, -1, -1, 68, -1, 66, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[68] = [ -1, -1, -1, -1, 67, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[69] = [ -1, -1, -1, 70, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[70] = [ -1, -1, -1, -1, 69, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[71] = [ -1, 71, 71, 71, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[72] = [ -1, 72, 72, 71, 71, 72, 55, 72, 73, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[73] = [ -1, 71, 71, 72, 72, 73, 73, 61, 71, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[74] = [ -1, 76, 75, 74, 74, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[75] = [ -1, 75, 75, 75, 75, -1, -1, -1, 76, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[76] = [ -1, 76, 75, 75, 76, 77, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[77] = [ -1, 75, 75, 75, 75, 76, 76, 76, 76, -1, -1, -1, 74, -1, -1, -1 ];
connections_start[78] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[79] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1 ];
connections_start[80] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];


resources=[];


 //OBJECTS

objects = [];
objectsAttrLO = [];
objectsAttrHI = [];
objectsLocation = [];
objectsNoun = [];
objectsAdjective = [];
objectsWeight = [];
objectsAttrLO_start = [];
objectsAttrHI_start = [];
objectsLocation_start = [];
objectsWeight_start = [];

objects[0] = "a low service trolley";
objectsNoun[0] = 255;
objectsAdjective[0] = 255;
objectsLocation[0] = 54;
objectsLocation_start[0] = 54;
objectsWeight[0] = 1;
objectsWeight_start[0] = 1;
objectsAttrLO[0] = 1;
objectsAttrLO_start[0] = 1;
objectsAttrHI[0] = 0;
objectsAttrHI_start[0] = 0;

objects[1] = "Dave Kellern.";
objectsNoun[1] = 255;
objectsAdjective[1] = 255;
objectsLocation[1] = 79;
objectsLocation_start[1] = 79;
objectsWeight[1] = 1;
objectsWeight_start[1] = 1;
objectsAttrLO[1] = 0;
objectsAttrLO_start[1] = 0;
objectsAttrHI[1] = 0;
objectsAttrHI_start[1] = 0;

objects[2] = "Sue Youart.";
objectsNoun[2] = 255;
objectsAdjective[2] = 255;
objectsLocation[2] = 25;
objectsLocation_start[2] = 25;
objectsWeight[2] = 1;
objectsWeight_start[2] = 1;
objectsAttrLO[2] = 0;
objectsAttrLO_start[2] = 0;
objectsAttrHI[2] = 0;
objectsAttrHI_start[2] = 0;

objects[3] = "Sam Wieyenski.";
objectsNoun[3] = 255;
objectsAdjective[3] = 255;
objectsLocation[3] = 38;
objectsLocation_start[3] = 38;
objectsWeight[3] = 1;
objectsWeight_start[3] = 1;
objectsAttrLO[3] = 0;
objectsAttrLO_start[3] = 0;
objectsAttrHI[3] = 0;
objectsAttrHI_start[3] = 0;

objects[4] = "Bill Price.";
objectsNoun[4] = 255;
objectsAdjective[4] = 255;
objectsLocation[4] = 74;
objectsLocation_start[4] = 74;
objectsWeight[4] = 1;
objectsWeight_start[4] = 1;
objectsAttrLO[4] = 0;
objectsAttrLO_start[4] = 0;
objectsAttrHI[4] = 0;
objectsAttrHI_start[4] = 0;

objects[5] = "Sylvia wade.";
objectsNoun[5] = 255;
objectsAdjective[5] = 255;
objectsLocation[5] = 29;
objectsLocation_start[5] = 29;
objectsWeight[5] = 1;
objectsWeight_start[5] = 1;
objectsAttrLO[5] = 0;
objectsAttrLO_start[5] = 0;
objectsAttrHI[5] = 0;
objectsAttrHI_start[5] = 0;

objects[6] = "Dave's body.";
objectsNoun[6] = 255;
objectsAdjective[6] = 255;
objectsLocation[6] = 252;
objectsLocation_start[6] = 252;
objectsWeight[6] = 1;
objectsWeight_start[6] = 1;
objectsAttrLO[6] = 0;
objectsAttrLO_start[6] = 0;
objectsAttrHI[6] = 0;
objectsAttrHI_start[6] = 0;

objects[7] = "Sue's body.";
objectsNoun[7] = 255;
objectsAdjective[7] = 255;
objectsLocation[7] = 252;
objectsLocation_start[7] = 252;
objectsWeight[7] = 1;
objectsWeight_start[7] = 1;
objectsAttrLO[7] = 0;
objectsAttrLO_start[7] = 0;
objectsAttrHI[7] = 0;
objectsAttrHI_start[7] = 0;

objects[8] = "Sam's body.";
objectsNoun[8] = 255;
objectsAdjective[8] = 255;
objectsLocation[8] = 252;
objectsLocation_start[8] = 252;
objectsWeight[8] = 1;
objectsWeight_start[8] = 1;
objectsAttrLO[8] = 0;
objectsAttrLO_start[8] = 0;
objectsAttrHI[8] = 0;
objectsAttrHI_start[8] = 0;

objects[9] = "Bill's body.";
objectsNoun[9] = 255;
objectsAdjective[9] = 255;
objectsLocation[9] = 252;
objectsLocation_start[9] = 252;
objectsWeight[9] = 1;
objectsWeight_start[9] = 1;
objectsAttrLO[9] = 0;
objectsAttrLO_start[9] = 0;
objectsAttrHI[9] = 0;
objectsAttrHI_start[9] = 0;

objects[10] = "Sylvia's body.";
objectsNoun[10] = 255;
objectsAdjective[10] = 255;
objectsLocation[10] = 252;
objectsLocation_start[10] = 252;
objectsWeight[10] = 1;
objectsWeight_start[10] = 1;
objectsAttrLO[10] = 0;
objectsAttrLO_start[10] = 0;
objectsAttrHI[10] = 0;
objectsAttrHI_start[10] = 0;

objects[11] = "a pair of delicate ladies leather gloves.";
objectsNoun[11] = 90;
objectsAdjective[11] = 255;
objectsLocation[11] = 18;
objectsLocation_start[11] = 18;
objectsWeight[11] = 1;
objectsWeight_start[11] = 1;
objectsAttrLO[11] = 0;
objectsAttrLO_start[11] = 0;
objectsAttrHI[11] = 0;
objectsAttrHI_start[11] = 0;

objects[12] = "a large electrical generator.\nIt seems in a state of disrepair and neglect.";
objectsNoun[12] = 255;
objectsAdjective[12] = 255;
objectsLocation[12] = 35;
objectsLocation_start[12] = 35;
objectsWeight[12] = 1;
objectsWeight_start[12] = 1;
objectsAttrLO[12] = 0;
objectsAttrLO_start[12] = 0;
objectsAttrHI[12] = 0;
objectsAttrHI_start[12] = 0;

objects[13] = "a buzzing electrical generator.";
objectsNoun[13] = 255;
objectsAdjective[13] = 255;
objectsLocation[13] = 252;
objectsLocation_start[13] = 252;
objectsWeight[13] = 1;
objectsWeight_start[13] = 1;
objectsAttrLO[13] = 0;
objectsAttrLO_start[13] = 0;
objectsAttrHI[13] = 0;
objectsAttrHI_start[13] = 0;

objects[14] = "a suitcase full of dollars.($)";
objectsNoun[14] = 255;
objectsAdjective[14] = 255;
objectsLocation[14] = 32;
objectsLocation_start[14] = 32;
objectsWeight[14] = 1;
objectsWeight_start[14] = 1;
objectsAttrLO[14] = 0;
objectsAttrLO_start[14] = 0;
objectsAttrHI[14] = 0;
objectsAttrHI_start[14] = 0;

objects[15] = "a newly-turned key.";
objectsNoun[15] = 87;
objectsAdjective[15] = 255;
objectsLocation[15] = 252;
objectsLocation_start[15] = 252;
objectsWeight[15] = 1;
objectsWeight_start[15] = 1;
objectsAttrLO[15] = 0;
objectsAttrLO_start[15] = 0;
objectsAttrHI[15] = 0;
objectsAttrHI_start[15] = 0;

objects[16] = "a small strip of strong metal.";
objectsNoun[16] = 86;
objectsAdjective[16] = 255;
objectsLocation[16] = 42;
objectsLocation_start[16] = 42;
objectsWeight[16] = 1;
objectsWeight_start[16] = 1;
objectsAttrLO[16] = 0;
objectsAttrLO_start[16] = 0;
objectsAttrHI[16] = 0;
objectsAttrHI_start[16] = 0;

objects[17] = "a small glass bottle marked, &#34;Bacter A/*1eu-(strain active 1)";
objectsNoun[17] = 85;
objectsAdjective[17] = 255;
objectsLocation[17] = 252;
objectsLocation_start[17] = 252;
objectsWeight[17] = 1;
objectsWeight_start[17] = 1;
objectsAttrLO[17] = 0;
objectsAttrLO_start[17] = 0;
objectsAttrHI[17] = 0;
objectsAttrHI_start[17] = 0;

objects[18] = "a small computer.";
objectsNoun[18] = 255;
objectsAdjective[18] = 255;
objectsLocation[18] = 51;
objectsLocation_start[18] = 51;
objectsWeight[18] = 1;
objectsWeight_start[18] = 1;
objectsAttrLO[18] = 0;
objectsAttrLO_start[18] = 0;
objectsAttrHI[18] = 0;
objectsAttrHI_start[18] = 0;

objects[19] = "a set of wires,jack-plugs and strange edge-connectors.";
objectsNoun[19] = 83;
objectsAdjective[19] = 255;
objectsLocation[19] = 78;
objectsLocation_start[19] = 78;
objectsWeight[19] = 1;
objectsWeight_start[19] = 1;
objectsAttrLO[19] = 0;
objectsAttrLO_start[19] = 0;
objectsAttrHI[19] = 0;
objectsAttrHI_start[19] = 0;

objects[20] = "a horribly burnt old man in the doorway.";
objectsNoun[20] = 255;
objectsAdjective[20] = 255;
objectsLocation[20] = 52;
objectsLocation_start[20] = 52;
objectsWeight[20] = 1;
objectsWeight_start[20] = 1;
objectsAttrLO[20] = 0;
objectsAttrLO_start[20] = 0;
objectsAttrHI[20] = 0;
objectsAttrHI_start[20] = 0;

objects[21] = "the charred remains of the madman.";
objectsNoun[21] = 255;
objectsAdjective[21] = 255;
objectsLocation[21] = 252;
objectsLocation_start[21] = 252;
objectsWeight[21] = 1;
objectsWeight_start[21] = 1;
objectsAttrLO[21] = 0;
objectsAttrLO_start[21] = 0;
objectsAttrHI[21] = 0;
objectsAttrHI_start[21] = 0;

objects[22] = "a small military-style pocket book.";
objectsNoun[22] = 82;
objectsAdjective[22] = 255;
objectsLocation[22] = 32;
objectsLocation_start[22] = 32;
objectsWeight[22] = 1;
objectsWeight_start[22] = 1;
objectsAttrLO[22] = 0;
objectsAttrLO_start[22] = 0;
objectsAttrHI[22] = 0;
objectsAttrHI_start[22] = 0;

objects[23] = "a small BMX bicycle.";
objectsNoun[23] = 255;
objectsAdjective[23] = 255;
objectsLocation[23] = 6;
objectsLocation_start[23] = 6;
objectsWeight[23] = 1;
objectsWeight_start[23] = 1;
objectsAttrLO[23] = 0;
objectsAttrLO_start[23] = 0;
objectsAttrHI[23] = 0;
objectsAttrHI_start[23] = 0;

objects[24] = "a BMX bicycle (riden)";
objectsNoun[24] = 255;
objectsAdjective[24] = 255;
objectsLocation[24] = 252;
objectsLocation_start[24] = 252;
objectsWeight[24] = 1;
objectsWeight_start[24] = 1;
objectsAttrLO[24] = 0;
objectsAttrLO_start[24] = 0;
objectsAttrHI[24] = 0;
objectsAttrHI_start[24] = 0;

objects[25] = "a dusty mini-bus.";
objectsNoun[25] = 255;
objectsAdjective[25] = 255;
objectsLocation[25] = 2;
objectsLocation_start[25] = 2;
objectsWeight[25] = 1;
objectsWeight_start[25] = 1;
objectsAttrLO[25] = 0;
objectsAttrLO_start[25] = 0;
objectsAttrHI[25] = 0;
objectsAttrHI_start[25] = 0;

objects[26] = "a 12&#34; Spanner";
objectsNoun[26] = 69;
objectsAdjective[26] = 255;
objectsLocation[26] = 12;
objectsLocation_start[26] = 12;
objectsWeight[26] = 1;
objectsWeight_start[26] = 1;
objectsAttrLO[26] = 0;
objectsAttrLO_start[26] = 0;
objectsAttrHI[26] = 0;
objectsAttrHI_start[26] = 0;

objects[27] = "a coiled Rope.";
objectsNoun[27] = 255;
objectsAdjective[27] = 255;
objectsLocation[27] = 77;
objectsLocation_start[27] = 77;
objectsWeight[27] = 1;
objectsWeight_start[27] = 1;
objectsAttrLO[27] = 0;
objectsAttrLO_start[27] = 0;
objectsAttrHI[27] = 0;
objectsAttrHI_start[27] = 0;

objects[28] = "a rope hanging down from above.";
objectsNoun[28] = 255;
objectsAdjective[28] = 255;
objectsLocation[28] = 252;
objectsLocation_start[28] = 252;
objectsWeight[28] = 1;
objectsWeight_start[28] = 1;
objectsAttrLO[28] = 0;
objectsAttrLO_start[28] = 0;
objectsAttrHI[28] = 0;
objectsAttrHI_start[28] = 0;

objects[29] = "Sue (unconcious).";
objectsNoun[29] = 255;
objectsAdjective[29] = 255;
objectsLocation[29] = 252;
objectsLocation_start[29] = 252;
objectsWeight[29] = 1;
objectsWeight_start[29] = 1;
objectsAttrLO[29] = 0;
objectsAttrLO_start[29] = 0;
objectsAttrHI[29] = 0;
objectsAttrHI_start[29] = 0;

objects[30] = "a heavily bolted lift-door. Strangely it seems to have been bolted from BOTH sides-though the main bolts are on this side!";
objectsNoun[30] = 255;
objectsAdjective[30] = 255;
objectsLocation[30] = 20;
objectsLocation_start[30] = 20;
objectsWeight[30] = 1;
objectsWeight_start[30] = 1;
objectsAttrLO[30] = 0;
objectsAttrLO_start[30] = 0;
objectsAttrHI[30] = 0;
objectsAttrHI_start[30] = 0;

objects[31] = "a pair of insulated boots.";
objectsNoun[31] = 55;
objectsAdjective[31] = 255;
objectsLocation[31] = 50;
objectsLocation_start[31] = 50;
objectsWeight[31] = 1;
objectsWeight_start[31] = 1;
objectsAttrLO[31] = 0;
objectsAttrLO_start[31] = 0;
objectsAttrHI[31] = 0;
objectsAttrHI_start[31] = 0;

objects[32] = "a roller-coaster car.";
objectsNoun[32] = 255;
objectsAdjective[32] = 255;
objectsLocation[32] = 252;
objectsLocation_start[32] = 252;
objectsWeight[32] = 1;
objectsWeight_start[32] = 1;
objectsAttrLO[32] = 0;
objectsAttrLO_start[32] = 0;
objectsAttrHI[32] = 0;
objectsAttrHI_start[32] = 0;

objects[33] = "the closed vault door.";
objectsNoun[33] = 255;
objectsAdjective[33] = 255;
objectsLocation[33] = 252;
objectsLocation_start[33] = 252;
objectsWeight[33] = 1;
objectsWeight_start[33] = 1;
objectsAttrLO[33] = 0;
objectsAttrLO_start[33] = 0;
objectsAttrHI[33] = 0;
objectsAttrHI_start[33] = 0;

objects[34] = "a large screwdriver.";
objectsNoun[34] = 49;
objectsAdjective[34] = 255;
objectsLocation[34] = 41;
objectsLocation_start[34] = 41;
objectsWeight[34] = 1;
objectsWeight_start[34] = 1;
objectsAttrLO[34] = 0;
objectsAttrLO_start[34] = 0;
objectsAttrHI[34] = 0;
objectsAttrHI_start[34] = 0;

objects[35] = "a petrol can(empty)";
objectsNoun[35] = 255;
objectsAdjective[35] = 255;
objectsLocation[35] = 48;
objectsLocation_start[35] = 48;
objectsWeight[35] = 1;
objectsWeight_start[35] = 1;
objectsAttrLO[35] = 0;
objectsAttrLO_start[35] = 0;
objectsAttrHI[35] = 0;
objectsAttrHI_start[35] = 0;

objects[36] = "a petrol can (full).";
objectsNoun[36] = 255;
objectsAdjective[36] = 255;
objectsLocation[36] = 252;
objectsLocation_start[36] = 252;
objectsWeight[36] = 1;
objectsWeight_start[36] = 1;
objectsAttrLO[36] = 0;
objectsAttrLO_start[36] = 0;
objectsAttrHI[36] = 0;
objectsAttrHI_start[36] = 0;

objects[37] = "a Computer mounted on a trolley";
objectsNoun[37] = 255;
objectsAdjective[37] = 255;
objectsLocation[37] = 252;
objectsLocation_start[37] = 252;
objectsWeight[37] = 1;
objectsWeight_start[37] = 1;
objectsAttrLO[37] = 0;
objectsAttrLO_start[37] = 0;
objectsAttrHI[37] = 0;
objectsAttrHI_start[37] = 0;

objects[38] = "a computer mounted on a trolley- 3 wires trail around the unit.";
objectsNoun[38] = 255;
objectsAdjective[38] = 255;
objectsLocation[38] = 252;
objectsLocation_start[38] = 252;
objectsWeight[38] = 1;
objectsWeight_start[38] = 1;
objectsAttrLO[38] = 0;
objectsAttrLO_start[38] = 0;
objectsAttrHI[38] = 0;
objectsAttrHI_start[38] = 0;

objects[39] = "a hastily erected road-block. Several shady-looking characters block the road.";
objectsNoun[39] = 255;
objectsAdjective[39] = 255;
objectsLocation[39] = 68;
objectsLocation_start[39] = 68;
objectsWeight[39] = 1;
objectsWeight_start[39] = 1;
objectsAttrLO[39] = 0;
objectsAttrLO_start[39] = 0;
objectsAttrHI[39] = 0;
objectsAttrHI_start[39] = 0;

objects[40] = "a burning wreck of a car-all the doors are crushed inwards!";
objectsNoun[40] = 255;
objectsAdjective[40] = 255;
objectsLocation[40] = 252;
objectsLocation_start[40] = 252;
objectsWeight[40] = 1;
objectsWeight_start[40] = 1;
objectsAttrLO[40] = 0;
objectsAttrLO_start[40] = 0;
objectsAttrHI[40] = 0;
objectsAttrHI_start[40] = 0;

objects[41] = "(extra message added so the newlines below aren't included in the message above)\n\n\n\n\n\n";
objectsNoun[41] = 0;
objectsAdjective[41] = 0;
objectsLocation[41] = 0;
objectsLocation_start[41] = 0;
objectsWeight[41] = 0;
objectsWeight_start[41] = 0;
objectsAttrLO[41] = 0;
objectsAttrLO_start[41] = 0;
objectsAttrHI[41] = 0;
objectsAttrHI_start[41] = 0;

last_object_number =  41; 
carried_objects = 0;
total_object_messages=42;

