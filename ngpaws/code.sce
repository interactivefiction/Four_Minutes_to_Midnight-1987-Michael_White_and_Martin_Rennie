






;#define const aLight       = 0
;#define const aWearable    = 1
;#define const aContainer   = 2
;#define const aNPC         = 3
;#define const aConcealed   = 4
;#define const aEdible      = 5
;#define const aDrinkable   = 6
;#define const aEnterable   = 7
;#define const aFemale      = 8
;#define const aLockable    = 9
;#define const aLocked      = 10
;#define const aMale        = 11
;#define const aNeuter      = 12
;#define const aOpenable    = 13
;#define const aOpen        = 14
;#define const aPluralName  = 15
;#define const aTransparent = 16
;#define const aScenery     = 17
;#define const aSupporter   = 18
;#define const aSwitchable  = 19
;#define const aOn          = 20
;#define const aStatic      = 21


;#define const object_0_trol = 0
;#define const object_1 = 1
;#define const object_2 = 2
;#define const object_3 = 3
;#define const object_4 = 4
;#define const object_5 = 5
;#define const object_6 = 6
;#define const object_7 = 7
;#define const object_8 = 8
;#define const object_9 = 9
;#define const object_10 = 10
;#define const object_11_glov = 11
;#define const object_12 = 12
;#define const object_13 = 13
;#define const object_14_suit = 14
;#define const object_15_key = 15
;#define const object_16_meta = 16
;#define const object_17_bott = 17
;#define const object_18 = 18
;#define const object_19_elec = 19
;#define const object_20 = 20
;#define const object_21 = 21
;#define const object_22_pock = 22
;#define const object_23 = 23
;#define const object_24 = 24
;#define const object_25 = 25
;#define const object_26_span = 26
;#define const object_27_rope = 27
;#define const object_28 = 28
;#define const object_29_sue = 29
;#define const object_30 = 30
;#define const object_31_boot = 31
;#define const object_32 = 32
;#define const object_33 = 33
;#define const object_34_scre = 34
;#define const object_35_can = 35
;#define const object_36_can = 36
;#define const object_37_trol = 37
;#define const object_38 = 38
;#define const object_39 = 39
;#define const object_40 = 40


;#define const room_0 = 0
;#define const room_1 = 1
;#define const room_2 = 2
;#define const room_3 = 3
;#define const room_4 = 4
;#define const room_5 = 5
;#define const room_6 = 6
;#define const room_7 = 7
;#define const room_8 = 8
;#define const room_9 = 9
;#define const room_10 = 10
;#define const room_11 = 11
;#define const room_12 = 12
;#define const room_13 = 13
;#define const room_14 = 14
;#define const room_15 = 15
;#define const room_16 = 16
;#define const room_17 = 17
;#define const room_18 = 18
;#define const room_19 = 19
;#define const room_20 = 20
;#define const room_21 = 21
;#define const room_22 = 22
;#define const room_23 = 23
;#define const room_24 = 24
;#define const room_25 = 25
;#define const room_26 = 26
;#define const room_27 = 27
;#define const room_28 = 28
;#define const room_29 = 29
;#define const room_30 = 30
;#define const room_31 = 31
;#define const room_32 = 32
;#define const room_33 = 33
;#define const room_34 = 34
;#define const room_35 = 35
;#define const room_36 = 36
;#define const room_37 = 37
;#define const room_38 = 38
;#define const room_39 = 39
;#define const room_40 = 40
;#define const room_41 = 41
;#define const room_42 = 42
;#define const room_43 = 43
;#define const room_44 = 44
;#define const room_45 = 45
;#define const room_46 = 46
;#define const room_47 = 47
;#define const room_48 = 48
;#define const room_49 = 49
;#define const room_50 = 50
;#define const room_51 = 51
;#define const room_52 = 52
;#define const room_53 = 53
;#define const room_54 = 54
;#define const room_55 = 55
;#define const room_56 = 56
;#define const room_57 = 57
;#define const room_58 = 58
;#define const room_59 = 59
;#define const room_60 = 60
;#define const room_61 = 61
;#define const room_62 = 62
;#define const room_63 = 63
;#define const room_64 = 64
;#define const room_65 = 65
;#define const room_66 = 66
;#define const room_67 = 67
;#define const room_68 = 68
;#define const room_69 = 69
;#define const room_70 = 70
;#define const room_71 = 71
;#define const room_72 = 72
;#define const room_73 = 73
;#define const room_74 = 74
;#define const room_75 = 75
;#define const room_76 = 76
;#define const room_77 = 77
;#define const room_78 = 78
;#define const room_79 = 79

;#define const room_noncreated = 252 ; destroying an object moves it to this room
;#define const room_worn = 253 ; wearing an object moves it to this room
;#define const room_carried = 254 ; carrying an object moves it to this room
;#define const room_here = 255 ; not used by any Quill condacts










;#define const yesno_is_dark                       =   0
;#define const total_carried                       =   1
;#define const countdown_location_description      =   2
;#define const countdown_location_description_dark =   3
;#define const countdown_object_description_unlit  =   4
;#define const countdown_player_input_1            =   5
;#define const countdown_player_input_2            =   6
;#define const countdown_player_input_3            =   7
;#define const countdown_player_input_4            =   8
;#define const countdown_player_input_dark         =   9
;#define const countdown_player_input_unlit        =  10
;#define const game_flag_11                        = 111 ; ngPAWS assigns a special meaning to flag 11
;#define const game_flag_12                        = 112 ; ngPAWS assigns a special meaning to flag 12
;#define const game_flag_13                        = 113 ; ngPAWS assigns a special meaning to flag 13
;#define const game_flag_14                        = 114 ; ngPAWS assigns a special meaning to flag 14
;#define const game_flag_15                        = 115 ; ngPAWS assigns a special meaning to flag 15
;#define const game_flag_16                        = 116 ; ngPAWS assigns a special meaning to flag 16
;#define const game_flag_17                        =  17
;#define const game_flag_18                        =  18
;#define const game_flag_19                        =  19
;#define const game_flag_20                        =  20
;#define const game_flag_21                        =  21
;#define const game_flag_22                        =  22
;#define const game_flag_23                        =  23
;#define const game_flag_24                        =  24
;#define const game_flag_25                        =  25
;#define const game_flag_26                        =  26
;#define const game_flag_27                        =  27
;#define const pause_parameter                     =  28
;#define const bitset_graphics_status              =  29
;#define const total_player_score                  =  30
;#define const total_turns_lower                   =  31
;#define const total_turns_higher                  =  32
;#define const word_verb                           =  33
;#define const word_noun                           =  34
;#define const room_number                         =  38 ; (35 in The Quill, but 38 in ngPAWS)




;#define const pause_sound_effect_tone_increasing = 1 ; increasing tone (value is the duration)
;#define const pause_sound_effect_telephone       = 2 ; ringing telephone (value is the number of rings)
;#define const pause_sound_effect_tone_decreasing = 3 ; decreasing tone (opposite of effect 1)
;#define const pause_sound_effect_white_noise     = 5 ; white noise (resembles an audience clapping, value is the number of repetitions)
;#define const pause_sound_effect_machine_gun     = 6 ; machine gun noise (value is the duration)

;#define const pause_flash = 4 ; flash the screen and the border (value is the number of flashes)
;#define const pause_box_wipe = 9 ; a series of coloured boxes expands out of the centre of the screen at speed

;#define const pause_font_default = 7 ; switch to the default font
;#define const pause_font_alternate = 8 ; switch to the alternative font.

;#define const pause_change_youcansee = 10 ; Change the "You can also see" message to the message number passed to PAUSE

;#define const pause_restart = 12 ; Restart the game without warning.
;#define const pause_reboot = 13 ; Reboot the Spectrum without warning
;#define const pause_ability = 11 ; Set the maximum number of objects carried at once to the value passed to PAUSE
;#define const pause_ability_plus = 14 ; Increase number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_ability_minus = 15 ; Decrease number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_toggle_graphics = 19 ; switch graphics off (for PAUSE 255) or on (for any other value)
;#define const pause_ram_save_load = 21 ; RAMload (for PAUSE 50) or RAMsave (for any other value)
;#define const pause_set_identity_byte = 22 ; Set the identity in saved games to the value passed to PAUSE

;#define const pause_click_effect_1 = 16
;#define const pause_click_effect_2 = 17
;#define const pause_click_effect_3 = 18









/CTL






/VOC






n         1    verb
nort      1    verb
s         2    verb
sout      2    verb
e         3    verb
east      3    verb
w         4    verb
west      4    verb
ne        5    verb
nw        6    verb
se        7    verb
sw        8    verb
clim      9    verb
u         9    verb
up        9    verb
d        10    verb
down     10    verb
ente     11    verb
in       11    verb
exit     12    verb
out      12    verb
jump     13    verb
leap     13    verb
swin     13    verb






give     25    verb
scor     26    verb
bast     27    verb
cunt     27    verb
fuck     27    verb
piss     27    verb
shit     27    verb
9za      28    noun
za9      28    noun
0za      29    noun
za0      29    noun
9yb      30    noun
yb9      30    noun
0yb      31    noun
yb0      31    noun
0xc      32    noun
xc0      32    noun
9xc      33    noun
xc9      33    noun
4        34    noun
four     34    noun
3        35    noun
thre     35    noun
2        36    noun
two      36    noun
1        37    noun
one      37    noun
yes      39    noun
type     40    verb
dial     41    verb
1922     42    noun
over     43    noun
page     43    noun
atta     44    verb
figh     44    verb
kill     44    verb
slay     44    verb
trol     45    noun
can      46    noun
petr     46    noun
read     47    verb
bibl     48    noun
scre     49    noun






sear     50    verb
exam     51    verb
hist     52    verb
slee     53    verb
wait     53    verb
time     54    verb
boot     55    noun
insu     55    noun
fix      56    verb
mend     56    verb
repa     56    verb
ask      58    verb
say      58    verb
wind     59    noun
conn     60    verb
inse     60    verb
open     62    verb
unlo     62    verb
door     63    noun
bolt     64    noun
help     65    verb
info     65    verb
belt     66    verb
hit      66    verb
knoc     66    verb
thum     66    verb
tie      67    verb
coil     68    noun
rope     68    noun
span     69    noun
driv     70    verb
igni     70    verb
star     70    verb
bus      71    noun
pull     71    noun
fill     72    verb
pres     73    verb
push     73    verb
turn     73    verb
butt     74    noun
pane     74    noun
gree     75    noun
off      75    noun
go       76    noun
on       76    noun
red      76    noun
food     77    noun
eat      78    verb
bicy     79    noun
bike     79    noun
cycl     79    noun
part     80    verb
who      80    verb
fee      81    noun
feed     81    verb
book     82    noun
pock     82    noun
elec     83    noun
jack     83    noun
plug     83    noun
wire     83    noun
wiri     83    noun
comp     84    noun
bact     85    noun
bott     85    noun
germ     85    noun
meta     86    noun
stri     86    noun
key      87    noun
case     88    noun
mone     88    noun
suit     88    noun
gene     89    noun
glov     90    noun
ladi     90    noun
sylv     92    noun
bill     93    noun
sam      94    noun
sue      95    noun
dave     96    noun
stat     97    verb
leav     98    verb
enli     99    verb
recr     99    verb
carr    100    verb
get     100    verb
t       100    verb
take    100    verb
dr      101    verb
drop    101    verb
thro    101    verb
remo    102    verb
undo    102    verb
unfa    102    verb
wear    103    verb
i       104    verb
inve    104    verb
l       105    verb
look    105    verb
r       105    verb
rede    105    verb
q       106    verb
quit    106    verb
save    107    verb
load    108    verb
rest    108    verb
word    109    verb
pict    110    verb
stor    111    verb
call    112    verb
vers    203    verb




AND          2    conjunction
THEN         2    conjunction






/STX





/0
Everything is dark!<br>
/1
Close to hand you can also see<br>
/2
\nWhat Next?<br>
/3
\nWhat Next?<br>
/4
\nWhat Now?<br>
/5 
Please type in your orders
/6
I nearly understand..<br>
/7
you can't go that way.<br>
/8
Please be more specific.<br>
/9
You have with you:-
/10
(worn)
/11
Nothing.<br>
/12
{CLASS|center|REALLY QUIT?}<br>
/13
{CLASS|center|END OF GAME \nANOTHER GO? (Y/N)}<br>
/14 
Good bye...<br>
/15
OK<br>
/16
MORE...<br>
/17 
You have typed 
/18 
 turn
/19 
s
/20 
.<br>
/21
You have scored 
/22
 out of 1OO.<br>
/23
You're not wearing it.<br>
/24
Your hands are full.<br>
/25
You already have it!<br>
/26
Go and find it then!<br>
/27
You can't carry any more!<br>
/28
You don't have it!<br>
/29
You ARE wearing it!<br>
/30
Y<br>
/31
N<br>
/32 
More...<br>
/33 
><br>
/34 
<br>
/35 
Time passes...<br>
/36 

/37 

/38 

/39 

/40 
Please be more specific.<br>
/41 
Please be more specific.<br>
/42 
Your hands are full.<br>
/43 
You can't carry any more!<br>
/44 
You put {OREF} into<br>
/45 
{OREF} is not into<br>
/46 
<br>
/47 
<br>
/48 
<br>
/49 
You don't have it!<br>
/50 
Please be more specific.<br>
/51 
.<br>
/52 
That is not into<br>
/53 
nothing at all<br>
/54 
File not found.<br>
/55 
File corrupt.<br>
/56 
I/O error. File not saved.<br>
/57 
Directory full.<br>
/58 
Please enter savegame name you used when saving the game status.
/59 
Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>
/60 
Please enter savegame name. Remember to note down the name you choose, as it will be requested in order to restore the game status.
/61 
<br>
/62 
Sorry? Please try other words.<br>
/63 
Here<br>
/64 
you can see<br>
/65 
you can see<br>
/66 
inside you see<br>
/67 
on top you see<br>
/68
(extra message added so the newlines below aren't included in the message above)






/MTX


/1000 
Exits: 
/1001 
You can't see any exits

/1003
{ACTION|nort|nort}
/1004
{ACTION|sout|sout}
/1005
{ACTION|east|east}
/1006
{ACTION|west|west}
/1007
{ACTION|ne|ne}
/1008
{ACTION|nw|nw}
/1009
{ACTION|se|se}
/1010
{ACTION|sw|sw}
/1011
{ACTION|up|up}
/1012
{ACTION|down|down}
/1014
{ACTION|out|out}
/1015
(extra message added so the newlines below aren't included in the message above)






/OTX
/0 ; sust(object_0_trol//0)
a low service trolley
/1 ; sust(object_1//1)
Dave Kellern.
/2 ; sust(object_2//2)
Sue Youart.
/3 ; sust(object_3//3)
Sam Wieyenski.
/4 ; sust(object_4//4)
Bill Price.
/5 ; sust(object_5//5)
Sylvia wade.
/6 ; sust(object_6//6)
Dave's body.
/7 ; sust(object_7//7)
Sue's body.
/8 ; sust(object_8//8)
Sam's body.
/9 ; sust(object_9//9)
Bill's body.
/10 ; sust(object_10//10)
Sylvia's body.
/11 ; sust(object_11_glov//11)
a pair of delicate ladies leather gloves.
/12 ; sust(object_12//12)
a large electrical generator.\nIt seems in a state of disrepair and neglect.
/13 ; sust(object_13//13)
a buzzing electrical generator.
/14 ; sust(object_14_suit//14)
a suitcase full of dollars.($)
/15 ; sust(object_15_key//15)
a newly-turned key.
/16 ; sust(object_16_meta//16)
a small strip of strong metal.
/17 ; sust(object_17_bott//17)
a small glass bottle marked, &#34;Bacter A/*1eu-(strain active 1)
/18 ; sust(object_18//18)
a small computer.
/19 ; sust(object_19_elec//19)
a set of wires,jack-plugs and strange edge-connectors.
/20 ; sust(object_20//20)
a horribly burnt old man in the doorway.
/21 ; sust(object_21//21)
the charred remains of the madman.
/22 ; sust(object_22_pock//22)
a small military-style pocket book.
/23 ; sust(object_23//23)
a small BMX bicycle.
/24 ; sust(object_24//24)
a BMX bicycle (riden)
/25 ; sust(object_25//25)
a dusty mini-bus.
/26 ; sust(object_26_span//26)
a 12&#34; Spanner
/27 ; sust(object_27_rope//27)
a coiled Rope.
/28 ; sust(object_28//28)
a rope hanging down from above.
/29 ; sust(object_29_sue//29)
Sue (unconcious).
/30 ; sust(object_30//30)
a heavily bolted lift-door. Strangely it seems to have been bolted from BOTH sides-though the main bolts are on this side!
/31 ; sust(object_31_boot//31)
a pair of insulated boots.
/32 ; sust(object_32//32)
a roller-coaster car.
/33 ; sust(object_33//33)
the closed vault door.
/34 ; sust(object_34_scre//34)
a large screwdriver.
/35 ; sust(object_35_can//35)
a petrol can(empty)
/36 ; sust(object_36_can//36)
a petrol can (full).
/37 ; sust(object_37_trol//37)
a Computer mounted on a trolley
/38 ; sust(object_38//38)
a computer mounted on a trolley- 3 wires trail around the unit.
/39 ; sust(object_39//39)
a hastily erected road-block. Several shady-looking characters block the road.
/40 ; sust(object_40//40)
a burning wreck of a car-all the doors are crushed inwards!
/41
(extra message added so the newlines below aren't included in the message above)






/LTX
/0 ; sust(room_0//0)
Welcome to ~ Four Minutes to Midnight ~ from Eighth Day.\n\nAfter a bacteriological disaster has wiped out 90 of the world'sp opulation, you must find five  c ompanions to establish a colonya cross the states of America to w in. Also de - arm your dead    c ountry's nuclear stockpile to  c reate a secure future for you  a nd your companions.\n                               N ote that there are many extra  c ommands involved in playing theg ame - type help for a summary  o f these extra commands.\n                               N ow press a key to play...
/1 ; sust(room_1//1)
You are beside a large box that is marked, ~ Gasoline Pump Controls ~. A red button is marked, ~ ON ~, whilst a green button is marked, ~ OFF ~.
/2 ; sust(room_2//2)
\n\nYou are on the forecourt of a small gasoline station beside a row of several neglected pumps. There is a small road running to the {ACTION|east|east} that seems to disappear to the horizon. The garage fronting is to the {ACTION|west|west}, the station itself being in a great state of general disrepair.
/3 ; sust(room_3//3)
You are seated in the cab of a grubby mini - bus looking through dust covered windows.
/4 ; sust(room_4//4)
You are on a long road leading east and {ACTION|west|west}. It stretches over a thin strip of neglected fields for what must be many miles.
/5 ; sust(room_5//5)
You are on a small rise over a large city. The road runs west whilst a freeway runs {ACTION|south|south} to the city from where comes a disgusting, sickly sweet smell.
/6 ; sust(room_6//6)
You are on a freeway that leads {ACTION|south|south}. A large sign reading,\n~ CITY LIMITS ONE MILE ~ hangs from a gantry over the six lane highway. Cars are slewed across the six lane highway in their hundreds. The scene of death is terrible, the smell of decay strong in the defiled air.
/7 ; sust(room_7//7)
\n\nYou are on the corner of east 4th and 5th - the city is oddly silent and dark. A small drugstore is {ACTION|south|south} whilst the open tunnel of the main freeway cross - river tunnel is east. No lights illuminate its darkened, silent interior. Cars are jack - knifed across the roads in great numbers, the traffic discarded.
/8 ; sust(room_8//8)
You are in the tunnel. The strong sickening smell of death rises from all around whilst cars seem to block the tunnel, the round concrete walls\nseeming to be clogged with all kinds of mechanical debris. Further {ACTION|east|east} the tunnel darkens.
/9 ; sust(room_9//9)
You are in a looted drugstore. The plate glass window has long since been shattered,but oddly enough,large amounts of tinned food cover every wall.
/10 ; sust(room_10//10)
You are crawling on the roof of a shattered car in the pitch black darkness-scurrying rats eyes blink in the darkness!
/11 ; sust(room_11//11)
You are just inside the exit of the tunnel by a huge pile of rotting bodies.The tunnel seems blocked,though a futile struggle has created a hole in the debris leading {ACTION|south|south}.
/12 ; sust(room_12//12)
You are inside one of the cars by the drivers seat.In the front seat the corpse of a middle aged man straddles the smashed wind- screen.
/13 ; sust(room_13//13)
You are on the west side at the junction of 6th and 7th
/14 ; sust(room_14//14)
You are {ACTION|northside|northside} of 3rd and 2nd below the blocks of {ACTION|westside|westside}.
/15 ; sust(room_15//15)
You are {ACTION|east|east} of blocks 8th and 9th {ACTION|west|west} of {ACTION|westside|westside} and {ACTION|north|north} of the road leading to the {ACTION|south|south}
/16 ; sust(room_16//16)
You are outside a towering department store south of 4th and 5th.A large hotel is {ACTION|east|east}. Large,tattered  drapes hang over head marked,&#34;PLACEYS DEPARTMENT STORE&#34;.
/17 ; sust(room_17//17)
You are outside a once plush hotel.Revolving doors lead into the hotel lobby whilst the street continues {ACTION|west|west}.
/18 ; sust(room_18//18)
You are in the lobby of the hotel.Rich red velvet trimmings cover every furnishing whilst the bodies of several well- dressed people lie scattered across the furniture.
/19 ; sust(room_19//19)
You are north of the warehouse on the inside of the store in the womens hosiery department. Shop dummies (newly stripped), lie discarded on the floor whilst now still escalators run {ACTION|up|up} to the 1st floor marked, &#34;HARDWARE AND LOST PROPERTY&#34;. Food parcels have been torn open all over the dept.
/20 ; sust(room_20//20)
You are on the 1st floor at the top of the escalator.Long aisles run in all directions,whilst a small lathe is below a hand written card marked,&#34;Key-cutting service $1.00 only!&#34;A lift is W.
/21 ; sust(room_21//21)
\n\nYou are in a graffiti covered lift.Large aerosoled letters cover the rusty metal walls, whilst an open inspection hatch which looks as if it has been recently removed,is in the ceiling.
/22 ; sust(room_22//22)
You are on the roof of the lift below a set of greased steel cables in the lift shaft.A dim light shines from above,as light shines into the shaft from the 2nd floor.The lift rocks slowly!
/23 ; sust(room_23//23)
You are clinging to thick steel cables high above the lift and just by the open doors of the 2nd floor.The doors are open, but just out of reach.
/24 ; sust(room_24//24)
You are on the second floor by an open lift shaft beside a long row of smashed cash registers. The tills have been levered open and emptied of all money.The run of registers continues {ACTION|west|west}.
/25 ; sust(room_25//25)
You are at the western end of the line of registers beside lines of dirty windows.
/26 ; sust(room_26//26)
You are south of the department by an open fire exit that leads {ACTION|up|up} onto the roof.
/27 ; sust(room_27//27)
\n\nYou are on the roof of the store overlooking the city.Strangely silent,the towering blocks of skyscrapers and office blocks rise like a vast monument whilst the sweet smell of decay is so strong as to be sickening in its intensity.The breeze is still this high,seeming to hang over the city like an enormous shroud clinging to the now still blocks of rising concrete and steel.
/28 ; sust(room_28//28)
You are at the northern end of a street along a small town by the entrance to a rustic school- house to the {ACTION|east|east}.The school bell rings softly as it swings back and forth in the breeze.
/29 ; sust(room_29//29)
You are in an empty classroom below a large blackboard on which is drawn the picture of a grinning giraffe.Crudely drawn pictures cover every wall and the desks are small as if to be minute,defenceless.
/30 ; sust(room_30//30)
You are at the southern end of the street outside a small provincial bank.Several cars lie over the road,slowly rusting.
/31 ; sust(room_31//31)
You are before a counter in the bank.The road is back {ACTION|east|east} while the vault is south.A strong wind blows in from outside.
/32 ; sust(room_32//32)
You are in the vault beside the thick security doors.The vault (and accompanying safe deposit boxes)have all been levered open
/33 ; sust(room_33//33)
\n\nYou are outside a large fair- ground that is on a permanent site outside the small town that lies south.Through the fields of high scaffolding the rides and big dipper rise high above.The fair is silent,the strung coloured bulbs and glittering signs unlit.
/34 ; sust(room_34//34)
You are at the base of the towering big dipper that rises high into the air above you.
/35 ; sust(room_35//35)
You are north of the fairground.
/36 ; sust(room_36//36)
{CLASS|center| ?????????????????????????????\n ? You are in a bumper car on?\n ? the big dipper.A large    ?\n ? button at your feet is    ?\n ? marked,&#34;GO&#34;               ?\n ?????????????????????????????}
/37 ; sust(room_37//37)
You are in the silent fairground between rides {ACTION|east|east} of the roller coaster and {ACTION|west|west} of the hall of mirrors whilst long lines of creaking swings are {ACTION|north|north}.
/38 ; sust(room_38//38)
You are beside a row of rusted metal swings that clank in the breeze,swaying slowly.
/39 ; sust(room_39//39)
You are in the hall of mirrors.
/40 ; sust(room_40//40)
You are in the hall of mirrors amongst scattered food parcels!
/41 ; sust(room_41//41)
You are in the hall of mirrors.
/42 ; sust(room_42//42)
You are on the top of the big dipper high above the fair.The narrow track swings back {ACTION|down|down} to the ground,the scaffold rising around you on all sides.The narrow track is the only way {ACTION|down|down}.
/43 ; sust(room_43//43)
You are below a huge gas tank, upon which hangs a steel ladder. To the {ACTION|south|south} a small church is beside the long N/{ACTION|S|S} highway.
/44 ; sust(room_44//44)
You are on a steel platform half way {ACTION|up|up} the tank.
/45 ; sust(room_45//45)
You are atop the tank on tight metal,indicating full tanks.Taps for gasoline run all around,but to your horror you see hundreds of tied bombs wired to the tank!
/46 ; sust(room_46//46)
\n\nYou are west of a small wooden slat church in the shadow of a tall gasoline tank.
/47 ; sust(room_47//47)
You are in the alcove of the church by the open entrance door leading in to the {ACTION|east|east}.
/48 ; sust(room_48//48)
You are amongst pews beside rows of kneeling corpses.Nearby,an open Bible lies on the pulpit.
/49 ; sust(room_49//49)
You are at the end of a long drive by an abandoned checkpoint that almost blocks the road.To the {ACTION|south|south} small concrete domes rise in great numbers,but are oddly spaced.Far east,tall sand dunes rise on the horizon.
/50 ; sust(room_50//50)
You are beneath the embedded concrete silo's that run in spaced distances both west and south.The pits are open,but the sides of the concrete walls are in darkness,making it impossible to see into them.A huge door is south.
/51 ; sust(room_51//51)
You are in a small cupboard.
/52 ; sust(room_52//52)
You are in the foyer of a cold stone bunker that throws shadows everywhere.Walk-in cupboards are {ACTION|E|E}+{ACTION|W|W},whilst war command centre is south.
/53 ; sust(room_53//53)
You are in the cluttered spares department.Army canteen food racks cover the walls!
/54 ; sust(room_54//54)
You are in War Command Centre.A video screen fills the north wall,whilst a mounted phone is on the wall beside a power point and an large,inset modem.
/55 ; sust(room_55//55)
You are outside a small research station that falls to the east.A large sign reads&#34;Bacteriological Warfare Research Station- Strictly no admittance!&#34;
/56 ; sust(room_56//56)
You are just inside the doorway of the instillation south of a long corridor.Bodies litter the floor,all of which are dressed in white gowns.
/57 ; sust(room_57//57)
This corridor runs {ACTION|north|north}/{ACTION|south|south}, whilst a small laboratory lies {ACTION|east|east}.
/58 ; sust(room_58//58)
The corridor here runs {ACTION|south|south}, but a large laboratory is {ACTION|east|east}.
/59 ; sust(room_59//59)
You are in a laboratory amongst work tables and pipette stands.
/60 ; sust(room_60//60)
You are beside a panel in a small room,the main feature of which is a large vacuum chamber.
/61 ; sust(room_61//61)
You are on the edge of a desert {ACTION|west|west} of a high mountain pass.A log cabin is {ACTION|south|south}.
/62 ; sust(room_62//62)
You are in the cabin.Food lies scattered about the floor!
/63 ; sust(room_63//63)
You are at the top of the pass overlooking a sprawling refugee camp.Even from here the smell  n oise is terrible.A high fence  s urrounds the camp,a small gate b eing {ACTION|east|east}.
/64 ; sust(room_64//64)
You are at a gap in the fence that is fashioned crudely into\na border-point.Several &#34;guards&#34; loiter at the entrance NW.
/65 ; sust(room_65//65)
\n\nYou are on a {ACTION|N|N}/{ACTION|S|S} path through a refugee camp.The squalor,disease and suffering is terrible to see Corpses lie unburied all around, wavering,sickly camp-fires light pitiful scenes of death as you pass.
/66 ; sust(room_66//66)
You are north of the camp.{ACTION|SW|SW} the exit leads through a fence onto open flat plains.Smoke hangs over the camp {ACTION|south|south},like a pyre.
/67 ; sust(room_67//67)
The road flattens out here to a deep valley across long plains that reach {ACTION|east|east}.
/68 ; sust(room_68//68)
You are on the borders of a small town.The main street is east.
/69 ; sust(room_69//69)
You are in the main street outside a small hall,the porch of which is covered in long, thick strands of ivy.The main entrance is {ACTION|east|east}.
/70 ; sust(room_70//70)
You are in a small,clean,hall.
/71 ; sust(room_71//71)
You are approaching the borders of a low desert that rises on all sides {ACTION|east|east}.
/72 ; sust(room_72//72)
You are in the desert.
/73 ; sust(room_73//73)
You in the desert below a low shelf of limestone rock.
/74 ; sust(room_74//74)
You are {ACTION|north|north} of the store ware- house {ACTION|south|south} of the 1st floor.A terribly cold chill seems to come from the {ACTION|south|south},and clouds of dank fog cover the ground.
/75 ; sust(room_75//75)
\n\nYou are in the cold store ware- house amongst blocks of packed ice that are slowly melting.
/76 ; sust(room_76//76)
You are in the cold store ware- house amongst blocks of packed ice that are slowly melting.
/77 ; sust(room_77//77)
You are amongst blocks of packed stinking ice in the cold-store
/78 ; sust(room_78//78)
You are on a slim window ledge- masonry crumbles underfoot!
/79 ; sust(room_79//79)
You are in a burning car.Smoke fills the wreck!
/80
(extra message added so the newlines below aren't included in the message above)






/CON
/0 ; sust(room_0//0)

/1 ; sust(room_1//1)
e 2 ; sust(room_2/2)

/2 ; sust(room_2//2)
e 4 ; sust(room_4/4)
w 1 ; sust(room_1/1)

/3 ; sust(room_3//3)

/4 ; sust(room_4//4)
w 2 ; sust(room_2/2)

/5 ; sust(room_5//5)
s 6 ; sust(room_6/6)

/6 ; sust(room_6//6)
n 5 ; sust(room_5/5)
s 7 ; sust(room_7/7)

/7 ; sust(room_7//7)
n 6 ; sust(room_6/6)
s 9 ; sust(room_9/9)

/8 ; sust(room_8//8)
e 10 ; sust(room_10/10)
w 7 ; sust(room_7/7)

/9 ; sust(room_9//9)
n 7 ; sust(room_7/7)
out 7 ; sust(room_7/7)

/10 ; sust(room_10//10)
e 11 ; sust(room_11/11)
w 8 ; sust(room_8/8)

/11 ; sust(room_11//11)
s 13 ; sust(room_13/13)

/12 ; sust(room_12//12)
out 10 ; sust(room_10/10)

/13 ; sust(room_13//13)
e 14 ; sust(room_14/14)
n 14 ; sust(room_14/14)
s 13 ; sust(room_13/13)

/14 ; sust(room_14//14)
e 15 ; sust(room_15/15)
n 15 ; sust(room_15/15)
s 14 ; sust(room_14/14)
w 11 ; sust(room_11/11)

/15 ; sust(room_15//15)
e 14 ; sust(room_14/14)
n 13 ; sust(room_13/13)
nw 15 ; sust(room_15/15)
s 14 ; sust(room_14/14)
sw 16 ; sust(room_16/16)
w 15 ; sust(room_15/15)

/16 ; sust(room_16//16)
e 17 ; sust(room_17/17)
nw 14 ; sust(room_14/14)

/17 ; sust(room_17//17)
w 16 ; sust(room_16/16)

/18 ; sust(room_18//18)
out 17 ; sust(room_17/17)

/19 ; sust(room_19//19)
e 16 ; sust(room_16/16)
out 16 ; sust(room_16/16)
u 20 ; sust(room_20/20)

/20 ; sust(room_20//20)
d 19 ; sust(room_19/19)

/21 ; sust(room_21//21)
e 20 ; sust(room_20/20)
out 20 ; sust(room_20/20)
u 22 ; sust(room_22/22)

/22 ; sust(room_22//22)
d 21 ; sust(room_21/21)

/23 ; sust(room_23//23)
d 22 ; sust(room_22/22)

/24 ; sust(room_24//24)
w 25 ; sust(room_25/25)

/25 ; sust(room_25//25)
e 24 ; sust(room_24/24)
s 26 ; sust(room_26/26)

/26 ; sust(room_26//26)
n 25 ; sust(room_25/25)
u 27 ; sust(room_27/27)

/27 ; sust(room_27//27)

/28 ; sust(room_28//28)
e 29 ; sust(room_29/29)
s 30 ; sust(room_30/30)

/29 ; sust(room_29//29)
out 28 ; sust(room_28/28)
w 28 ; sust(room_28/28)

/30 ; sust(room_30//30)
n 28 ; sust(room_28/28)
w 31 ; sust(room_31/31)

/31 ; sust(room_31//31)
e 30 ; sust(room_30/30)

/32 ; sust(room_32//32)

/33 ; sust(room_33//33)
e 34 ; sust(room_34/34)

/34 ; sust(room_34//34)
n 35 ; sust(room_35/35)
s 37 ; sust(room_37/37)
w 33 ; sust(room_33/33)

/35 ; sust(room_35//35)

/36 ; sust(room_36//36)

/37 ; sust(room_37//37)
e 39 ; sust(room_39/39)
n 38 ; sust(room_38/38)
w 34 ; sust(room_34/34)

/38 ; sust(room_38//38)
s 37 ; sust(room_37/37)

/39 ; sust(room_39//39)
e 40 ; sust(room_40/40)
n 40 ; sust(room_40/40)
nw 41 ; sust(room_41/41)
s 40 ; sust(room_40/40)
w 40 ; sust(room_40/40)

/40 ; sust(room_40//40)
e 41 ; sust(room_41/41)
n 40 ; sust(room_40/40)
s 40 ; sust(room_40/40)
sw 40 ; sust(room_40/40)
w 41 ; sust(room_41/41)

/41 ; sust(room_41//41)
e 40 ; sust(room_40/40)
n 40 ; sust(room_40/40)
ne 40 ; sust(room_40/40)
nw 40 ; sust(room_40/40)
s 40 ; sust(room_40/40)
se 37 ; sust(room_37/37)
sw 41 ; sust(room_41/41)
w 40 ; sust(room_40/40)

/42 ; sust(room_42//42)
d 37 ; sust(room_37/37)

/43 ; sust(room_43//43)
s 46 ; sust(room_46/46)
u 44 ; sust(room_44/44)

/44 ; sust(room_44//44)
d 43 ; sust(room_43/43)
u 45 ; sust(room_45/45)

/45 ; sust(room_45//45)
d 44 ; sust(room_44/44)

/46 ; sust(room_46//46)
e 47 ; sust(room_47/47)
n 43 ; sust(room_43/43)

/47 ; sust(room_47//47)
e 48 ; sust(room_48/48)
w 46 ; sust(room_46/46)

/48 ; sust(room_48//48)
w 47 ; sust(room_47/47)

/49 ; sust(room_49//49)
s 50 ; sust(room_50/50)

/50 ; sust(room_50//50)
n 49 ; sust(room_49/49)

/51 ; sust(room_51//51)
e 52 ; sust(room_52/52)

/52 ; sust(room_52//52)
e 53 ; sust(room_53/53)
w 51 ; sust(room_51/51)

/53 ; sust(room_53//53)
w 52 ; sust(room_52/52)

/54 ; sust(room_54//54)

/55 ; sust(room_55//55)
sw 72 ; sust(room_72/72)

/56 ; sust(room_56//56)
n 57 ; sust(room_57/57)
out 55 ; sust(room_55/55)
w 55 ; sust(room_55/55)

/57 ; sust(room_57//57)
e 60 ; sust(room_60/60)
n 58 ; sust(room_58/58)
s 56 ; sust(room_56/56)

/58 ; sust(room_58//58)
e 59 ; sust(room_59/59)
s 57 ; sust(room_57/57)

/59 ; sust(room_59//59)
w 58 ; sust(room_58/58)

/60 ; sust(room_60//60)
w 57 ; sust(room_57/57)

/61 ; sust(room_61//61)
e 63 ; sust(room_63/63)
s 62 ; sust(room_62/62)
w 72 ; sust(room_72/72)

/62 ; sust(room_62//62)
n 61 ; sust(room_61/61)

/63 ; sust(room_63//63)
e 64 ; sust(room_64/64)
w 61 ; sust(room_61/61)

/64 ; sust(room_64//64)
w 63 ; sust(room_63/63)

/65 ; sust(room_65//65)
n 66 ; sust(room_66/66)
s 64 ; sust(room_64/64)

/66 ; sust(room_66//66)
s 65 ; sust(room_65/65)
sw 67 ; sust(room_67/67)

/67 ; sust(room_67//67)
e 68 ; sust(room_68/68)
ne 66 ; sust(room_66/66)

/68 ; sust(room_68//68)
w 67 ; sust(room_67/67)

/69 ; sust(room_69//69)
e 70 ; sust(room_70/70)
w 68 ; sust(room_68/68)

/70 ; sust(room_70//70)
w 69 ; sust(room_69/69)

/71 ; sust(room_71//71)
e 71 ; sust(room_71/71)
n 71 ; sust(room_71/71)
s 71 ; sust(room_71/71)
w 72 ; sust(room_72/72)

/72 ; sust(room_72//72)
e 71 ; sust(room_71/71)
n 72 ; sust(room_72/72)
ne 72 ; sust(room_72/72)
nw 55 ; sust(room_55/55)
s 72 ; sust(room_72/72)
se 72 ; sust(room_72/72)
sw 73 ; sust(room_73/73)
w 71 ; sust(room_71/71)

/73 ; sust(room_73//73)
e 72 ; sust(room_72/72)
n 71 ; sust(room_71/71)
ne 73 ; sust(room_73/73)
nw 73 ; sust(room_73/73)
s 71 ; sust(room_71/71)
se 61 ; sust(room_61/61)
sw 71 ; sust(room_71/71)
w 72 ; sust(room_72/72)

/74 ; sust(room_74//74)
e 74 ; sust(room_74/74)
n 76 ; sust(room_76/76)
ne 19 ; sust(room_19/19)
s 75 ; sust(room_75/75)
w 74 ; sust(room_74/74)

/75 ; sust(room_75//75)
e 75 ; sust(room_75/75)
n 75 ; sust(room_75/75)
s 75 ; sust(room_75/75)
sw 76 ; sust(room_76/76)
w 75 ; sust(room_75/75)

/76 ; sust(room_76//76)
e 75 ; sust(room_75/75)
n 76 ; sust(room_76/76)
ne 77 ; sust(room_77/77)
s 75 ; sust(room_75/75)
w 76 ; sust(room_76/76)

/77 ; sust(room_77//77)
e 75 ; sust(room_75/75)
n 75 ; sust(room_75/75)
ne 76 ; sust(room_76/76)
nw 76 ; sust(room_76/76)
out 74 ; sust(room_74/74)
s 75 ; sust(room_75/75)
se 76 ; sust(room_76/76)
sw 76 ; sust(room_76/76)
w 75 ; sust(room_75/75)

/78 ; sust(room_78//78)

/79 ; sust(room_79//79)
out 1 ; sust(room_1/1)







/OBJ

/0          54      1    _    _       10000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_0_trol//0) ; sust(room_54/54) ; sust(ght/ATTR)
/1               79      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_1//1) ; sust(room_79/79) ; sust(ATTR/ATTR)
/2               25      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_2//2) ; sust(room_25/25) ; sust(ATTR/ATTR)
/3               38      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_3//3) ; sust(room_38/38) ; sust(ATTR/ATTR)
/4               74      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_4//4) ; sust(room_74/74) ; sust(ATTR/ATTR)
/5               29      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_5//5) ; sust(room_29/29) ; sust(ATTR/ATTR)
/6       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_6//6) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/7       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_7//7) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/8       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_8//8) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/9       252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_9//9) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/10      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_10//10) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/11         18      1 glov _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_11_glov//11) ; sust(room_18/18) ; sust(ATTR/ATTR)
/12              35      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_12//12) ; sust(room_35/35) ; sust(ATTR/ATTR)
/13      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_13//13) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/14         32      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_14_suit//14) ; sust(room_32/32) ; sust(ATTR/ATTR)
/15  252      1 key  _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_15_key//15) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/16         42      1 meta _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_16_meta//16) ; sust(room_42/42) ; sust(ATTR/ATTR)
/17 252      1 bott _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_17_bott//17) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/18              51      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_18//18) ; sust(room_51/51) ; sust(ATTR/ATTR)
/19         78      1 elec _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_19_elec//19) ; sust(room_78/78) ; sust(ATTR/ATTR)
/20              52      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_20//20) ; sust(room_52/52) ; sust(ATTR/ATTR)
/21      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_21//21) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/22         32      1 pock _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_22_pock//22) ; sust(room_32/32) ; sust(ATTR/ATTR)
/23               6      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_23//23) ; sust(room_6/6) ; sust(ATTR/ATTR)
/24      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_24//24) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/25               2      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_25//25) ; sust(room_2/2) ; sust(ATTR/ATTR)
/26         12      1 span _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_26_span//26) ; sust(room_12/12) ; sust(ATTR/ATTR)
/27         77      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_27_rope//27) ; sust(room_77/77) ; sust(ATTR/ATTR)
/28      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_28//28) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/29  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_29_sue//29) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/30              20      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_30//30) ; sust(room_20/20) ; sust(ATTR/ATTR)
/31         50      1 boot _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_31_boot//31) ; sust(room_50/50) ; sust(ATTR/ATTR)
/32      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_32//32) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/33      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_33//33) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/34         41      1 scre _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_34_scre//34) ; sust(room_41/41) ; sust(ATTR/ATTR)
/35          48      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_35_can//35) ; sust(room_48/48) ; sust(ATTR/ATTR)
/36  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_36_can//36) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/37 252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_37_trol//37) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/38      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_38//38) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/39              68      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_39//39) ; sust(room_68/68) ; sust(ATTR/ATTR)
/40      252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_40//40) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)






/PRO 0








_ _
 hook    "RESPONSE_START"


_ _
 hook    "RESPONSE_USER"

n _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 eq      27 140 ; sust(game_flag_27/27)
 minus   27 5 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

n _
 at      3 ; sust(room_3/3)
 eq      27 160 ; sust(game_flag_27/27)
 notzero 113 ; sust(game_flag_13/113)
 minus   27 10 ; sust(game_flag_27/27)
 clear   5 ; sust(countdown_player_input_1/5)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

n _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 eq      27 170 ; sust(game_flag_27/27)
 minus   27 20 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

n _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

n _
 at      30 ; sust(room_30/30)
 writeln "It looks a long way-I'd get some transport if I were you!"

n _
 at      33 ; sust(room_33/33)
 writeln "It looks a long way-I'd get some transport if I were you!"

n _
 at      43 ; sust(room_43/43)
 writeln "It looks a long way-I'd get some transport if I were you!"

n _
 at      46 ; sust(room_46/46)
 writeln "It looks a long way-I'd get some transport if I were you!"

n _
 at      49 ; sust(room_49/49)
 writeln "It looks a long way-I'd get some transport if I were you!"

n _
 at      28 ; sust(room_28/28)
 writeln "It looks a long way-I'd get some transport if I were you!"

n _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 chance  50
 clear   113 ; sust(game_flag_13/113)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)

n _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 114 ; sust(game_flag_14/114)
 chance  30
 clear   114 ; sust(game_flag_14/114)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)

n _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 116 ; sust(game_flag_16/116)
 chance  75
 clear   116 ; sust(game_flag_16/116)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)

n _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

n _
 at      32 ; sust(room_32/32)
 present 33 ; sust(object_33/33)
 writeln "You're not strong enough!"
 done

n _
 at      32 ; sust(room_32/32)
 gt      8 0 ; sust(countdown_player_input_4/8)
 let     8 1 ; sust(countdown_player_input_4/8)
 goto    31 ; sust(room_31/31)
 desc

n _
 at      3 ; sust(room_3/3)
 eq      27 161 ; sust(game_flag_27/27)
 writeln "The mini-bus is out of gas!"

n _
 eq      20 0 ; sust(game_flag_20/20)
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 eq      27 160 ; sust(game_flag_27/27)
 minus   27 10 ; sust(game_flag_27/27)

n _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 eq      27 150 ; sust(game_flag_27/27)
 minus   27 10 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

n _
 at      52 ; sust(room_52/52)
 carried 0 ; sust(object_0_trol/0)
 writeln "The trolley won't fit through the door!"
 pause   100

n _
 at      52 ; sust(room_52/52)
 carried 37 ; sust(object_37_trol/37)
 writeln "The trolley won't fit through the door!"
 pause   100

n _
 at      52 ; sust(room_52/52)
 notcarr 37 ; sust(object_37_trol/37)
 notcarr 0 ; sust(object_0_trol/0)
 goto    50 ; sust(room_50/50)
 desc

n _
 at      54 ; sust(room_54/54)
 notzero 5 ; sust(countdown_player_input_1/5)
 let     5 1 ; sust(countdown_player_input_1/5)
 goto    52 ; sust(room_52/52)
 desc

n _
 at      52 ; sust(room_52/52)
 eq      5 0 ; sust(countdown_player_input_1/5)
 goto    52 ; sust(room_52/52)
 desc

n _
 at      54 ; sust(room_54/54)
 goto    52 ; sust(room_52/52)
 desc

s _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

s _
 at      3 ; sust(room_3/3)
 lt      27 130 ; sust(game_flag_27/27)
 writeln "The mini-bus is out of gas!"
 done

s _
 at      3 ; sust(room_3/3)
 eq      27 135 ; sust(game_flag_27/27)
 notzero 113 ; sust(game_flag_13/113)
 plus    27 5 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

s _
 at      3 ; sust(room_3/3)
 eq      27 140 ; sust(game_flag_27/27)
 notzero 113 ; sust(game_flag_13/113)
 plus    27 10 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

s _
 at      3 ; sust(room_3/3)
 eq      20 1 ; sust(game_flag_20/20)
 eq      27 150 ; sust(game_flag_27/27)
 notzero 113 ; sust(game_flag_13/113)
 plus    27 11 ; sust(game_flag_27/27)
 set     5 ; sust(countdown_player_input_1/5)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

s _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 eq      27 160 ; sust(game_flag_27/27)
 plus    27 10 ; sust(game_flag_27/27)
 clear   5 ; sust(countdown_player_input_1/5)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

s _
 at      19 ; sust(room_19/19)
 notzero 112 ; sust(game_flag_12/112)
 writeln "&#34;I'll stay here!&#34;"
 clear   112 ; sust(game_flag_12/112)
 create  1 ; sust(object_1/1)
 minus   17 2 ; sust(game_flag_17/17)
 minus   18 6 ; sust(game_flag_18/18)
 minus   19 1 ; sust(game_flag_19/19)
 minus   21 8 ; sust(game_flag_21/21)
 done

s _
 at      19 ; sust(room_19/19)
 notzero 113 ; sust(game_flag_13/113)
 clear   113 ; sust(game_flag_13/113)
 create  2 ; sust(object_2/2)
 writeln "&#34;UGH!I'm not going in there!&#34;"
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)
 done

s _
 at      19 ; sust(room_19/19)
 notzero 114 ; sust(game_flag_14/114)
 clear   114 ; sust(game_flag_14/114)
 create  3 ; sust(object_3/3)
 writeln "&#34;I'll hang around here.&#34;"
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)
 done

s _
 at      19 ; sust(room_19/19)
 notzero 115 ; sust(game_flag_15/115)
 clear   115 ; sust(game_flag_15/115)
 create  4 ; sust(object_4/4)
 writeln "&#34;Too cold in there for me!&#34;"
 minus   17 6 ; sust(game_flag_17/17)
 minus   18 3 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 6 ; sust(game_flag_21/21)
 done

s _
 at      19 ; sust(room_19/19)
 notzero 116 ; sust(game_flag_16/116)
 clear   116 ; sust(game_flag_16/116)
 writeln "&#34;OOH NO!I'll wait here!&#34;"
 create  5 ; sust(object_5/5)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)
 done

s _
 at      19 ; sust(room_19/19)
 eq      112 0 ; sust(game_flag_12/112)
 eq      113 0 ; sust(game_flag_13/113)
 eq      114 0 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 eq      116 0 ; sust(game_flag_16/116)
 goto    74 ; sust(room_74/74)
 desc

s _
 at      50 ; sust(room_50/50)
 writeln "The door is locked"

s _
 at      35 ; sust(room_35/35)
 goto    34 ; sust(room_34/34)
 desc

s _
 at      2 ; sust(room_2/2)
 writeln "It looks a long way-I'd get some transport if I were you!"

s _
 at      30 ; sust(room_30/30)
 writeln "It looks a long way-I'd get some transport if I were you!"

s _
 at      33 ; sust(room_33/33)
 writeln "It looks a long way-I'd get some transport if I were you!"
 done

s _
 at      43 ; sust(room_43/43)
 writeln "It looks a long way-I'd get some transport if I were you!"

s _
 at      46 ; sust(room_46/46)
 writeln "It looks a long way-I'd get some transport if I were you!"

s _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 chance  50
 clear   113 ; sust(game_flag_13/113)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)

s _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 114 ; sust(game_flag_14/114)
 chance  30
 clear   114 ; sust(game_flag_14/114)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)

s _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 116 ; sust(game_flag_16/116)
 chance  75
 clear   116 ; sust(game_flag_16/116)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)

s _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

s _
 at      31 ; sust(room_31/31)
 absent  33 ; sust(object_33/33)
 eq      112 0 ; sust(game_flag_12/112)
 let     8 5 ; sust(countdown_player_input_4/8)
 goto    32 ; sust(room_32/32)
 desc

s _
 at      31 ; sust(room_31/31)
 present 33 ; sust(object_33/33)
 writeln "You're not strong enough!"

s _
 at      31 ; sust(room_31/31)
 absent  33 ; sust(object_33/33)
 notzero 112 ; sust(game_flag_12/112)
 plus    8 6 ; sust(countdown_player_input_4/8)
 goto    32 ; sust(room_32/32)
 desc

s _
 at      3 ; sust(room_3/3)
 eq      27 161 ; sust(game_flag_27/27)
 writeln "The mini-bus is out of gas!"
 done

s _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 eq      20 0 ; sust(game_flag_20/20)
 eq      27 160 ; sust(game_flag_27/27)
 plus    27 10 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 clear   5 ; sust(countdown_player_input_1/5)
 done

s _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 eq      20 0 ; sust(game_flag_20/20)
 eq      27 150 ; sust(game_flag_27/27)
 plus    27 20 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 clear   5 ; sust(countdown_player_input_1/5)
 done

s _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"
 done

s _
 at      52 ; sust(room_52/52)
 present 20 ; sust(object_20/20)
 writeln "The mad-man blocks you!"

s _
 at      52 ; sust(room_52/52)
 absent  20 ; sust(object_20/20)
 goto    54 ; sust(room_54/54)
 desc

e _
 at      4 ; sust(room_4/4)
 notcarr 24 ; sust(object_24/24)
 goto    5 ; sust(room_5/5)
 let     111 95 ; sust(game_flag_11/111)
 writeln "You move along the road.."
 anykey
 desc

e _
 at      4 ; sust(room_4/4)
 carried 24 ; sust(object_24/24)
 writeln "You move along the road on the bicycle.."
 anykey
 goto    5 ; sust(room_5/5)
 desc

e _
 at      3 ; sust(room_3/3)
 eq      27 170 ; sust(game_flag_27/27)
 plus    27 10 ; sust(game_flag_27/27)
 writeln "The bus moves along the road.You travel for a while until Sue pulls the bus to a halt."
 done

e _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"

e _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

e _
 at      7 ; sust(room_7/7)
 notzero 113 ; sust(game_flag_13/113)
 writeln "&#34;I'm not going in there-it's dark in there!&#34;"

e _
 at      7 ; sust(room_7/7)
 eq      113 0 ; sust(game_flag_13/113)
 goto    8 ; sust(room_8/8)
 desc

e _
 at      49 ; sust(room_49/49)
 writeln "It looks a long way-I'd get some transport if I were you!"

e _
 at      55 ; sust(room_55/55)
 writeln "The door is locked"
 done

e _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 chance  50
 clear   113 ; sust(game_flag_13/113)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)

e _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 114 ; sust(game_flag_14/114)
 chance  30
 clear   114 ; sust(game_flag_14/114)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)

e _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 116 ; sust(game_flag_16/116)
 chance  75
 clear   116 ; sust(game_flag_16/116)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)

e _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

e _
 at      68 ; sust(room_68/68)
 present 39 ; sust(object_39/39)
 notcarr 17 ; sust(object_17_bott/17)
 writeln "&#34;GET BACK TO THE CAMP!&#34;A fight breaks out,but you are overcome!"
 anykey
 goto    64 ; sust(room_64/64)
 desc

e _
 at      68 ; sust(room_68/68)
 absent  39 ; sust(object_39/39)
 goto    69 ; sust(room_69/69)
 desc

e _
 at      24 ; sust(room_24/24)
 writeln "You fall to your death!"
 score
 end

w _
 at      5 ; sust(room_5/5)
 notcarr 24 ; sust(object_24/24)
 plus    111 3 ; sust(game_flag_11/111)
 writeln "You move along the road.."
 pause   100
 goto    4 ; sust(room_4/4)
 desc

w _
 at      5 ; sust(room_5/5)
 carried 24 ; sust(object_24/24)
 writeln "You move along the road on the bicycle.."
 pause   100
 goto    4 ; sust(room_4/4)
 desc

w _
 at      3 ; sust(room_3/3)
 eq      27 180 ; sust(game_flag_27/27)
 writeln "The mini-bus is out of gas!"
 done

w _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"

w _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

w _
 at      11 ; sust(room_11/11)
 eq      113 0 ; sust(game_flag_13/113)
 goto    10 ; sust(room_10/10)
 desc

w _
 at      11 ; sust(room_11/11)
 notzero 113 ; sust(game_flag_13/113)
 writeln "&#34;I'm not going in there-it's dark in there!&#34;"

w _
 at      20 ; sust(room_20/20)
 present 30 ; sust(object_30/30)
 writeln "It's bolted shut!"

w _
 at      20 ; sust(room_20/20)
 absent  30 ; sust(object_30/30)
 goto    21 ; sust(room_21/21)
 desc

w _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 chance  50
 clear   113 ; sust(game_flag_13/113)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)

w _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 114 ; sust(game_flag_14/114)
 chance  30
 clear   114 ; sust(game_flag_14/114)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)

w _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 116 ; sust(game_flag_16/116)
 chance  75
 clear   116 ; sust(game_flag_16/116)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)

w _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

ne _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

ne _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"

ne _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

nw _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

nw _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"

nw _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

nw _
 at      64 ; sust(room_64/64)
 notzero 112 ; sust(game_flag_12/112)
 eq      113 0 ; sust(game_flag_13/113)
 eq      114 0 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 eq      116 0 ; sust(game_flag_16/116)
 writeln "Go on then-but don't go making a nuisance of yourself!"
 pause   100
 goto    65 ; sust(room_65/65)
 desc

nw _
 at      64 ; sust(room_64/64)
 eq      112 0 ; sust(game_flag_12/112)
 notzero 113 ; sust(game_flag_13/113)
 eq      114 0 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 eq      116 0 ; sust(game_flag_16/116)
 goto    65 ; sust(room_65/65)
 writeln "Go on then-but don't go making a nuisance of yourself!"
 pause   100
 desc

nw _
 at      64 ; sust(room_64/64)
 eq      112 0 ; sust(game_flag_12/112)
 eq      113 0 ; sust(game_flag_13/113)
 notzero 114 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 eq      116 0 ; sust(game_flag_16/116)
 goto    65 ; sust(room_65/65)
 writeln "Go on then-but don't go making a nuisance of yourself!"
 pause   100
 desc

nw _
 at      64 ; sust(room_64/64)
 eq      112 0 ; sust(game_flag_12/112)
 eq      113 0 ; sust(game_flag_13/113)
 eq      114 0 ; sust(game_flag_14/114)
 notzero 115 ; sust(game_flag_15/115)
 eq      116 0 ; sust(game_flag_16/116)
 goto    65 ; sust(room_65/65)
 writeln "Go on then-but don't go making a nuisance of yourself!"
 pause   100
 desc

nw _
 at      64 ; sust(room_64/64)
 eq      112 0 ; sust(game_flag_12/112)
 eq      113 0 ; sust(game_flag_13/113)
 eq      114 0 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 notzero 116 ; sust(game_flag_16/116)
 goto    65 ; sust(room_65/65)
 writeln "Go on then-but don't go making a nuisance of yourself!"
 pause   100
 desc

nw _
 at      64 ; sust(room_64/64)
 writeln "Hang on!Hang on!Don't be in such a hurry-it's more than me job's worth to just let anyone in!"
 done

se _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

se _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"

se _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

sw _
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

sw _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"

sw _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

u _
 at      22 ; sust(room_22/22)
 notworn 11 ; sust(object_11_glov/11)
 writeln "The cables are greased!You slip down the shaft!"
 score
 end

u _
 at      22 ; sust(room_22/22)
 worn    11 ; sust(object_11_glov/11)
 goto    23 ; sust(room_23/23)
 desc

d _
 at      27 ; sust(room_27/27)
 present 28 ; sust(object_28/28)
 writeln "You descend the rope!"
 pause   100
 goto    16 ; sust(room_16/16)
 desc

d _
 at      27 ; sust(room_27/27)
 absent  28 ; sust(object_28/28)
 goto    26 ; sust(room_26/26)
 desc

in _
 notat   3 ; sust(room_3/3)
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

in _
 present 25 ; sust(object_25/25)
 notcarr 24 ; sust(object_24/24)
 goto    3 ; sust(room_3/3)
 desc

in _
 present 32 ; sust(object_32/32)
 goto    36 ; sust(room_36/36)
 desc

in _
 carried 24 ; sust(object_24/24)
 writeln "I'd drop the cycle 1st!"

in _
 at      10 ; sust(room_10/10)
 notcarr 24 ; sust(object_24/24)
 goto    12 ; sust(room_12/12)
 desc

in _
 at      16 ; sust(room_16/16)
 notcarr 24 ; sust(object_24/24)
 goto    19 ; sust(room_19/19)
 desc

in _
 present 40 ; sust(object_40/40)
 goto    79 ; sust(room_79/79)
 desc

in _
 at      17 ; sust(room_17/17)
 notcarr 24 ; sust(object_24/24)
 goto    18 ; sust(room_18/18)
 desc

out _
 notat   3 ; sust(room_3/3)
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "DO NOT MOVE AROUND IN THE DARK! {CLASS|center|-YOU MAY LOSE SOMEONE!}"

out _
 at      3 ; sust(room_3/3)
 lt      27 140 ; sust(game_flag_27/27)
 goto    2 ; sust(room_2/2)
 create  25 ; sust(object_25/25)
 desc

out _
 at      3 ; sust(room_3/3)
 eq      27 140 ; sust(game_flag_27/27)
 goto    28 ; sust(room_28/28)
 create  25 ; sust(object_25/25)
 desc

out _
 at      3 ; sust(room_3/3)
 eq      27 150 ; sust(game_flag_27/27)
 goto    33 ; sust(room_33/33)
 create  25 ; sust(object_25/25)
 desc

out _
 at      3 ; sust(room_3/3)
 eq      27 160 ; sust(game_flag_27/27)
 goto    43 ; sust(room_43/43)
 create  25 ; sust(object_25/25)
 desc

out _
 at      3 ; sust(room_3/3)
 eq      27 170 ; sust(game_flag_27/27)
 goto    49 ; sust(room_49/49)
 create  25 ; sust(object_25/25)
 desc

out _
 eq      27 180 ; sust(game_flag_27/27)
 goto    71 ; sust(room_71/71)
 create  25 ; sust(object_25/25)
 desc

out _
 at      36 ; sust(room_36/36)
 lt      31 4 ; sust(total_turns_lower/31)
 goto    34 ; sust(room_34/34)
 desc

out _
 at      36 ; sust(room_36/36)
 eq      31 4 ; sust(total_turns_lower/31)
 goto    42 ; sust(room_42/42)
 create  32 ; sust(object_32/32)
 desc

out _
 at      3 ; sust(room_3/3)
 eq      27 161 ; sust(game_flag_27/27)
 goto    43 ; sust(room_43/43)
 create  25 ; sust(object_25/25)
 desc

jump _
 at      23 ; sust(room_23/23)
 goto    24 ; sust(room_24/24)
 desc

jump _
 at      24 ; sust(room_24/24)
 writeln "You fall to your death!"
 score
 end

jump _
 writeln "Please be more specific."

give _
 writeln "Nobody here wants it!"

scor _
 score

fuck _
 notzero 112 ; sust(game_flag_12/112)
 writeln "What are you cussing for?"

fuck _
 notzero 113 ; sust(game_flag_13/113)
 writeln "That's exactly how I feel!"

fuck _
 notzero 114 ; sust(game_flag_14/114)
 writeln "Sheesh!Bad language as well!"

fuck _
 notzero 115 ; sust(game_flag_15/115)
 writeln "You're not the only one round here,you know!"

fuck _
 notzero 116 ; sust(game_flag_16/116)
 writeln "REALLY!"

fuck _
 eq      112 0 ; sust(game_flag_12/112)
 eq      113 0 ; sust(game_flag_13/113)
 eq      114 0 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 eq      116 0 ; sust(game_flag_16/116)
 writeln "Talking to ourselves now,huh?"

type za9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      7 0 ; sust(countdown_player_input_3/7)
 let     7 50 ; sust(countdown_player_input_3/7)
 writeln "MISSILES LAUNCHED.."
 done

type za9
 gt      6 29 ; sust(countdown_player_input_2/6)
 gt      7 50 ; sust(countdown_player_input_3/7)
 writeln "--ACTION UNAVAILABLE--"

type za9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      7 50 ; sust(countdown_player_input_3/7)
 writeln "--ACTION UNAVAILABLE--"

type za0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      7 0 ; sust(countdown_player_input_3/7)
 let     7 100 ; sust(countdown_player_input_3/7)
 writeln "MISSILES DE-ARMED"
 done

type za0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      7 50 ; sust(countdown_player_input_3/7)
 writeln "--ACTION UNAVAILABLE--"
 done

type za0
 gt      6 29 ; sust(countdown_player_input_2/6)
 gt      7 50 ; sust(countdown_player_input_3/7)
 writeln "--ACTION UNAVAILABLE--"
 done

type yb9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      4 0 ; sust(countdown_object_description_unlit/4)
 let     4 50 ; sust(countdown_object_description_unlit/4)
 writeln "MISSILES LAUNCHED.."
 done

type yb9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      4 100 ; sust(countdown_object_description_unlit/4)
 writeln "--ACTION UNAVAILABLE--"
 done

type yb9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      4 50 ; sust(countdown_object_description_unlit/4)
 writeln "--ACTION UNAVAILABLE--"
 done

type yb0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      4 0 ; sust(countdown_object_description_unlit/4)
 let     4 100 ; sust(countdown_object_description_unlit/4)
 writeln "MISSILES DE-ARMED"
 done

type yb0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      4 50 ; sust(countdown_object_description_unlit/4)
 writeln "--ACTION UNAVAILABLE--"
 done

type yb0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      4 100 ; sust(countdown_object_description_unlit/4)
 writeln "--ACTION UNAVAILABLE--"
 done

type xc0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      8 0 ; sust(countdown_player_input_4/8)
 let     8 100 ; sust(countdown_player_input_4/8)
 writeln "MISSILES DE-ARMED"
 done

type xc0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      8 50 ; sust(countdown_player_input_4/8)
 writeln "--ACTION UNAVAILABLE--"
 done

type xc0
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      8 100 ; sust(countdown_player_input_4/8)
 writeln "--ACTION UNAVAILABLE--"
 done

type xc9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      8 0 ; sust(countdown_player_input_4/8)
 let     8 50 ; sust(countdown_player_input_4/8)
 writeln "MISSILES LAUNCHED.."
 done

type xc9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      8 100 ; sust(countdown_player_input_4/8)
 writeln "--ACTION UNAVAILABLE--"
 done

type xc9
 gt      6 29 ; sust(countdown_player_input_2/6)
 eq      8 50 ; sust(countdown_player_input_4/8)
 writeln "--ACTION UNAVAILABLE--"
 done

type 4
 gt      6 2 ; sust(countdown_player_input_2/6)
 let     6 70 ; sust(countdown_player_input_2/6)
 writeln "-PLEASE PROCEED SECURITY OPTION-"

type 3
 gt      6 2 ; sust(countdown_player_input_2/6)
 let     6 59 ; sust(countdown_player_input_2/6)
 writeln "-PLEASE PROCEED SECURITY OPTION-"
 done

type 2
 gt      6 2 ; sust(countdown_player_input_2/6)
 let     6 50 ; sust(countdown_player_input_2/6)
 writeln "-PLEASE PROCEED SECURITY OPTION-"
 done

type 1
 gt      6 2 ; sust(countdown_player_input_2/6)
 let     6 40 ; sust(countdown_player_input_2/6)
 writeln "-PLEASE PROCEED SECURITY OPTION-"
 done

type yes
 at      54 ; sust(room_54/54)
 present 38 ; sust(object_38/38)
 eq      3 10 ; sust(countdown_location_description_dark/3)
 eq      6 1 ; sust(countdown_player_input_2/6)
 let     6 30 ; sust(countdown_player_input_2/6)
 writeln "{CLASS|center|LETS PLAY GLOBAL    }   THERMONUCLEAR WARFARE..."
 pause   100
 let     5 15 ; sust(countdown_player_input_1/5)
 done

type _
 absent  38 ; sust(object_38/38)
 writeln "Please be more specific."
 done

type _
 present 38 ; sust(object_38/38)
 lt      6 60 ; sust(countdown_player_input_2/6)
 writeln "PLEASE BE MORE SPECIFIC."

dial 1922
 at      54 ; sust(room_54/54)
 present 38 ; sust(object_38/38)
 eq      3 10 ; sust(countdown_location_description_dark/3)
 let     6 1 ; sust(countdown_player_input_2/6)
 writeln "GREETINGS,COMMANDER WALKER."
 pause   50
 writeln "   SHALL WE PLAY A GAME?"
 done

dial 1922
 at      54 ; sust(room_54/54)
 absent  38 ; sust(object_38/38)
 writeln "Sounds like interference!"
 done

dial 1922
 at      54 ; sust(room_54/54)
 eq      3 0 ; sust(countdown_location_description_dark/3)
 writeln "Sounds like interference!"
 done

dial _
 at      54 ; sust(room_54/54)
 writeln "LINE DEAD"

kill _
 writeln "Don't you think there's been enough of that already?"

read bibl
 at      48 ; sust(room_48/48)
 writeln "..shout unto God with the voice of triumph for..he is a great king over all the Earth..       {CLASS|center|          (Psalm 47)}"
 done

read pock
 carried 22 ; sust(object_22_pock/22)
 eq      9 0 ; sust(countdown_player_input_dark/9)
 writeln "???????????????????\n? ?? ACB Co. 2356 A-H\n? ?? D.Amri. 3578\n? ?? B.hart. 4789\n\n???????????????????"

read pock
 carried 22 ; sust(object_22_pock/22)
 eq      9 10 ; sust(countdown_player_input_dark/9)
 writeln "???????????????????\n? ?? Dr.Iril 4912 I-P\n? ?? P.Larl  1101\n? ?? S.King  00\n\n???????????????????"

read pock
 carried 22 ; sust(object_22_pock/22)
 eq      9 20 ; sust(countdown_player_input_dark/9)
 writeln "???????????????????\n? ?? T.Satim 8901 Q-Z\n? ?? M.White 9010\n? ?? F.Cross 1922\n\n???????????????????"

sear _
 writeln "You find nothing."

exam can
 present 35 ; sust(object_35_can/35)
 writeln "It's empty."
 done

exam can
 present 36 ; sust(object_36_can/36)
 writeln "It's full."
 done

exam scre
 eq      6 30 ; sust(countdown_player_input_2/6)
 writeln "     ????????????????????????\n     ??**COMPUTER COMMAND**??\n     ????????????????????????   {CLASS|center|1=HELP MENU   \nMAIN MENU             \n [2]STRIKES/MAIN SITINGS\n[3]STATUS 1ST STRIKE  \n [4]DE-ARM/*FIRE OPTIONS}"
 done

exam scre
 eq      6 40 ; sust(countdown_player_input_2/6)
 writeln "{CLASS|center|         HELP MENU\n         ?????????}                [1]FOLLOW MENU'S                 [2]TYPE CMNDS."
 minus   6 10 ; sust(countdown_player_input_2/6)
 done

exam scre
 eq      6 50 ; sust(countdown_player_input_2/6)
 writeln "{CLASS|center|[MENU 2]\n}WEST COAST DEFENCE\n     LOCNO.Z VANCOUVER CODE A\n      &#34; &#34;  Z NEVADA    CODE A\n      &#34; &#34;  Y ARIZONA   CODE B EAST COAST DEFENCE\n     LOCNO.Y MISSOURI  CODE B\n      &#34; &#34;  X MINNESOTA CODE C\n      &#34; &#34;  X MEMPHIS   CODE C"
 minus   6 20 ; sust(countdown_player_input_2/6)
 done

exam scre
 eq      6 59 ; sust(countdown_player_input_2/6)
 writeln "{CLASS|center|[ MENU 3 ]\n}TARGET DE-ARM:(STRIKE ABORT)- ASSIGN LOCATION/CODE.\nTARGET LAUNCH:(DUAL-KEY OVER- RIDE):ASSIGN LOCATION/CODE."
 minus   6 29 ; sust(countdown_player_input_2/6)
 done

exam scre
 gt      6 60 ; sust(countdown_player_input_2/6)
 writeln "{CLASS|center|          [ MENU 4 ]\n TYPE 9 TO FIRE/CODE LOCNO (?)\n TYPE 0 TO DE-ARM /LOCNO   (?)}"

exam scre
 gt      6 60 ; sust(countdown_player_input_2/6)
 eq      7 50 ; sust(countdown_player_input_3/7)
 writeln "A:?"

exam scre
 gt      6 60 ; sust(countdown_player_input_2/6)
 gt      7 50 ; sust(countdown_player_input_3/7)
 writeln "A:?"

exam scre
 gt      6 60 ; sust(countdown_player_input_2/6)
 eq      4 50 ; sust(countdown_object_description_unlit/4)
 writeln "B:?"

exam scre
 gt      6 60 ; sust(countdown_player_input_2/6)
 eq      8 50 ; sust(countdown_player_input_4/8)
 writeln "C:?"

exam scre
 gt      6 60 ; sust(countdown_player_input_2/6)
 eq      8 100 ; sust(countdown_player_input_4/8)
 writeln "C:?"

exam scre
 gt      6 60 ; sust(countdown_player_input_2/6)
 eq      4 100 ; sust(countdown_object_description_unlit/4)
 writeln "B:?"

exam scre
 at      54 ; sust(room_54/54)
 eq      6 1 ; sust(countdown_player_input_2/6)
 eq      3 10 ; sust(countdown_location_description_dark/3)
 writeln "GREETINGS,COMMANDER WALKER."
 writeln "   SHALL WE PLAY A GAME?"

exam boot
 present 31 ; sust(object_31_boot/31)
 writeln "Insulated against shocks."

exam butt
 at      60 ; sust(room_60/60)
 writeln "2 Buttons:- ON + OFF"
 done

exam pock
 carried 22 ; sust(object_22_pock/22)
 writeln "CMDR.C.WALKER."
 done

exam elec
 present 19 ; sust(object_19_elec/19)
 writeln "3 dual leads."

exam comp
 present 18 ; sust(object_18/18)
 writeln "A heavy-duty military Dual drive system,it has 3 edge connectors, labelled:SCREEN:POWER:MODEM."
 done

exam comp
 present 37 ; sust(object_37_trol/37)
 writeln "A heavy-duty military Dual drive system,it has 3 edge connectors, labelled:SCREEN:POWER:MODEM."
 done

exam comp
 present 38 ; sust(object_38/38)
 writeln "A heavy-duty military Dual drive system,it has 3 edge connectors, labelled:SCREEN:POWER:MODEM."
 done

exam bott
 present 17 ; sust(object_17_bott/17)
 writeln "A highly dangerous bacteria, developed at the cost of millions of dollars to kill mass amounts of people:it seems to have been very effective."
 done

exam _
 notat   54 ; sust(room_54/54)
 writeln "You notice nothing unusual."

hist sylv
 notzero 116 ; sust(game_flag_16/116)
 writeln "I've been waiting for somebody for an age!"
 done

hist bill
 notzero 115 ; sust(game_flag_15/115)
 writeln "There's a nuclear command centre a couple of hundred miles south where we'll have to de-arm all the bombs by command computer if we are to be free from nuclear attack!"
 done

hist sam
 notzero 114 ; sust(game_flag_14/114)
 writeln "I used to be with the fairground until the power went off.."
 done

hist sue
 notzero 113 ; sust(game_flag_13/113)
 writeln "When the plague broke me and my husband were in Mexico-we rushed back to our families-it was too late.They died some 3 weeks ago, -I headed N,passing a huge refugee camp."
 done

hist dave
 notzero 112 ; sust(game_flag_12/112)
 writeln "The food will suffer from bacter infection if moved-and attack is also more likely.My brother was killed by looters in New York, stealing Hi-Fi's&#59;crazy isn't it- no electricity,but the stores are looted!"
 done

hist _
 writeln "Please be more specific."

wait _
 writeln "Time passes..."
 plus    111 5 ; sust(game_flag_11/111)
 pause   0
 desc

time _
 lt      111 25 ; sust(game_flag_11/111)
 writeln "Approx 6 am."
 done

time _
 gt      111 24 ; sust(game_flag_11/111)
 lt      111 45 ; sust(game_flag_11/111)
 writeln "Approx 10 am."
 done

time _
 gt      111 44 ; sust(game_flag_11/111)
 lt      111 70 ; sust(game_flag_11/111)
 writeln "Approx 12 Noon."
 done

time _
 gt      111 69 ; sust(game_flag_11/111)
 lt      111 90 ; sust(game_flag_11/111)
 writeln "Approx 5 pm."
 done

time _
 gt      111 89 ; sust(game_flag_11/111)
 lt      111 100 ; sust(game_flag_11/111)
 writeln "Approx 9 pm."
 done

time _
 gt      111 99 ; sust(game_flag_11/111)
 lt      111 106 ; sust(game_flag_11/111)
 writeln "Approx 12 Midnight"
 done

time _
 gt      111 105 ; sust(game_flag_11/111)
 lt      111 112 ; sust(game_flag_11/111)
 writeln "Approx 3 am."
 done

time _
 gt      111 111 ; sust(game_flag_11/111)
 lt      111 118 ; sust(game_flag_11/111)
 writeln "Approx 5 a.m."
 done

fix gene
 present 12 ; sust(object_12/12)
 lt      18 12 ; sust(game_flag_18/18)
 writeln "Your engineering capabilities are not high enough!"
 done

fix gene
 present 12 ; sust(object_12/12)
 gt      18 11 ; sust(game_flag_18/18)
 carried 34 ; sust(object_34_scre/34)
 carried 26 ; sust(object_26_span/26)
 notworn 31 ; sust(object_31_boot/31)
 writeln "The generator crackles into life but you are not insulated-bolts of electricity shower you!"


 score
 end

fix gene
 present 12 ; sust(object_12/12)
 gt      18 11 ; sust(game_flag_18/18)
 worn    31 ; sust(object_31_boot/31)
 carried 26 ; sust(object_26_span/26)
 carried 34 ; sust(object_34_scre/34)
 writeln "The generator takes a long time to fix,but a few of you get it in working order after a while. As you flick the switch the generator buzzes into life,the boots protecting you!"


 let     31 2 ; sust(total_turns_lower/31)
 swap    12 13 ; sust(object_12/12) ; sust(object_13/13)
 plus    111 25 ; sust(game_flag_11/111)
 done

fix gene
 present 12 ; sust(object_12/12)
 notcarr 26 ; sust(object_26_span/26)
 notcarr 34 ; sust(object_34_scre/34)
 writeln "You don't have all the correct tools"
 done

fix gene
 present 12 ; sust(object_12/12)
 notcarr 34 ; sust(object_34_scre/34)
 writeln "You don't have all the correct tools"
 done

fix gene
 present 12 ; sust(object_12/12)
 notcarr 26 ; sust(object_26_span/26)
 writeln "You don't have all the correct tools"
 done

fix _
 writeln "Please be more specific."

ask sue
 at      27 ; sust(room_27/27)
 notzero 113 ; sust(game_flag_13/113)
 writeln "We could climb down with a rope!"
 done

ask sue
 at      24 ; sust(room_24/24)
 notzero 113 ; sust(game_flag_13/113)
 writeln "We'd fall!"

ask dave
 at      31 ; sust(room_31/31)
 notzero 112 ; sust(game_flag_12/112)
 absent  33 ; sust(object_33/33)
 writeln "I might be able to hold it back a little while.."

ask _
 eq      21 18 ; sust(game_flag_21/21)
 present 12 ; sust(object_12/12)
 writeln "&#34;looks repairable&#34;"

ask _
 eq      112 0 ; sust(game_flag_12/112)
 eq      113 0 ; sust(game_flag_13/113)
 eq      114 0 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 eq      116 0 ; sust(game_flag_16/116)
 absent  1 ; sust(object_1/1)
 absent  2 ; sust(object_2/2)
 absent  3 ; sust(object_3/3)
 absent  4 ; sust(object_4/4)
 absent  5 ; sust(object_5/5)
 writeln "Talking to ourselves now,huh?"
 done

ask _
 writeln "You are told nothing of interest"

inse elec
 at      54 ; sust(room_54/54)
 present 37 ; sust(object_37_trol/37)
 carried 19 ; sust(object_19_elec/19)
 gt      17 9 ; sust(game_flag_17/17)
 destroy 37 ; sust(object_37_trol/37)
 create  38 ; sust(object_38/38)
 destroy 19 ; sust(object_19_elec/19)
 desc

inse elec
 at      54 ; sust(room_54/54)
 present 37 ; sust(object_37_trol/37)
 carried 19 ; sust(object_19_elec/19)
 lt      17 10 ; sust(game_flag_17/17)
 writeln "Your technical abilities are not high enough!"
 done

inse elec
 notcarr 19 ; sust(object_19_elec/19)
 writeln "You have no wiring!"
 done

inse elec
 absent  37 ; sust(object_37_trol/37)
 absent  38 ; sust(object_38/38)
 absent  18 ; sust(object_18/18)
 writeln "To what?"
 done

inse elec
 notat   54 ; sust(room_54/54)
 writeln "To what?"
 done

inse meta
 at      20 ; sust(room_20/20)
 lt      17 10 ; sust(game_flag_17/17)
 writeln "Your partys' engineering ability is not high enough to ensure the use of the machine correctly!"
 done

inse meta
 at      20 ; sust(room_20/20)
 gt      17 10 ; sust(game_flag_17/17)
 carried 16 ; sust(object_16_meta/16)
 swap    16 15 ; sust(object_16_meta/16) ; sust(object_15_key/15)
 writeln "The lathe turns a new key!"
 done

inse _
 writeln "Please be more specific."

open wind
 at      25 ; sust(room_25/25)
 goto    78 ; sust(room_78/78)
 writeln "You open it and enter as it closes behind you.."
 pause   75
 desc

open wind
 at      78 ; sust(room_78/78)
 writeln "You open it and enter as it closes behind you.."
 goto    25 ; sust(room_25/25)
 pause   75
 desc

open door
 present 30 ; sust(object_30/30)
 writeln "You're not strong enough!"
 done

open door
 at      50 ; sust(room_50/50)
 notcarr 15 ; sust(object_15_key/15)
 writeln "The door is locked"
 done

open door
 at      50 ; sust(room_50/50)
 carried 15 ; sust(object_15_key/15)
 goto    52 ; sust(room_52/52)
 writeln "You open it and enter as it closes behind you.."
 pause   75
 desc

open door
 at      50 ; sust(room_50/50)
 notcarr 15 ; sust(object_15_key/15)
 writeln "You have no key"
 done

open door
 at      55 ; sust(room_55/55)
 carried 15 ; sust(object_15_key/15)
 writeln "You open it and enter as it closes behind you.."
 goto    56 ; sust(room_56/56)
 pause   100
 desc

open door
 at      60 ; sust(room_60/60)
 eq      10 10 ; sust(countdown_player_input_unlit/10)
 create  17 ; sust(object_17_bott/17)
 plus    10 1 ; sust(countdown_player_input_unlit/10)
 desc

open door
 at      60 ; sust(room_60/60)
 eq      10 0 ; sust(countdown_player_input_unlit/10)
 writeln "The pressurised chamber explodes as it is mechanically opened!Gas fills the room!"
 score
 end

open door
 at      60 ; sust(room_60/60)
 eq      10 11 ; sust(countdown_player_input_unlit/10)
 writeln "NO POWER"
 done

open bott
 carried 17 ; sust(object_17_bott/17)
 writeln "You really should watch what you are doing!"
 score
 end

open _
 at      55 ; sust(room_55/55)
 notcarr 15 ; sust(object_15_key/15)
 writeln "You have no key"
 done

open _
 at      31 ; sust(room_31/31)
 writeln "You'll need a couple of people to move the bolts-and some kind of wrench!"
 done

open _
 writeln "Please be more specific."

info _
 cls
 writeln "HISTORY x gives recent history of a character. TIME gives time, WAIT  SLEEP passing it. FEED x will feed a character, but only if food is available at that    location. STATISTICS x will telly ou a characters skills in      engineering, technical, social strength. The larger the party, the higher the skill. This      command only works at night with a pyschic in your party. RECRUIT x will get a character into your party, LEAVE x releasing them.  PARTY gives your party. STORE   will ram save your positon,     CALL loading it back. WORDS is  for text only, PICTURES turning pictures back on. Too big a party results in stress, and          characters may leave. This is   shown by a }} symbol."
 anykey
 desc

hit sue
 notzero 113 ; sust(game_flag_13/113)
 clear   113 ; sust(game_flag_13/113)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)
 create  29 ; sust(object_29_sue/29)
 writeln "Sue slumps to the ground!"
 done

hit sue
 atgt    27 ; sust(room_27/27)
 notzero 113 ; sust(game_flag_13/113)
 writeln "&#34;I don't know if you're just plain sexist or some kind of a moron-But I'm off!&#34;"
 clear   113 ; sust(game_flag_13/113)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)
 done

hit _
 writeln "..shout unto God with the voice of triumph for..he is a great king over all the Earth..       {CLASS|center|          (Psalm 47)}"

tie rope
 at      27 ; sust(room_27/27)
 carried 27 ; sust(object_27_rope/27)
 swap    27 28 ; sust(object_27_rope/27) ; sust(object_28/28)
 drop    28 ; sust(object_28/28)
 ok

tie rope
 carried 27 ; sust(object_27_rope/27)
 notat   27 ; sust(room_27/27)
 writeln "To what?"

driv _
 at      3 ; sust(room_3/3)
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No one here can drive the bus!"

driv _
 at      3 ; sust(room_3/3)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue snarls,&#34;Would you like to leave the driving to me-or are you going to lie there all day, giving orders?&#34;"
 done

fill can
 at      45 ; sust(room_45/45)
 carried 35 ; sust(object_35_can/35)
 swap    35 36 ; sust(object_35_can/35) ; sust(object_36_can/36)
 ok

fill can
 notat   45 ; sust(room_45/45)
 carried 35 ; sust(object_35_can/35)
 writeln "With what?"
 done

fill can
 carried 36 ; sust(object_36_can/36)
 writeln "It's full."
 done

fill bus
 at      2 ; sust(room_2/2)
 eq      27 125 ; sust(game_flag_27/27)
 plus    27 10 ; sust(game_flag_27/27)
 writeln "You fill the bus,emptying the damaged tanks."
 done

fill bus
 at      2 ; sust(room_2/2)
 eq      27 120 ; sust(game_flag_27/27)
 writeln "The pumps are off."
 done

fill bus
 carried 36 ; sust(object_36_can/36)
 at      43 ; sust(room_43/43)
 eq      20 1 ; sust(game_flag_20/20)
 clear   20 ; sust(game_flag_20/20)
 minus   27 1 ; sust(game_flag_27/27)
 writeln "You fill the bus and discard the can."
 destroy 36 ; sust(object_36_can/36)
 done

fill bott
 present 17 ; sust(object_17_bott/17)
 writeln "With what?"
 done

fill _
 at      2 ; sust(room_2/2)
 eq      27 135 ; sust(game_flag_27/27)
 writeln "PUMP EMPTY."
 done

fill _
 writeln "Please be more specific."

pres page
 carried 22 ; sust(object_22_pock/22)
 eq      9 0 ; sust(countdown_player_input_dark/9)
 plus    9 10 ; sust(countdown_player_input_dark/9)
 ok

pres page
 carried 22 ; sust(object_22_pock/22)
 eq      9 10 ; sust(countdown_player_input_dark/9)
 plus    9 10 ; sust(countdown_player_input_dark/9)
 ok

pres page
 eq      9 20 ; sust(countdown_player_input_dark/9)
 clear   9 ; sust(countdown_player_input_dark/9)
 ok

pres off
 lt      27 120 ; sust(game_flag_27/27)
 at      1 ; sust(room_1/1)
 let     27 120 ; sust(game_flag_27/27)
 writeln "The car hits one of the pumps and overturns!"
 create  40 ; sust(object_40/40)
 done

pres off
 at      1 ; sust(room_1/1)
 eq      27 125 ; sust(game_flag_27/27)
 writeln "OFF"
 minus   27 5 ; sust(game_flag_27/27)
 done

pres off
 at      1 ; sust(room_1/1)
 eq      27 120 ; sust(game_flag_27/27)
 writeln "Nothing happens."
 done

pres off
 eq      10 0 ; sust(countdown_player_input_unlit/10)
 at      60 ; sust(room_60/60)
 let     10 10 ; sust(countdown_player_input_unlit/10)
 writeln "OFF"
 done

pres off
 at      54 ; sust(room_54/54)
 eq      3 0 ; sust(countdown_location_description_dark/3)
 writeln "OFF"
 done

pres off
 present 38 ; sust(object_38/38)
 at      54 ; sust(room_54/54)
 clear   3 ; sust(countdown_location_description_dark/3)
 writeln "OFF"
 clear   5 ; sust(countdown_player_input_1/5)
 clear   6 ; sust(countdown_player_input_2/6)
 done

pres off
 at      54 ; sust(room_54/54)
 writeln "OFF"
 done

pres on
 at      1 ; sust(room_1/1)
 eq      27 120 ; sust(game_flag_27/27)
 writeln "ON"
 plus    27 5 ; sust(game_flag_27/27)
 done

pres on
 at      1 ; sust(room_1/1)
 eq      27 125 ; sust(game_flag_27/27)
 writeln "Nothing happens."
 done

pres on
 at      36 ; sust(room_36/36)
 eq      31 3 ; sust(total_turns_lower/31)
 plus    31 1 ; sust(total_turns_lower/31)
 writeln "The car climbs slowly to the top of the dipper and stops!"
 done

pres on
 at      36 ; sust(room_36/36)
 gt      31 3 ; sust(total_turns_lower/31)
 writeln "Nothing happens."
 done

pres on
 at      60 ; sust(room_60/60)
 eq      10 10 ; sust(countdown_player_input_unlit/10)
 clear   10 ; sust(countdown_player_input_unlit/10)
 writeln "ON"
 done

pres on
 at      54 ; sust(room_54/54)
 eq      3 0 ; sust(countdown_location_description_dark/3)
 let     3 10 ; sust(countdown_location_description_dark/3)
 writeln "ON"
 done

pres on
 at      54 ; sust(room_54/54)
 eq      3 10 ; sust(countdown_location_description_dark/3)
 writeln "ON"
 done

pres _
 at      1 ; sust(room_1/1)
 eq      27 135 ; sust(game_flag_27/27)
 writeln "PUMP EMPTY."
 done

pres _
 writeln "Please be more specific."

pres _
 at      60 ; sust(room_60/60)
 eq      10 11 ; sust(countdown_player_input_unlit/10)
 writeln "NO POWER"
 done

eat food
 at      40 ; sust(room_40/40)
 ok

eat food
 at      53 ; sust(room_53/53)
 ok

eat food
 at      9 ; sust(room_9/9)
 ok

eat food
 at      19 ; sust(room_19/19)
 ok

eat food
 at      62 ; sust(room_62/62)
 ok

who _
 writeln "{CLASS|center| ?????????????????????????\n ?Your party consists of:?\n ?????????????????????????}"

who _
 notzero 113 ; sust(game_flag_13/113)
 writeln "{CLASS|center|Sue Youart.}"

who _
 notzero 114 ; sust(game_flag_14/114)
 writeln "{CLASS|center|Sam Wieyenski.}"

who _
 notzero 115 ; sust(game_flag_15/115)
 writeln "{CLASS|center|Bill price.}"

who _
 notzero 116 ; sust(game_flag_16/116)
 writeln "{CLASS|center|Sylvia Wade.}"

who _
 notzero 112 ; sust(game_flag_12/112)
 writeln "{CLASS|center|Dave Kellern.}"

who _
 zero    112 ; sust(game_flag_12/112)
 zero    113 ; sust(game_flag_13/113)
 zero    114 ; sust(game_flag_14/114)
 zero    115 ; sust(game_flag_15/115)
 zero    116 ; sust(game_flag_16/116)
 writeln "{CLASS|center|No-one.}"

feed sylv
 at      40 ; sust(room_40/40)
 notzero 116 ; sust(game_flag_16/116)
 minus   26 100 ; sust(game_flag_26/26)
 writeln "The food is hastily scoffed!"
 done

feed sylv
 at      53 ; sust(room_53/53)
 notzero 116 ; sust(game_flag_16/116)
 minus   26 100 ; sust(game_flag_26/26)
 writeln "The food is hastily scoffed!"
 done

feed sylv
 at      9 ; sust(room_9/9)
 notzero 116 ; sust(game_flag_16/116)
 minus   26 100 ; sust(game_flag_26/26)
 writeln "The food is hastily scoffed!"
 done

feed sylv
 at      19 ; sust(room_19/19)
 notzero 116 ; sust(game_flag_16/116)
 minus   26 100 ; sust(game_flag_26/26)
 writeln "The food is hastily scoffed!"
 done

feed sylv
 at      62 ; sust(room_62/62)
 notzero 116 ; sust(game_flag_16/116)
 minus   26 100 ; sust(game_flag_26/26)
 writeln "The food is hastily scoffed!"
 done

feed bill
 notzero 115 ; sust(game_flag_15/115)
 at      40 ; sust(room_40/40)
 minus   25 100 ; sust(game_flag_25/25)
 writeln "The food is hastily scoffed!"
 done

feed bill
 at      53 ; sust(room_53/53)
 notzero 115 ; sust(game_flag_15/115)
 minus   25 100 ; sust(game_flag_25/25)
 writeln "The food is hastily scoffed!"
 done

feed bill
 at      9 ; sust(room_9/9)
 notzero 115 ; sust(game_flag_15/115)
 minus   25 100 ; sust(game_flag_25/25)
 writeln "The food is hastily scoffed!"
 done

feed bill
 at      19 ; sust(room_19/19)
 notzero 115 ; sust(game_flag_15/115)
 minus   25 100 ; sust(game_flag_25/25)
 writeln "The food is hastily scoffed!"
 done

feed bill
 at      62 ; sust(room_62/62)
 notzero 115 ; sust(game_flag_15/115)
 minus   25 100 ; sust(game_flag_25/25)
 writeln "The food is hastily scoffed!"
 done

feed sam
 at      40 ; sust(room_40/40)
 notzero 114 ; sust(game_flag_14/114)
 minus   24 100 ; sust(game_flag_24/24)
 writeln "The food is hastily scoffed!"
 done

feed sam
 at      53 ; sust(room_53/53)
 notzero 114 ; sust(game_flag_14/114)
 minus   24 100 ; sust(game_flag_24/24)
 writeln "The food is hastily scoffed!"
 done

feed sam
 at      9 ; sust(room_9/9)
 notzero 114 ; sust(game_flag_14/114)
 minus   24 100 ; sust(game_flag_24/24)
 writeln "The food is hastily scoffed!"
 done

feed sam
 at      19 ; sust(room_19/19)
 notzero 114 ; sust(game_flag_14/114)
 minus   24 100 ; sust(game_flag_24/24)
 writeln "The food is hastily scoffed!"
 done

feed sam
 at      62 ; sust(room_62/62)
 notzero 114 ; sust(game_flag_14/114)
 minus   24 100 ; sust(game_flag_24/24)
 writeln "The food is hastily scoffed!"
 done

feed sue
 at      40 ; sust(room_40/40)
 notzero 113 ; sust(game_flag_13/113)
 minus   23 100 ; sust(game_flag_23/23)
 writeln "The food is hastily scoffed!"
 done

feed sue
 at      53 ; sust(room_53/53)
 notzero 113 ; sust(game_flag_13/113)
 minus   23 100 ; sust(game_flag_23/23)
 writeln "The food is hastily scoffed!"
 done

feed sue
 at      9 ; sust(room_9/9)
 notzero 113 ; sust(game_flag_13/113)
 minus   23 100 ; sust(game_flag_23/23)
 writeln "The food is hastily scoffed!"
 done

feed sue
 at      19 ; sust(room_19/19)
 notzero 113 ; sust(game_flag_13/113)
 minus   23 100 ; sust(game_flag_23/23)
 writeln "The food is hastily scoffed!"
 done

feed sue
 at      62 ; sust(room_62/62)
 notzero 113 ; sust(game_flag_13/113)
 minus   23 100 ; sust(game_flag_23/23)
 writeln "The food is hastily scoffed!"
 done

feed dave
 at      40 ; sust(room_40/40)
 notzero 112 ; sust(game_flag_12/112)
 minus   22 100 ; sust(game_flag_22/22)
 writeln "The food is hastily scoffed!"
 done

feed dave
 at      53 ; sust(room_53/53)
 notzero 112 ; sust(game_flag_12/112)
 minus   22 100 ; sust(game_flag_22/22)
 writeln "The food is hastily scoffed!"
 done

feed dave
 at      9 ; sust(room_9/9)
 notzero 112 ; sust(game_flag_12/112)
 minus   22 100 ; sust(game_flag_22/22)
 writeln "The food is hastily scoffed!"
 done

feed dave
 at      19 ; sust(room_19/19)
 notzero 112 ; sust(game_flag_12/112)
 minus   22 100 ; sust(game_flag_22/22)
 writeln "The food is hastily scoffed!"
 done

feed dave
 at      62 ; sust(room_62/62)
 notzero 112 ; sust(game_flag_12/112)
 minus   22 100 ; sust(game_flag_22/22)
 writeln "The food is hastily scoffed!"
 done

feed _
 writeln "Please be more specific."

stat sylv
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 notzero 116 ; sust(game_flag_16/116)
 writeln "{CLASS|center|-The statistics are-}"
 writeln "Sylvia Wade:T 1:E 1:S 6:ST 1."
 done

stat bill
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 notzero 115 ; sust(game_flag_15/115)
 writeln "{CLASS|center|-The statistics are-}"
 writeln "Bill Price:T 6:E 3:S 4:ST 6."
 done

stat sam
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 notzero 114 ; sust(game_flag_14/114)
 writeln "{CLASS|center|-The statistics are-}"
 writeln "Sam Wieyenski:T 4:E 4:S 4:ST 4."
 done

stat sue
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 113 ; sust(game_flag_13/113)
 writeln "{CLASS|center|-The statistics are-}"
 writeln "Sue Youart:T 1:E 2:S 8:ST 3."
 done

stat dave
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 112 ; sust(game_flag_12/112)
 notzero 113 ; sust(game_flag_13/113)
 writeln "{CLASS|center|-The statistics are-}"
 writeln "Dave Kellern:T 2:E 6:S 1:ST 8."
 done

stat _
 eq      113 0 ; sust(game_flag_13/113)
 writeln "No-one here is a capable psychic"
 done

stat _
 eq      0 0 ; sust(yesno_is_dark/0)
 writeln "Sue says that she only seems to be able to do that when night falls!"
 done

leav sylv
 notat   70 ; sust(room_70/70)
 notzero 116 ; sust(game_flag_16/116)
 clear   116 ; sust(game_flag_16/116)
 create  5 ; sust(object_5/5)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)
 ok

leav sylv
 at      70 ; sust(room_70/70)
 notzero 116 ; sust(game_flag_16/116)
 clear   116 ; sust(game_flag_16/116)
 writeln "Goodbye-whenever you're around, don't forget to come and see me!"
 minus   17 1 ; sust(game_flag_17/17)
 plus    30 5 ; sust(total_player_score/30)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)
 done

leav sylv
 eq      116 0 ; sust(game_flag_16/116)
 writeln "-Not in party-"
 done

leav bill
 notat   70 ; sust(room_70/70)
 notzero 115 ; sust(game_flag_15/115)
 clear   115 ; sust(game_flag_15/115)
 create  4 ; sust(object_4/4)
 minus   17 6 ; sust(game_flag_17/17)
 minus   18 3 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 6 ; sust(game_flag_21/21)
 ok

leav bill
 at      70 ; sust(room_70/70)
 notzero 115 ; sust(game_flag_15/115)
 clear   115 ; sust(game_flag_15/115)
 writeln "You ever need me,just give me a call-d'you hear,now?"
 plus    30 20 ; sust(total_player_score/30)
 minus   17 6 ; sust(game_flag_17/17)
 minus   18 3 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 6 ; sust(game_flag_21/21)
 done

leav bill
 eq      115 0 ; sust(game_flag_15/115)
 writeln "-Not in party-"
 done

leav sam
 notat   70 ; sust(room_70/70)
 notzero 114 ; sust(game_flag_14/114)
 clear   114 ; sust(game_flag_14/114)
 create  3 ; sust(object_3/3)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)
 ok

leav sam
 at      70 ; sust(room_70/70)
 notzero 114 ; sust(game_flag_14/114)
 clear   114 ; sust(game_flag_14/114)
 writeln "This place looks just fine to me -thanks for everything!"
 plus    30 20 ; sust(total_player_score/30)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)
 done

leav sam
 eq      114 0 ; sust(game_flag_14/114)
 writeln "-Not in party-"
 done

leav sue
 notat   70 ; sust(room_70/70)
 notzero 113 ; sust(game_flag_13/113)
 clear   113 ; sust(game_flag_13/113)
 create  2 ; sust(object_2/2)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)
 ok

leav sue
 at      70 ; sust(room_70/70)
 notzero 113 ; sust(game_flag_13/113)
 clear   113 ; sust(game_flag_13/113)
 writeln "&#34;I've got kind of fond of you being round-come and see me OK?"
 plus    30 20 ; sust(total_player_score/30)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)
 done

leav sue
 eq      113 0 ; sust(game_flag_13/113)
 writeln "-Not in party-"
 done

leav dave
 notat   70 ; sust(room_70/70)
 notzero 112 ; sust(game_flag_12/112)
 clear   112 ; sust(game_flag_12/112)
 create  1 ; sust(object_1/1)
 minus   17 2 ; sust(game_flag_17/17)
 minus   18 6 ; sust(game_flag_18/18)
 minus   19 1 ; sust(game_flag_19/19)
 minus   21 8 ; sust(game_flag_21/21)
 ok

leav dave
 at      70 ; sust(room_70/70)
 notzero 112 ; sust(game_flag_12/112)
 writeln "&#34;So long,friend-I'll be seeing you!&#34;"
 clear   112 ; sust(game_flag_12/112)
 plus    30 20 ; sust(total_player_score/30)
 minus   17 2 ; sust(game_flag_17/17)
 minus   18 6 ; sust(game_flag_18/18)
 minus   19 1 ; sust(game_flag_19/19)
 minus   21 8 ; sust(game_flag_21/21)
 done

leav dave
 eq      112 0 ; sust(game_flag_12/112)
 writeln "-Not in party-"
 done

leav _
 writeln "Please be more specific."

recr sylv
 present 5 ; sust(object_5/5)
 destroy 5 ; sust(object_5/5)
 set     116 ; sust(game_flag_16/116)
 plus    17 1 ; sust(game_flag_17/17)
 plus    18 1 ; sust(game_flag_18/18)
 plus    19 6 ; sust(game_flag_19/19)
 plus    21 1 ; sust(game_flag_21/21)
 ok

recr sylv
 notzero 116 ; sust(game_flag_16/116)
 writeln "-Already in party-"
 done

recr sylv
 present 10 ; sust(object_10/10)
 writeln "Gosh!I never knew you were into necrophilia!"
 done

recr bill
 present 4 ; sust(object_4/4)
 destroy 4 ; sust(object_4/4)
 set     115 ; sust(game_flag_15/115)
 plus    17 6 ; sust(game_flag_17/17)
 plus    18 3 ; sust(game_flag_18/18)
 plus    19 4 ; sust(game_flag_19/19)
 plus    21 6 ; sust(game_flag_21/21)
 ok

recr bill
 notzero 115 ; sust(game_flag_15/115)
 writeln "-Already in party-"
 done

recr bill
 present 9 ; sust(object_9/9)
 writeln "Gosh!I never knew you were into necrophilia!"
 done

recr sam
 present 3 ; sust(object_3/3)
 destroy 3 ; sust(object_3/3)
 set     114 ; sust(game_flag_14/114)
 plus    17 4 ; sust(game_flag_17/17)
 plus    18 4 ; sust(game_flag_18/18)
 plus    19 4 ; sust(game_flag_19/19)
 plus    21 4 ; sust(game_flag_21/21)
 ok

recr sam
 notzero 114 ; sust(game_flag_14/114)
 writeln "-Already in party-"
 done

recr sam
 present 8 ; sust(object_8/8)
 writeln "Gosh!I never knew you were into necrophilia!"
 done

recr sue
 present 2 ; sust(object_2/2)
 destroy 2 ; sust(object_2/2)
 set     113 ; sust(game_flag_13/113)
 plus    17 1 ; sust(game_flag_17/17)
 plus    18 2 ; sust(game_flag_18/18)
 plus    19 8 ; sust(game_flag_19/19)
 plus    21 3 ; sust(game_flag_21/21)
 ok

recr sue
 notzero 113 ; sust(game_flag_13/113)
 writeln "-Already in party-"
 done

recr sue
 present 7 ; sust(object_7/7)
 writeln "Gosh!I never knew you were into necrophilia!"
 done

recr dave
 present 1 ; sust(object_1/1)
 destroy 1 ; sust(object_1/1)
 set     112 ; sust(game_flag_12/112)
 plus    17 2 ; sust(game_flag_17/17)
 plus    18 6 ; sust(game_flag_18/18)
 plus    19 1 ; sust(game_flag_19/19)
 plus    21 8 ; sust(game_flag_21/21)
 ok

recr dave
 notzero 112 ; sust(game_flag_12/112)
 writeln "-Already in party-"
 done

recr dave
 present 6 ; sust(object_6/6)
 writeln "Gosh!I never knew you were into necrophilia!"
 done

recr _
 writeln "Please be more specific."

t trol
 present 0 ; sust(object_0_trol/0)
 get     0 ; sust(object_0_trol/0)
 ok

t trol
 present 37 ; sust(object_37_trol/37)
 get     37 ; sust(object_37_trol/37)
 ok

t trol
 present 38 ; sust(object_38/38)
 writeln "You're not strong enough!"
 done

t can
 present 35 ; sust(object_35_can/35)
 get     35 ; sust(object_35_can/35)
 ok

t can
 present 36 ; sust(object_36_can/36)
 get     36 ; sust(object_36_can/36)
 ok

t bibl
 at      48 ; sust(room_48/48)
 writeln "It's nailed down!"
 done

t rope
 present 27 ; sust(object_27_rope/27)
 get     27 ; sust(object_27_rope/27)
 ok

t rope
 present 28 ; sust(object_28/28)
 swap    28 27 ; sust(object_28/28) ; sust(object_27_rope/27)
 get     27 ; sust(object_27_rope/27)
 ok

t bike
 present 23 ; sust(object_23/23)
 get     23 ; sust(object_23/23)
 swap    23 24 ; sust(object_23/23) ; sust(object_24/24)
 ok

t fee
 get     23 ; sust(object_23/23)
 swap    23 24 ; sust(object_23/23) ; sust(object_24/24)
 ok

t comp
 present 18 ; sust(object_18/18)
 lt      21 19 ; sust(game_flag_21/21)
 writeln "You're not strong enough!"
 done

t comp
 present 37 ; sust(object_37_trol/37)
 writeln "Goddamnit!It took long enough to get it where it is!"
 done

t comp
 present 18 ; sust(object_18/18)
 gt      21 18 ; sust(game_flag_21/21)
 absent  0 ; sust(object_0_trol/0)
 writeln "You'll need something to put it on-it's too heavy to carry about"
 done

t comp
 present 18 ; sust(object_18/18)
 present 0 ; sust(object_0_trol/0)
 gt      21 18 ; sust(game_flag_21/21)
 destroy 18 ; sust(object_18/18)
 swap    0 37 ; sust(object_0_trol/0) ; sust(object_37_trol/37)
 writeln "The computer slips onto the trolley."
 done

t comp
 present 38 ; sust(object_38/38)
 writeln "Goddamnit!It took long enough to get it where it is!"
 done

t suit
 at      32 ; sust(room_32/32)
 get     14 ; sust(object_14_suit/14)
 minus   8 2 ; sust(countdown_player_input_4/8)
 ok

t suit
 notat   32 ; sust(room_32/32)
 get     14 ; sust(object_14_suit/14)
 ok

t sue
 present 29 ; sust(object_29_sue/29)
 get     29 ; sust(object_29_sue/29)
 ok

t sue
 absent  29 ; sust(object_29_sue/29)
 writeln "Please be more specific."
 done

t _
 autog
 ok

t _
 writeln "Please be more specific."

dr trol
 present 0 ; sust(object_0_trol/0)
 drop    0 ; sust(object_0_trol/0)
 ok

dr trol
 present 37 ; sust(object_37_trol/37)
 drop    37 ; sust(object_37_trol/37)
 ok

dr can
 carried 35 ; sust(object_35_can/35)
 drop    35 ; sust(object_35_can/35)
 ok

dr can
 carried 36 ; sust(object_36_can/36)
 drop    36 ; sust(object_36_can/36)
 ok

dr rope
 drop    27 ; sust(object_27_rope/27)
 ok

dr bike
 carried 24 ; sust(object_24/24)
 swap    24 23 ; sust(object_24/24) ; sust(object_23/23)
 drop    23 ; sust(object_23/23)
 ok

dr fee
 carried 24 ; sust(object_24/24)
 swap    24 23 ; sust(object_24/24) ; sust(object_23/23)
 drop    23 ; sust(object_23/23)
 ok

dr comp
 drop    18 ; sust(object_18/18)
 ok

dr bott
 writeln "You really should watch what you are doing!"
 score
 end

dr suit
 drop    14 ; sust(object_14_suit/14)
 ok

dr sue
 drop    29 ; sust(object_29_sue/29)
 ok

dr _
 autod
 ok

dr _
 writeln "Please be more specific."

remo boot
 remove  31 ; sust(object_31_boot/31)
 ok

remo bolt
 at      20 ; sust(room_20/20)
 carried 26 ; sust(object_26_span/26)
 present 30 ; sust(object_30/30)
 gt      21 7 ; sust(game_flag_21/21)
 destroy 30 ; sust(object_30/30)
 writeln "The door falls into the shaft!"
 done

remo bolt
 at      20 ; sust(room_20/20)
 present 30 ; sust(object_30/30)
 lt      21 8 ; sust(game_flag_21/21)
 writeln "You'll need a couple of people to move the bolts-and some kind of wrench!"
 done

remo elec
 present 38 ; sust(object_38/38)
 destroy 38 ; sust(object_38/38)
 create  37 ; sust(object_37_trol/37)
 create  19 ; sust(object_19_elec/19)
 get     19 ; sust(object_19_elec/19)
 clear   6 ; sust(countdown_player_input_2/6)
 clear   5 ; sust(countdown_player_input_1/5)
 clear   3 ; sust(countdown_location_description_dark/3)
 desc

remo glov
 remove  11 ; sust(object_11_glov/11)
 ok

remo _
 writeln "Please be more specific."

wear boot
 wear    31 ; sust(object_31_boot/31)
 ok

wear glov
 wear    11 ; sust(object_11_glov/11)
 ok


_ _
 hook    "RESPONSE_DEFAULT_START"

wear _
 writeln "Please be more specific."

i _
 writeln "{CLASS|center|-??INVENTORY??-}"
 inven

r _
 let     2 101 ; sust(countdown_location_description/2)
 set     29 ; sust(bitset_graphics_status/29)
 desc

q _
 lt      30 80 ; sust(total_player_score/30)
 quit
 score
 end

q _
 eq      30 80 ; sust(total_player_score/30)
 writeln "You have formed the nucleus of a new colony to re-build your past but still nuclear missiles lie undiscovered,a permanent threat to your new life!"
 score
 end

save _
 let     2 101 ; sust(countdown_location_description/2)
 save

load _
 let     2 101 ; sust(countdown_location_description/2)
 load

word _
 let     28 19 ; sust(pause_parameter/28) ; sust(pause_toggle_graphics/19)
 pause   255
 writeln "Text - only selected."
 done

pict _
 let     28 19 ; sust(pause_parameter/28) ; sust(pause_toggle_graphics/19)
 pause   19
 writeln "Pictures and text selected."
 set     29 ; sust(bitset_graphics_status/29)
 done

stor _
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   21
 ok

call _
 let     28 21 ; sust(pause_parameter/28) ; sust(pause_ram_save_load/21)
 pause   50
 ok

vers _
 writeln "VERSION A03.\nWRITTEN BY M. WHITE.\nGRAPHICS BY MARTIN RENNIE.\n(  EIGHTH DAY 1987 )."
 done


_ _
 hook    "RESPONSE_DEFAULT_END"







/PRO 1


_ _
 hook "PRO1"

_ _
 at     0
 bclear 12 5                      ; Set language to English

_ _
 islight
 listobj                        ; Lists present objects
 listnpc @38                    ; Lists present NPCs







/PRO 2


_ _
 eq      31 0 ; sust(total_turns_lower/31)
 eq      32 0 ; sust(total_turns_higher/32)
 ability 254 254


_ _
 hook    "PRO2"

_ _
 lt      8 6 ; sust(countdown_player_input_4/8)
 gt      8 0 ; sust(countdown_player_input_4/8)
 writeln "The vault door is closing.."

_ _
 at      32 ; sust(room_32/32)
 lt      8 1 ; sust(countdown_player_input_4/8)
 absent  33 ; sust(object_33/33)
 create  33 ; sust(object_33/33)
 writeln "The door slams shut!"

_ _
 at      31 ; sust(room_31/31)
 eq      8 1 ; sust(countdown_player_input_4/8)
 writeln "The door slams shut!"
 create  33 ; sust(object_33/33)

_ _
 at      32 ; sust(room_32/32)
 notzero 112 ; sust(game_flag_12/112)
 lt      8 5 ; sust(countdown_player_input_4/8)
 gt      8 0 ; sust(countdown_player_input_4/8)
 writeln "I can't keep the door open much longer-HURRY!"

_ _
 notzero 31 ; sust(total_turns_lower/31)
 minus   31 1 ; sust(total_turns_lower/31)

_ _
 at      0 ; sust(room_0/0)
 let     27 12 ; sust(game_flag_27/27)

_ _
 atgt    0 ; sust(room_0/0)
 plus    111 1 ; sust(game_flag_11/111)

_ _
 gt      111 95 ; sust(game_flag_11/111)
 lt      111 100 ; sust(game_flag_11/111)
 writeln "{CLASS|center| ?\n ???\nThe Sun is setting!}"

_ _
 eq      111 100 ; sust(game_flag_11/111)
 writeln "{CLASS|center| ?}                ???The sun sets!"
 set     0 ; sust(yesno_is_dark/0)

_ _
 eq      0 0 ; sust(yesno_is_dark/0)
 gt      111 100 ; sust(game_flag_11/111)
 lt      111 111 ; sust(game_flag_11/111)
 set     0 ; sust(yesno_is_dark/0)
 writeln "{CLASS|center| ?}                ???The sun sets!"

_ _
 gt      111 111 ; sust(game_flag_11/111)
 lt      111 116 ; sust(game_flag_11/111)
 writeln "{CLASS|center| ?\n??? \nThe sun is rising!}"

_ _
 notzero 0 ; sust(yesno_is_dark/0)
 gt      111 115 ; sust(game_flag_11/111)
 writeln "                ? The sun has\n                ???      risen!"
 clear   0 ; sust(yesno_is_dark/0)
 pause   100
 desc

_ _
 notzero 0 ; sust(yesno_is_dark/0)
 gt      111 117 ; sust(game_flag_11/111)
 let     111 0 ; sust(game_flag_11/111)
 clear   0 ; sust(yesno_is_dark/0)

_ _
 lt      111 112 ; sust(game_flag_11/111)
 notzero 0 ; sust(yesno_is_dark/0)
 writeln "{CLASS|center| ?\n??? }Darkness looms all around-on the edges of the night sounds of movement can be heard.."

_ _
 at      11 ; sust(room_11/11)
 lt      32 36 ; sust(total_turns_higher/32)
 let     32 200 ; sust(total_turns_higher/32)

_ _
 at      10 ; sust(room_10/10)
 eq      32 200 ; sust(total_turns_higher/32)
 minus   32 165 ; sust(total_turns_higher/32)

_ _
 eq      32 200 ; sust(total_turns_higher/32)
 chance  10
 writeln "??You can hear a phone ringing\n  in the distance! ??"

_ _
 at      76 ; sust(room_76/76)
 plus    32 1 ; sust(total_turns_higher/32)

_ _
 at      77 ; sust(room_77/77)
 plus    32 1 ; sust(total_turns_higher/32)

_ _
 eq      32 205 ; sust(total_turns_higher/32)
 writeln "You freeze to death!"
 score
 end

_ _
 at      19 ; sust(room_19/19)
 let     32 200 ; sust(total_turns_higher/32)

_ _
 gt      32 200 ; sust(total_turns_higher/32)
 writeln "You are very cold!"

_ _
 at      6 ; sust(room_6/6)
 carried 29 ; sust(object_29_sue/29)
 destroy 29 ; sust(object_29_sue/29)
 create  2 ; sust(object_2/2)
 writeln "Sue comes to.&#34;Do that again and you'll regret it!&#34;"

_ _
 gt      111 117 ; sust(game_flag_11/111)
 clear   0 ; sust(yesno_is_dark/0)
 let     111 1 ; sust(game_flag_11/111)

_ _
 notat   0 ; sust(room_0/0)
 eq      111 0 ; sust(game_flag_11/111)
 writeln "                ? The sun has\n                ???      risen!"

_ _
 at      0 ; sust(room_0/0)
 let     27 110 ; sust(game_flag_27/27)

_ _
 lt      27 115 ; sust(game_flag_27/27)
 minus   27 1 ; sust(game_flag_27/27)

_ _
 notat   0 ; sust(room_0/0)
 lt      27 110 ; sust(game_flag_27/27)
 writeln "A car is careering down the road towards the station!"

_ _
 eq      27 106 ; sust(game_flag_27/27)
 writeln "The car hits the pumps!With a massive explosion the station erupts in a ball of flames!"
 score
 end

_ _
 at      23 ; sust(room_23/23)
 notworn 11 ; sust(object_11_glov/11)
 writeln "The cables are greased!You slip down the shaft!"
 score
 end

_ _
 at      23 ; sust(room_23/23)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue screams as she falls!"
 clear   113 ; sust(game_flag_13/113)

_ _
 at      23 ; sust(room_23/23)
 notzero 112 ; sust(game_flag_12/112)
 writeln "Dave falls!"
 clear   112 ; sust(game_flag_12/112)
 minus   17 2 ; sust(game_flag_17/17)
 minus   18 6 ; sust(game_flag_18/18)
 minus   19 1 ; sust(game_flag_19/19)
 minus   21 8 ; sust(game_flag_21/21)

_ _
 at      23 ; sust(room_23/23)
 notzero 113 ; sust(game_flag_13/113)
 writeln "Sue screams as she falls!"
 clear   113 ; sust(game_flag_13/113)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)

_ _
 at      23 ; sust(room_23/23)
 notzero 114 ; sust(game_flag_14/114)
 writeln "Sam yells as he falls!"
 clear   114 ; sust(game_flag_14/114)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)

_ _
 at      23 ; sust(room_23/23)
 notzero 115 ; sust(game_flag_15/115)
 clear   115 ; sust(game_flag_15/115)
 writeln "Bill falls to his death!"
 minus   17 6 ; sust(game_flag_17/17)
 minus   18 3 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 6 ; sust(game_flag_21/21)

_ _
 at      23 ; sust(room_23/23)
 notzero 116 ; sust(game_flag_16/116)
 writeln "Sylvia falls!"
 clear   116 ; sust(game_flag_16/116)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)

_ _
 at      75 ; sust(room_75/75)
 plus    32 1 ; sust(total_turns_higher/32)

_ _
 eq      30 100 ; sust(total_player_score/30)
 writeln "CONGRATULATIONS! With the threat of nuclear destruction no longer threatening your colony,you have formed the nucleus of a bright new future,full of hope."
 score
 end

_ _
 eq      7 100 ; sust(countdown_player_input_3/7)
 eq      4 100 ; sust(countdown_object_description_unlit/4)
 eq      8 100 ; sust(countdown_player_input_4/8)
 plus    30 15 ; sust(total_player_score/30)
 minus   7 1 ; sust(countdown_player_input_3/7)
 writeln "-PLEASE PROCEED SECURITY OPTION-"
 let     5 3 ; sust(countdown_player_input_1/5)

_ _
 notzero 0 ; sust(yesno_is_dark/0)
 notzero 4 ; sust(countdown_object_description_unlit/4)
 absent  0 ; sust(object_0_trol/0)
 plus    4 1 ; sust(countdown_object_description_unlit/4)

_ _
 lt      5 195 ; sust(countdown_player_input_1/5)
 gt      5 20 ; sust(countdown_player_input_1/5)
 writeln "The car explodes!!"
 destroy 40 ; sust(object_40/40)
 clear   5 ; sust(countdown_player_input_1/5)

_ _
 eq      5 9 ; sust(countdown_player_input_1/5)
 writeln "-PLEASE PROCEED SECURITY OPTION-"

_ _
 eq      5 4 ; sust(countdown_player_input_1/5)
 writeln "-PLEASE PROCEED SECURITY OPTION-"

_ _
 eq      5 2 ; sust(countdown_player_input_1/5)
 writeln "..SECURITY COMPUTER SHUTDOWN.."

_ _
 eq      5 1 ; sust(countdown_player_input_1/5)
 writeln "The computer self-destructs!"
 destroy 38 ; sust(object_38/38)
 let     6 3 ; sust(countdown_player_input_2/6)
 clear   3 ; sust(countdown_location_description_dark/3)
 clear   5 ; sust(countdown_player_input_1/5)
 done

_ _
 gt      6 60 ; sust(countdown_player_input_2/6)
 minus   6 5 ; sust(countdown_player_input_2/6)

_ _
 eq      6 60 ; sust(countdown_player_input_2/6)
 let     6 30 ; sust(countdown_player_input_2/6)

_ _
 present 40 ; sust(object_40/40)
 lt      5 201 ; sust(countdown_player_input_1/5)
 gt      5 194 ; sust(countdown_player_input_1/5)
 writeln "Flames lick about the car!"

_ _
 at      79 ; sust(room_79/79)
 eq      5 195 ; sust(countdown_player_input_1/5)
 writeln "The car explodes!!"
 writeln "-engulfing you in flames!"
 score
 end

_ _
 eq      5 195 ; sust(countdown_player_input_1/5)
 writeln "The car explodes!!"
 destroy 40 ; sust(object_40/40)
 clear   5 ; sust(countdown_player_input_1/5)
 done

_ _
 eq      112 0 ; sust(game_flag_12/112)
 present 40 ; sust(object_40/40)
 writeln "Help!I'm trapped!"

_ _
 at      1 ; sust(room_1/1)
 eq      2 100 ; sust(countdown_location_description/2)
 absent  40 ; sust(object_40/40)
 writeln "Wreckage is strewn across the ground."
 minus   2 1 ; sust(countdown_location_description/2)

_ _
 gt      21 21 ; sust(game_flag_21/21)
 notzero 112 ; sust(game_flag_12/112)
 chance  1
 clear   112 ; sust(game_flag_12/112)
 create  1 ; sust(object_1/1)
 minus   21 8 ; sust(game_flag_21/21)
 minus   17 2 ; sust(game_flag_17/17)
 minus   18 6 ; sust(game_flag_18/18)
 minus   19 1 ; sust(game_flag_19/19)
 writeln "{CLASS|center|  ??}"

_ _
 gt      21 21 ; sust(game_flag_21/21)
 notzero 113 ; sust(game_flag_13/113)
 chance  10
 clear   113 ; sust(game_flag_13/113)
 create  2 ; sust(object_2/2)
 minus   21 3 ; sust(game_flag_21/21)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 writeln "{CLASS|center|  ??}"

_ _
 gt      21 21 ; sust(game_flag_21/21)
 notzero 114 ; sust(game_flag_14/114)
 chance  5
 clear   114 ; sust(game_flag_14/114)
 create  3 ; sust(object_3/3)
 minus   21 4 ; sust(game_flag_21/21)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 writeln "{CLASS|center|  ??}"

_ _
 gt      21 21 ; sust(game_flag_21/21)
 notzero 115 ; sust(game_flag_15/115)
 chance  1
 clear   115 ; sust(game_flag_15/115)
 create  4 ; sust(object_4/4)
 minus   21 6 ; sust(game_flag_21/21)
 minus   17 6 ; sust(game_flag_17/17)
 minus   18 3 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 writeln "{CLASS|center|  ??}"

_ _
 gt      21 21 ; sust(game_flag_21/21)
 notzero 116 ; sust(game_flag_16/116)
 chance  20
 clear   116 ; sust(game_flag_16/116)
 create  5 ; sust(object_5/5)
 minus   21 1 ; sust(game_flag_21/21)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 writeln "{CLASS|center|  ??}"

_ _
 at      34 ; sust(room_34/34)
 eq      31 1 ; sust(total_turns_lower/31)
 create  32 ; sust(object_32/32)
 writeln "A roller-coaster car rolls down into the stalls!"
 plus    31 1 ; sust(total_turns_lower/31)














 done

_ _
 notzero 0 ; sust(yesno_is_dark/0)
 eq      9 10 ; sust(countdown_player_input_dark/9)
 plus    9 1 ; sust(countdown_player_input_dark/9)

_ _
 notzero 0 ; sust(yesno_is_dark/0)
 eq      9 20 ; sust(countdown_player_input_dark/9)
 plus    9 1 ; sust(countdown_player_input_dark/9)

_ _
 at      52 ; sust(room_52/52)
 present 20 ; sust(object_20/20)
 eq      2 100 ; sust(countdown_location_description/2)
 writeln "The madman is agitated by your presence,and twitches nervously! &#34;GO AWAY!&#34;"
 minus   2 1 ; sust(countdown_location_description/2)

_ _
 notzero 3 ; sust(countdown_location_description_dark/3)
 notzero 0 ; sust(yesno_is_dark/0)
 plus    3 1 ; sust(countdown_location_description_dark/3)

_ _
 at      68 ; sust(room_68/68)
 present 39 ; sust(object_39/39)
 present 17 ; sust(object_17_bott/17)
 destroy 39 ; sust(object_39/39)
 writeln "The road-block is hastily given up when the bottle is seen!"

_ _
 atgt    0 ; sust(room_0/0)
 notzero 112 ; sust(game_flag_12/112)
 plus    22 1 ; sust(game_flag_22/22)

_ _
 atgt    0 ; sust(room_0/0)
 notzero 113 ; sust(game_flag_13/113)
 plus    23 1 ; sust(game_flag_23/23)

_ _
 atgt    0 ; sust(room_0/0)
 notzero 114 ; sust(game_flag_14/114)
 plus    24 1 ; sust(game_flag_24/24)

_ _
 atgt    0 ; sust(room_0/0)
 notzero 115 ; sust(game_flag_15/115)
 plus    25 1 ; sust(game_flag_25/25)

_ _
 atgt    0 ; sust(room_0/0)
 notzero 116 ; sust(game_flag_16/116)
 plus    26 1 ; sust(game_flag_26/26)

_ _
 notzero 112 ; sust(game_flag_12/112)
 gt      22 80 ; sust(game_flag_22/22)
 writeln "Dave says he is very hungry!"

_ _
 notzero 112 ; sust(game_flag_12/112)
 gt      22 96 ; sust(game_flag_22/22)
 writeln "Dave dies of hunger!"
 clear   112 ; sust(game_flag_12/112)
 create  6 ; sust(object_6/6)
 clear   22 ; sust(game_flag_22/22)
 minus   17 2 ; sust(game_flag_17/17)
 minus   18 6 ; sust(game_flag_18/18)
 minus   19 1 ; sust(game_flag_19/19)
 minus   21 8 ; sust(game_flag_21/21)

_ _
 notzero 113 ; sust(game_flag_13/113)
 gt      23 40 ; sust(game_flag_23/23)
 writeln "Sue says she is very hungry!"

_ _
 notzero 113 ; sust(game_flag_13/113)
 gt      23 51 ; sust(game_flag_23/23)
 writeln "Sue dies of hunger!"
 clear   113 ; sust(game_flag_13/113)
 create  7 ; sust(object_7/7)
 clear   23 ; sust(game_flag_23/23)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 2 ; sust(game_flag_18/18)
 minus   19 8 ; sust(game_flag_19/19)
 minus   21 3 ; sust(game_flag_21/21)

_ _
 notzero 114 ; sust(game_flag_14/114)
 gt      24 60 ; sust(game_flag_24/24)
 writeln "Sam says he is starving!"

_ _
 notzero 114 ; sust(game_flag_14/114)
 gt      24 71 ; sust(game_flag_24/24)
 writeln "Sam collapses with starvation!"
 clear   114 ; sust(game_flag_14/114)
 create  8 ; sust(object_8/8)
 clear   24 ; sust(game_flag_24/24)
 minus   17 4 ; sust(game_flag_17/17)
 minus   18 4 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 4 ; sust(game_flag_21/21)

_ _
 notzero 115 ; sust(game_flag_15/115)
 gt      25 50 ; sust(game_flag_25/25)
 writeln "Bill grumbles that he is hungry!"

_ _
 notzero 115 ; sust(game_flag_15/115)
 gt      25 61 ; sust(game_flag_25/25)
 writeln "Bill dies from lack of food!"
 clear   115 ; sust(game_flag_15/115)
 create  9 ; sust(object_9/9)
 clear   25 ; sust(game_flag_25/25)
 minus   17 6 ; sust(game_flag_17/17)
 minus   18 3 ; sust(game_flag_18/18)
 minus   19 4 ; sust(game_flag_19/19)
 minus   21 6 ; sust(game_flag_21/21)

_ _
 notzero 116 ; sust(game_flag_16/116)
 gt      26 30 ; sust(game_flag_26/26)
 writeln "Sylvia complains that she is a little bit peckish!"

_ _
 notzero 116 ; sust(game_flag_16/116)
 gt      26 36 ; sust(game_flag_26/26)
 writeln "Sylvia slumps to the ground!"
 create  10 ; sust(object_10/10)
 clear   116 ; sust(game_flag_16/116)
 clear   26 ; sust(game_flag_26/26)
 minus   17 1 ; sust(game_flag_17/17)
 minus   18 1 ; sust(game_flag_18/18)
 minus   19 6 ; sust(game_flag_19/19)
 minus   21 1 ; sust(game_flag_21/21)

_ _
 at      0 ; sust(room_0/0)
 plus    20 1 ; sust(game_flag_20/20)
 goto    2 ; sust(room_2/2)
 let     28 8 ; sust(pause_parameter/28) ; sust(pause_font_alternate/8)
 pause   8
 minus   31 1 ; sust(total_turns_lower/31)
 let     5 200 ; sust(countdown_player_input_1/5)
 let     28 22 ; sust(pause_parameter/28) ; sust(pause_set_identity_byte/22)
 pause   1
 anykey
 desc

_ _
 at      78 ; sust(room_78/78)
 gt      0 19 ; sust(yesno_is_dark/0)
 writeln "The ledge collapses beneath your parties' weight!"
 score
 end

_ _
 gt      8 49 ; sust(countdown_player_input_4/8)
 plus    8 1 ; sust(countdown_player_input_4/8)

_ _
 eq      5 252 ; sust(countdown_player_input_1/5)
 writeln "You notice a man running across the top of the tank!"

_ _
 eq      5 235 ; sust(countdown_player_input_1/5)
 writeln "You hear mad laughter nearby!"

_ _
 eq      5 240 ; sust(countdown_player_input_1/5)
 writeln "You hear mad laughter nearby!"

_ _
 eq      5 234 ; sust(countdown_player_input_1/5)
 writeln "With a Tremendous roar that scorches your lungs the tanks ignite into a ball of flames, incinerating you!!"
 score
 end

_ _
 at      52 ; sust(room_52/52)
 present 20 ; sust(object_20/20)
 notzero 113 ; sust(game_flag_13/113)
 notzero 116 ; sust(game_flag_16/116)
 eq      112 0 ; sust(game_flag_12/112)
 eq      114 0 ; sust(game_flag_14/114)
 eq      115 0 ; sust(game_flag_15/115)
 writeln "Sue and Sylvia calm the mad-man down.As you approach,however,he leaps past you and runs away!"
 destroy 20 ; sust(object_20/20)

_ _
 eq      10 10 ; sust(countdown_player_input_unlit/10)
 notzero 0 ; sust(yesno_is_dark/0)
 absent  0 ; sust(object_0_trol/0)
 plus    10 1 ; sust(countdown_player_input_unlit/10)
 done

_ _
 notzero 0 ; sust(yesno_is_dark/0)
 eq      10 11 ; sust(countdown_player_input_unlit/10)
 absent  0 ; sust(object_0_trol/0)
 plus    10 1 ; sust(countdown_player_input_unlit/10)
 done

_ _
 notzero 6 ; sust(countdown_player_input_2/6)
 plus    6 1 ; sust(countdown_player_input_2/6)

_ _
 gt      7 10 ; sust(countdown_player_input_3/7)
 plus    7 1 ; sust(countdown_player_input_3/7)
